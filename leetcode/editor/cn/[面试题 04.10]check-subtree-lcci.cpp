//T1 and T2 are two very large binary trees, with T1 much bigger than T2. Create
// an algorithm to determine if T2 is a subtree of T1. 
//
// A tree T2 is a subtree of T1 if there exists a node n in T1 such that the sub
//tree of n is identical to T2. That is, if you cut off the tree at node n, the tw
//o trees would be identical. 
//
// Example1: 
//
// 
// Input: t1 = [1, 2, 3], t2 = [2]
// Output: true
// 
//
// Example2: 
//
// 
// Input: t1 = [1, null, 2, 4], t2 = [3, 2]
// Output: false
// 
//
// Note: 
//
// 
// The node numbers of both tree are in [0, 20000]. 
// 
// Related Topics 树


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    bool isSameTree(TreeNode *p, TreeNode *q) {
        if (!p && !q) return true;
        if (!p || !q) return false;
        if (p->val != q->val) return false;
        return isSameTree(p->left, q->left) && isSameTree(p->right, q->right);
    }

public:
    bool checkSubTree(TreeNode *t1, TreeNode *t2) {
        if (!t1 || !t2) return false;
        return checkSubTree(t1->left, t2) || checkSubTree(t1->right, t2) || isSameTree(t1, t2);
    }
};
//leetcode submit region end(Prohibit modification and deletion)
