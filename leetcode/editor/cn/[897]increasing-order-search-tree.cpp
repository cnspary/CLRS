//Given a binary search tree, rearrange the tree in in-order so that the leftmos
//t node in the tree is now the root of the tree, and every node has no left child
// and only 1 right child. 
//
// 
//Example 1:
//Input: [5,3,6,2,4,null,8,1,null,null,null,7,9]
//
//       5
//      / \
//    3    6
//   / \    \
//  2   4    8
// /        / \ 
//1        7   9
//
//Output: [1,null,2,null,3,null,4,null,5,null,6,null,7,null,8,null,9]
//
// 1
//  \
//   2
//    \
//     3
//      \
//       4
//        \
//         5
//          \
//           6
//            \
//             7
//              \
//               8
//                \
//                 9  
// 
// Constraints: 
//
// 
// The number of nodes in the given tree will be between 1 and 100. 
// Each node will have a unique integer value from 0 to 1000. 
// 
// Related Topics 树 深度优先搜索


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    vector<TreeNode *> ans;

    void DFS(TreeNode *root) {
        if (!root) return;
        DFS(root->left);
        ans.push_back(root);
        DFS(root->right);
    }

public:
    TreeNode *increasingBST(TreeNode *root) {
        DFS(root);
        for (int i = 0; i < ans.size() - 1; ++i) {
            ans[i]->right = ans[i + 1];
            ans[i]->left = nullptr;
        }
        return ans[0];
    }
};
//leetcode submit region end(Prohibit modification and deletion)
