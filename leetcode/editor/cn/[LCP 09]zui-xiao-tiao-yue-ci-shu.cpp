//


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
public:
    int minJump(vector<int> &jump) {
        int n = jump.size();
        vector<int> dp(n);

        for (int i = n - 1; 0 <= i; --i) {
            dp[i] = i + jump[i] >= n ? 1 : dp[i + jump[i]] + 1;
            for (int j = i + 1; j < n && dp[j] >= dp[i] + 1; ++j)
                dp[j] = dp[i] + 1;
        }

        return dp[0];
    }
};
//leetcode submit region end(Prohibit modification and deletion)
