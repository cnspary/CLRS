//Given n nodes labeled from 0 to n - 1 and a list of undirected edges (each edg
//e is a pair of nodes), write a function to find the number of connected componen
//ts in an undirected graph. 
//
// Example 1: 
//
// 
//Input: n = 5 and edges = [[0, 1], [1, 2], [3, 4]]
//
//     0          3
//     |          |
//     1 --- 2    4 
//
//Output: 2
// 
//
// Example 2: 
//
// 
//Input: n = 5 and edges = [[0, 1], [1, 2], [2, 3], [3, 4]]
//
//     0           4
//     |           |
//     1 --- 2 --- 3
//
//Output:  1
// 
//
// Note: 
//You can assume that no duplicate edges will appear in edges. Since all edges a
//re undirected, [0, 1] is the same as [1, 0] and thus will not appear together in
// edges. 
// Related Topics 深度优先搜索 广度优先搜索 并查集 图


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
private:
    vector<int> uf;
    vector<int> rank;
    int component;

    int find(int x) {
        int root = x;
        while (uf[root] != root)
            root = uf[root];

        while (uf[x] != root) {
            int t = uf[x];
            uf[x] = root;
            x = t;
        }
        return root;
    }

    void unionn(int x, int y) {
        int r1 = find(x);
        int r2 = find(y);

        if (r1 == r2) return;

        component--;

        if (rank[r1] > rank[r2]) {
            uf[r2] = r1;
            rank[r1] += rank[r2];
        } else {
            uf[r1] = r2;
            rank[r2] += rank[r1];
        }
        return;
    }

public:
    int countComponents(int n, vector <vector<int>> &edges) {
        uf.resize(n);
        rank.resize(n, 1);
        component = n;

        for (int i = 0; i < uf.size(); ++i)
            uf[i] = i;

        for(int i = 0; i < edges.size(); ++i){
            int a = edges[i][0];
            int b = edges[i][1];

            unionn(a, b);
        }

        return component;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
