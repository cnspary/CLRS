//There are n houses in a village. We want to supply water for all the houses by
// building wells and laying pipes. 
//
// For each house i, we can either build a well inside it directly with cost wel
//ls[i], or pipe in water from another well to it. The costs to lay pipes between 
//houses are given by the array pipes, where each pipes[i] = [house1, house2, cost
//] represents the cost to connect house1 and house2 together using a pipe. Connec
//tions are bidirectional. 
//
// Find the minimum total cost to supply water to all houses. 
//
// 
// Example 1: 
//
// 
//
// 
//Input: n = 3, wells = [1,2,2], pipes = [[1,2,1],[2,3,1]]
//Output: 3
//Explanation: 
//The image shows the costs of connecting houses using pipes.
//The best strategy is to build a well in the first house with cost 1 and connec
//t the other houses to it with cost 2 so the total cost is 3.
// 
//
// 
// Constraints: 
//
// 
// 1 <= n <= 10000 
// wells.length == n 
// 0 <= wells[i] <= 10^5 
// 1 <= pipes.length <= 10000 
// 1 <= pipes[i][0], pipes[i][1] <= n 
// 0 <= pipes[i][2] <= 10^5 
// pipes[i][0] != pipes[i][1] 
// 
// Related Topics 并查集 图


//leetcode submit region begin(Prohibit modification and deletion)
class UnionFind {
private:
    int cnt;
    vector<int> uf;
    vector<int> rank;

public:
    UnionFind(int n) {
        cnt = n;
        uf = vector<int>(n);
        rank = vector<int>(n, 1);
        for (int i = 0; i < n; ++i)
            uf[i] = i;
    }

    int find(int a) {
        int p = a;
        while (uf[a] != a) a = uf[a];
        int root = uf[a];
        while (uf[p] != root) {
            int x = uf[p];
            uf[p] = root;
            p = x;
        }
        return root;
    }

    bool uunion(int a, int b) {
        int r1 = find(a);
        int r2 = find(b);
        if (r1 == r2) return false;

        cnt--;
        if (rank[r1] < rank[r2]) {
            uf[r1] = r2;
            rank[r2] += rank[r1];
        } else {
            uf[r2] = r1;
            rank[r1] += rank[r2];
        }
        return true;
    }

    int getRank(int i) {
        return rank[find(i)];
    }

    int count() {
        return cnt;
    }

    bool connected(int x, int y) {
        return find(x) == find(y);
    }
};

class Solution {
public:
    int minCostToSupplyWater(int n, vector<int> &wells, vector<vector<int>> &pipes) {
        UnionFind uf(n + 1);
        for (int i = 0; i < wells.size(); ++i)
            pipes.push_back({0, i + 1, wells[i]});

        auto cmp = [](vector<int> a, vector<int> b) -> bool {
            return a[2] > b[2];
        };

        priority_queue<vector<int>, vector<vector<int>>, decltype(cmp)> PQ(cmp);
        for (int i = 0; i < pipes.size(); ++i)
            PQ.push(pipes[i]);

        int allCost = 0;
        while (!PQ.empty()) {
            auto x = PQ.top();
            PQ.pop();

            if (!uf.connected(x[0], x[1])) {
                uf.uunion(x[0], x[1]);
                allCost += x[2];
            }
        }

        return allCost;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
