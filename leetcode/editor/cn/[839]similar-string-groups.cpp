//Two strings X and Y are similar if we can swap two letters (in different posit
//ions) of X, so that it equals Y. Also two strings X and Y are similar if they ar
//e equal. 
//
// For example, "tars" and "rats" are similar (swapping at positions 0 and 2), a
//nd "rats" and "arts" are similar, but "star" is not similar to "tars", "rats", o
//r "arts". 
//
// Together, these form two connected groups by similarity: {"tars", "rats", "ar
//ts"} and {"star"}. Notice that "tars" and "arts" are in the same group even thou
//gh they are not similar. Formally, each group is such that a word is in the grou
//p if and only if it is similar to at least one other word in the group. 
//
// We are given a list A of strings. Every string in A is an anagram of every ot
//her string in A. How many groups are there? 
//
// 
// Example 1: 
// Input: A = ["tars","rats","arts","star"]
//Output: 2
// 
// 
// Constraints: 
//
// 
// 1 <= A.length <= 2000 
// 1 <= A[i].length <= 1000 
// A.length * A[i].length <= 20000 
// All words in A consist of lowercase letters only. 
// All words in A have the same length and are anagrams of each other. 
// The judging time limit has been increased for this question. 
// 
// Related Topics 深度优先搜索 并查集 图


//leetcode submit region begin(Prohibit modification and deletion)
class UnionFind {
private:
    int cnt;
    vector<int> uf;
    vector<int> rank;

public:
    UnionFind(int n) {
        cnt = n;
        uf = vector<int>(n);
        rank = vector<int>(n, 1);
        for (int i = 0; i < n; ++i)
            uf[i] = i;
    }

    int find(int a) {
        int p = a;
        while (uf[a] != a) a = uf[a];
        int root = uf[a];
        while (uf[p] != root) {
            int x = uf[p];
            uf[p] = root;
            p = x;
        }
        return root;
    }

    bool uunion(int a, int b) {
        int r1 = find(a);
        int r2 = find(b);
        if (r1 == r2) return false;

        cnt--;
        if (rank[r1] < rank[r2]) {
            uf[r1] = r2;
            rank[r2] += rank[r1];
        } else {
            uf[r2] = r1;
            rank[r1] += rank[r2];
        }
        return true;
    }

    int getBiggestRank() {
        int biggest = 0;
        for (int i = 0; i < rank.size(); ++i)
            biggest = max(biggest, rank[i]);
        return biggest;
    }

    int getRank(int i) {
        return rank[find(i)];
    }

    int count() {
        return cnt;
    }

    bool connected(int x, int y) {
        return find(x) == find(y);
    }
};

class Solution {
public:
    bool isSimilar(string a, string b) {
        int diff = 0;
        for (int i = 0; i < a.size(); ++i) {
            if (a[i] != b[i])
                diff++;
        }
        if (diff == 2 || diff == 0) return true;
        return false;
    }

    int case1(vector <string> &A) {
        UnionFind uf(A.size());
        for (int i = 0; i < A.size(); ++i)
            for (int j = i + 1; j < A.size(); j++) {
                if (isSimilar(A[i], A[j]))
                    uf.uunion(i, j);
            }
        return uf.count();
    }

    int case2(vector <string> &A) {
        UnionFind uf(A.size());
        map <string, vector<int>> HT;
        set <string> duplicate;
        for (int i = 0; i < A.size(); ++i)
            HT[A[i]].push_back(i);

        for (int k = 0; k < A.size(); ++k) {
            string s = A[k];
            if (duplicate.find(s) != duplicate.end()) continue;

            duplicate.insert(s);
            for (int i = 0; i < s.size(); ++i)
                for (int j = i + 1; j < s.size(); ++j) {
                    swap(s[i], s[j]);
                    if (HT.find(s) != HT.end()) {
                        for (auto id : HT[s])
                            uf.uunion(id, k);
                    }
                    swap(s[i], s[j]);
                }
        }
        return uf.count();
    }

    int numSimilarGroups(vector <string> &A) {
        int n = A.size();
        int w = A[0].size();

        if (n < w * w) return case1(A);
        else return case2(A);
    }


};
//leetcode submit region end(Prohibit modification and deletion)
