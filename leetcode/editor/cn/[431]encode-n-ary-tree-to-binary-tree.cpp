//Design an algorithm to encode an N-ary tree into a binary tree and decode the 
//binary tree to get the original N-ary tree. An N-ary tree is a rooted tree in wh
//ich each node has no more than N children. Similarly, a binary tree is a rooted 
//tree in which each node has no more than 2 children. There is no restriction on 
//how your encode/decode algorithm should work. You just need to ensure that an N-
//ary tree can be encoded to a binary tree and this binary tree can be decoded to 
//the original N-nary tree structure. 
//
// Nary-Tree input serialization is represented in their level order traversal, 
//each group of children is separated by the null value (See following example). 
//
// For example, you may encode the following 3-ary tree to a binary tree in this
// way: 
//
// 
//
// 
//Input: root = [1,null,3,2,4,null,5,6]
// 
//
// Note that the above is just an example which might or might not work. You do 
//not necessarily need to follow this format, so please be creative and come up wi
//th different approaches yourself. 
//
// 
// 
//
// 
// Constraints: 
//
// 
// The height of the n-ary tree is less than or equal to 1000 
// The total number of nodes is between [0, 10^4] 
// Do not use class member/global/static variables to store states. Your encode 
//and decode algorithms should be stateless. 
// 
// Related Topics 树


//leetcode submit region begin(Prohibit modification and deletion)
/*
// Definition for a Node.
class Node {
public:
    int val;
    vector<Node*> children;

    Node() {}

    Node(int _val) {
        val = _val;
    }

    Node(int _val, vector<Node*> _children) {
        val = _val;
        children = _children;
    }
};
*/

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

class Codec {
public:
    // Encodes an n-ary tree to a binary tree.
    TreeNode *encode(Node *root) {
        if (!root) return nullptr;

        queue <pair<TreeNode *, Node *>> Q;
        TreeNode *binRoot = new TreeNode(root->val);
        Q.push({binRoot, root});
        while (!Q.empty()) {
            TreeNode *b = Q.front().first;
            Node *n = Q.front().second;
            Q.pop();

            vector < TreeNode * > tmp;
            for (int i = 0; i < n->children.size(); ++i) {
                tmp.push_back(new TreeNode(n->children[i]->val));
                Q.push({tmp.back(), n->children[i]});
                if (tmp.size() > 1)
                    tmp[tmp.size() - 2]->right = tmp.back();
            }
            if (tmp.size() > 0) b->left = tmp[0];
        }
        return binRoot;
    }

    // Decodes your binary tree to an n-ary tree.
    Node *decode(TreeNode *root) {
        if (!root) return nullptr;

        queue <pair<Node *, TreeNode *>> Q;
        Node *nRoot = new Node(root->val);
        Q.push({nRoot, root});

        while (!Q.empty()) {
            Node *n = Q.front().first;
            TreeNode *b = Q.front().second;
            Q.pop();

            vector < Node * > tmp;
            TreeNode *x = b->left;
            while (x) {
                n->children.push_back(new Node(x->val));
                Q.push({n->children.back(), x});
                x = x->right;
            }
        }
        return nRoot;
    }
};

// Your Codec object will be instantiated and called as such:
// Codec codec;
// codec.decode(codec.encode(root));
//leetcode submit region end(Prohibit modification and deletion)
