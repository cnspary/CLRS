//A binary search tree was created by traversing through an array from left to r
//ight and inserting each element. Given a binary search tree with distinct elemen
//ts, print all possible arrays that could have led to this tree.
//
// Example:
//Given the following tree:
//
//
//        2
//       / \
//      1   3
//
//
// Output:
//
//
//[
//   [2,1,3],
//   [2,3,1]
//]
//
// Related Topics 树 动态规划


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    void
    merge(vector<int> &lVec, int lIndex, vector<int> &rVec, int rIndex, vector<int> &tmp, vector <vector<int>> &ans) {
        if (lIndex == lVec.size() && rIndex == rVec.size()) {
            ans.push_back(tmp);
            return;
        }

        if (lIndex < lVec.size()) {
            tmp.push_back(lVec[lIndex]);
            merge(lVec, lIndex + 1, rVec, rIndex, tmp, ans);
            tmp.pop_back();
        }

        if (rIndex < rVec.size()) {
            tmp.push_back(rVec[rIndex]);
            merge(lVec, lIndex, rVec, rIndex + 1, tmp, ans);
            tmp.pop_back();
        }
    }

public:
    vector <vector<int>> BSTSequences(TreeNode *root) {
        if (!root) return {{}};

        vector <vector<int >> ans;
        vector <vector<int>> lVec = BSTSequences(root->left);
        vector <vector<int>> rVec = BSTSequences(root->right);

        vector<int> tmp = {root->val};
        for (auto a : lVec)
            for (auto b : rVec)
                merge(a, 0, b, 0, tmp, ans);

        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
