//Given the root of a binary tree, find the maximum value V for which there exis
//ts different nodes A and B where V = |A.val - B.val| and A is an ancestor of B. 
//
//
// (A node A is an ancestor of B if either: any child of A is equal to B, or any
// child of A is an ancestor of B.) 
//
// 
//
// Example 1: 
//
// 
//
// 
//Input: [8,3,10,1,6,null,14,null,null,4,7,13]
//Output: 7
//Explanation: 
//We have various ancestor-node differences, some of which are given below :
//|8 - 3| = 5
//|3 - 7| = 4
//|8 - 1| = 7
//|10 - 13| = 3
//Among all possible differences, the maximum value of 7 is obtained by |8 - 1| 
//= 7.
// 
//
// 
//
// Note: 
//
// 
// The number of nodes in the tree is between 2 and 5000. 
// Each node will have value between 0 and 100000. 
// 
// Related Topics 树 深度优先搜索


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    int ans = 0;

    vector<int> MaxMin(TreeNode *root) {

        if (!root->left && !root->right) return {root->val, root->val};

        vector<int> l = {root->val, root->val}, r = {root->val, root->val};
        if (root->left) l = MaxMin(root->left);
        if (root->right) r = MaxMin(root->right);

        int MAX = max(l[0], r[0]);
        int MIN = min(l[1], r[1]);

        ans = max(ans, abs(root->val - MAX));
        ans = max(ans, abs(root->val - MIN));

        return {max(MAX, root->val), min(MIN, root->val)};
    }

public:
    int maxAncestorDiff(TreeNode *root) {
        MaxMin(root);
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
