//Given a Binary Search Tree (BST), convert it to a Greater Tree such that every
// key of the original BST is changed to the original key plus sum of all keys gre
//ater than the original key in BST. 
//
// Example: 
//
// 
//Input: The root of a Binary Search Tree like this:
//              5
//            /   \
//           2     13
//
//Output: The root of a Greater Tree like this:
//             18
//            /   \
//          20     13
// 
//
// Note: This question is the same as 1038: https://leetcode.com/problems/binary
//-search-tree-to-greater-sum-tree/ 
// Related Topics 树


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    int pre = 0;

    void InOrder(TreeNode *root) {
        if (!root) return;

        if (root->right) InOrder(root->right);
        pre += root->val;
        root->val = pre;
        if (root->left) InOrder(root->left);

    }

public:
    TreeNode *convertBST(TreeNode *root) {
        InOrder(root);
        return root;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
