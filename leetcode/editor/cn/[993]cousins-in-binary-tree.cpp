//In a binary tree, the root node is at depth 0, and children of each depth k no
//de are at depth k+1. 
//
// Two nodes of a binary tree are cousins if they have the same depth, but have 
//different parents. 
//
// We are given the root of a binary tree with unique values, and the values x a
//nd y of two different nodes in the tree. 
//
// Return true if and only if the nodes corresponding to the values x and y are 
//cousins. 
//
// 
//
// Example 1: 
// 
//
// 
//Input: root = [1,2,3,4], x = 4, y = 3
//Output: false
// 
//
// 
// Example 2: 
// 
//
// 
//Input: root = [1,2,3,null,4,null,5], x = 5, y = 4
//Output: true
// 
//
// 
// Example 3: 
//
// 
//
// 
//Input: root = [1,2,3,null,4], x = 2, y = 3
//Output: false 
//
// 
// 
// 
//
// Note: 
//
// 
// The number of nodes in the tree will be between 2 and 100. 
// Each node has a unique integer value from 1 to 100. 
// 
//
// 
// 
// 
// 
// Related Topics 树 广度优先搜索


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    int xLevel = -1, yLevel = -1;
    int xParent = -1, yParent = -1;

    void dfs(TreeNode *root, int parent, int level, int target1, int target2) {
        if (!root) return;

        if (root->val == target1 || root->val == target2) {
            if (xLevel == -1) {
                xLevel = level;
                xParent = parent;
            } else {
                yLevel = level;
                yParent = parent;
            }

            if (xLevel != -1 && yLevel != -1)
                return;
        }
        dfs(root->left, root->val, level + 1, target1, target2);
        dfs(root->right, root->val, level + 1, target1, target2);
    }

public:
    bool isCousins(TreeNode *root, int x, int y) {
        dfs(root, 0, 0, x, y);
        if (xParent != yParent && xLevel == yLevel) return true;
        else return false;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
