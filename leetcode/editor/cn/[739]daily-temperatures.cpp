//
//Given a list of daily temperatures T, return a list such that, for each day in
// the input, tells you how many days you would have to wait until a warmer temper
//ature. If there is no future day for which this is possible, put 0 instead.
// 
//For example, given the list of temperatures T = [73, 74, 75, 71, 69, 72, 76, 7
//3], your output should be [1, 1, 4, 2, 1, 1, 0, 0].
// 
//
// Note:
//The length of temperatures will be in the range [1, 30000].
//Each temperature will be an integer in the range [30, 100].
// Related Topics 栈 哈希表


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
public:
    vector<int> dailyTemperatures(vector<int> &T) {
        vector<int> ans(T.size(), 0);
        stack<int> S;
        for (int i = 0; i < T.size(); ++i) {
            while(!S.empty() && T[S.top()] < T[i]) {
                ans[S.top()] = i - S.top();
                S.pop();
            }
            S.push(i);
        }

        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
