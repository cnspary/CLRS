//Given a non-empty binary tree, find the maximum path sum. 
//
// For this problem, a path is defined as any sequence of nodes from some starti
//ng node to any node in the tree along the parent-child connections. The path mus
//t contain at least one node and does not need to go through the root. 
//
// Example 1: 
//
// 
//Input: [1,2,3]
//
//       1
//      / \
//     2   3
//
//Output: 6
// 
//
// Example 2: 
//
// 
//Input: [-10,9,20,null,null,15,7]
//
//   -10
//   / \
//  9  20
//    /  \
//   15   7
//
//Output: 42
// 
// Related Topics 树 深度优先搜索


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    int maxPath = INT_MIN;

    int maxChild(TreeNode *root) {
        if (!root) return 0;
        int maxLPath = maxChild(root->left);
        int maxRPath = maxChild(root->right);

        maxPath = max(maxPath, max(0, maxLPath) + root->val + max(0, maxRPath));
        if (maxLPath > maxRPath) return root->val + max(0, maxLPath);
        else return root->val + max(0, maxRPath);
    }

    int maxPathSum(TreeNode *root) {
        maxChild(root);
        return maxPath;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
