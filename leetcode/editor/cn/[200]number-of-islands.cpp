//Given a 2d grid map of '1's (land) and '0's (water), count the number of islan
//ds. An island is surrounded by water and is formed by connecting adjacent lands 
//horizontally or vertically. You may assume all four edges of the grid are all su
//rrounded by water. 
//
// Example 1: 
//
// 
//Input:
//11110
//11010
//11000
//00000
//
//Output: 1
// 
//
// Example 2: 
//
// 
//Input:
//11000
//11000
//00100
//00011
//
//Output: 3
// Related Topics 深度优先搜索 广度优先搜索 并查集


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
private:
    vector <vector<int>> pos = {{1,  0},
                                {0,  1},
                                {-1, 0},
                                {0,  -1}};
    int maxROW, maxCOL;

    void DFS(vector <vector<char>> &grid, int row, int col) {
        for (int i = 0; i < pos.size(); ++i) {
            int x = row + pos[i][0];
            int y = col + pos[i][1];

            if (-1 < x && x < maxROW && -1 < y && y < maxCOL)
                if (grid[x][y] == '1') {
                    grid[x][y] = '0';
                    DFS(grid, x, y);
                }
        }
    }

public:
    int numIslands(vector <vector<char>> &grid) {
        maxROW = grid.size();
        if (maxROW == 0)
            return 0;
        maxCOL = grid[0].size();
        int count = 0;
        for (int i = 0; i < maxROW; ++i)
            for (int j = 0; j < maxCOL; ++j)
                if (grid[i][j] == '1') {
                    DFS(grid, i, j);
                    count++;
                }

        return count;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
