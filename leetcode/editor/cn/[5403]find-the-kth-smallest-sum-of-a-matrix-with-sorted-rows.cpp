//You are given an m * n matrix, mat, and an integer k, which has its rows sorte
//d in non-decreasing order. 
//
// You are allowed to choose exactly 1 element from each row to form an array. R
//eturn the Kth smallest array sum among all possible arrays. 
//
// 
// Example 1: 
//
// 
//Input: mat = [[1,3,11],[2,4,6]], k = 5
//Output: 7
//Explanation: Choosing one element from each row, the first k smallest sum are:
//
//[1,2], [1,4], [3,2], [3,4], [1,6]. Where the 5th sum is 7.  
//
// Example 2: 
//
// 
//Input: mat = [[1,3,11],[2,4,6]], k = 9
//Output: 17
// 
//
// Example 3: 
//
// 
//Input: mat = [[1,10,10],[1,4,5],[2,3,6]], k = 7
//Output: 9
//Explanation: Choosing one element from each row, the first k smallest sum are:
//
//[1,1,2], [1,1,3], [1,4,2], [1,4,3], [1,1,6], [1,5,2], [1,5,3]. Where the 7th s
//um is 9.  
// 
//
// Example 4: 
//
// 
//Input: mat = [[1,1,10],[2,2,9]], k = 7
//Output: 12
// 
//
// 
// Constraints: 
//
// 
// m == mat.length 
// n == mat.length[i] 
// 1 <= m, n <= 40 
// 1 <= k <= min(200, n ^ m) 
// 1 <= mat[i][j] <= 5000 
// mat[i] is a non decreasing array. 
// 
// Related Topics 堆


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
public:
    int kthSmallest(vector <vector<int>> &mat, int k) {
        int n = mat.size(), m = mat[0].size();
        vector<int> v1 = {0}, v2;
        for (int row = 0; row < n; ++row) {

            for (int i = 0; i < v1.size(); ++i)
                for (int j = 0; j < m; ++j)
                    v2.push_back(v1[i] + mat[row][j]);

            sort(v2.begin(), v2.end());

            while (v2.size() > k) v2.pop_back();
            v1 = v2;
            v2.clear();
        }
        return v1.back();
    }
};
//leetcode submit region end(Prohibit modification and deletion)
