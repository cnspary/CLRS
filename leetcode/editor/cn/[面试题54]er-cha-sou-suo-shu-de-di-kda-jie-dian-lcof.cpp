//English description is not available for the problem. Please switch to Chinese
//. Related Topics 树


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    int count(TreeNode *root) {
        if (!root) return 0;
        return 1 + count(root->left) + count(root->right);
    }

public:
    int kthLargest(TreeNode *root, int k) {
        int rc = count(root->right);
        if (rc == k - 1) return root->val;
        else if (rc >= k) return kthLargest(root->right, k);
        else return kthLargest(root->left, k - rc - 1);
    }
};
//leetcode submit region end(Prohibit modification and deletion)
