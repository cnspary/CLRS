//You are given an integer num. You will apply the following steps exactly two t
//imes: 
//
// 
// Pick a digit x (0 <= x <= 9). 
// Pick another digit y (0 <= y <= 9). The digit y can be equal to x. 
// Replace all the occurrences of x in the decimal representation of num by y. 
// The new integer cannot have any leading zeros, also the new integer cannot be
// 0. 
// 
//
// Let a and b be the results of applying the operations to num the first and se
//cond times, respectively. 
//
// Return the max difference between a and b. 
//
// 
// Example 1: 
//
// 
//Input: num = 555
//Output: 888
//Explanation: The first time pick x = 5 and y = 9 and store the new integer in 
//a.
//The second time pick x = 5 and y = 1 and store the new integer in b.
//We have now a = 999 and b = 111 and max difference = 888
// 
//
// Example 2: 
//
// 
//Input: num = 9
//Output: 8
//Explanation: The first time pick x = 9 and y = 9 and store the new integer in 
//a.
//The second time pick x = 9 and y = 1 and store the new integer in b.
//We have now a = 9 and b = 1 and max difference = 8
// 
//
// Example 3: 
//
// 
//Input: num = 123456
//Output: 820000
// 
//
// Example 4: 
//
// 
//Input: num = 10000
//Output: 80000
// 
//
// Example 5: 
//
// 
//Input: num = 9288
//Output: 8700
// 
//
// 
// Constraints: 
//
// 
// 1 <= num <= 10^8 
// 
// Related Topics 字符串


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
public:
    int maxDiff(int num) {
        vector<int> a, b;
        while (num != 0) {
            a.push_back(num % 10);
            b.push_back(num % 10);
            num /= 10;
        }

        int pivot = -1, i = a.size() - 1;
        for (; -1 < i; --i) {
            if (a[i] != 9 && (pivot < 0 || a[i] == pivot)) {
                pivot = a[i];
                a[i] = 9;
            }
        }

        if (b.back() != 1) {
            pivot = b.back();
            for (i = b.size() - 1; -1 < i; --i)
                if (b[i] == pivot) b[i] = 1;
        } else {
            pivot = -1;
            for (i = b.size() - 2; -1 < i; --i) {
                if (b[i] != 0 && b[i] != b.back() && (pivot < 0 || b[i] == pivot)) {
                    pivot = b[i];
                    b[i] = 0;
                }
            }
        }

        int n1 = 0, n2 = 0;
        for (int i = a.size() - 1; -1 < i; --i) {
            n1 = n1 * 10 + a[i];
            n2 = n2 * 10 + b[i];
        }

        return abs(n1 - n2);
    }
};
//leetcode submit region end(Prohibit modification and deletion)
