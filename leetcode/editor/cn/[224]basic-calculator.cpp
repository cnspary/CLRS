//Implement a basic calculator to evaluate a simple expression string. 
//
// The expression string may contain open ( and closing parentheses ), the plus 
//+ or minus sign -, non-negative integers and empty spaces . 
//
// Example 1: 
//
// 
//Input: "1 + 1"
//Output: 2
// 
//
// Example 2: 
//
// 
//Input: " 2-1 + 2 "
//Output: 3 
//
// Example 3: 
//
// 
//Input: "(1+(4+5+2)-3)+(6+8)"
//Output: 23 
//Note:
//
// 
// You may assume that the given expression is always valid. 
// Do not use the eval built-in library function. 
// 
// Related Topics 栈 数学


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
public:
    int calculate(string st) {

        stack<int> s;
        stack<char> op;
        st.push_back('+');
        long long res = 0;
        long long curnum = 0;
        bool sign = true;
        for (auto &cur:st) {
            if (cur == ' ') continue;
            if (cur == '(') {
                s.push(res);
                if (sign) op.push('+');
                else op.push('-');
                res = 0;
                curnum = 0;
                sign = true;
            }
            if (cur == '+' || cur == '-') {
                res = sign ? res + curnum : res - curnum;
                curnum = 0;
                sign = cur == '+';

            }
            if (isdigit(cur)) {
                curnum = curnum * 10 + cur - '0';
            }
            if (cur == ')') {
                res = sign ? res + curnum : res - curnum;
                curnum = res;
                res = s.top();
                sign = op.top() == '+';
                op.pop();
                s.pop();
            }

        }
        return res;

    }
};
//leetcode submit region end(Prohibit modification and deletion)
