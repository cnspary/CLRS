//Given an array equations of strings that represent relationships between varia
//bles, each string equations[i] has length 4 and takes one of two different forms
//: "a==b" or "a!=b". Here, a and b are lowercase letters (not necessarily differe
//nt) that represent one-letter variable names. 
//
// Return true if and only if it is possible to assign integers to variable name
//s so as to satisfy all the given equations. 
//
// 
//
// 
// 
//
// 
// Example 1: 
//
// 
//Input: ["a==b","b!=a"]
//Output: false
//Explanation: If we assign say, a = 1 and b = 1, then the first equation is sat
//isfied, but not the second.  There is no way to assign the variables to satisfy 
//both equations.
// 
//
// 
// Example 2: 
//
// 
//Input: ["b==a","a==b"]
//Output: true
//Explanation: We could assign a = 1 and b = 1 to satisfy both equations.
// 
//
// 
// Example 3: 
//
// 
//Input: ["a==b","b==c","a==c"]
//Output: true
// 
//
// 
// Example 4: 
//
// 
//Input: ["a==b","b!=c","c==a"]
//Output: false
// 
//
// 
// Example 5: 
//
// 
//Input: ["c==c","b==d","x!=z"]
//Output: true
// 
//
// 
//
// Note: 
//
// 
// 1 <= equations.length <= 500 
// equations[i].length == 4 
// equations[i][0] and equations[i][3] are lowercase letters 
// equations[i][1] is either '=' or '!' 
// equations[i][2] is '=' 
// 
// 
// 
// 
// 
// 
// Related Topics 并查集 图


//leetcode submit region begin(Prohibit modification and deletion)
class UnionFind {
private:
    int cnt;
    vector<int> uf;
    vector<int> rank;

public:
    UnionFind(int n) {
        cnt = n;
        uf = vector<int>(n);
        rank = vector<int>(n, 1);
        for (int i = 0; i < n; ++i)
            uf[i] = i;
    }

    int find(int a) {
        int p = a;
        while (uf[a] != a) a = uf[a];
        int root = uf[a];
        while (uf[p] != root) {
            int x = uf[p];
            uf[p] = root;
            p = x;
        }
        return root;
    }

    bool uunion(int a, int b) {
        int r1 = find(a);
        int r2 = find(b);
        if (r1 == r2) return false;

        cnt--;
        if (rank[r1] < rank[r2]) {
            uf[r1] = r2;
            rank[r2] += rank[r1];
        } else {
            uf[r2] = r1;
            rank[r1] += rank[r2];
        }
        return true;
    }

    int getRank(int i) {
        return rank[find(i)];
    }

    int count() {
        return cnt;
    }

    bool connected(int x, int y) {
        return find(x) == find(y);
    }
};

class Solution {
public:
    bool equationsPossible(vector <string> &equations) {
        UnionFind uf(26);
        vector <string> inequality;
        for (int i = 0; i < equations.size(); ++i) {
            if (equations[i][1] == '=') {
                int a = equations[i][0] - 'a';
                int b = equations[i][3] - 'a';
                uf.uunion(a, b);
            } else inequality.push_back(equations[i]);
        }

        for (int i = 0; i < inequality.size(); ++i) {
            int a = inequality[i][0] - 'a';
            int b = inequality[i][3] - 'a';
            if (uf.connected(a, b)) return false;
        }
        return true;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
