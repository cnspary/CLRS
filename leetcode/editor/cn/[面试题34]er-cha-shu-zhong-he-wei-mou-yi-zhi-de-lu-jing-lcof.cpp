//English description is not available for the problem. Please switch to Chinese
//. Related Topics 树 深度优先搜索


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    vector <vector<int>> ans;
    vector<int> S;

    void DFS(TreeNode *root, int pathSum, int sum) {
        S.push_back(root->val);

        if (root->left) DFS(root->left, pathSum + root->val, sum);
        if (root->right) DFS(root->right, pathSum + root->val, sum);

        if (!root->left && !root->right && pathSum + root->val == sum)
            ans.push_back(S);

        S.pop_back();

        return;
    }

public:
    vector <vector<int>> pathSum(TreeNode *root, int sum) {
        if (!root) return {};

        DFS(root, 0, sum);
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
