//Given the string croakOfFrogs, which represents a combination of the string "c
//roak" from different frogs, that is, multiple frogs can croak at the same time, 
//so multiple “croak” are mixed. Return the minimum number of different frogs to f
//inish all the croak in the given string. 
//
// A valid "croak" means a frog is printing 5 letters ‘c’, ’r’, ’o’, ’a’, ’k’ se
//quentially. The frogs have to print all five letters to finish a croak. If the g
//iven string is not a combination of valid "croak" return -1. 
//
// 
// Example 1: 
//
// 
//Input: croakOfFrogs = "croakcroak"
//Output: 1 
//Explanation: One frog yelling "croak" twice.
// 
//
// Example 2: 
//
// 
//Input: croakOfFrogs = "crcoakroak"
//Output: 2 
//Explanation: The minimum number of frogs is two. 
//The first frog could yell "crcoakroak".
//The second frog could yell later "crcoakroak".
// 
//
// Example 3: 
//
// 
//Input: croakOfFrogs = "croakcrook"
//Output: -1
//Explanation: The given string is an invalid combination of "croak" from differ
//ent frogs.
// 
//
// Example 4: 
//
// 
//Input: croakOfFrogs = "croakcroa"
//Output: -1
// 
//
// 
// Constraints: 
//
// 
// 1 <= croakOfFrogs.length <= 10^5 
// All characters in the string are: 'c', 'r', 'o', 'a' or 'k'. 
// Related Topics 字符串


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
public:
    vector<int> alph;

    int minNumberOfFrogs(string croakOfFrogs) {
        alph.resize(26, 0);
        int minFrog = 0;

        for (char c : croakOfFrogs) {
            alph[c - 'a']++;

            if (!(alph['c' - 'a'] >= alph['r' - 'a'] &&
                  alph['r' - 'a'] >= alph['o' - 'a'] &&
                  alph['o' - 'a'] >= alph['a' - 'a'] &&
                  alph['a' - 'a'] >= alph['k' - 'a']))
                return -1;

            minFrog = max(alph['c' - 'a'], minFrog);
            while (alph['c' - 'a'] > 0 && alph['r' - 'a'] > 0 && alph['o' - 'a'] > 0 && alph['a' - 'a'] > 0 &&
                   alph['k' - 'a'] > 0) {
                alph['c' - 'a']--;
                alph['r' - 'a']--;
                alph['o' - 'a']--;
                alph['a' - 'a']--;
                alph['k' - 'a']--;
            }
        }
        if (alph['c' - 'a'] != 0 ||
            alph['r' - 'a'] != 0 ||
            alph['o' - 'a'] != 0 ||
            alph['a' - 'a'] != 0 ||
            alph['k' - 'a'] != 0)
            return -1;
        else return minFrog;
    }

};
//leetcode submit region end(Prohibit modification and deletion)
