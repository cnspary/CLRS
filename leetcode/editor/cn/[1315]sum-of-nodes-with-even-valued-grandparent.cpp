//Given a binary tree, return the sum of values of nodes with even-valued grandp
//arent. (A grandparent of a node is the parent of its parent, if it exists.) 
//
// If there are no nodes with an even-valued grandparent, return 0. 
//
// 
// Example 1: 
//
// 
//
// 
//Input: root = [6,7,8,2,7,1,3,9,null,1,4,null,null,null,5]
//Output: 18
//Explanation: The red nodes are the nodes with even-value grandparent while the
// blue nodes are the even-value grandparents.
// 
//
// 
// Constraints: 
//
// 
// The number of nodes in the tree is between 1 and 10^4. 
// The value of nodes is between 1 and 100. 
// Related Topics 树 深度优先搜索


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    int ans = 0;

    void dfs(TreeNode *root, int p, int gp) {
        if (!root) return;

        if (gp % 2 == 0)
            ans += root->val;

        dfs(root->left, root->val, p);
        dfs(root->right, root->val, p);
    }

public:
    int sumEvenGrandparent(TreeNode *root) {
        dfs(root, -1, -1);
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
