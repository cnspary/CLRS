//Given a Binary Search Tree and a target number, return true if there exist two
// elements in the BST such that their sum is equal to the given target. 
//
// Example 1: 
//
// 
//Input: 
//    5
//   / \
//  3   6
// / \   \
//2   4   7
//
//Target = 9
//
//Output: True
// 
//
// 
//
// Example 2: 
//
// 
//Input: 
//    5
//   / \
//  3   6
// / \   \
//2   4   7
//
//Target = 28
//
//Output: False
// 
//
// 
// Related Topics 树


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    vector<int> orderSeq;

    void InOrder(TreeNode *root) {
        if (!root) return;

        InOrder(root->left);
        orderSeq.push_back(root->val);
        InOrder(root->right);
    }

public:
    bool findTarget(TreeNode *root, int k) {
        InOrder(root);
        for (int i = 0, j = orderSeq.size() - 1; i < j;) {
            if (orderSeq[i] + orderSeq[j] == k) return true;
            else if (orderSeq[i] + orderSeq[j] > k) j--;
            else if (orderSeq[i] + orderSeq[j] < k) i++;
        }
        return false;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
