//Given a binary tree, return the inorder traversal of its nodes' values. 
//
// Example: 
//
// 
//Input: [1,null,2,3]
//   1
//    \
//     2
//    /
//   3
//
//Output: [1,3,2] 
//
// Follow up: Recursive solution is trivial, could you do it iteratively? 
// Related Topics 栈 树 哈希表


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    stack<TreeNode *> S;

    void goAlongLeftBranch(TreeNode *p) {
        while (p) {
            S.push(p);
            p = p->left;
        }
        return;
    }

public:
    vector<int> inorderTraversal(TreeNode *root) {
        vector<int> ans;
        TreeNode *p = root;
        while (true) {
            goAlongLeftBranch(p);
            if (S.empty()) break;
            p = S.top();
            S.pop();
            ans.push_back(p->val);
            p = p->right;
        }
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
