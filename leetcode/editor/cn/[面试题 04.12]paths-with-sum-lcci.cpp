//You are given a binary tree in which each node contains an integer value (whic
//h might be positive or negative). Design an algorithm to count the number of pat
//hs that sum to a given value. The path does not need to start or end at the root
// or a leaf, but it must go downwards (traveling only from parent nodes to child 
//nodes). 
//
// Example: 
//Given the following tree and sum = 22, 
//
// 
//              5
//             / \
//            4   8
//           /   / \
//          11  13  4
//         /  \    / \
//        7    2  5   1
// 
//
// Output: 
//
// 
//3
//Explanation: Paths that have sum 22 are: [5,4,11,2], [5,8,4,5], [4,11,7] 
//
// Note: 
//
// 
// node number <= 10000 
// 
// Related Topics 树 深度优先搜索


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    int ans = 0;
    map<int, int> prefixSum;

    void DFS(TreeNode *root, int sum, int target) {
        if (!root) return;

        sum += root->val;
        if (prefixSum[sum - target] != 0)
            ans += prefixSum[sum - target];

        prefixSum[sum]++;

        DFS(root->left, sum, target);
        DFS(root->right, sum, target);

        prefixSum[sum]--;
        sum -= root->val;
    }

public:
    int pathSum(TreeNode *root, int target) {
        prefixSum[0] = 1;
        DFS(root, 0, target);
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
