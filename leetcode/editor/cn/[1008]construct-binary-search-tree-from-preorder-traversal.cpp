//Return the root node of a binary search tree that matches the given preorder t
//raversal. 
//
// (Recall that a binary search tree is a binary tree where for every node, any 
//descendant of node.left has a value < node.val, and any descendant of node.right
// has a value > node.val. Also recall that a preorder traversal displays the valu
//e of the node first, then traverses node.left, then traverses node.right.) 
//
// 
//
// Example 1: 
//
// 
//Input: [8,5,1,7,10,12]
//Output: [8,5,10,1,7,null,12]
//
// 
//
// 
//
// Note: 
//
// 
// 1 <= preorder.length <= 100 
// The values of preorder are distinct. 
// 
// Related Topics 树


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    TreeNode *buildTree(vector<int> preorder, int l, int r) {
        if (l > r) return nullptr;

        TreeNode *root = new TreeNode(preorder[l]);
        l++;

        int mid = l;
        while (mid < preorder.size() && preorder[mid] < root->val) mid++;

        root->left = buildTree(preorder, l, mid - 1);
        root->right = buildTree(preorder, mid, r);
        return root;
    }

public:
    TreeNode *bstFromPreorder(vector<int> &preorder) {
        return buildTree(preorder, 0, preorder.size() - 1);
    }
};
//leetcode submit region end(Prohibit modification and deletion)
