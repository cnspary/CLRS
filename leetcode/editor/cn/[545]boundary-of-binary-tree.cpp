//Given a binary tree, return the values of its boundary in anti-clockwise direc
//tion starting from root. Boundary includes left boundary, leaves, and right boun
//dary in order without duplicate nodes. (The values of the nodes may still be dup
//licates.) 
//
// Left boundary is defined as the path from root to the left-most node. Right b
//oundary is defined as the path from root to the right-most node. If the root doe
//sn't have left subtree or right subtree, then the root itself is left boundary o
//r right boundary. Note this definition only applies to the input binary tree, an
//d not applies to any subtrees. 
//
// The left-most node is defined as a leaf node you could reach when you always 
//firstly travel to the left subtree if exists. If not, travel to the right subtre
//e. Repeat until you reach a leaf node. 
//
// The right-most node is also defined by the same way with left and right excha
//nged. 
//
// Example 1 
//
// 
//Input:
//  1
//   \
//    2
//   / \
//  3   4
//
//Ouput:
//[1, 3, 4, 2]
//
//Explanation:
//The root doesn't have left subtree, so the root itself is left boundary.
//The leaves are node 3 and 4.
//The right boundary are node 1,2,4. Note the anti-clockwise direction means you
// should output reversed right boundary.
//So order them in anti-clockwise without duplicates and we have [1,3,4,2].
// 
//
// 
//
// Example 2 
//
// 
//Input:
//    ____1_____
//   /          \
//  2            3
// / \          / 
//4   5        6   
//   / \      / \
//  7   8    9  10  
//       
//Ouput:
//[1,2,4,7,8,9,10,6,3]
//
//Explanation:
//The left boundary are node 1,2,4. (4 is the left-most node according to defini
//tion)
//The leaves are node 4,7,8,9,10.
//The right boundary are node 1,3,6,10. (10 is the right-most node).
//So order them in anti-clockwise without duplicate nodes we have [1,2,4,7,8,9,1
//0,6,3].
// 
//
// 
// Related Topics 树


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    vector<int> LB, RB, LEAF;

    bool isLeaf(TreeNode *root) {
        return !root->left && !root->right;
    }

    bool isRoot(int type) {
        return type == 0;
    }

    bool isLeftBoundary(int type) {
        return type == 1;
    }

    bool isRightBoundary(int type) {
        return type == 2;
    }

    int leftChildType(TreeNode *x, int type) {
        if (isRoot(type) || isLeftBoundary(type))
            return 1;
        else if (isRightBoundary(type) && !x->right)
            return 2;
        else return 3;
    }

    int rightChildType(TreeNode *x, int type) {
        if (isRoot(type) || isRightBoundary(type))
            return 2;
        else if (isLeftBoundary(type) && !x->left)
            return 1;
        return 3;
    }

    void preOrder(TreeNode *root, int type) {
        if (!root) return;

        if (isLeftBoundary(type) || isRoot(type))
            LB.push_back(root->val);
        else if (isRightBoundary(type))
            RB.push_back(root->val);
        else if (isLeaf(root))
            LEAF.push_back(root->val);

        preOrder(root->left, leftChildType(root, type));
        preOrder(root->right, rightChildType(root, type));
    }

public:
    vector<int> boundaryOfBinaryTree(TreeNode *root) {
        preOrder(root, 0);
        for (int i = 0; i < LEAF.size(); ++i)
            LB.push_back(LEAF[i]);
        for (int i = RB.size() - 1; -1 < i; --i)
            LB.push_back(RB[i]);

        return LB;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
