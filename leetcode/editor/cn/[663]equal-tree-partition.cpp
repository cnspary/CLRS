//
//Given a binary tree with n nodes, your task is to check if it's possible to pa
//rtition the tree to two trees which have the equal sum of values after removing 
//exactly one edge on the original tree.
// 
//
// Example 1: 
// 
//Input:     
//    5
//   / \
//  10 10
//    /  \
//   2   3
//
//Output: True
//Explanation: 
//    5
//   / 
//  10
//      
//Sum: 15
//
//   10
//  /  \
// 2    3
//
//Sum: 15
// 
// 
//
//
// Example 2: 
// 
//Input:     
//    1
//   / \
//  2  10
//    /  \
//   2   20
//
//Output: False
//Explanation: You can't split the tree into two trees with equal sum after remo
//ving exactly one edge on the tree.
// 
// 
//
// Note: 
// 
// The range of tree node value is in the range of [-100000, 100000]. 
// 1 <= n <= 10000 
// 
// Related Topics 树


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    TreeNode *R;
    set<int> sum;

    int subTreeSum(TreeNode *root) {
        if (!root) return 0;

        int lsum = subTreeSum(root->left);
        int rsum = subTreeSum(root->right);
        if (R != root)
            sum.insert(root->val + lsum + rsum);
        return root->val + lsum + rsum;
    }

public:
    bool checkEqualTree(TreeNode *root) {
        R = root;
        int allSum = subTreeSum(root);
        if (allSum % 2 == 0 && sum.find(allSum / 2) != sum.end())
            return true;
        else return false;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
