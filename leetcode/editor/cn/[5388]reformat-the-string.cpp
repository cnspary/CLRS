//Given alphanumeric string s. (Alphanumeric string is a string consisting of lo
//wercase English letters and digits). 
//
// You have to find a permutation of the string where no letter is followed by a
//nother letter and no digit is followed by another digit. That is, no two adjacen
//t characters have the same type. 
//
// Return the reformatted string or return an empty string if it is impossible t
//o reformat the string. 
//
// 
// Example 1: 
//
// 
//Input: s = "a0b1c2"
//Output: "0a1b2c"
//Explanation: "0a1b2c" doesn't have any letter followed by digit or any digit f
//ollowed by character. "a0b1c2", "0a1b2c", "0c2a1b" are also valid permutations.
// 
//
// Example 2: 
//
// 
//Input: s = "leetcode"
//Output: ""
//Explanation: "leetcode" has only characters so we cannot separate them by digi
//ts.
// 
//
// Example 3: 
//
// 
//Input: s = "1229857369"
//Output: ""
//Explanation: "1229857369" has only digits so we cannot separate them by charac
//ters.
// 
//
// Example 4: 
//
// 
//Input: s = "covid2019"
//Output: "c2o0v1i9d"
// 
//
// Example 5: 
//
// 
//Input: s = "ab123"
//Output: "1a2b3"
// 
//
// 
// Constraints: 
//
// 
// 1 <= s.length <= 500 
// s consists of only lowercase English letters and/or digits. 
// Related Topics 字符串


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
public:
    string reformat(string s) {
        vector<char> a;
        vector<char> b;
        for (char x : s) {
            if ('0' <= x && x <= '9') b.push_back(x);
            else a.push_back(x);
        }

        if (abs(int(a.size() - b.size())) > 1) return "";

        string ans = "";
        if (a.size() < b.size()) swap(a, b);
        for (int i = 0; i < b.size(); ++i)
            ans = ans + a[i] + b[i];
        for (int i = b.size(); i < a.size(); ++i)
            ans += a[i];
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
