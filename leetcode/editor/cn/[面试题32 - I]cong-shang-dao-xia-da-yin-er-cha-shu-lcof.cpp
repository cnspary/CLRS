//English description is not available for the problem. Please switch to Chinese
//. Related Topics 树 广度优先搜索


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<int> levelOrder(TreeNode *root) {
        if (!root) return {};
        vector<int> ans;
        queue < TreeNode * > Q;
        Q.push(root);
        while (!Q.empty()) {
            root = Q.front();
            Q.pop();
            ans.push_back(root->val);

            if (root->left) Q.push(root->left);
            if (root->right) Q.push(root->right);
        }
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
