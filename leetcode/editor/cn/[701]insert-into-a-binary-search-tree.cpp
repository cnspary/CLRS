//Given the root node of a binary search tree (BST) and a value to be inserted i
//nto the tree, insert the value into the BST. Return the root node of the BST aft
//er the insertion. It is guaranteed that the new value does not exist in the orig
//inal BST. 
//
// Note that there may exist multiple valid ways for the insertion, as long as t
//he tree remains a BST after insertion. You can return any of them. 
//
// For example, 
//
// 
//Given the tree:
//        4
//       / \
//      2   7
//     / \
//    1   3
//And the value to insert: 5
// 
//
// You can return this binary search tree: 
//
// 
//         4
//       /   \
//      2     7
//     / \   /
//    1   3 5
// 
//
// This tree is also valid: 
//
// 
//         5
//       /   \
//      2     7
//     / \   
//    1   3
//         \
//          4
// 
// Related Topics 树


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    TreeNode *insertIntoBST(TreeNode *root, int val) {
        if (!root) return new TreeNode(val);
        TreeNode *pivot = root, *hot = nullptr;

        while (pivot) {
            hot = pivot;
            if (pivot->val == val) break;
            else if (val < pivot->val) pivot = pivot->left;
            else pivot = pivot->right;
        }

        if (!pivot) {
            if (val < hot->val) hot->left = new TreeNode(val);
            else hot->right = new TreeNode(val);
        }
        return root;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
