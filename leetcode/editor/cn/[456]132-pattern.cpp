//
//Given a sequence of n integers a1, a2, ..., an, a 132 pattern is a subsequence
// ai, aj, ak such
//that i < j < k and ai < ak < aj. Design an algorithm that takes a list of n nu
//mbers as input and checks whether there is a 132 pattern in the list. 
//
// Note: n will be less than 15,000. 
//
// Example 1: 
// 
//Input: [1, 2, 3, 4]
//
//Output: False
//
//Explanation: There is no 132 pattern in the sequence.
// 
// 
//
// Example 2: 
// 
//Input: [3, 1, 4, 2]
//
//Output: True
//
//Explanation: There is a 132 pattern in the sequence: [1, 4, 2].
// 
// 
//
// Example 3: 
// 
//Input: [-1, 3, 2, 0]
//
//Output: True
//
//Explanation: There are three 132 patterns in the sequence: [-1, 3, 2], [-1, 3,
// 0] and [-1, 2, 0].
// 
// Related Topics 栈


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
public:
    bool find132pattern(vector<int> &nums) {
        if (nums.size() < 3) return false;
        vector<int> Min(nums.size(), 0);
        Min[0] = nums[0];
        for (int i = 1; i < nums.size(); ++i)
            Min[i] = min(Min[i - 1], nums[i]);

        stack<int> S;
        for (int i = nums.size() - 1; -1 < i; --i) {
            if (nums[i] > Min[i]) {
                while (!S.empty() && S.top() <= Min[i])
                    S.pop();
                if (!S.empty() && S.top() < nums[i])
                    return true;
                S.push(nums[i]);
            }
        }

        return false;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
