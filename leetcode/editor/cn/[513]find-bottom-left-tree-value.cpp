//
//Given a binary tree, find the leftmost value in the last row of the tree. 
// 
//
// Example 1: 
// 
//Input:
//
//    2
//   / \
//  1   3
//
//Output:
//1
// 
// 
//
// Example 2: 
// 
//Input:
//
//        1
//       / \
//      2   3
//     /   / \
//    4   5   6
//       /
//      7
//
//Output:
//7
// 
// 
//
// Note:
//You may assume the tree (i.e., the given root node) is not NULL.
// Related Topics 树 深度优先搜索 广度优先搜索


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    int val, deep = -1;

    void BFS(TreeNode *root) {
        queue <pair<TreeNode *, int>> Q;
        Q.push({root, 0});

        while (!Q.empty()) {
            TreeNode *p = Q.front().first;
            int d = Q.front().second;
            Q.pop();

            if (deep < d) {
                deep = d;
                val = p->val;
            }

            if (p->left) Q.push({p->left, d + 1});
            if (p->right) Q.push({p->right, d + 1});
        }
        return;
    }

public:
    int findBottomLeftValue(TreeNode *root) {
        BFS(root);
        return val;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
