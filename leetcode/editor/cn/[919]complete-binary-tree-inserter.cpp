//A complete binary tree is a binary tree in which every level, except possibly 
//the last, is completely filled, and all nodes are as far left as possible. 
//
// Write a data structure CBTInserter that is initialized with a complete binary
// tree and supports the following operations: 
//
// 
// CBTInserter(TreeNode root) initializes the data structure on a given tree wit
//h head node root; 
// CBTInserter.insert(int v) will insert a TreeNode into the tree with value nod
//e.val = v so that the tree remains complete, and returns the value of the parent
// of the inserted TreeNode; 
// CBTInserter.get_root() will return the head node of the tree. 
// 
//
// 
// 
//
// 
// 
//
// Example 1: 
//
// 
//Input: inputs = ["CBTInserter","insert","get_root"], inputs = [[[1]],[2],[]]
//Output: [null,1,[1,2]]
// 
//
// 
// Example 2: 
//
// 
//Input: inputs = ["CBTInserter","insert","insert","get_root"], inputs = [[[1,2,
//3,4,5,6]],[7],[8],[]]
//Output: [null,3,4,[1,2,3,4,5,6,7,8]] 
// 
//
// 
// 
//
// Note: 
//
// 
// The initial given tree is complete and contains between 1 and 1000 nodes. 
// CBTInserter.insert is called at most 10000 times per test case. 
// Every value of a given or inserted node is between 0 and 5000. 
// 
// 
// 
//
// 
// 
//
// 
// 
// Related Topics 树


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class CBTInserter {
private:
    vector<TreeNode *> CBTs;
public:
    CBTInserter(TreeNode *root) {
        if (!root) return;

        queue < TreeNode * > Q;
        Q.push(root);

        while (!Q.empty()) {
            root = Q.front();
            Q.pop();

            CBTs.push_back(root);
            if (root->left) Q.push(root->left);
            if (root->right) Q.push(root->right);
        }
    }

    int insert(int v) {
        CBTs.push_back(new TreeNode(v));
        int parent = (CBTs.size() - 2) / 2;
        if (!CBTs[parent]->left) CBTs[parent]->left = CBTs.back();
        else CBTs[parent]->right = CBTs.back();
        return CBTs[parent]->val;
    }

    TreeNode *get_root() {
        return CBTs[0];
    }
};

/**
 * Your CBTInserter object will be instantiated and called as such:
 * CBTInserter* obj = new CBTInserter(root);
 * int param_1 = obj->insert(v);
 * TreeNode* param_2 = obj->get_root();
 */
//leetcode submit region end(Prohibit modification and deletion)
