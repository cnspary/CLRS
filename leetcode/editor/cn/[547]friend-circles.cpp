//
//There are N students in a class. Some of them are friends, while some are not.
// Their friendship is transitive in nature. For example, if A is a direct friend 
//of B, and B is a direct friend of C, then A is an indirect friend of C. And we d
//efined a friend circle is a group of students who are direct or indirect friends
//.
// 
//
// 
//Given a N*N matrix M representing the friend relationship between students in 
//the class. If M[i][j] = 1, then the ith and jth students are direct friends with
// each other, otherwise not. And you have to output the total number of friend ci
//rcles among all the students.
// 
//
// Example 1: 
// 
//Input: 
//[[1,1,0],
// [1,1,0],
// [0,0,1]]
//Output: 2
//Explanation:The 0th and 1st students are direct friends, so they are in a frie
//nd circle. The 2nd student himself is in a friend circle. So return 2.
// 
// 
//
// Example 2: 
// 
//Input: 
//[[1,1,0],
// [1,1,1],
// [0,1,1]]
//Output: 1
//Explanation:The 0th and 1st students are direct friends, the 1st and 2nd stude
//nts are direct friends, so the 0th and 2nd students are indirect friends. All of
// them are in the same friend circle, so return 1.
// 
// 
//
//
// Note: 
// 
// N is in range [1,200]. 
// M[i][i] = 1 for all students. 
// If M[i][j] = 1, then M[j][i] = 1. 
// 
// Related Topics 深度优先搜索 并查集


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
private:
    vector<int> uf;
    vector<int> rank;
    int component;

    int find(int x) {
        int root = x;
        while (uf[root] != root)
            root = uf[root];

        while (uf[x] != root) {
            int t = uf[x];
            uf[x] = root;
            x = t;
        }
        return root;
    }

    void unionn(int x, int y) {
        int r1 = find(x);
        int r2 = find(y);

        if (r1 == r2) return;

        component--;
        if (rank[r1] > rank[r2]) {
            uf[r2] = r1;
            rank[r1] += rank[r2];
        } else {
            uf[r1] = r2;
            rank[r2] += rank[r1];
        }
        return;
    }

public:
    int findCircleNum(vector <vector<int>> &M) {
        uf = vector<int>(M.size());
        rank = vector<int>(M.size(), 1);
        component = M.size();
        for (int i = 0; i < uf.size(); ++i)
            uf[i] = i;

        for (int i = 0; i < M.size(); ++i)
            for (int j = i + 1; j < M.size(); ++j)
                if (M[i][j] == 1)
                    unionn(i, j);

        return component;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
