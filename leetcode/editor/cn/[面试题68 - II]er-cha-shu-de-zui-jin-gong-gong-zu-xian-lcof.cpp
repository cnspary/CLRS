//English description is not available for the problem. Please switch to Chinese
//. Related Topics 树


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    vector<TreeNode *> path1, path2;

    bool DFS(TreeNode *root, TreeNode *x, vector<TreeNode *> &path) {
        if (!root) return false;

        path.push_back(root);
        if (root == x) return true;

        if (DFS(root->left, x, path)) return true;
        if (DFS(root->right, x, path)) return true;
        path.pop_back();
        return false;
    }

public:
    TreeNode *lowestCommonAncestor(TreeNode *root, TreeNode *p, TreeNode *q) {
        DFS(root, p, path1);
        DFS(root, q, path2);

        int i;
        for (i = 0; i < min(path1.size(), path2.size()); ++i)
            if (path1[i] != path2[i]) break;

        return path1[i - 1];
    }
};
//leetcode submit region end(Prohibit modification and deletion)
