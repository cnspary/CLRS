//Given a 2D board containing 'X' and 'O' (the letter O), capture all regions su
//rrounded by 'X'. 
//
// A region is captured by flipping all 'O's into 'X's in that surrounded region
//. 
//
// Example: 
//
// 
//X X X X
//X O O X
//X X O X
//X O X X
// 
//
// After running your function, the board should be: 
//
// 
//X X X X
//X X X X
//X X X X
//X O X X
// 
//
// Explanation: 
//
// Surrounded regions shouldn’t be on the border, which means that any 'O' on th
//e border of the board are not flipped to 'X'. Any 'O' that is not on the border 
//and it is not connected to an 'O' on the border will be flipped to 'X'. Two cell
//s are connected if they are adjacent cells connected horizontally or vertically.
// 
// Related Topics 深度优先搜索 广度优先搜索 并查集


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
private:
    int ROW, COL;
    int PIVOT;
    vector<int> uf;
    vector<int> rank;

    int find(int x) {
        int root = x;
        while (uf[root] != root)
            root = uf[root];

        while (uf[x] != root) {
            int t = uf[x];
            uf[x] = root;
            x = t;
        }
        return root;
    }

    void unionn(int x, int y) {
        int r1 = find(x);
        int r2 = find(y);

        if (r1 == r2) return;

        if (rank[r1] > rank[r2]) {
            uf[r2] = r1;
            rank[r1] += rank[r2];
        } else {
            uf[r1] = r2;
            rank[r2] += rank[r1];
        }
        return;
    }

    bool connected(int x, int y) {
        return find(x) == find(y);
    }

public:
    void solve(vector <vector<char>> &board) {
        ROW = board.size();
        if (ROW == 0) return;

        COL = board[0].size();
        PIVOT = ROW * COL;

        uf.resize(ROW * COL + 1);
        rank.resize(ROW * COL + 1);

        for (int i = 0; i < uf.size(); ++i)
            uf[i] = i;

        vector <vector<int>> pos = {{1,  0},
                                    {0,  1},
                                    {-1, 0},
                                    {0,  -1}};

        for (int r = 0; r < ROW; ++r) {
            for (int c = 0; c < COL; ++c) {
                if (board[r][c] == 'O') {
                    if (r == 0 || r == ROW - 1 || c == 0 || c == COL - 1)
                        unionn(r * COL + c, PIVOT);

                    for (int i = 0; i < pos.size(); ++i) {
                        int nx = r + pos[i][0];
                        int ny = c + pos[i][1];

                        if (0 <= nx && nx < ROW && 0 <= ny && ny < COL && board[nx][ny] == 'O')
                            unionn(r * COL + c, nx * COL + ny);
                    }
                }
            }
        }

        for (int r = 0; r < ROW; ++r)
            for (int c = 0; c < COL; ++c)
                if (!connected(r * COL + c, PIVOT))
                    board[r][c] = 'X';

        return;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
