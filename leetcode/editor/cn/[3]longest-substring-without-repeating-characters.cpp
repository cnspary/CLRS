//Given a string, find the length of the longest substring without repeating cha
//racters. 
//
// 
// Example 1: 
//
// 
//Input: "abcabcbb"
//Output: 3 
//Explanation: The answer is "abc", with the length of 3. 
// 
//
// 
// Example 2: 
//
// 
//Input: "bbbbb"
//Output: 1
//Explanation: The answer is "b", with the length of 1.
// 
//
// 
// Example 3: 
//
// 
//Input: "pwwkew"
//Output: 3
//Explanation: The answer is "wke", with the length of 3. 
//             Note that the answer must be a substring, "pwke" is a subsequence
// and not a substring.
// 
// 
// 
// 
// Related Topics 哈希表 双指针 字符串 Sliding Window


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        map<char, int> hash;

        int ans = 0;
        int i, j;
        for (j = 0, i = 0; i < s.size();) {
            char c = s[i];
            if (hash.find(c) != hash.end()) {
                ans = max(ans, i - j);
                while (j < hash[c] + 1)
                    hash.erase(s[j++]);
            }
            hash[c] = i++;
        }
        return max(ans, i - j);
    }
};
//leetcode submit region end(Prohibit modification and deletion)
