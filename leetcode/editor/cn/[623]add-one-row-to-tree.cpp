//Given the root of a binary tree, then value v and depth d, you need to add a r
//ow of nodes with value v at the given depth d. The root node is at depth 1. 
//
// The adding rule is: given a positive integer depth d, for each NOT null tree 
//nodes N in depth d-1, create two tree nodes with value v as N's left subtree roo
//t and right subtree root. And N's original left subtree should be the left subtr
//ee of the new left subtree root, its original right subtree should be the right 
//subtree of the new right subtree root. If depth d is 1 that means there is no de
//pth d-1 at all, then create a tree node with value v as the new root of the whol
//e original tree, and the original tree is the new root's left subtree. 
//
// Example 1: 
// 
//Input: 
//A binary tree as following:
//       4
//     /   \
//    2     6
//   / \   / 
//  3   1 5   
//
//v = 1
//
//d = 2
//
//Output: 
//       4
//      / \
//     1   1
//    /     \
//   2       6
//  / \     / 
// 3   1   5   
//
// 
// 
//
//
// Example 2: 
// 
//Input: 
//A binary tree as following:
//      4
//     /   
//    2    
//   / \   
//  3   1    
//
//v = 1
//
//d = 3
//
//Output: 
//      4
//     /   
//    2
//   / \    
//  1   1
// /     \  
//3       1
// 
// 
//
// Note: 
// 
// The given d is in range [1, maximum depth of the given tree + 1]. 
// The given binary tree has at least one tree node. 
// 
// Related Topics 树


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    TreeNode *addOneRow(TreeNode *root, int v, int d) {
        TreeNode *virRoot = new TreeNode(-1);
        virRoot->left = root;
        queue <pair<TreeNode *, int >> Q;

        Q.push({virRoot, 1});
        while (!Q.empty() && Q.front().second < d) {
            auto p = Q.front().first;
            auto deep = Q.front().second;
            Q.pop();

            if (p->left) Q.push({p->left, deep + 1});
            if (p->right) Q.push({p->right, deep + 1});
        }

        while (!Q.empty()) {
            auto p = Q.front().first;
            Q.pop();
            TreeNode *a1 = new TreeNode(v);
            a1->left = p->left;
            p->left = a1;

            TreeNode *a2 = new TreeNode(v);
            a2->right = p->right;
            p->right = a2;
        }

        return virRoot->left;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
