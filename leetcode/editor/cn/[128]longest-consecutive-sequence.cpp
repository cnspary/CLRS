//Given an unsorted array of integers, find the length of the longest consecutiv
//e elements sequence. 
//
// Your algorithm should run in O(n) complexity. 
//
// Example: 
//
// 
//Input: [100, 4, 200, 1, 3, 2]
//Output: 4
//Explanation: The longest consecutive elements sequence is [1, 2, 3, 4]. Theref
//ore its length is 4.
// 
// Related Topics 并查集 数组


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
private:
    map<int, int> HashTable;

    vector<int> uf;
    vector<int> rank;

    int find(int x) {
        int root = x;
        while (uf[root] != root)
            root = uf[root];

        while (uf[x] != root) {
            int t = uf[x];
            uf[x] = root;
            x = t;
        }
        return root;
    }

    void unionn(int x, int y) {
        int r1 = find(x);
        int r2 = find(y);

        if (r1 == r2) return;

        if (rank[r1] > rank[r2]) {
            uf[r2] = r1;
            rank[r1] += rank[r2];
        } else {
            uf[r1] = r2;
            rank[r2] += rank[r1];
        }
        return;
    }

public:
    int longestConsecutive(vector<int> &nums) {
        uf.resize(nums.size());
        rank.resize(nums.size(), 1);

        for (int i = 0; i < uf.size(); ++i)
            uf[i] = i;

        for (int i = 0; i < nums.size(); ++i)
            HashTable[nums[i]] = i;

        for (int i = 0; i < nums.size(); ++i) {
            if (HashTable.find(nums[i] - 1) != HashTable.end())
                unionn(HashTable[nums[i]], HashTable[nums[i] - 1]);
            if (HashTable.find(nums[i] + 1) != HashTable.end())
                unionn(HashTable[nums[i]], HashTable[nums[i] + 1]);
        }

        int maxRank = 0;
        for (int i = 0; i < rank.size(); ++i)
            maxRank = max(maxRank, rank[i]);

        return maxRank;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
