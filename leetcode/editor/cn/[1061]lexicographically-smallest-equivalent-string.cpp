//Given strings A and B of the same length, we say A[i] and B[i] are equivalent 
//characters. For example, if A = "abc" and B = "cde", then we have 'a' == 'c', 'b
//' == 'd', 'c' == 'e'. 
//
// Equivalent characters follow the usual rules of any equivalence relation: 
//
// 
// Reflexivity: 'a' == 'a' 
// Symmetry: 'a' == 'b' implies 'b' == 'a' 
// Transitivity: 'a' == 'b' and 'b' == 'c' implies 'a' == 'c' 
// 
//
// For example, given the equivalency information from A and B above, S = "eed",
// "acd", and "aab" are equivalent strings, and "aab" is the lexicographically sma
//llest equivalent string of S. 
//
// Return the lexicographically smallest equivalent string of S by using the equ
//ivalency information from A and B. 
//
// 
//
// Example 1: 
//
// 
//Input: A = "parker", B = "morris", S = "parser"
//Output: "makkek"
//Explanation: Based on the equivalency information in A and B, we can group the
//ir characters as [m,p], [a,o], [k,r,s], [e,i]. The characters in each group are 
//equivalent and sorted in lexicographical order. So the answer is "makkek".
// 
//
// Example 2: 
//
// 
//Input: A = "hello", B = "world", S = "hold"
//Output: "hdld"
//Explanation:  Based on the equivalency information in A and B, we can group th
//eir characters as [h,w], [d,e,o], [l,r]. So only the second letter 'o' in S is c
//hanged to 'd', the answer is "hdld".
// 
//
// Example 3: 
//
// 
//Input: A = "leetcode", B = "programs", S = "sourcecode"
//Output: "aauaaaaada"
//Explanation:  We group the equivalent characters in A and B as [a,o,e,r,s,c], 
//[l,p], [g,t] and [d,m], thus all letters in S except 'u' and 'd' are transformed
// to 'a', the answer is "aauaaaaada".
// 
//
// 
//
// Note: 
//
// 
// String A, B and S consist of only lowercase English letters from 'a' - 'z'. 
// The lengths of string A, B and S are between 1 and 1000. 
// String A and B are of the same length. 
// Related Topics 深度优先搜索 并查集


//leetcode submit region begin(Prohibit modification and deletion)
class UnionFind {
private:
    vector<int> uf;

public:
    UnionFind(int n) {
        uf = vector<int>(n);
        for (int i = 0; i < n; ++i)
            uf[i] = i;
    }

    int find(int a) {
        int p = a;
        while (uf[a] != a) a = uf[a];
        int root = uf[a];
        while (uf[p] != root) {
            int x = uf[p];
            uf[p] = root;
            p = x;
        }
        return root;
    }

    bool uunion(int a, int b) {
        int r1 = find(a);
        int r2 = find(b);
        if (r1 == r2) return false;

        if (r1 < r2)
            uf[r2] = r1;
        else
            uf[r1] = r2;
        return true;
    }
};

class Solution {
public:
    string smallestEquivalentString(string A, string B, string S) {
        UnionFind uf(26);
        for (int i = 0; i < A.size(); ++i) {
            int a = A[i] - 'a';
            int b = B[i] - 'a';
            uf.uunion(a, b);
        }

        string ans = "";
        for (int i = 0; i < S.size(); ++i) {
            int x = uf.find(S[i] - 'a');
            ans += ('a' + x);
        }

        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
