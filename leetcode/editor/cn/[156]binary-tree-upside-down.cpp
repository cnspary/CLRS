//Given a binary tree where all the right nodes are either leaf nodes with a sib
//ling (a left node that shares the same parent node) or empty, flip it upside dow
//n and turn it into a tree where the original right nodes turned into left leaf n
//odes. Return the new root. 
//
// Example: 
//
// 
//Input: [1,2,3,4,5]
//
//    1
//   / \
//  2   3
// / \
//4   5
//
//Output: return the root of the binary tree [4,5,2,#,#,3,1]
//
//   4
//  / \
// 5   2
//    / \
//   3   1  
// 
//
// Clarification: 
//
// Confused what [4,5,2,#,#,3,1] means? Read more below on how binary tree is se
//rialized on OJ. 
//
// The serialization of a binary tree follows a level order traversal, where '#'
// signifies a path terminator where no node exists below. 
//
// Here's an example: 
//
// 
//   1
//  / \
// 2   3
//    /
//   4
//    \
//     5
// 
//
// The above binary tree is serialized as [1,2,3,#,#,4,#,#,5]. 
// Related Topics 树


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {

public:
    TreeNode *upsideDownBinaryTree(TreeNode *root) {
        if (!root) return root;

        TreeNode *NewRoot;
        stack < TreeNode * > S;
        while (root) {
            S.push(root);
            root = root->left;
        }

        NewRoot = S.top();
        while (!S.empty()) {
            root = S.top();
            S.pop();
            root->left = S.empty() ? nullptr : S.top()->right;
            root->right = S.empty() ? nullptr : S.top();
        }
        return NewRoot;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
