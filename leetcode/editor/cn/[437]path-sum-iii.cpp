//You are given a binary tree in which each node contains an integer value. 
//
// Find the number of paths that sum to a given value. 
//
// The path does not need to start or end at the root or a leaf, but it must go 
//downwards
//(traveling only from parent nodes to child nodes). 
//
// The tree has no more than 1,000 nodes and the values are in the range -1,000,
//000 to 1,000,000.
//
// Example:
// 
//root = [10,5,-3,3,2,null,11,3,-2,null,1], sum = 8
//
//      10
//     /  \
//    5   -3
//   / \    \
//  3   2   11
// / \   \
//3  -2   1
//
//Return 3. The paths that sum to 8 are:
//
//1.  5 -> 3
//2.  5 -> 2 -> 1
//3. -3 -> 11
// 
// Related Topics 树


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    map<int, int> S;
    int ans = 0;

    void DFS(TreeNode *root, int preSum, int sum) {
        if (!root) return;

        preSum += root->val;
        if (S.find(preSum - sum) != S.end())
            ans += S[preSum - sum];

        S[preSum]++;
        DFS(root->left, preSum, sum);
        DFS(root->right, preSum, sum);
        S[preSum]--;
        preSum -= root->val;
    }

public:
    int pathSum(TreeNode *root, int sum) {
        S[0] = 1;
        DFS(root, 0, sum);
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
