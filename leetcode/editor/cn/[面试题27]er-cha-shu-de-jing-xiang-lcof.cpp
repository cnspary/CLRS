//English description is not available for the problem. Please switch to Chinese
//. Related Topics 树


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    TreeNode* mirrorTree(TreeNode* root) {
        if(!root) return nullptr;

        swap(root->left, root->right);
        root->left = mirrorTree(root->left);
        root->right = mirrorTree(root->right);

        return root;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
