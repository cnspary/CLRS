//Given a binary tree root and a linked list with head as the first node. 
//
// Return True if all the elements in the linked list starting from the head cor
//respond to some downward path connected in the binary tree otherwise return Fals
//e. 
//
// In this context downward path means a path that starts at some node and goes 
//downwards. 
//
// 
// Example 1: 
//
// 
//
// 
//Input: head = [4,2,8], root = [1,4,4,null,2,2,null,1,null,6,8,null,null,null,n
//ull,1,3]
//Output: true
//Explanation: Nodes in blue form a subpath in the binary Tree.  
// 
//
// Example 2: 
//
// 
//
// 
//Input: head = [1,4,2,6], root = [1,4,4,null,2,2,null,1,null,6,8,null,null,null
//,null,1,3]
//Output: true
// 
//
// Example 3: 
//
// 
//Input: head = [1,4,2,6,8], root = [1,4,4,null,2,2,null,1,null,6,8,null,null,nu
//ll,null,1,3]
//Output: false
//Explanation: There is no path in the binary tree that contains all the element
//s of the linked list from head.
// 
//
// 
// Constraints: 
//
// 
// 1 <= node.val <= 100 for each node in the linked list and binary tree. 
// The given linked list will contain between 1 and 100 nodes. 
// The given binary tree will contain between 1 and 2500 nodes. 
// Related Topics 树 链表 动态规划


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    bool isOK(ListNode *head, TreeNode *root) {
        if (!head) return true;
        if (!root || root->val != head->val) return false;
        return isOK(head->next, root->left) || isOK(head->next, root->right);
    }

public:
    bool isSubPath(ListNode *head, TreeNode *root) {
        if (!root) return false;

        if (isOK(head, root)) return true;
        if (isSubPath(head, root->left)) return true;
        if (isSubPath(head, root->right)) return true;

        return false;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
