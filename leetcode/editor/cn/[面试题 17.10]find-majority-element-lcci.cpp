//A majority element is an element that makes up more than half of the items in 
//an array. Given a positive integers array, find the majority element. If there i
//s no majority element, return -1. Do this in O(N) time and O(1) space. 
//
// Example 1: 
//
// 
//Input: [1,2,5,9,5,9,5,5,5]
//Output: 5 
//
// 
//
// Example 2: 
//
// 
//Input: [3,2]
//Output: -1 
//
// 
//
// Example 3: 
//
// 
//Input: [2,2,1,1,1,2,2]
//Output: 2
// 
// Related Topics 位运算 数组 分治算法


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
public:
    int majorityElement(vector<int> &nums) {
        int mCnt = 1;
        int mElem = nums[0];
        for (int i = 1; i < nums.size(); ++i) {
            if (nums[i] != mElem) {
                mCnt--;

                if (mCnt < 0) {
                    mElem = nums[i];
                    mCnt = 1;
                }
            } else mCnt++;
        }
        return mElem;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
