//
//Given two non-empty binary trees s and t, check whether tree t has exactly the
// same structure and node values with a subtree of s. A subtree of s is a tree co
//nsists of a node in s and all of this node's descendants. The tree s could also 
//be considered as a subtree of itself.
// 
//
// Example 1: 
//
//Given tree s:
// 
//     3
//    / \
//   4   5
//  / \
// 1   2
// 
//Given tree t:
// 
//   4 
//  / \
// 1   2
// 
//Return true, because t has the same structure and node values with a subtree o
//f s.
// 
//
// Example 2: 
//
//Given tree s:
// 
//     3
//    / \
//   4   5
//  / \
// 1   2
//    /
//   0
// 
//Given tree t:
// 
//   4
//  / \
// 1   2
// 
//Return false.
// Related Topics 树


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    string serialize(TreeNode *root) {
        if (!root) return "#N";
        string stree = "#" + to_string(root->val);
        return stree + serialize(root->left) + serialize(root->right);
    }

    vector<int> buildNext(string s) {
        vector<int> next(s.size(), 0);
        next[0] = -1;

        int i = -1, j = 0;
        while (j < s.size() - 1) {
            if (i < 0 || s[i] == s[j]) {
                i++;
                j++;
                next[j] = (s[j] == s[i]) ? next[i] : i;
            } else
                i = next[i];
        }

        return next;
    }

    bool kmp(string s1, string s2) {
        vector<int> next = buildNext(s2);

        int l1 = s1.size(), l2 = s2.size();
        int i = 0, j = 0;

        while (i < l1 && j < l2) {

            if (j < 0 || s1[i] == s2[j]) {
                i++;
                j++;
            } else j = next[j];
        }

        if (j == l2) return true;
        else return false;
    }

public:
    bool isSubtree(TreeNode *s, TreeNode *t) {
        cout << serialize(s) << endl;
        cout << serialize(t) << endl;
        return kmp(serialize(s), serialize(t));
    }
};
//leetcode submit region end(Prohibit modification and deletion)
