//You are given a string representing an attendance record for a student. The re
//cord only contains the following three characters:
//
// 
// 
// 'A' : Absent. 
// 'L' : Late. 
// 'P' : Present. 
// 
// 
//
// 
//A student could be rewarded if his attendance record doesn't contain more than
// one 'A' (absent) or more than two continuous 'L' (late). 
//
// You need to return whether the student could be rewarded according to his att
//endance record. 
//
// Example 1: 
// 
//Input: "PPALLP"
//Output: True
// 
// 
//
// Example 2: 
// 
//Input: "PPALLL"
//Output: False
// 
// 
//
//
// Related Topics 字符串


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
public:
    bool checkRecord(string s) {
        return s.find('A', s.find('A') + 1) == -1 && s.find("LLL") == -1;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
