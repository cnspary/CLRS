//Given preorder and inorder traversal of a tree, construct the binary tree. 
//
// Note: 
//You may assume that duplicates do not exist in the tree. 
//
// For example, given 
//
// 
//preorder = [3,9,20,15,7]
//inorder = [9,3,15,20,7] 
//
// Return the following binary tree: 
//
// 
//    3
//   / \
//  9  20
//    /  \
//   15   7 
// Related Topics 树 深度优先搜索 数组


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    vector<int> pre;
    vector<int> in;
public:
    TreeNode *constructTree(int preL, int preR, int inL, int inR) {

        int root_index;
        for (root_index = inL; root_index <= inR; ++root_index)
            if (in[root_index] == pre[preL]) break;

        TreeNode *x = new TreeNode(pre[preL]);

        if (root_index - inL > 0)
            x->left = constructTree(preL + 1, preL + root_index - inL,
                                    inL, root_index - 1);
        if (inR - root_index > 0)
            x->right = constructTree(preL + root_index - inL + 1, preR,
                                     root_index + 1, inR);
        return x;
    }

    TreeNode *buildTree(vector<int> &preorder, vector<int> &inorder) {
        if (preorder.size() == 0 && inorder.size() == 0) return NULL;

        for (int i = 0; i < preorder.size(); ++i) {
            pre.push_back(preorder[i]);
            in.push_back(inorder[i]);
        }

        return constructTree(0, preorder.size() - 1, 0, inorder.size() - 1);
    }
};
//leetcode submit region end(Prohibit modification and deletion)
