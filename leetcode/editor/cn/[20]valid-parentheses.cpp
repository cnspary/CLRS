//Given a string containing just the characters '(', ')', '{', '}', '[' and ']',
// determine if the input string is valid. 
//
// An input string is valid if: 
//
// 
// Open brackets must be closed by the same type of brackets. 
// Open brackets must be closed in the correct order. 
// 
//
// Note that an empty string is also considered valid. 
//
// Example 1: 
//
// 
//Input: "()"
//Output: true
// 
//
// Example 2: 
//
// 
//Input: "()[]{}"
//Output: true
// 
//
// Example 3: 
//
// 
//Input: "(]"
//Output: false
// 
//
// Example 4: 
//
// 
//Input: "([)]"
//Output: false
// 
//
// Example 5: 
//
// 
//Input: "{[]}"
//Output: true
// 
// Related Topics 栈 字符串


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
public:
    bool isValid(string s) {
        stack<char> S;
        for (int i = 0; i < s.size(); ++i) {
            if (s[i] == '[' || s[i] == '{' || s[i] == '(')
                S.push(s[i]);
            else {
                if (S.empty() ||
                    (s[i] == ']' && S.top() != '[') ||
                    (s[i] == '}' && S.top() != '{') ||
                    (s[i] == ')' && S.top() != '('))
                    return false;
                S.pop();
            }
        }

        if (S.empty()) return true;
        else return false;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
