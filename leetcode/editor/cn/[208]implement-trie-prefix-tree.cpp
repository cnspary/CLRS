//Implement a trie with insert, search, and startsWith methods. 
//
// Example: 
//
// 
//Trie trie = new Trie();
//
//trie.insert("apple");
//trie.search("apple");   // returns true
//trie.search("app");     // returns false
//trie.startsWith("app"); // returns true
//trie.insert("app");   
//trie.search("app");     // returns true
// 
//
// Note: 
//
// 
// You may assume that all inputs are consist of lowercase letters a-z. 
// All inputs are guaranteed to be non-empty strings. 
// 
// Related Topics 设计 字典树


//leetcode submit region begin(Prohibit modification and deletion)
struct TrieNode {
    vector<TrieNode *> next;
    bool endPos;

    TrieNode() {
        next = vector<TrieNode *>(26, nullptr);
        endPos = false;
    }
};

class Trie {
private:
    TrieNode *root;
    TrieNode *_hot;
    int index;

    bool _search(string word, int &index) {
        TrieNode *snode = root;
        int i;
        for (i = 0; i < word.size(); ++i) {
            _hot = snode;
            snode = snode->next[word[i] - 'a'];
            if (snode == nullptr) break;
        }

        if (i != word.size()) {
            index = i;
            return false;
        } else {
            index = -1;
            _hot = snode;
            return snode->endPos;
        }
    }

public:
    /** Initialize your data structure here. */
    Trie() {
        root = new TrieNode();
        _hot = nullptr;
    }

    /** Inserts a word into the trie. */
    void insert(string word) {
        if (search(word)) return;

        for (; index != -1 && index < word.size(); ++index) {
            _hot->next[word[index] - 'a'] = new TrieNode();
            _hot = _hot->next[word[index] - 'a'];
        }

        _hot->endPos = true;
        return;
    }

    /** Returns if the word is in the trie. */
    bool search(string word) {
        return _search(word, index);
    }

    /** Returns if there is any word in the trie that starts with the given prefix. */
    bool startsWith(string prefix) {
        return search(prefix) || index == -1;
    }
};

/**
 * Your Trie object will be instantiated and called as such:
 * Trie* obj = new Trie();
 * obj->insert(word);
 * bool param_2 = obj->search(word);
 * bool param_3 = obj->startsWith(prefix);
 */
//leetcode submit region end(Prohibit modification and deletion)
