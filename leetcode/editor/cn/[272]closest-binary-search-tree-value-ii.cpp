//Given a non-empty binary search tree and a target value, find k values in the 
//BST that are closest to the target. 
//
// Note: 
//
// 
// Given target value is a floating point. 
// You may assume k is always valid, that is: k ≤ total nodes. 
// You are guaranteed to have only one unique set of k values in the BST that ar
//e closest to the target. 
// 
//
// Example: 
//
// 
//Input: root = [4,2,5,1,3], target = 3.714286, and k = 2
//
//    4
//   / \
//  2   5
// / \
//1   3
//
//Output: [4,3] 
//
// Follow up: 
//Assume that the BST is balanced, could you solve it in less than O(n) runtime 
//(where n = total nodes)? 
// Related Topics 栈 树


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

struct Node {
    int val;
    double diff;

    bool operator<(const Node a) const { return diff < a.diff; }

    Node(int v, double d) : val(v), diff(d) {}
};

class Solution {
private:
    double target;
    int k;

    priority_queue <Node> PQ;

    void dfs(TreeNode *p) {
        if (!p) return;
        dfs(p->left);

        if (PQ.size() < k)
            PQ.push(Node(p->val, abs(target - p->val)));
        else if (PQ.size() == k && PQ.top().diff > abs(target - p->val)) {
            PQ.pop();
            PQ.push(Node(p->val, abs(target - p->val)));
        } else return;

        dfs(p->right);
    }

public:
    vector<int> closestKValues(TreeNode *root, double target, int k) {
        this->target = target;
        this->k = k;
        dfs(root);
        vector<int> res;
        for (int i = 0; i < k && !PQ.empty(); ++i) {
            auto j = PQ.top();
            res.push_back(j.val);
            PQ.pop();
        }
        return res;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
