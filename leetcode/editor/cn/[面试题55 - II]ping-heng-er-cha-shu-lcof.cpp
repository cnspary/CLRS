//English description is not available for the problem. Please switch to Chinese
//. Related Topics 树 深度优先搜索


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    int BalanceFactor(TreeNode *x) {
        if (!x) return 0;

        int LH = BalanceFactor(x->left);
        if (LH < 0) return -1;

        int RH = BalanceFactor(x->right);
        if (RH < 0) return -1;

        if (abs(LH - RH) < 2)
            return max(LH, RH) + 1;
        else
            return -1;

    }
public:
    bool isBalanced(TreeNode* root) {
        return !(BalanceFactor(root) < 0);
    }
};
//leetcode submit region end(Prohibit modification and deletion)
