//Given a binary search tree with non-negative values, find the minimum absolute
// difference between values of any two nodes. 
//
// Example: 
//
// 
//Input:
//
//   1
//    \
//     3
//    /
//   2
//
//Output:
//1
//
//Explanation:
//The minimum absolute difference is 1, which is the difference between 2 and 1 
//(or between 2 and 3).
// 
//
// 
//
// Note: 
//
// 
// There are at least two nodes in this BST. 
// This question is the same as 783: https://leetcode.com/problems/minimum-dista
//nce-between-bst-nodes/ 
// 
// Related Topics 树


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    int pre = -1, ans = INT_MAX;

    void InOrder(TreeNode *root) {
        if (!root) return;

        InOrder(root->left);

        if (pre != -1) ans = min(ans, root->val - pre);
        pre = root->val;

        InOrder(root->right);
    }

public:
    int getMinimumDifference(TreeNode *root) {
        InOrder(root);
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
