//We have a grid of 1s and 0s; the 1s in a cell represent bricks. A brick will n
//ot drop if and only if it is directly connected to the top of the grid, or at le
//ast one of its (4-way) adjacent bricks will not drop. 
//
// We will do some erasures sequentially. Each time we want to do the erasure at
// the location (i, j), the brick (if it exists) on that location will disappear, 
//and then some other bricks may drop because of that erasure. 
//
// Return an array representing the number of bricks that will drop after each e
//rasure in sequence. 
//
// 
//Example 1:
//Input: 
//grid = [[1,0,0,0],[1,1,1,0]]
//hits = [[1,0]]
//Output: [2]
//Explanation: 
//If we erase the brick at (1, 0), the brick at (1, 1) and (1, 2) will drop. So 
//we should return 2. 
//
// 
//Example 2:
//Input: 
//grid = [[1,0,0,0],[1,1,0,0]]
//hits = [[1,1],[1,0]]
//Output: [0,0]
//Explanation: 
//When we erase the brick at (1, 0), the brick at (1, 1) has already disappeared
// due to the last move. So each erasure will cause no bricks dropping.  Note that
// the erased brick (1, 0) will not be counted as a dropped brick. 
//
// 
//
// Note: 
//
// 
// The number of rows and columns in the grid will be in the range [1, 200]. 
// The number of erasures will not exceed the area of the grid. 
// It is guaranteed that each erasure will be different from any other erasure, 
//and located inside the grid. 
// An erasure may refer to a location with no brick - if it does, no bricks drop
//. 
// 
// Related Topics 并查集


//leetcode submit region begin(Prohibit modification and deletion)
class UnionFind {
private:
    int cnt;
    vector<int> uf;
    vector<int> rank;

public:
    UnionFind(int n) {
        cnt = n;
        uf = vector<int>(n);
        rank = vector<int>(n, 1);
        for (int i = 0; i < n; ++i)
            uf[i] = i;
    }

    int find(int a) {
        int p = a;
        while (uf[a] != a) a = uf[a];
        int root = uf[a];
        while (uf[p] != root) {
            int x = uf[p];
            uf[p] = root;
            p = x;
        }
        return root;
    }

    bool uunion(int a, int b) {
        int r1 = find(a);
        int r2 = find(b);
        if (r1 == r2) return false;

        cnt--;
        if (rank[r1] < rank[r2]) {
            uf[r1] = r2;
            rank[r2] += rank[r1];
        } else {
            uf[r2] = r1;
            rank[r1] += rank[r2];
        }
        return true;
    }

    int getRank(int i) {
        return rank[find(i)];
    }

    int count() {
        return cnt;
    }

    bool connected(int x, int y) {
        return find(x) == find(y);
    }
};

class Solution {
public:
    vector <vector<int>> pos = {{1,  0},
                                {0,  1},
                                {-1, 0},
                                {0,  -1}};


    vector<int> hitBricks(vector <vector<int>> &grid, vector <vector<int>> &hits) {
        for (int i = 0; i < hits.size(); ++i) {
            int x = hits[i][0], y = hits[i][1];
            if (grid[x][y] == 1) grid[x][y] = 0;
            else grid[x][y] = -1;
        }

        int row = grid.size();
        int col = grid[0].size();
        int theTop = row * col;
        UnionFind uf(row * col + 1);

        for (int i = 0; i < row; ++i)
            for (int j = 0; j < col; ++j) {
                if (grid[i][j] == 1)
                    for (int k = 0; k < 4; ++k) {
                        int x = i + pos[k][0];
                        int y = j + pos[k][1];
                        if (-1 < x && x < row && -1 < y && y < col && grid[x][y] == 1)
                            uf.uunion(i * col + j, x * col + y);
                    }
            }

        for (int j = 0; j < col; ++j)
            if (grid[0][j] == 1)
                uf.uunion(j, theTop);

        vector<int> ans;
        for (int i = hits.size() - 1; -1 < i; --i) {
            int x = hits[i][0], y = hits[i][1];
            if (grid[x][y] == -1) {
                grid[x][y] = 0;
                ans.push_back(0);
            } else {
                grid[x][y] = 1;
                int oldRank = uf.getRank(theTop);
                if (x == 0) uf.uunion(x * col + y, theTop);
                for (int k = 0; k < 4; ++k) {
                    int xx = x + pos[k][0];
                    int yy = y + pos[k][1];
                    if (-1 < xx && xx < row && -1 < yy && yy < col && grid[xx][yy] == 1)
                        uf.uunion(xx * col + yy, x * col + y);
                }
                int newRank = uf.getRank(theTop);

                if (newRank == oldRank) ans.push_back(0);
                else ans.push_back(newRank - oldRank - 1);
            }
        }
        for (int i = 0; i < ans.size() / 2; ++i)
            swap(ans[i], ans[ans.size() - 1 - i]);
        return ans;
    }

};
//leetcode submit region end(Prohibit modification and deletion)
