//
//Given n pairs of parentheses, write a function to generate all combinations of
// well-formed parentheses.
// 
//
// 
//For example, given n = 3, a solution set is:
// 
// 
//[
//  "((()))",
//  "(()())",
//  "(())()",
//  "()(())",
//  "()()()"
//]
// Related Topics 字符串 回溯算法


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
private:
    vector<string> ans;

    void DFS(string s, int l, int r, int n) {
        if (l == n && r == n) {
            ans.push_back(s);
            return;
        }

        if (l < n) DFS(s + "(", l + 1, r, n);
        if (l > r && r < n) DFS(s + ")", l, r + 1, n);
    }

public:
    vector <string> generateParenthesis(int n) {
        DFS("", 0, 0, n);
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
