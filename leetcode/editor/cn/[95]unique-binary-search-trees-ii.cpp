//Given an integer n, generate all structurally unique BST's (binary search tree
//s) that store values 1 ... n. 
//
// Example: 
//
// 
//Input: 3
//Output:
//[
//  [1,null,3,2],
//  [3,2,null,1],
//  [3,1,null,null,2],
//  [2,1,3],
//  [1,null,2,null,3]
//]
//Explanation:
//The above output corresponds to the 5 unique BST's shown below:
//
//   1         3     3      2      1
//    \       /     /      / \      \
//     3     2     1      1   3      2
//    /     /       \                 \
//   2     1         2                 3
// 
// Related Topics 树 动态规划


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    TreeNode *copyTree(TreeNode *t) {
        if (t == nullptr) return nullptr;

        TreeNode *copyt = new TreeNode(t->val);
        copyt->left = copyTree(t->left);
        copyt->right = copyTree(t->right);
        return copyt;
    }

    vector<TreeNode *> generateTrees(int n) {
        if (n == 0) return {};

        queue <pair<int, TreeNode *>> Q;
        TreeNode *node, *pre, *tmp;
        Q.push({0, nullptr});
        while (true) {
            if (Q.front().first == n) break;

            int num = Q.front().first;
            pre = Q.front().second;
            Q.pop();

            // add x as root
            node = new TreeNode(num + 1);
            node->left = copyTree(pre);
            Q.push({num + 1, node});

            int l = 0;
            while (true) {
                TreeNode *root = copyTree(pre);
                tmp = root;
                for (int i = 0; tmp != nullptr && i < l; i++, tmp = tmp->right);
                ++l;
                if (tmp == nullptr)
                    break;

                node = new TreeNode(num + 1);
                node->left = tmp->right;
                tmp->right = node;
                Q.push({num + 1, root});
            }
        }
        vector < TreeNode * > ans;
        while (!Q.empty()) {
            ans.push_back(Q.front().second);
            Q.pop();
        }
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
