//Given a non-empty array of unique positive integers A, consider the following 
//graph: 
//
// 
// There are A.length nodes, labelled A[0] to A[A.length - 1]; 
// There is an edge between A[i] and A[j] if and only if A[i] and A[j] share a c
//ommon factor greater than 1. 
// 
//
// Return the size of the largest connected component in the graph. 
//
// 
//
// 
// 
//
// 
// Example 1: 
//
// 
//Input: [4,6,15,35]
//Output: 4
//
// 
//
// 
// Example 2: 
//
// 
//Input: [20,50,9,63]
//Output: 2
//
// 
//
// 
// Example 3: 
//
// 
//Input: [2,3,6,7,4,12,21,39]
//Output: 8
//
// 
//
// Note: 
//
// 
// 1 <= A.length <= 20000 
// 1 <= A[i] <= 100000 
// 
// 
// 
// 
// Related Topics 并查集 数学


//leetcode submit region begin(Prohibit modification and deletion)
class UnionFind {
private:
    int cnt;
    vector<int> uf;
    vector<int> rank;

public:
    UnionFind(int n) {
        cnt = n;
        uf = vector<int>(n);
        rank = vector<int>(n, 1);
        for (int i = 0; i < n; ++i)
            uf[i] = i;
    }

    int find(int a) {
        int p = a;
        while (uf[a] != a) a = uf[a];
        int root = uf[a];
        while (uf[p] != root) {
            int x = uf[p];
            uf[p] = root;
            p = x;
        }
        return root;
    }

    bool uunion(int a, int b) {
        int r1 = find(a);
        int r2 = find(b);
        if (r1 == r2) return false;

        cnt--;
        if (rank[r1] < rank[r2]) {
            uf[r1] = r2;
            rank[r2] += rank[r1];
        } else {
            uf[r2] = r1;
            rank[r1] += rank[r2];
        }
        return true;
    }

    int getBiggestRank() {
        int biggest = 0;
        for (int i = 0; i < rank.size(); ++i)
            biggest = max(biggest, rank[i]);
        return biggest;
    }

    int getRank(int i) {
        return rank[find(i)];
    }

    int count() {
        return cnt;
    }

    bool connected(int x, int y) {
        return find(x) == find(y);
    }
};

class Solution {
public:

    int largestComponentSize(vector<int> &A) {
        UnionFind uf(A.size());
        map<int, vector<int>> factors;
        for (int i = 0; i < A.size(); ++i) {
            int f = 2, d = A[i];
            while (f * f <= d) {
                if (d % f == 0) {
                    while (d % f == 0)
                        d /= f;
                    factors[f].push_back(i);
                }
                f++;
            }
            // d = A[i] is a prime
            if (d != 1)
                factors[d].push_back(i);
        }

        for (auto cfn : factors)
            for (int i = 1; i < cfn.second.size(); ++i)
                uf.uunion(cfn.second[i - 1], cfn.second[i]);

        return uf.getBiggestRank();
    }
};
//leetcode submit region end(Prohibit modification and deletion)
