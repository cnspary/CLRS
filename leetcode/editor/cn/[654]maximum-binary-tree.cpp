//
//Given an integer array with no duplicates. A maximum tree building on this arr
//ay is defined as follow:
// 
// The root is the maximum number in the array. 
// The left subtree is the maximum tree constructed from left part subarray divi
//ded by the maximum number. 
// The right subtree is the maximum tree constructed from right part subarray di
//vided by the maximum number. 
// 
// 
//
// 
//Construct the maximum tree by the given array and output the root node of this
// tree.
// 
//
// Example 1: 
// 
//Input: [3,2,1,6,0,5]
//Output: return the tree root node representing the following tree:
//
//      6
//    /   \
//   3     5
//    \    / 
//     2  0   
//       \
//        1
// 
// 
//
// Note: 
// 
// The size of the given array will be in the range [1,1000]. 
// 
// Related Topics 树


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    TreeNode *build(vector<int> &nums, int l, int r) {
        if (l > r) return nullptr;

        int maxIndex = l;
        for (int i = l; i <= r; i++)
            if (nums[i] > nums[maxIndex]) maxIndex = i;

        TreeNode *root = new TreeNode(nums[maxIndex]);
        root->left = build(nums, l, maxIndex - 1);
        root->right = build(nums, maxIndex + 1, r);
        return root;
    }

public:
    TreeNode *constructMaximumBinaryTree(vector<int> &nums) {
        return build(nums, 0, nums.size() - 1);
    }
};
//leetcode submit region end(Prohibit modification and deletion)
