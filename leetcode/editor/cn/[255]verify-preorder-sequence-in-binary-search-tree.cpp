//Given an array of numbers, verify whether it is the correct preorder traversal
// sequence of a binary search tree. 
//
// You may assume each number in the sequence is unique. 
//
// Consider the following binary search tree: 
//
// 
//     5
//    / \
//   2   6
//  / \
// 1   3 
//
// Example 1: 
//
// 
//Input: [5,2,6,1,3]
//Output: false 
//
// Example 2: 
//
// 
//Input: [5,2,1,3,6]
//Output: true 
//
// Follow up: 
//Could you do it using only constant space complexity? 
// Related Topics 栈 树


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
public:
    bool verifyPreorder(vector<int> &preorder) {
        stack<int> S;
        int min = INT_MIN;
        for (int e : preorder) {
            if (e < min) return false;
            while (!S.empty() && S.top() < e) {
                min = S.top();
                S.pop();
            }
            S.push(e);

        }
        return true;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
