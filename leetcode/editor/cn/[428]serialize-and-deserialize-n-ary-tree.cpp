//Serialization is the process of converting a data structure or object into a s
//equence of bits so that it can be stored in a file or memory buffer, or transmit
//ted across a network connection link to be reconstructed later in the same or an
//other computer environment. 
//
// Design an algorithm to serialize and deserialize an N-ary tree. An N-ary tree
// is a rooted tree in which each node has no more than N children. There is no re
//striction on how your serialization/deserialization algorithm should work. You j
//ust need to ensure that an N-ary tree can be serialized to a string and this str
//ing can be deserialized to the original tree structure. 
//
// For example, you may serialize the following 3-ary tree 
//
// 
//
// as [1 [3[5 6] 2 4]]. Note that this is just an example, you do not necessaril
//y need to follow this format. 
//
// Or you can follow LeetCode's level order traversal serialization format, wher
//e each group of children is separated by the null value. 
//
// 
//
// For example, the above tree may be serialized as [1,null,2,3,4,5,null,null,6,
//7,null,8,null,9,10,null,null,11,null,12,null,13,null,null,14]. 
//
// You do not necessarily need to follow the above suggested formats, there are 
//many more different formats that work so please be creative and come up with dif
//ferent approaches yourself. 
//
// 
// Constraints: 
//
// 
// The height of the n-ary tree is less than or equal to 1000 
// The total number of nodes is between [0, 10^4] 
// Do not use class member/global/static variables to store states. Your encode 
//and decode algorithms should be stateless. 
// 
// Related Topics 树


//leetcode submit region begin(Prohibit modification and deletion)
/*
// Definition for a Node.
class Node {
public:
    int val;
    vector<Node*> children;

    Node() {}

    Node(int _val) {
        val = _val;
    }

    Node(int _val, vector<Node*> _children) {
        val = _val;
        children = _children;
    }
};
*/

class Codec {
public:
    // Encodes a tree to a single string.
    string serialize(Node *root) {
        if (!root) return "";

        string treeStr = "";
        string preLevelStr = to_string(root->val) + " null ";

        int curLevel = -1;
        queue <pair<Node *, int>> Q;
        Q.push({root, 0});

        while (!Q.empty()) {

            Node *p = Q.front().first;
            int level = Q.front().second;
            Q.pop();

            if (level != curLevel) {
                treeStr += preLevelStr;
                preLevelStr = "";
                curLevel = level;
            }

            for (Node *c : p->children) {
                preLevelStr += to_string(c->val) + " ";
                Q.push({c, level + 1});
            }
            preLevelStr += "null ";
        }

        return treeStr;
    }

    // Decodes your encoded data to tree.
    Node *deserialize(string data) {
        if (data == "") return nullptr;
        vector <string> trees;
        for (int i = 0, j = 0; j < data.size(); ++j) {
            if (data[j] == ' ') {
                trees.push_back(data.substr(i, j - i));
                i = j + 1;
            }
        }

        queue < Node * > Q;
        Node *root = new Node(stoi(trees[0]));
        Q.push(root);
        for (int i = 1; i < trees.size();) {
            if (trees[i] == "null") {
                Node *p = Q.front();
                Q.pop();
                i++;
                while (i < trees.size() && trees[i] != "null") {
                    p->children.push_back(new Node(stoi(trees[i])));
                    Q.push(p->children.back());
                    i++;
                }
            }
        }
        return root;
    }
};

// Your Codec object will be instantiated and called as such:
// Codec codec;
// codec.deserialize(codec.serialize(root));
//leetcode submit region end(Prohibit modification and deletion)
