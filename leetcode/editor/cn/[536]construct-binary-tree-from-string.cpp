//You need to construct a binary tree from a string consisting of parenthesis an
//d integers. 
//
// The whole input represents a binary tree. It contains an integer followed by 
//zero, one or two pairs of parenthesis. The integer represents the root's value a
//nd a pair of parenthesis contains a child binary tree with the same structure. 
//
// You always start to construct the left child node of the parent first if it e
//xists. 
//
// Example: 
// 
//Input: "4(2(3)(1))(6(5))"
//Output: return the tree root node representing the following tree:
//
//       4
//     /   \
//    2     6
//   / \   / 
//  3   1 5   
// 
// 
//
// Note: 
// 
// There will only be '(', ')', '-' and '0' ~ '9' in the input string. 
// An empty tree is represented by "" instead of "()". 
// 
// Related Topics 树 字符串


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    string s;

    int getNum(int &l) {
        if (s[l] == '-') {
            l++;
            return 0 - getNum(l);
        } else {
            int num = 0;
            while ('0' <= s[l] && s[l] <= '9') {
                num = num * 10 + s[l] - '0';
                l++;
            }
            return num;
        }
    }

    TreeNode *buildTree(int l, int r) {
        if (l > r) return nullptr;

        TreeNode *p;
        p = new TreeNode(getNum(l));

        int ll, lr, rl, rr;

        ll = l;
        int i, count = 0;
        for (i = ll; i <= r; ++i) {
            if (s[i] == '(') count++;
            else if (s[i] == ')') count--;
            if (count == 0) break;
        }
        lr = i;

        rl = lr + 1;
        rr = r;

        p->left = buildTree(ll + 1, lr - 1);
        p->right = buildTree(rl + 1, rr - 1);

        return p;
    }

public:
    TreeNode *str2tree(string a) {
        if (a == "") return nullptr;
        s = a;
        return buildTree(0, s.size() - 1);
    }
};
//leetcode submit region end(Prohibit modification and deletion)
