//
//N couples sit in 2N seats arranged in a row and want to hold hands. We want to
// know the minimum number of swaps so that every couple is sitting side by side. 
//A swap consists of choosing any two people, then they stand up and switch seats.
// 
// 
//The people and seats are represented by an integer from 0 to 2N-1, the couples
// are numbered in order, the first couple being (0, 1), the second couple being (
//2, 3), and so on with the last couple being (2N-2, 2N-1).
// 
//The couples' initial seating is given by row[i] being the value of the person 
//who is initially sitting in the i-th seat.
//
// Example 1: 
//Input: row = [0, 2, 1, 3]
//Output: 1
//Explanation: We only need to swap the second (row[1]) and third (row[2]) perso
//n.
// 
//
// Example 2: 
//Input: row = [3, 2, 0, 1]
//Output: 0
//Explanation: All couples are already seated side by side.
// 
//
// 
//Note:
// 
// len(row) is even and in the range of [4, 60]. 
// row is guaranteed to be a permutation of 0...len(row)-1. 
// Related Topics 贪心算法 并查集 图


//leetcode submit region begin(Prohibit modification and deletion)
class UnionFind {
private:
    vector<int> uf;
    vector<int> rank;
    int component;

public:
    UnionFind(int n) {
        uf = vector<int>(n);
        rank = vector<int>(n, 1);
        component = n;
        for (int i = 0; i < n; ++i)
            uf[i] = i;
    }

    int find(int a) {
        int p = a;
        while (uf[a] != a) a = uf[a];
        int root = uf[a];
        while (uf[p] != root) {
            int x = uf[p];
            uf[p] = root;
            p = x;
        }
        return root;
    }

    bool uunion(int a, int b) {
        int r1 = find(a);
        int r2 = find(b);
        if (r1 == r2) return false;

        component--;
        if (rank[r1] < rank[r2]) {
            uf[r1] = r2;
            rank[r2] += rank[r1];
        } else {
            uf[r2] = r1;
            rank[r1] += rank[r2];
        }
        return true;
    }

    int count() {
        return component;
    }

    bool connected(int x, int y) {
        return find(x) == find(y);
    }
};

class Solution {
public:
    int minSwapsCouples(vector<int> &row) {
        UnionFind uf(row.size() / 2);

        for (int i = 0; i < row.size(); i += 2) {
            uf.uunion(row[i] / 2, row[i + 1] / 2);
        }

        return row.size() / 2 - uf.count();
    }
};
//leetcode submit region end(Prohibit modification and deletion)
