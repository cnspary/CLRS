//There are N cities numbered from 1 to N. 
//
// You are given connections, where each connections[i] = [city1, city2, cost] r
//epresents the cost to connect city1 and city2 together. (A connection is bidirec
//tional: connecting city1 and city2 is the same as connecting city2 and city1.) 
//
// Return the minimum cost so that for every pair of cities, there exists a path
// of connections (possibly of length 1) that connects those two cities together. 
//The cost is the sum of the connection costs used. If the task is impossible, ret
//urn -1. 
//
// 
//
// Example 1: 
//
// 
//
// 
//Input: N = 3, connections = [[1,2,5],[1,3,6],[2,3,1]]
//Output: 6
//Explanation: 
//Choosing any 2 edges will connect all cities so we choose the minimum 2.
// 
//
// Example 2: 
//
// 
//
// 
//Input: N = 4, connections = [[1,2,3],[3,4,4]]
//Output: -1
//Explanation: 
//There is no way to connect all cities even if all edges are used.
// 
//
// 
//
// Note: 
//
// 
// 1 <= N <= 10000 
// 1 <= connections.length <= 10000 
// 1 <= connections[i][0], connections[i][1] <= N 
// 0 <= connections[i][2] <= 10^5 
// connections[i][0] != connections[i][1] 
// 
// Related Topics 并查集 图


//leetcode submit region begin(Prohibit modification and deletion)
struct Edge {
    int tail;
    int head;
    int weight;

    Edge(int t, int h, int w) : tail(t), head(h), weight(w) {}
};

class UnionFind {
private:
    int cnt;
    vector<int> uf;
    vector<int> rank;

public:
    UnionFind(int n) {
        cnt = n;
        uf = vector<int>(n);
        rank = vector<int>(n, 1);
        for (int i = 0; i < n; ++i)
            uf[i] = i;
    }

    int find(int a) {
        int p = a;
        while (uf[a] != a) a = uf[a];
        int root = uf[a];
        while (uf[p] != root) {
            int x = uf[p];
            uf[p] = root;
            p = x;
        }
        return root;
    }

    bool uunion(int a, int b) {
        int r1 = find(a);
        int r2 = find(b);
        if (r1 == r2) return false;

        cnt--;
        if (rank[r1] < rank[r2]) {
            uf[r1] = r2;
            rank[r2] += rank[r1];
        } else {
            uf[r2] = r1;
            rank[r1] += rank[r2];
        }
        return true;
    }

    int count() {
        return cnt;
    }

    bool connected(int x, int y) {
        return find(x) == find(y);
    }
};

class Solution {
public:
    int minimumCost(int N, vector<vector<int>> &connections) {
        auto cmp = [](Edge A, Edge B) -> bool { return B.weight < A.weight; };
        priority_queue<Edge, vector<Edge>, decltype(cmp)> PQ(cmp);

        for (int i = 0; i < connections.size(); ++i)
            PQ.push(Edge(connections[i][0],
                         connections[i][1],
                         connections[i][2]));

        int cost = 0;
        UnionFind uf(N);

        while (!PQ.empty()) {
            auto x = PQ.top();
            PQ.pop();

            int p = x.head - 1;
            int q = x.tail - 1;

            if (!uf.connected(p, q)) {
                uf.uunion(p, q);
                cost += x.weight;
            }

            if (uf.count() == 1)
                return cost;
        }
        return -1;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
