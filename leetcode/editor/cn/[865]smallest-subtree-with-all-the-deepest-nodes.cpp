//Given a binary tree rooted at root, the depth of each node is the shortest dis
//tance to the root. 
//
// A node is deepest if it has the largest depth possible among any node in the 
//entire tree. 
//
// The subtree of a node is that node, plus the set of all descendants of that n
//ode. 
//
// Return the node with the largest depth such that it contains all the deepest 
//nodes in its subtree. 
//
// 
//
// Example 1: 
//
// 
//Input: [3,5,1,6,2,0,8,null,null,7,4]
//Output: [2,7,4]
//Explanation:
//
//
//
//We return the node with value 2, colored in yellow in the diagram.
//The nodes colored in blue are the deepest nodes of the tree.
//The input "[3, 5, 1, 6, 2, 0, 8, null, null, 7, 4]" is a serialization of the 
//given tree.
//The output "[2, 7, 4]" is a serialization of the subtree rooted at the node wi
//th value 2.
//Both the input and output have TreeNode type.
// 
//
// 
//
// Note: 
//
// 
// The number of nodes in the tree will be between 1 and 500. 
// The values of each node are unique. 
// 
// Related Topics 树


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    map<TreeNode *, int> Deepth;

    int getDeep(TreeNode *root) {
        if (!root) return 0;
        int d = max(getDeep(root->left), getDeep(root->right)) + 1;
        Deepth.insert({root, d});
        return d;
    }

public:
    TreeNode *subtreeWithAllDeepest(TreeNode *root) {
        getDeep(root);
        while (true) {
            int ld = Deepth[root->left];
            int rd = Deepth[root->right];

            if (ld == rd) return root;
            else if (ld < rd) root = root->right;
            else root = root->left;
        }
        return nullptr;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
