//Given an n-ary tree, return the level order traversal of its nodes' values. 
//
// Nary-Tree input serialization is represented in their level order traversal, 
//each group of children is separated by the null value (See examples). 
//
// 
// Example 1: 
//
// 
//
// 
//Input: root = [1,null,3,2,4,null,5,6]
//Output: [[1],[3,2,4],[5,6]]
// 
//
// Example 2: 
//
// 
//
// 
//Input: root = [1,null,2,3,4,5,null,null,6,7,null,8,null,9,10,null,null,11,null
//,12,null,13,null,null,14]
//Output: [[1],[2,3,4,5],[6,7,8,9,10],[11,12,13],[14]]
// 
//
// 
// Constraints: 
//
// 
// The height of the n-ary tree is less than or equal to 1000 
// The total number of nodes is between [0, 10^4] 
// 
// Related Topics 树 广度优先搜索


//leetcode submit region begin(Prohibit modification and deletion)
/*
// Definition for a Node.
class Node {
public:
    int val;
    vector<Node*> children;

    Node() {}

    Node(int _val) {
        val = _val;
    }

    Node(int _val, vector<Node*> _children) {
        val = _val;
        children = _children;
    }
};
*/

class Solution {
public:
    vector <vector<int>> levelOrder(Node *root) {
        if (!root) return {};

        vector <vector<int>> ans;
        vector<int> tmp;
        int level = -1;

        queue <pair<Node *, int>> Q;
        Q.push({root, 0});

        while (!Q.empty()) {
            Node *p = Q.front().first;
            int l = Q.front().second;
            Q.pop();

            if (level == -1 || l == level) {
                tmp.push_back(p->val);
            } else {
                ans.push_back(tmp);
                tmp.clear();
                tmp.push_back(p->val);
            }
            level = l;

            for (auto x : p->children)
                Q.push({x, l + 1});
        }
        ans.push_back(tmp);
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
