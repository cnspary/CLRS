//Given a binary tree, determine if it is a valid binary search tree (BST). 
//
// Assume a BST is defined as follows: 
//
// 
// The left subtree of a node contains only nodes with keys less than the node's
// key. 
// The right subtree of a node contains only nodes with keys greater than the no
//de's key. 
// Both the left and right subtrees must also be binary search trees. 
// 
//
// 
//
// Example 1: 
//
// 
//    2
//   / \
//  1   3
//
//Input: [2,1,3]
//Output: true
// 
//
// Example 2: 
//
// 
//    5
//   / \
//  1   4
//     / \
//    3   6
//
//Input: [5,1,4,null,null,3,6]
//Output: false
//Explanation: The root node's value is 5 but its right child's value is 4.
// 
// Related Topics 树 深度优先搜索


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    stack<TreeNode *> S;

    void goAlongLeftBranch(TreeNode *p) {
        while (p) {
            S.push(p);
            p = p->left;
        }
        return;
    }

public:
    bool isValidBST(TreeNode *root) {
        bool init = true;
        int preVal;
        TreeNode *p = root;
        while (true) {
            goAlongLeftBranch(p);
            if (S.empty()) break;
            p = S.top();
            S.pop();
            if (!init && p->val <= preVal) return false;
            else {
                preVal = p->val;
                init = false;
            }
            p = p->right;
        }
        return true;

    }
};
//leetcode submit region end(Prohibit modification and deletion)
