//Given a string which contains only lowercase letters, remove duplicate letters
// so that every letter appears once and only once. You must make sure your result
// is the smallest in lexicographical order among all possible results. 
//
// Example 1: 
//
// 
//Input: "bcabc"
//Output: "abc"
// 
//
// Example 2: 
//
// 
//Input: "cbacdcbc"
//Output: "acdb"
// 
//
// Note: This question is the same as 1081: https://leetcode.com/problems/smalle
//st-subsequence-of-distinct-characters/ 
// Related Topics 栈 贪心算法


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
public:
    string removeDuplicateLetters(string s) {
        vector<int> count(26, 0);
        vector<char> S;
        for (int i = 0; i < s.size(); ++i)
            count[s[i] - 'a']++;

        vector<bool> exist(26, false);
        for (int i = 0; i < s.size(); ++i) {
            count[s[i] - 'a']--;

            if (exist[s[i] - 'a']) continue;

            while (!S.empty() && S.back() > s[i] && count[S.back() - 'a'] > 0) {
                exist[S.back() - 'a'] = false;
                S.pop_back();
            }
            S.push_back(s[i]);
            exist[s[i] - 'a'] = true;
        }

        string ans = "";
        for (int i = 0; i < S.size(); ++i)
            ans += S[i];

        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
