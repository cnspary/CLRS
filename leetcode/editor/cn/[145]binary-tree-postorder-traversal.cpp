//Given a binary tree, return the postorder traversal of its nodes' values. 
//
// Example: 
//
// 
//Input: [1,null,2,3]
//   1
//    \
//     2
//    /
//   3
//
//Output: [3,2,1]
// 
//
// Follow up: Recursive solution is trivial, could you do it iteratively? 
// Related Topics 栈 树


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
#define isParent(p, x) (p->left == x || p->right == x)

class Solution {
private:
    vector<int> postarray;
    stack<TreeNode *> S;
public:
    void gotoHLVFL() {
        TreeNode *x;
        while (S.top()) {
            x = S.top();
            if (x->left) {
                if (x->right) S.push(x->right);
                S.push(x->left);
            } else
                S.push(x->right);
        }
        S.pop();
    }

    vector<int> postorderTraversal(TreeNode *root) {
        if (root) S.push(root);

        TreeNode *x = root;
        while (!S.empty()) {
            if (!(isParent(S.top(), x))) gotoHLVFL();
            x = S.top();
            S.pop();
            postarray.push_back(x->val);
        }

        return postarray;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
