//There are n computers numbered from 0 to n-1 connected by ethernet cables conn
//ections forming a network where connections[i] = [a, b] represents a connection 
//between computers a and b. Any computer can reach any other computer directly or
// indirectly through the network. 
//
// Given an initial computer network connections. You can extract certain cables
// between two directly connected computers, and place them between any pair of di
//sconnected computers to make them directly connected. Return the minimum number 
//of times you need to do this in order to make all the computers connected. If it
//'s not possible, return -1. 
//
// 
// Example 1: 
//
// 
//
// 
//Input: n = 4, connections = [[0,1],[0,2],[1,2]]
//Output: 1
//Explanation: Remove cable between computer 1 and 2 and place between computers
// 1 and 3.
// 
//
// Example 2: 
//
// 
//
// 
//Input: n = 6, connections = [[0,1],[0,2],[0,3],[1,2],[1,3]]
//Output: 2
// 
//
// Example 3: 
//
// 
//Input: n = 6, connections = [[0,1],[0,2],[0,3],[1,2]]
//Output: -1
//Explanation: There are not enough cables.
// 
//
// Example 4: 
//
// 
//Input: n = 5, connections = [[0,1],[0,2],[3,4],[2,3]]
//Output: 0
// 
//
// 
// Constraints: 
//
// 
// 1 <= n <= 10^5 
// 1 <= connections.length <= min(n*(n-1)/2, 10^5) 
// connections[i].length == 2 
// 0 <= connections[i][0], connections[i][1] < n 
// connections[i][0] != connections[i][1] 
// There are no repeated connections. 
// No two computers are connected by more than one cable. 
// Related Topics 深度优先搜索 广度优先搜索 并查集


//leetcode submit region begin(Prohibit modification and deletion)
class UnionFind {
private:
    vector<int> uf;
    vector<int> rank;
public:
    UnionFind(int n) {
        uf = vector<int>(n);
        rank = vector<int>(n, 1);
        for (int i = 0; i < n; ++i)
            uf[i] = i;
    }

    int find(int a) {
        int p = a;
        while (uf[a] != a) a = uf[a];
        int root = uf[a];
        while (uf[p] != root) {
            int x = uf[p];
            uf[p] = root;
            p = x;
        }
        rank[root]++;
        return root;
    }

    bool uunion(int a, int b) {
        int r1 = find(a);
        int r2 = find(b);
        if (r1 == r2) return false;

        if (rank[r1] < rank[r2]) {
            uf[r1] = r2;
            rank[r2] += rank[r1];
        } else {
            uf[r2] = r1;
            rank[r1] += rank[r2];
        }
        return true;
    }

    int count() {
        int cnt = 0;
        for (int i = 0; i < uf.size(); ++i)
            if (uf[i] == i) cnt++;
        return cnt;
    }
};


class Solution {
public:
    int makeConnected(int n, vector <vector<int>> &connections) {
        UnionFind uf(n);
        int slops = 0;
        for (int i = 0; i < connections.size(); ++i) {
            if (!uf.uunion(connections[i][0], connections[i][1]))
                slops++;
        }
        int cnt = uf.count();
        if (slops >= (cnt - 1))return cnt - 1;
        else return -1;

    }
};
//leetcode submit region end(Prohibit modification and deletion)
