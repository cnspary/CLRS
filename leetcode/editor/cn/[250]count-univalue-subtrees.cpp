//Given a binary tree, count the number of uni-value subtrees. 
//
// A Uni-value subtree means all nodes of the subtree have the same value. 
//
// Example : 
//
// 
//Input:  root = [5,1,5,5,5,null,5]
//
//              5
//             / \
//            1   5
//           / \   \
//          5   5   5
//
//Output: 4
// 
// Related Topics 树


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    int ans = 0;

    bool isUnivalSubtrees(TreeNode *root) {
        if (!root) return true;

        bool l, r;
        l = isUnivalSubtrees(root->left);
        r = isUnivalSubtrees(root->right);

        if (l && r) {
            if (root->left && root->right) {
                if (root->val == root->left->val && root->val == root->right->val) {
                    ans++;
                    return true;
                } else return false;
            }

            if (root->left) {
                if (root->val == root->left->val) {
                    ans++;
                    return true;
                } else return false;
            }

            if (root->right) {
                if (root->val == root->right->val) {
                    ans++;
                    return true;
                } else return false;
            }

            if (!root->left && !root->right) {
                ans++;
                return true;
            }
        }
        return false;

    }

public:
    int countUnivalSubtrees(TreeNode *root) {
        if (!root) return ans;
        isUnivalSubtrees(root);
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
