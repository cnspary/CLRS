//Given a binary tree, return all duplicate subtrees. For each kind of duplicate
// subtrees, you only need to return the root node of any one of them. 
//
// Two trees are duplicate if they have the same structure with same node values
//. 
//
// Example 1: 
//
// 
//        1
//       / \
//      2   3
//     /   / \
//    4   2   4
//       /
//      4
// 
//
// The following are two duplicate subtrees: 
//
// 
//      2
//     /
//    4
// 
//
// and 
//
// 
//    4
// 
//Therefore, you need to return above trees' root in the form of a list. Related
// Topics 树


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
    map<string, TreeNode *> ansMap;
    set <string> S;

    string solver(TreeNode *root) {
        if (!root) return "null";

        string lstr = solver(root->left);
        string rstr = solver(root->right);

        string id = to_string(root->val) + lstr + rstr;

        if (S.find(id) != S.end())
            ansMap[id] = root;
        else S.insert(id);

        return id;
    }

public:
    vector<TreeNode *> findDuplicateSubtrees(TreeNode *root) {
        solver(root);
        vector < TreeNode * > ans;
        for (auto p : ansMap)
            ans.push_back(p.second);
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
