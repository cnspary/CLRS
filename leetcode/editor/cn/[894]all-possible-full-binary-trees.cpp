//A full binary tree is a binary tree where each node has exactly 0 or 2 childre
//n.
//
// Return a list of all possible full binary trees with N nodes. Each element of
// the answer is the root node of one possible tree.
//
// Each node of each tree in the answer must have node.val = 0.
//
// You may return the final list of trees in any order.
//
//
//
// Example 1:
//
//
//Input: 7
//Output: [[0,0,0,null,null,0,0,null,null,0,0],[0,0,0,null,null,0,0,0,0],[0,0,0,
//0,0,0,0],[0,0,0,0,0,null,null,null,null,0,0],[0,0,0,0,0,null,null,0,0]]
//Explanation:
//
//
//
//
//
// Note:
//
//
// 1 <= N <= 20
//
// Related Topics 树 递归


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<TreeNode *> allPossibleFBT(int N) {
        if (N % 2 == 0) return {};
        if (N == 1) return {new TreeNode(0)};

        vector < TreeNode * > ans;
        for (int i = 1; i <= N - 2; i += 2) {
            vector < TreeNode * > LFBT = allPossibleFBT(i);
            vector < TreeNode * > RFBT = allPossibleFBT(N - 1 - i);

            for (int j = 0; j < LFBT.size(); ++j)
                for (int k = 0; k < RFBT.size(); ++k) {
                    TreeNode *root = new TreeNode(0);
                    root->left = LFBT[j];
                    root->right = RFBT[k];
                    ans.push_back(root);
                }
        }

        return ans;

    }
};
//leetcode submit region end(Prohibit modification and deletion)
