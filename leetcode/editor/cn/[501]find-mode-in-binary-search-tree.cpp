//Given a binary search tree (BST) with duplicates, find all the mode(s) (the mo
//st frequently occurred element) in the given BST. 
//
// Assume a BST is defined as follows: 
//
// 
// The left subtree of a node contains only nodes with keys less than or equal t
//o the node's key. 
// The right subtree of a node contains only nodes with keys greater than or equ
//al to the node's key. 
// Both the left and right subtrees must also be binary search trees. 
// 
//
// 
//
// For example: 
//Given BST [1,null,2,2], 
//
// 
//   1
//    \
//     2
//    /
//   2
// 
//
// 
//
// return [2]. 
//
// Note: If a tree has more than one mode, you can return them in any order. 
//
// Follow up: Could you do that without using any extra space? (Assume that the 
//implicit stack space incurred due to recursion does not count). 
// Related Topics 树


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    int MODE = 0;
    int curNum = -1, curCount = 0;
    vector<int> ans;

    void InOrder(TreeNode *root) {
        if (!root) return;
        InOrder(root->left);

        if (curNum == -1 || curNum == root->val) {
            curNum = root->val;
            curCount += 1;
        } else {
            if (curCount > MODE) {
                ans = vector<int>();
                ans.push_back(curNum);
                MODE = curCount;
            } else if (curCount == MODE)
                ans.push_back(curNum);

            curNum = root->val;
            curCount = 1;
        }

        InOrder(root->right);
    }

public:
    vector<int> findMode(TreeNode *root) {
        if (!root) return ans;
        InOrder(root);

        if (curCount > MODE) {
            ans = vector<int>();
            ans.push_back(curNum);
        } else if (curCount == MODE)
            ans.push_back(curNum);

        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
