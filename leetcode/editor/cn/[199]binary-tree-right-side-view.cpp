//Given a binary tree, imagine yourself standing on the right side of it, return
// the values of the nodes you can see ordered from top to bottom. 
//
// Example: 
//
// 
//Input: [1,2,3,null,5,null,4]
//Output: [1, 3, 4]
//Explanation:
//
//   1            <---
// /   \
//2     3         <---
// \     \
//  5     4       <---
// Related Topics 树 深度优先搜索 广度优先搜索


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    vector<int> ans;

    void BFS(TreeNode *x) {
        queue <pair<TreeNode *, int>> Q;
        Q.push({x, 0});
        while (!Q.empty()) {
            auto e = Q.front();
            Q.pop();

            TreeNode *p = e.first;
            int curDeep = e.second;

            if (Q.empty() || Q.front().second != curDeep)
                ans.push_back(p->val);

            if (p->left) Q.push({p->left, curDeep + 1});
            if (p->right) Q.push({p->right, curDeep + 1});
        }
        return;
    }

public:
    vector<int> rightSideView(TreeNode *root) {
        if (!root) return ans;
        BFS(root);
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
