//Print a binary tree in an m*n 2D string array following these rules: 
//
// 
// The row number m should be equal to the height of the given binary tree.
// The column number n should always be an odd number.
// The root node's value (in string format) should be put in the exactly middle
//of the first row it can be put. The column and the row where the root node belon
//gs will separate the rest space into two parts (left-bottom part and right-botto
//m part). You should print the left subtree in the left-bottom part and print the
// right subtree in the right-bottom part. The left-bottom part and the right-bott
//om part should have the same size. Even if one subtree is none while the other i
//s not, you don't need to print anything for the none subtree but still need to l
//eave the space as large as that for the other subtree. However, if two subtrees 
//are none, then you don't need to leave space for both of them. 
// Each unused space should contain an empty string "". 
// Print the subtrees following the same rules. 
// 
//
// Example 1: 
// 
//Input:
//     1
//    /
//   2
//Output:
//[["", "1", ""],
// ["2", "", ""]]
// 
// 
//
//
// Example 2: 
// 
//Input:
//     1
//    / \
//   2   3
//    \
//     4
//Output:
//[["", "", "", "1", "", "", ""],
// ["", "2", "", "", "", "3", ""],
// ["", "", "4", "", "", "", ""]]
// 
// 
//
// Example 3: 
// 
//Input:
//      1
//     / \
//    2   5
//   / 
//  3 
// / 
//4 
//Output:
//
//[["",  "",  "", "",  "", "", "", "1", "",  "",  "",  "",  "", "", ""]
// ["",  "",  "", "2", "", "", "", "",  "",  "",  "",  "5", "", "", ""]
// ["",  "3", "", "",  "", "", "", "",  "",  "",  "",  "",  "", "", ""]
// ["4", "",  "", "",  "", "", "", "",  "",  "",  "",  "",  "", "", ""]]
// 
// 
//
// Note:
//The height of binary tree is in the range of [1, 10].
// Related Topics 树


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

struct Node {
    TreeNode *t;
    int l, m, r, level;

    Node(TreeNode *t, int l, int m, int r, int level) {
        this->t = t;
        this->l = l;
        this->m = m;
        this->r = r;
        this->level = level;
    }
};

class Solution {
private:
    int height(TreeNode *root) {
        if (!root) return 0;
        return max(height(root->left), height(root->right)) + 1;
    }

public:
    vector <vector<string>> printTree(TreeNode *root) {
        if (!root) return {};
        int Height = height(root);
        int Width = pow(2, Height) - 1;
        vector <vector<string>> ans(Height, vector<string>(Width, ""));
        queue <Node> Q;
        Q.push({root, 0, Width / 2, Width - 1, 0});
        ans[0][Width / 2] = to_string(root->val);

        while (!Q.empty()) {
            Node node = Q.front();
            Q.pop();

            if (node.t->left) {
                ans[node.level + 1][(node.l + node.m - 1) / 2] = to_string(node.t->left->val);
                Q.push({node.t->left, node.l, (node.l + node.m - 1) / 2, node.m - 1, node.level + 1});
            }

            if (node.t->right) {
                ans[node.level + 1][(node.m + 1 + node.r) / 2] = to_string(node.t->right->val);
                Q.push({node.t->right, node.m + 1, (node.m + 1 + node.r) / 2, node.r, node.level + 1});
            }
        }

        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
