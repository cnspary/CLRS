//
//Given the root of a tree, you are asked to find the most frequent subtree sum.
// The subtree sum of a node is defined as the sum of all the node values formed b
//y the subtree rooted at that node (including the node itself). So what is the mo
//st frequent subtree sum value? If there is a tie, return all the values with the
// highest frequency in any order.
// 
//
// Examples 1 
//Input:
// 
//  5
// /  \
//2   -3
// 
//return [2, -3, 4], since all the values happen only once, return all of them i
//n any order.
// 
//
// Examples 2 
//Input:
// 
//  5
// /  \
//2   -5
// 
//return [2], since 2 happens twice, however -5 only occur once.
// 
//
// Note:
//You may assume the sum of values in any subtree is in the range of 32-bit sign
//ed integer.
// Related Topics 树 哈希表


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    map<int, int> hashTable;

    int postOrder(TreeNode *root) {
        if (!root) return 0;

        int l = postOrder(root->left);
        int r = postOrder(root->right);

        hashTable[l + r + root->val]++;

        return l + r + root->val;
    }

public:
    vector<int> findFrequentTreeSum(TreeNode *root) {
        postOrder(root);
        vector<int> ans;
        int preCount = 0;
        for (auto item : hashTable) {
            int sum = item.first;
            int times = item.second;

            if (times == preCount) ans.push_back(sum);
            else if (times > preCount) {
                ans.clear();
                ans.push_back(sum);
                preCount = times;
            }
        }
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
