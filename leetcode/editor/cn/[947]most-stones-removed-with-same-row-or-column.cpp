//On a 2D plane, we place stones at some integer coordinate points. Each coordin
//ate point may have at most one stone. 
//
// Now, a move consists of removing a stone that shares a column or row with ano
//ther stone on the grid. 
//
// What is the largest possible number of moves we can make? 
//
// 
//
// 
// Example 1: 
//
// 
//Input: stones = [[0,0],[0,1],[1,0],[1,2],[2,1],[2,2]]
//Output: 5
// 
//
// 
// Example 2: 
//
// 
//Input: stones = [[0,0],[0,2],[1,1],[2,0],[2,2]]
//Output: 3
// 
//
// 
// Example 3: 
//
// 
//Input: stones = [[0,0]]
//Output: 0
// 
//
// 
//
// Note: 
//
// 
// 1 <= stones.length <= 1000 
// 0 <= stones[i][j] < 10000 
// 
// 
// 
// 
// Related Topics 深度优先搜索 并查集


//leetcode submit region begin(Prohibit modification and deletion)
class UnionFind {
private:
    int cnt;
    vector<int> uf;
    vector<int> rank;

public:
    UnionFind(int n) {
        cnt = n;
        uf = vector<int>(n);
        rank = vector<int>(n, 1);
        for (int i = 0; i < n; ++i)
            uf[i] = i;
    }

    int find(int a) {
        int p = a;
        while (uf[a] != a) a = uf[a];
        int root = uf[a];
        while (uf[p] != root) {
            int x = uf[p];
            uf[p] = root;
            p = x;
        }
        return root;
    }

    bool uunion(int a, int b) {
        int r1 = find(a);
        int r2 = find(b);
        if (r1 == r2) return false;

        cnt--;
        if (rank[r1] < rank[r2]) {
            uf[r1] = r2;
            rank[r2] += rank[r1];
        } else {
            uf[r2] = r1;
            rank[r1] += rank[r2];
        }
        return true;
    }

    int count() {
        return cnt;
    }

    bool connected(int x, int y) {
        return find(x) == find(y);
    }
};

class Solution {
public:
    int removeStones(vector <vector<int>> &stones) {
        int N = stones.size();
        map<int, int> mpRow;
        map<int, int> mpCol;
        UnionFind uf(N);

        for (int i = 0; i < stones.size(); ++i) {
            if (mpRow.find(stones[i][0]) == mpRow.end())
                mpRow.insert({stones[i][0], i});
            else
                uf.uunion(i, mpRow[stones[i][0]]);

            if (mpCol.find(stones[i][1]) == mpCol.end())
                mpCol.insert({stones[i][1], i});
            else
                uf.uunion(i, mpCol[stones[i][1]]);
        }

        return stones.size() - uf.count();
    }
};
//leetcode submit region end(Prohibit modification and deletion)
