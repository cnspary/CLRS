//Given a binary tree, find the length of the longest consecutive sequence path.
// 
//
// The path refers to any sequence of nodes from some starting node to any node 
//in the tree along the parent-child connections. The longest consecutive path nee
//d to be from parent to child (cannot be the reverse). 
//
// Example 1: 
//
// 
//Input:
//
//   1
//    \
//     3
//    / \
//   2   4
//        \
//         5
//
//Output: 3
//
//Explanation: Longest consecutive sequence path is 3-4-5, so return 3. 
//
// Example 2: 
//
// 
//Input:
//
//   2
//    \
//     3
//    / 
//   2    
//  / 
// 1
//
//Output: 2 
//
//Explanation: Longest consecutive sequence path is 2-3, not 3-2-1, so return 2.
// Related Topics 树


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    int longestConsecutive(TreeNode *root) {
        if (!root) return 0;

        int ans = 1;
        queue <pair<TreeNode *, int>> Q;
        Q.push({root, 1});

        while (!Q.empty()) {
            root = Q.front().first;
            int l = Q.front().second;
            Q.pop();
            ans = max(ans, l);

            if (root->left) {
                if (root->left->val == root->val + 1)
                    Q.push({root->left, l + 1});
                else
                    Q.push({root->left, 1});
            }

            if (root->right) {
                if (root->right->val == root->val + 1)
                    Q.push({root->right, l + 1});
                else
                    Q.push({root->right, 1});
            }
        }

        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
