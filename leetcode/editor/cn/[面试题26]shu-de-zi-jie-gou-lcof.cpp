`//English description is not available for the problem. Please switch to Chinese
//. Related Topics 树


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    bool isSameTree(TreeNode *root, TreeNode *B) {
        if (!B) return true;
        if (!root) return false;
        if (B->val != root->val) return false;
        return isSameTree(root->left, B->left) && isSameTree(root->right, B->right);
    }

public:
    bool isSubStructure(TreeNode *A, TreeNode *B) {
        if(!A || !B) return false;
        return isSameTree(A, B) || isSubStructure(A->left, B) || isSubStructure(A->right, B);
    }
};
//leetcode submit region end(Prohibit modification and deletion)
`