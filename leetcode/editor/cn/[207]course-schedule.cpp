//There are a total of numCourses courses you have to take, labeled from 0 to nu
//mCourses-1.
//
// Some courses may have prerequisites, for example to take course 0 you have to
// first take course 1, which is expressed as a pair: [0,1]
//
// Given the total number of courses and a list of prerequisite pairs, is it pos
//sible for you to finish all courses?
//
//
// Example 1:
//
//
//Input: numCourses = 2, prerequisites = [[1,0]]
//Output: true
//Explanation: There are a total of 2 courses to take.
//             To take course 1 you should have finished course 0. So it is poss
//ible.
//
//
// Example 2:
//
//
//Input: numCourses = 2, prerequisites = [[1,0],[0,1]]
//Output: false
//Explanation: There are a total of 2 courses to take.
//             To take course 1 you should have finished course 0, and to take c
//ourse 0 you should
//             also have finished course 1. So it is impossible.
//
//
//
// Constraints:
//
//
// The input prerequisites is a graph represented by a list of edges, not adjace
//ncy matrices. Read more about how a graph is represented.
// You may assume that there are no duplicate edges in the input prerequisites.
//
// 1 <= numCourses <= 10^5
//
// Related Topics 深度优先搜索 广度优先搜索 图 拓扑排序


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
public:
    bool canFinish(int numCourses, vector <vector<int>> &prerequisites) {
        vector<int> Degree(numCourses, 0);
        vector <vector<int>> Graph(numCourses);
        for (int i = 0; i < prerequisites.size(); ++i) {
            int pre = prerequisites[i][1];
            int cur = prerequisites[i][0];
            Graph[pre].push_back(cur);
            Degree[cur]++;
        }

        queue<int> Q;
        for (int i = 0; i < numCourses; ++i)
            if (Degree[i] == 0) Q.push(i);

        int cnt = 0;
        while (!Q.empty()) {
            int x = Q.front();
            Q.pop();
            cnt++;

            for (int i = 0; i < Graph[x].size(); ++i) {
                Degree[Graph[x][i]]--;
                if (Degree[Graph[x][i]] == 0)
                    Q.push(Graph[x][i]);
            }
        }

        return cnt == numCourses;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
