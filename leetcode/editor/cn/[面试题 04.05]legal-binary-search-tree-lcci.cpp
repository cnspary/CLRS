//Implement a function to check if a binary tree is a binary search tree. 
//
// Example 1: 
//
// 
//Input:
//    2
//   / \
//  1   3
//Output: true
// 
//
// Example 2: 
//
// 
//Input:
//    5
//   / \
//  1   4
//     / \
//    3   6
//Output: false
//Explanation: Input: [5,1,4,null,null,3,6].
//     the value of root node is 5, but its right child has value 4. 
// Related Topics 树 深度优先搜索


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    bool init = true;
    int pre = INT_MIN;

    bool dfs(TreeNode *root) {
        if (!root) return true;

        if (!dfs(root->left)) return false;

        if (!init && pre >= root->val) return false;
        else {
            init = false;
            pre = root->val;
        }

        if (!dfs(root->right)) return false;

        return true;
    }

public:
    bool isValidBST(TreeNode *root) {
        return dfs(root);
    }
};
//leetcode submit region end(Prohibit modification and deletion)
