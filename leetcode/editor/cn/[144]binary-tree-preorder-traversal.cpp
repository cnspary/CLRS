//Given a binary tree, return the preorder traversal of its nodes' values. 
//
// Example: 
//
// 
//Input: [1,null,2,3]
//   1
//    \
//     2
//    /
//   3
//
//Output: [1,2,3]
// 
//
// Follow up: Recursive solution is trivial, could you do it iteratively? 
// Related Topics 栈 树


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    stack<TreeNode *> S;
    vector<int> ans;

    void goAlongLeftBranch(TreeNode *p) {
        while (p) {
            ans.push_back(p->val);
            S.push(p);
            p = p->left;
        }
        return;
    }

public:
    vector<int> preorderTraversal(TreeNode *root) {
        while (true) {
            goAlongLeftBranch(root);
            if (S.empty()) break;
            root = S.top();
            S.pop();
            root = root->right;
        }
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
