//Given two sentences words1, words2 (each represented as an array of strings), 
//and a list of similar word pairs pairs, determine if two sentences are similar. 
//
//
// For example, words1 = ["great", "acting", "skills"] and words2 = ["fine", "dr
//ama", "talent"] are similar, if the similar word pairs are pairs = [["great", "g
//ood"], ["fine", "good"], ["acting","drama"], ["skills","talent"]]. 
//
// Note that the similarity relation is transitive. For example, if "great" and 
//"good" are similar, and "fine" and "good" are similar, then "great" and "fine" a
//re similar. 
//
// Similarity is also symmetric. For example, "great" and "fine" being similar i
//s the same as "fine" and "great" being similar. 
//
// Also, a word is always similar with itself. For example, the sentences words1
// = ["great"], words2 = ["great"], pairs = [] are similar, even though there are 
//no specified similar word pairs. 
//
// Finally, sentences can only be similar if they have the same number of words.
// So a sentence like words1 = ["great"] can never be similar to words2 = ["double
//plus","good"]. 
//
// Note: 
//
// 
// The length of words1 and words2 will not exceed 1000. 
// The length of pairs will not exceed 2000. 
// The length of each pairs[i] will be 2. 
// The length of each words[i] and pairs[i][j] will be in the range [1, 20]. 
// 
//
// 
// Related Topics 深度优先搜索 并查集


//leetcode submit region begin(Prohibit modification and deletion)
class UnionFind {
private:
    int cnt;
    vector<int> uf;
    vector<int> rank;

public:
    UnionFind(int n) {
        cnt = n;
        uf = vector<int>(n);
        rank = vector<int>(n, 1);
        for (int i = 0; i < n; ++i)
            uf[i] = i;
    }

    int find(int a) {
        int p = a;
        while (uf[a] != a) a = uf[a];
        int root = uf[a];
        while (uf[p] != root) {
            int x = uf[p];
            uf[p] = root;
            p = x;
        }
        return root;
    }

    bool uunion(int a, int b) {
        int r1 = find(a);
        int r2 = find(b);
        if (r1 == r2) return false;

        cnt--;
        if (rank[r1] < rank[r2]) {
            uf[r1] = r2;
            rank[r2] += rank[r1];
        } else {
            uf[r2] = r1;
            rank[r1] += rank[r2];
        }
        return true;
    }

    int getRank(int i) {
        return rank[find(i)];
    }

    int count() {
        return cnt;
    }

    bool connected(int x, int y) {
        return find(x) == find(y);
    }
};
class Solution {
public:
    bool areSentencesSimilarTwo(vector<string> &words1, vector<string> &words2, vector<vector<string>> &pairs) {
        if (words1.size() != words2.size()) return false;
        map<string, int> HT;
        int cnt = 0;
        for (int i = 0; i < pairs.size(); ++i) {
            string a = pairs[i][0];
            string b = pairs[i][1];

            if (HT.find(a) == HT.end())
                HT.insert({a, cnt++});

            if (HT.find(b) == HT.end())
                HT.insert({b, cnt++});
        }

        UnionFind uf(HT.size());

        for (int i = 0; i < pairs.size(); ++i) {
            int a = HT[pairs[i][0]];
            int b = HT[pairs[i][1]];
            uf.uunion(a, b);
        }

        for (int i = 0; i < words1.size(); ++i) {
            string a = words1[i];
            string b = words2[i];
            if (a == b) continue;
            if (HT.find(a) != HT.end() && HT.find(b) != HT.end() && uf.connected(HT[a], HT[b]))
                continue;
            return false;
        }
        return true;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
