//Given a binary tree, return all root-to-leaf paths. 
//
// Note: A leaf is a node with no children. 
//
// Example: 
//
// 
//Input:
//
//   1
// /   \
//2     3
// \
//  5
//
//Output: ["1->2->5", "1->3"]
//
//Explanation: All root-to-leaf paths are: 1->2->5, 1->3
// Related Topics 树 深度优先搜索


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    vector <string> ans;

    void BFS(TreeNode *root) {
        queue <pair<TreeNode *, string >> Q;
        string s;
        Q.push({root, to_string(root->val)});
        while (!Q.empty()) {
            root = Q.front().first;
            s = Q.front().second;
            Q.pop();

            if (root->left)
                Q.push({root->left, s + "->" + to_string(root->left->val)});
            if (root->right)
                Q.push({root->right, s + "->" + to_string(root->right->val)});

            if (!root->left && !root->right)
                ans.push_back(s);
        }
    }

public:
    vector <string> binaryTreePaths(TreeNode *root) {
        if (!root) return {};
        BFS(root);
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
