//Given a root node reference of a BST and a key, delete the node with the given
// key in the BST. Return the root node reference (possibly updated) of the BST.
//
// Basically, the deletion can be divided into two stages:
//
// Search for a node to remove.
// If the node is found, delete the node.
//
//
//
// Note: Time complexity should be O(height of tree).
//
// Example:
//
//root = [5,3,6,2,4,null,7]
//key = 3
//
//    5
//   / \
//  3   6
// / \   \
//2   4   7
//
//Given key to delete is 3. So we find the node with value 3 and delete it.
//
//One valid answer is [5,4,6,2,null,null,7], shown in the following BST.
//
//    5
//   / \
//  4   6
// /     \
//2       7
//
//Another valid answer is [5,2,6,null,4,null,7].
//
//    5
//   / \
//  2   6
//   \   \
//    4   7
//
// Related Topics 树


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    TreeNode *virRoot;
    TreeNode *_hot, *e;

    void Search(TreeNode *root, int key) {
        _hot = virRoot;
        e = root;
        while (e) {
            if (e->val == key) break;

            _hot = e;
            if (key < e->val) e = e->left;
            else e = e->right;
        }
        return;
    }

    void succ(TreeNode *x) {
        _hot = x;
        e = x->right;
        while (e->left) {
            _hot = e;
            e = e->left;
        }
        return;
    }

    void remove(TreeNode *root) {
        if (!e) return;

        if (!e->left) _hot->left == e ? _hot->left = e->right : _hot->right = e->right;
        else if (!e->right) _hot->left == e ? _hot->left = e->left : _hot->right = e->left;
        else {
            TreeNode *x = e;
            succ(x);
            swap(x->val, e->val);
            remove(e);
        }
        return;
    }

public:
    TreeNode *deleteNode(TreeNode *root, int key) {
        virRoot = new TreeNode(0);
        virRoot->left = root;

        Search(root, key);
        remove(root);

        root = virRoot->left;
        return root;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
