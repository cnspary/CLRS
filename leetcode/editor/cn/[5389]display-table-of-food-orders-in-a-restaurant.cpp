//Given the array orders, which represents the orders that customers have done i
//n a restaurant. More specifically orders[i]=[customerNamei,tableNumberi,foodItem
//i] where customerNamei is the name of the customer, tableNumberi is the table cu
//stomer sit at, and foodItemi is the item customer orders. 
//
// Return the restaurant's “display table”. The “display table” is a table whose
// row entries denote how many of each food item each table ordered. The first col
//umn is the table number and the remaining columns correspond to each food item i
//n alphabetical order. The first row should be a header whose first column is “Ta
//ble”, followed by the names of the food items. Note that the customer names are 
//not part of the table. Additionally, the rows should be sorted in numerically in
//creasing order. 
//
// 
// Example 1: 
//
// 
//Input: orders = [["David","3","Ceviche"],["Corina","10","Beef Burrito"],["Davi
//d","3","Fried Chicken"],["Carla","5","Water"],["Carla","5","Ceviche"],["Rous","3
//","Ceviche"]]
//Output: [["Table","Beef Burrito","Ceviche","Fried Chicken","Water"],["3","0","
//2","1","0"],["5","0","1","0","1"],["10","1","0","0","0"]] 
//Explanation:
//The displaying table looks like:
//Table,Beef Burrito,Ceviche,Fried Chicken,Water
//3    ,0           ,2      ,1            ,0
//5    ,0           ,1      ,0            ,1
//10   ,1           ,0      ,0            ,0
//For the table 3: David orders "Ceviche" and "Fried Chicken", and Rous orders "
//Ceviche".
//For the table 5: Carla orders "Water" and "Ceviche".
//For the table 10: Corina orders "Beef Burrito". 
// 
//
// Example 2: 
//
// 
//Input: orders = [["James","12","Fried Chicken"],["Ratesh","12","Fried Chicken"
//],["Amadeus","12","Fried Chicken"],["Adam","1","Canadian Waffles"],["Brianna","1
//","Canadian Waffles"]]
//Output: [["Table","Canadian Waffles","Fried Chicken"],["1","2","0"],["12","0",
//"3"]] 
//Explanation: 
//For the table 1: Adam and Brianna order "Canadian Waffles".
//For the table 12: James, Ratesh and Amadeus order "Fried Chicken".
// 
//
// Example 3: 
//
// 
//Input: orders = [["Laura","2","Bean Burrito"],["Jhon","2","Beef Burrito"],["Me
//lissa","2","Soda"]]
//Output: [["Table","Bean Burrito","Beef Burrito","Soda"],["2","1","1","1"]]
// 
//
// 
// Constraints: 
//
// 
// 1 <= orders.length <= 5 * 10^4 
// orders[i].length == 3 
// 1 <= customerNamei.length, foodItemi.length <= 20 
// customerNamei and foodItemi consist of lowercase and uppercase English letter
//s and the space character. 
// tableNumberi is a valid integer between 1 and 500. 
// 
// Related Topics 哈希表


//leetcode submit region begin(Prohibit modification and deletion)
bool cmp(vector<string> a, vector<string> b) {
    return stoi(a[0]) < stoi(b[0]);
}

class Solution {
public:
    set<string> footItem;
    map<string, int> HT;
    map<int, vector<string>> Order;

    vector<vector<string>> displayTable(vector<vector<string>> &orders) {
        vector<vector<string>> ans;
        for (int i = 0; i < orders.size(); ++i) {
            Order[stoi(orders[i][1])].push_back(orders[i][2]);
            footItem.insert(orders[i][2]);
        }

        vector<string> Row1 = {"Table"};
        int cnt = 1;
        for (string x : footItem) {
            Row1.push_back(x);
            HT.insert({x, cnt});
            cnt++;
        }
        ans.push_back(Row1);

        for (auto x : Order) {
            vector<string> a(1 + footItem.size(), "0");
            a[0] = to_string(x.first);
            for (string item : x.second)
                a[HT[item]] = to_string(stoi(a[HT[item]]) + 1);
            ans.push_back(a);
        }

        sort(ans.begin() + 1, ans.end(), cmp);
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
