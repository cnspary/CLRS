//Equations are given in the format A / B = k, where A and B are variables repre
//sented as strings, and k is a real number (floating point number). Given some qu
//eries, return the answers. If the answer does not exist, return -1.0. 
//
// Example: 
//Given a / b = 2.0, b / c = 3.0. 
//queries are: a / c = ?, b / a = ?, a / e = ?, a / a = ?, x / x = ? . 
//return [6.0, 0.5, -1.0, 1.0, -1.0 ]. 
//
// The input is: vector<pair<string, string>> equations, vector<double>& values,
// vector<pair<string, string>> queries , where equations.size() == values.size(),
// and the values are positive. This represents the equations. Return vector<doubl
//e>. 
//
// According to the example above: 
//
// 
//equations = [ ["a", "b"], ["b", "c"] ],
//values = [2.0, 3.0],
//queries = [ ["a", "c"], ["b", "a"], ["a", "e"], ["a", "a"], ["x", "x"] ]. 
//
// 
//
// The input is always valid. You may assume that evaluating the queries will re
//sult in no division by zero and there is no contradiction. 
// Related Topics 并查集 图


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
private:
    vector<int> uf;
    vector<double> val;

    map<string, int> Index;

    int find(int a) {
        int root = a;
        while (uf[root] != root) root = uf[root];
        while (uf[a] != root) {
            int t = uf[a];
            uf[a] = root;
            a = t;
        }
        return root;
    }

    void unionn(int a, int b, double ratio) {

        int r1 = find(a);
        int r2 = find(b);
        if (r1 == r2) return;

        if (ratio >= 1.0) {
            double times = ratio * val[b] / val[a];
            for (int i = 0; i < uf.size(); ++i)
                if (find(i) == r1) val[i] *= times;
            uf[r1] = r2;
        } else {
            double times = val[a] / val[b] / ratio;
            for (int i = 0; i < uf.size(); ++i)
                if (find(i) == r2) val[i] *= times;
            uf[r2] = r1;
        }
    }

    bool connected(int a, int b) {
        return find(a) == find(b);
    }

public:
    vector<double>
    calcEquation(vector <vector<string>> &equations, vector<double> &values, vector <vector<string>> &queries) {
        int cnt = 0;
        for (int i = 0; i < equations.size(); ++i) {
            if (Index.find(equations[i][0]) == Index.end())
                Index.insert({equations[i][0], cnt++});
            if (Index.find(equations[i][1]) == Index.end())
                Index.insert({equations[i][1], cnt++});
        }

        uf.resize(cnt);
        val.resize(cnt, 1.0);
        for (int i = 0; i < uf.size(); ++i)
            uf[i] = i;

        for (int i = 0; i < equations.size(); ++i) {
            int a = Index[equations[i][0]];
            int b = Index[equations[i][1]];
            unionn(a, b, values[i]);
        }

        vector<double> ans;
        for (int i = 0; i < queries.size(); ++i) {
            if (Index.find(queries[i][0]) == Index.end()
                || Index.find(queries[i][1]) == Index.end())
                ans.push_back(-1.0);
            else {
                int a = Index[queries[i][0]];
                int b = Index[queries[i][1]];
                if (connected(a, b))
                    ans.push_back(val[a] / val[b]);
                else
                    ans.push_back(-1.0);
            }
        }
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
