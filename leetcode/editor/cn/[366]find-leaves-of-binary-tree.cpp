//Given a binary tree, collect a tree's nodes as if you were doing this: Collect
// and remove all leaves, repeat until the tree is empty. 
//
// 
//
// Example: 
//
// 
//Input: [1,2,3,4,5]
//  
//          1
//         / \
//        2   3
//       / \     
//      4   5    
//
//Output: [[4,5,3],[2],[1]]
// 
//
// 
//
// Explanation: 
//
// 1. Removing the leaves [4,5,3] would result in this tree: 
//
// 
//          1
//         / 
//        2          
// 
//
// 
//
// 2. Now removing the leaf [2] would result in this tree: 
//
// 
//          1          
// 
//
// 
//
// 3. Now removing the leaf [1] would result in the empty tree: 
//
// 
//          []         
// Related Topics 树 深度优先搜索


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    vector <vector<int>> ans;

    int dfs(TreeNode *root) {
        if (!root) return -1;

        int dl = dfs(root->left);
        int dr = dfs(root->right);

        int d = max(dl, dr) + 1;
        if (ans.size() <= d) ans.push_back({});
        ans[d].push_back(root->val);
        return d;
    }

public:
    vector <vector<int>> findLeaves(TreeNode *root) {
        dfs(root);
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
