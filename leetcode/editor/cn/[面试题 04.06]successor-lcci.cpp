//Write an algorithm to find the "next" node (i.e., in-order successor) of a giv
//en node in a binary search tree. 
//
// Return null if there's no "next" node for the given node. 
//
// Example 1: 
//
// 
//Input: root = [2,1,3], p = 1
//
//  2
// / \
//1   3
//
//Output: 2 
//
// Example 2: 
//
// 
//Input: root = [5,3,6,2,4,null,null,1], p = 6
//
//      5
//     / \
//    3   6
//   / \
//  2   4
// /   
//1
//
//Output: null 
// Related Topics 树 深度优先搜索


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    bool next = false;
    TreeNode *ans = nullptr;

    bool inorder(TreeNode *root, TreeNode *p) {
        if (!root) return false;
        if (inorder(root->left, p)) return true;
        if (next) {
            ans = root;
            return true;
        }
        if (root == p) next = true;
        if (inorder(root->right, p)) return true;
        return false;
    }

public:
    TreeNode *inorderSuccessor(TreeNode *root, TreeNode *p) {
        inorder(root, p);
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
