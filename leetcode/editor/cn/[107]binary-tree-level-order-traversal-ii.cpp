//Given a binary tree, return the bottom-up level order traversal of its nodes' 
//values. (ie, from left to right, level by level from leaf to root). 
//
// 
//For example: 
//Given binary tree [3,9,20,null,null,15,7], 
// 
//    3
//   / \
//  9  20
//    /  \
//   15   7
// 
// 
// 
//return its bottom-up level order traversal as: 
// 
//[
//  [15,7],
//  [9,20],
//  [3]
//]
// 
// Related Topics 树 广度优先搜索


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<vector<int>> levelOrderBottom(TreeNode *root) {
        vector<vector<int>> ans;
        if (!root) return ans;

        stack<vector<int>> S;
        queue<TreeNode *> Q1, Q2;

        Q1.push(root);
        vector<int> seq;

        while (!Q1.empty() || !Q2.empty()) {
            vector<int> seq1;
            if (!Q1.empty()) {
                while (!Q1.empty()) {
                    TreeNode *x = Q1.front();
                    Q1.pop();
                    seq1.push_back(x->val);
                    if (x->left) Q2.push(x->left);
                    if (x->right) Q2.push(x->right);
                }
                S.push(seq1);
            }
            if (!Q2.empty()) {
                vector<int> seq2;
                while (!Q2.empty()) {
                    TreeNode *x = Q2.front();
                    Q2.pop();
                    seq2.push_back(x->val);
                    if (x->left) Q1.push(x->left);
                    if (x->right) Q1.push(x->right);
                }
                S.push(seq2);
            }
        }

        while (!S.empty()) {
            ans.push_back(S.top());
            S.pop();
        }

        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
