//English description is not available for the problem. Please switch to Chinese
//. Related Topics 树


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    bool isSame(TreeNode *r1, TreeNode *r2) {
        if (!r1 && !r2) return true;
        if (!r1 || !r2) return false;
        if (r1->val != r2->val) return false;
        return isSame(r1->left, r2->right) && isSame(r1->right, r2->left);
    }

public:
    bool isSymmetric(TreeNode *root) {
        return isSame(root, root);
    }
};
//leetcode submit region end(Prohibit modification and deletion)
