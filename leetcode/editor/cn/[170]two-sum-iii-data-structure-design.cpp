//Design and implement a TwoSum class. It should support the following operation
//s: add and find. 
//
// add - Add the number to an internal data structure. 
//find - Find if there exists any pair of numbers which sum is equal to the valu
//e. 
//
// Example 1: 
//
// 
//add(1); add(3); add(5);
//find(4) -> true
//find(7) -> false
// 
//
// Example 2: 
//
// 
//add(3); add(1); add(2);
//find(3) -> true
//find(6) -> false 
// Related Topics 设计 哈希表


//leetcode submit region begin(Prohibit modification and deletion)
class TwoSum {
private:
    multiset<int> Collection;
public:
    /** Initialize your data structure here. */
    TwoSum() {

    }

    /** Add the number to an internal data structure.. */
    void add(int number) {
        Collection.insert(number);
    }

    /** Find if there exists any pair of numbers which sum is equal to the value. */
    bool find(int value) {
        multiset<int>::iterator Iter1, Iter2;
        for (Iter1 = Collection.begin(); Iter1 != Collection.end(); ++Iter1) {
            if (Collection.find(value - *Iter1) != Collection.end()) {
                Iter2 = Collection.find(value - *Iter1);
                if (Iter2 == Iter1) continue;
                else return true;
            }
        }
        return false;
    }
};

/**
 * Your TwoSum object will be instantiated and called as such:
 * TwoSum* obj = new TwoSum();
 * obj->add(number);
 * bool param_2 = obj->find(value);
 */
//leetcode submit region end(Prohibit modification and deletion)
