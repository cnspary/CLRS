//In an infinite binary tree where every node has two children, the nodes are la
//belled in row order. 
//
// In the odd numbered rows (ie., the first, third, fifth,...), the labelling is
// left to right, while in the even numbered rows (second, fourth, sixth,...), the
// labelling is right to left. 
//
// 
//
// Given the label of a node in this tree, return the labels in the path from th
//e root of the tree to the node with that label. 
//
// 
// Example 1: 
//
// 
//Input: label = 14
//Output: [1,3,4,14]
// 
//
// Example 2: 
//
// 
//Input: label = 26
//Output: [1,2,6,10,26]
// 
//
// 
// Constraints: 
//
// 
// 1 <= label <= 10^6 
// 
// Related Topics 树 数学


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
public:
    vector<int> pathInZigZagTree(int label) {
        vector<int> path;
        while (label != 0) {
            path.push_back(label);
            label /= 2;
        }

        reverse(path.begin(), path.end());
        for (int level = path.size() - 1; 0 < level; level -= 2)
            path[level - 1] = pow(2, level - 1) + pow(2, level) - 1 - path[level - 1];
        return path;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
