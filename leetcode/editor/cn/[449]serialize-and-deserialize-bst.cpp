//Serialization is the process of converting a data structure or object into a s
//equence of bits so that it can be stored in a file or memory buffer, or transmit
//ted across a network connection link to be reconstructed later in the same or an
//other computer environment. 
//
// Design an algorithm to serialize and deserialize a binary search tree. There 
//is no restriction on how your serialization/deserialization algorithm should wor
//k. You just need to ensure that a binary search tree can be serialized to a stri
//ng and this string can be deserialized to the original tree structure. 
//
// The encoded string should be as compact as possible. 
//
// Note: Do not use class member/global/static variables to store states. Your s
//erialize and deserialize algorithms should be stateless. 
// Related Topics 树


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Codec {
public:
    // Encodes a tree to a single string.
    string serialize(TreeNode *root) {
        string tree;
        vector <string> data;
        queue < TreeNode * > Q;
        Q.push(root);
        while (!Q.empty()) {
            auto e = Q.front();
            Q.pop();
            if (e == nullptr)
                data.push_back("null");
            else {
                data.push_back(to_string(e->val));
                Q.push(e->left);
                Q.push(e->right);
            }
        }
        int j = data.size() - 1;
        while (-1 < j && data[j] == "null") --j;
        tree = "[";
        for (int i = 0; i <= j; ++i)
            tree += data[i] + ",";
        tree.back() = ']';
        return tree;
    }

    // Decodes your encoded data to tree.
    TreeNode *deserialize(string data) {
        vector <string> array;

        data = data.substr(1, data.size() - 2);
        string item;
        stringstream ss;
        ss.str(data);
        while (getline(ss, item, ',')) {
            if (item == "null") array.push_back("null");
            else array.push_back(item);
        }

        if (array.size() < 1) return nullptr;

        vector < TreeNode * > TreeArray;

        for (size_t i = 0; i < array.size(); ++i) {
            if (array[i] != "null") TreeArray.push_back(new TreeNode(stoi(array[i])));
            else TreeArray.push_back(nullptr);
        }

        TreeNode *root = TreeArray[0];
        for (int r = 0, c = 1; c < TreeArray.size();) {
            if (TreeArray[r] != nullptr) {
                TreeArray[r]->left = TreeArray[c++];
                if (c < TreeArray.size())
                    TreeArray[r]->right = TreeArray[c++];
            }
            r++;
        }

        return root;
    }

};

// Your Codec object will be instantiated and called as such:
// Codec codec;
// codec.deserialize(codec.serialize(root));
//leetcode submit region end(Prohibit modification and deletion)
