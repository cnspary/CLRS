//You are given K eggs, and you have access to a building with N floors from 1 t
//o N. 
//
// Each egg is identical in function, and if an egg breaks, you cannot drop it a
//gain. 
//
// You know that there exists a floor F with 0 <= F <= N such that any egg dropp
//ed at a floor higher than F will break, and any egg dropped at or below floor F 
//will not break. 
//
// Each move, you may take an egg (if you have an unbroken one) and drop it from
// any floor X (with 1 <= X <= N). 
//
// Your goal is to know with certainty what the value of F is. 
//
// What is the minimum number of moves that you need to know with certainty what
// F is, regardless of the initial value of F? 
//
// 
//
// 
// 
//
// 
// Example 1: 
//
// 
//Input: K = 1, N = 2
//Output: 2
//Explanation: 
//Drop the egg from floor 1.  If it breaks, we know with certainty that F = 0.
//Otherwise, drop the egg from floor 2.  If it breaks, we know with certainty th
//at F = 1.
//If it didn't break, then we know with certainty F = 2.
//Hence, we needed 2 moves in the worst case to know what F is with certainty.
// 
//
// 
// Example 2: 
//
// 
//Input: K = 2, N = 6
//Output: 3
// 
//
// 
// Example 3: 
//
// 
//Input: K = 3, N = 14
//Output: 4
// 
//
// 
//
// Note: 
//
// 
// 1 <= K <= 100 
// 1 <= N <= 10000 
// 
// 
// 
// 
// Related Topics 数学 二分查找 动态规划


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
private:
    vector<int> DP = vector<int>(1000103, -1);
public:
    int superEggDrop(int K, int N) {
        if (N == 0) return 0;
        if (K == 1) return N;

        if (DP[N * 100 + K] != -1)
            return DP[N * 100 + K];

        int res = INT_MAX;
        int low = 1, high = N;
        while (low <= high) {
            int floor = low + (high - low) / 2;
            int broken = superEggDrop(K - 1, floor - 1);
            int not_broken = superEggDrop(K, N - floor);

            if (broken > not_broken) {
                high = floor - 1;
                res = min(res, broken + 1);
            } else {
                low = floor + 1;
                res = min(res, not_broken + 1);
            }
        }
        DP[N * 100 + K] = res;
        return DP[N * 100 + K];
    }
};
//leetcode submit region end(Prohibit modification and deletion)
