//Given a binary tree and a sum, find all root-to-leaf paths where each path's s
//um equals the given sum. 
//
// Note: A leaf is a node with no children. 
//
// Example: 
//
// Given the below binary tree and sum = 22, 
//
// 
//      5
//     / \
//    4   8
//   /   / \
//  11  13  4
// /  \    / \
//7    2  5   1
// 
//
// Return: 
//
// 
//[
//   [5,4,11,2],
//   [5,8,4,5]
//]
// 
// Related Topics 树 深度优先搜索


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    vector <vector<int>> ans;
    vector<int> S;

    void DFS(TreeNode *root, int pathSum, int sum) {
        S.push_back(root->val);

        if (root->left) DFS(root->left, pathSum + root->val, sum);
        if (root->right) DFS(root->right, pathSum + root->val, sum);

        if (!root->left && !root->right && pathSum + root->val == sum)
            ans.push_back(S);

        S.pop_back();

        return;
    }

public:
    vector <vector<int>> pathSum(TreeNode *root, int sum) {
        if (!root) return {};

        DFS(root, 0, sum);
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
