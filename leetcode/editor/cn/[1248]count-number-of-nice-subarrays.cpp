//Given an array of integers nums and an integer k. A subarray is called nice if
// there are k odd numbers on it. 
//
// Return the number of nice sub-arrays. 
//
// 
// Example 1: 
//
// 
//Input: nums = [1,1,2,1,1], k = 3
//Output: 2
//Explanation: The only sub-arrays with 3 odd numbers are [1,1,2,1] and [1,2,1,1
//].
// 
//
// Example 2: 
//
// 
//Input: nums = [2,4,6], k = 1
//Output: 0
//Explanation: There is no odd numbers in the array.
// 
//
// Example 3: 
//
// 
//Input: nums = [2,2,2,1,2,2,1,2,2,2], k = 2
//Output: 16
// 
//
// 
// Constraints: 
//
// 
// 1 <= nums.length <= 50000 
// 1 <= nums[i] <= 10^5 
// 1 <= k <= nums.length 
// 
// Related Topics 双指针


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
public:
    int numberOfSubarrays(vector<int> &nums, int k) {
        int N = nums.size();
        int ans = 0, cnt = 0;

        int lastOddPos = -1, nextOddPos = 0;
        int winL = -1, winR = 0;

        while (winR < N) {
            lastOddPos = winL;
            winL++;
            while (winL < N && nums[winL] % 2 == 0) winL++;
            while (winR < N) {
                if (nums[winR] % 2 == 1) cnt++;
                if (cnt == k) break;
                winR++;
            }
            if (cnt != k) break;
            int nextOddPos = winR + 1;
            while (nextOddPos < N && nums[nextOddPos] % 2 == 0) nextOddPos++;
            ans += (winL - lastOddPos) * (nextOddPos - winR);
            winR = nextOddPos;
            cnt--;
        }
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
