//Given a binary tree, write a function to get the maximum width of the given tr
//ee. The width of a tree is the maximum width among all levels. The binary tree h
//as the same structure as a full binary tree, but some nodes are null. 
//
// The width of one level is defined as the length between the end-nodes (the le
//ftmost and right most non-null nodes in the level, where the null nodes between 
//the end-nodes are also counted into the length calculation. 
//
// Example 1: 
//
// 
//Input: 
//
//           1
//         /   \
//        3     2
//       / \     \  
//      5   3     9 
//
//Output: 4
//Explanation: The maximum width existing in the third level with the length 4 (
//5,3,null,9).
// 
//
// Example 2: 
//
// 
//Input: 
//
//          1
//         /  
//        3    
//       / \       
//      5   3     
//
//Output: 2
//Explanation: The maximum width existing in the third level with the length 2 (
//5,3).
// 
//
// Example 3: 
//
// 
//Input: 
//
//          1
//         / \
//        3   2 
//       /        
//      5      
//
//Output: 2
//Explanation: The maximum width existing in the second level with the length 2 
//(3,2).
// 
//
// Example 4: 
//
// 
//Input: 
//
//          1
//         / \
//        3   2
//       /     \  
//      5       9 
//     /         \
//    6           7
//Output: 8
//Explanation:The maximum width existing in the fourth level with the length 8 (
//6,null,null,null,null,null,null,7).
//
//
// 
//
// Note: Answer will in the range of 32-bit signed integer. 
// Related Topics 树


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

struct Node {
    TreeNode *t;
    int level;
    unsigned long long index;

    Node(TreeNode *t, int l, unsigned long long i) {
        this->t = t;
        level = l;
        index = i;
    }
};

class Solution {
public:
    int widthOfBinaryTree(TreeNode *root) {
        if (!root) return 0;

        int  ans = 0;
        int preLevel = -1;
        unsigned long long leftMost = 0, rightMost = 0;
        queue <Node> Q;
        Q.push({root, 0, 0});

        while (!Q.empty()) {
            Node node = Q.front();
            Q.pop();

            if (preLevel != node.level) {
                ans = max(ans, int(rightMost - leftMost + 1));
                preLevel = node.level;
                leftMost = node.index;
                rightMost = node.index;
            } else rightMost = node.index;

            if (node.t->left) Q.push({node.t->left, node.level + 1, (node.index << 1)});
            if (node.t->right) Q.push({node.t->right, node.level + 1, ((node.index << 1) + 1)});
        }

        ans = max(ans, int(rightMost - leftMost + 1));
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
