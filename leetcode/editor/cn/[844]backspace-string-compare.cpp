//Given two strings S and T, return if they are equal when both are typed into e
//mpty text editors. # means a backspace character. 
//
// Note that after backspacing an empty text, the text will continue empty. 
//
// 
// Example 1: 
//
// 
//Input: S = "ab#c", T = "ad#c"
//Output: true
//Explanation: Both S and T become "ac".
// 
//
// 
// Example 2: 
//
// 
//Input: S = "ab##", T = "c#d#"
//Output: true
//Explanation: Both S and T become "".
// 
//
// 
// Example 3: 
//
// 
//Input: S = "a##c", T = "#a#c"
//Output: true
//Explanation: Both S and T become "c".
// 
//
// 
// Example 4: 
//
// 
//Input: S = "a#c", T = "b"
//Output: false
//Explanation: S becomes "c" while T becomes "b".
// 
//
// Note: 
//
// 
// 1 <= S.length <= 200 
// 1 <= T.length <= 200 
// S and T only contain lowercase letters and '#' characters. 
// 
//
// Follow up: 
//
// 
// Can you solve it in O(N) time and O(1) space? 
// 
// 
// 
// 
// 
// Related Topics 栈 双指针


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
public:
    bool backspaceCompare(string S, string T) {
        vector<char> S1, S2;
        for (int i = 0; i < S.size(); ++i) {
            if (S[i] == '#') {
                if (!S1.empty())
                    S1.pop_back();
            } else S1.push_back(S[i]);
        }

        for (int i = 0; i < T.size(); ++i) {
            if (T[i] == '#') {
                if (!S2.empty())
                    S2.pop_back();
            } else S2.push_back(T[i]);
        }

        return S1 == S2;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
