//Given two straight line segments (represented as a start point and an end poin
//t), compute the point of intersection, if any. If there's no intersection, retur
//n an empty array. 
//The absolute error should not exceed 10^-6. If there are more than one interse
//ctions, return the one with smallest X axis value. If there are more than one in
//tersections that have same X axis value, return the one with smallest Y axis val
//ue.
//
// Example 1: 
//
// 
//Input: 
//line1 = {0, 0}, {1, 0}
//line2 = {1, 1}, {0, -1}
//Output:  {0.5, 0}
// 
//
// Example 2: 
//
// 
//Input: 
//line1 = {0, 0}, {3, 3}
//line2 = {1, 1}, {2, 2}
//Output:  {1, 1}
// 
//
// Example 3: 
//
// 
//Input: 
//line1 = {0, 0}, {1, 1}
//line2 = {1, 0}, {2, 1}
//Output:  {}  (no intersection)
// 
//
// Note: 
//
// 
// The absolute value of coordinate value will not exceed 2^7. 
// All coordinates are valid 2D coordinates. 
// 
// Related Topics 几何 数学


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
public:
    long int area2(vector<int> p0, vector<int> p1, vector<int> p2) {
        return p0[0] * p1[1] - p0[1] * p1[0] +
               p1[0] * p2[1] - p2[0] * p1[1] +
               p2[0] * p0[1] - p0[0] * p2[1];
    }

    int Angle(vector<int> p0, vector<int> p1, vector<int> p2) {
        long int s = area2(p0, p1, p2);
        if (s > 0) return -1; // p1 < p2
        else if (s < 0) return 1; // p1 > p2
        else return 0; // p1 p2 collinear
    }

    bool between(vector<int> p0, vector<int> p1, vector<int> p2) {
        if (p0[0] == p1[0]) // vertical
            return (p2[1] - p0[1]) * (p1[1] - p2[1]) >= 0;
        else
            return (p2[0] - p0[0]) * (p1[0] - p2[0]) >= 0;
    }

    vector<double> intersection(vector<int> &start1, vector<int> &end1, vector<int> &start2, vector<int> &end2) {
        int a = Angle(start1, end1, start2);
        int b = Angle(end1, start1, end2);
        int c = Angle(start2, end2, start1);
        int d = Angle(end2, start2, end1);

        vector<vector<double>> ans;

        if (a == b && c == d && a != 0 && c != 0) {
            // intersect;
            double x1 = start1[0], y1 = start1[1];
            double x2 = end1[0], y2 = end1[1];
            double x3 = start2[0], y3 = start2[1];
            double x4 = end2[0], y4 = end2[1];

            double S = x1 * (y4 - y3) + x3 * (y1 - y4) + x4 * (y3 - y1);
            double D = x1 * (y4 - y3) + x2 * (y3 - y4) + x4 * (y2 - y1) + x3 * (y1 - y2);
            S = S / D;
            double x = x1 + S * (x2 - x1);
            double y = y1 + S * (y2 - y1);
            ans.push_back({x, y});
        } else if (a == b && c == d && a == 0 && c == 0) {
            // collinear;
            if (between(start1, end1, start2))
                ans.push_back({(double) start2[0], (double) start2[1]});
            if (between(start1, end1, end2))
                ans.push_back({(double) end2[0], (double) end2[1]});
            if (between(start2, end2, start1))
                ans.push_back({(double) start1[0], (double) start1[1]});
            if (between(start2, end2, end1))
                ans.push_back({(double) end1[0], (double) end1[1]});
        } else {
            if (a == 0) ans.push_back({(double) start2[0], (double) start2[1]});
            if (b == 0) ans.push_back({(double) end2[0], (double) end2[1]});
            if (c == 0) ans.push_back({(double) start1[0], (double) start1[1]});
            if (d == 0) ans.push_back({(double) end1[0], (double) end1[1]});
        };

        if (ans.size() == 0) return {};
        else if (ans.size() == 1) return ans[0];
        else {
            vector<double> inter = ans[0];
            for (int i = 1; i < ans.size(); ++i) {
                if ((ans[i][0] < inter[0]) || (ans[i][0] == inter[0] && ans[i][1] < inter[1]))
                    inter = ans[i];
            }
            return inter;
        }
    }
};
//leetcode submit region end(Prohibit modification and deletion)
