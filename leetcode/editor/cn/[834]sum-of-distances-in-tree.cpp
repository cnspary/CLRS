//An undirected, connected tree with N nodes labelled 0...N-1 and N-1 edges are
//given.
//
// The ith edge connects nodes edges[i][0] and edges[i][1] together.
//
// Return a list ans, where ans[i] is the sum of the distances between node i an
//d all other nodes.
//
// Example 1:
//
//
//Input: N = 6, edges = [[0,1],[0,2],[2,3],[2,4],[2,5]]
//Output: [8,12,6,10,10,10]
//Explanation:
//Here is a diagram of the given tree:
//  0
// / \
//1   2
//   /|\
//  3 4 5
//We can see that dist(0,1) + dist(0,2) + dist(0,3) + dist(0,4) + dist(0,5)
//equals 1 + 1 + 2 + 2 + 2 = 8.  Hence, answer[0] = 8, and so on.
//
//
// Note: 1 <= N <= 10000
// Related Topics 树 深度优先搜索


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
private:
    vector <vector<int>> Graph;
    vector<int> count;
    vector<int> subsum;

    void DFS(int node, int parent) {
        for (int i = 0; i < Graph[node].size(); ++i) {
            int child = Graph[node][i];
            if (child != parent) {
                DFS(child, node);
                subsum[node] += subsum[child] + count[child];
                count[node] += count[child];
            }
        }

        return;
    }

    void solver(int node, int parent, int N) {
        for (int i = 0; i < Graph[node].size(); ++i) {
            int child = Graph[node][i];
            if (child != parent) {
//                subsum[c] = subsum[c] + (subsum[root] - subsum[c] - count[c]) + (N - count[c]);
                subsum[child] = subsum[node] - 2 * count[child] + N;
                solver(child, node, N);
            }
        }
    }

public:
    vector<int> sumOfDistancesInTree(int N, vector <vector<int>> &edges) {
        Graph.resize(N);
        count.resize(N, 1);
        subsum.resize(N, 0);

        for (int i = 0; i < edges.size(); ++i) {
            int a = edges[i][0], b = edges[i][1];
            Graph[a].push_back(b);
            Graph[b].push_back(a);
        }

        DFS(0, -1);
        solver(0, -1, N);
        return subsum;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
