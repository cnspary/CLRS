//On an N x N grid, each square grid[i][j] represents the elevation at that poin
//t (i,j). 
//
// Now rain starts to fall. At time t, the depth of the water everywhere is t. Y
//ou can swim from a square to another 4-directionally adjacent square if and only
// if the elevation of both squares individually are at most t. You can swim infin
//ite distance in zero time. Of course, you must stay within the boundaries of the
// grid during your swim. 
//
// You start at the top left square (0, 0). What is the least time until you can
// reach the bottom right square (N-1, N-1)? 
//
// Example 1: 
//
// 
//Input: [[0,2],[1,3]]
//Output: 3
//Explanation:
//At time 0, you are in grid location (0, 0).
//You cannot go anywhere else because 4-directionally adjacent neighbors have a 
//higher elevation than t = 0.
//
//You cannot reach point (1, 1) until time 3.
//When the depth of water is 3, we can swim anywhere inside the grid.
// 
//
// Example 2: 
//
// 
//Input: [[0,1,2,3,4],[24,23,22,21,5],[12,13,14,15,16],[11,17,18,19,20],[10,9,8,
//7,6]]
//Output: 16
//Explanation:
// 0  1  2  3  4
//24 23 22 21  5
//12 13 14 15 16
//11 17 18 19 20
//10  9  8  7  6
//
//The final route is marked in bold.
//We need to wait until time 16 so that (0, 0) and (4, 4) are connected.
// 
//
// Note: 
//
// 
// 2 <= N <= 50. 
// grid[i][j] is a permutation of [0, ..., N*N - 1]. 
// 
// Related Topics 堆 深度优先搜索 并查集 二分查找


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
private:
    int N;
    vector <vector<int>> pos = {{1,  0},
                                {0,  1},
                                {-1, 0},
                                {0,  -1}};

    bool DFS(vector <vector<int>> &grid, int time, int row, int col) {
        if (row == N - 1 && col == N - 1)
            return true;

        grid[row][col] = -1;

        for (int i = 0; i < pos.size(); ++i) {
            int x = row + pos[i][0];
            int y = col + pos[i][1];

            if (-1 < x && x < N && -1 < y && y < N)
                if (0 <= grid[x][y] && grid[x][y] <= time && DFS(grid, time, x, y))
                    return true;
        }

        return false;
    }

public:
    int swimInWater(vector <vector<int>> &grid) {
        N = grid.size();

        int lo = max(grid[0][0], grid[N - 1][N - 1]), hi = N * N;
        while (lo < hi) {
            vector <vector<int>> copy(grid);
            int mid = lo + (hi - lo) / 2;
            if (DFS(copy, mid, 0, 0))
                hi = mid;
            else
                lo = mid + 1;
        }

        return lo;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
