//Given a sorted (increasing order) array with unique integer elements, write an
// algorithm to create a binary search tree with minimal height. 
//
// Example: 
//
// 
//Given sorted array: [-10,-3,0,5,9],
//
//One possible answer is: [0,-3,9,-10,null,5]，which represents the following tre
//e: 
//
//          0 
//         / \ 
//       -3   9 
//       /   / 
//     -10  5 
// 
// Related Topics 树 深度优先搜索


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    TreeNode *builTree(vector<int> &nums, int l, int r) {
        if (l > r) return nullptr;

        int mid = (l + r) >> 1;
        TreeNode *root = new TreeNode(nums[mid]);

        root->left = builTree(nums, l, mid - 1);
        root->right = builTree(nums, mid + 1, r);
        return root;
    }

public:
    TreeNode *sortedArrayToBST(vector<int> &nums) {
        return builTree(nums, 0, nums.size() - 1);
    }
};
//leetcode submit region end(Prohibit modification and deletion)
