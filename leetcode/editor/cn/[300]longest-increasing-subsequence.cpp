//Given an unsorted array of integers, find the length of longest increasing sub
//sequence. 
//
// Example: 
//
// 
//Input: [10,9,2,5,3,7,101,18]
//Output: 4 
//Explanation: The longest increasing subsequence is [2,3,7,101], therefore the 
//length is 4. 
//
// Note: 
//
// 
// There may be more than one LIS combination, it is only necessary for you to r
//eturn the length. 
// Your algorithm should run in O(n2) complexity. 
// 
//
// Follow up: Could you improve it to O(n log n) time complexity? 
// Related Topics 二分查找 动态规划


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
public:
    int lengthOfLIS(vector<int> &nums) {
        int ans = 0;
        vector<int> dp;

        for (int i = 0; i < nums.size(); ++i) {
            if (dp.empty() || dp.back() < nums[i])
                dp.push_back(nums[i]);
            else {
                int l = 0, r = dp.size();
                while (l < r) {
                    int m = l + (r - l) / 2;
                    if (dp[m] < nums[i]) l = m + 1;
                    else r = m;
                }
                dp[l] = nums[i];
            }
            ans = max(ans, int(dp.size()));
        }
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
