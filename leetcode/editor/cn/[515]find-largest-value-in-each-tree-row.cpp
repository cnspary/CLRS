//You need to find the largest value in each row of a binary tree. 
//
// Example: 
// 
//Input: 
//
//          1
//         / \
//        3   2
//       / \   \  
//      5   3   9 
//
//Output: [1, 3, 9]
// 
// 
// Related Topics 树 深度优先搜索 广度优先搜索


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    vector<int> ans;

    void BFS(TreeNode *root) {
        int curDeep = 0, curMax = INT_MIN;

        queue <pair<TreeNode *, int>> Q;
        Q.push({root, 0});

        while (!Q.empty()) {
            TreeNode *p = Q.front().first;
            int d = Q.front().second;
            Q.pop();

            if (curDeep != d) {
                ans.push_back(curMax);
                curDeep = d;
                curMax = p->val;
            } else curMax = max(curMax, p->val);

            if (p->left) Q.push({p->left, d + 1});
            if (p->right) Q.push({p->right, d + 1});
        }

        ans.push_back(curMax);
        return;
    }

public:
    vector<int> largestValues(TreeNode *root) {
        if(!root) return ans;
        BFS(root);
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
