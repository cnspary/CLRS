//You are given a string s, and an array of pairs of indices in the string pairs
// where pairs[i] = [a, b] indicates 2 indices(0-indexed) of the string. 
//
// You can swap the characters at any pair of indices in the given pairs any num
//ber of times. 
//
// Return the lexicographically smallest string that s can be changed to after u
//sing the swaps. 
//
// 
// Example 1: 
//
// 
//Input: s = "dcab", pairs = [[0,3],[1,2]]
//Output: "bacd"
//Explaination: 
//Swap s[0] and s[3], s = "bcad"
//Swap s[1] and s[2], s = "bacd"
// 
//
// Example 2: 
//
// 
//Input: s = "dcab", pairs = [[0,3],[1,2],[0,2]]
//Output: "abcd"
//Explaination: 
//Swap s[0] and s[3], s = "bcad"
//Swap s[0] and s[2], s = "acbd"
//Swap s[1] and s[2], s = "abcd" 
//
// Example 3: 
//
// 
//Input: s = "cba", pairs = [[0,1],[1,2]]
//Output: "abc"
//Explaination: 
//Swap s[0] and s[1], s = "bca"
//Swap s[1] and s[2], s = "bac"
//Swap s[0] and s[1], s = "abc"
// 
//
// 
// Constraints: 
//
// 
// 1 <= s.length <= 10^5 
// 0 <= pairs.length <= 10^5 
// 0 <= pairs[i][0], pairs[i][1] < s.length 
// s only contains lower case English letters. 
// 
// Related Topics 并查集 数组


//leetcode submit region begin(Prohibit modification and deletion)
class UnionFind {
private:
    int cnt;
    vector<int> uf;
    vector<int> rank;

public:
    UnionFind(int n) {
        cnt = n;
        uf = vector<int>(n);
        rank = vector<int>(n, 1);
        for (int i = 0; i < n; ++i)
            uf[i] = i;
    }

    int find(int a) {
        int p = a;
        while (uf[a] != a) a = uf[a];
        int root = uf[a];
        while (uf[p] != root) {
            int x = uf[p];
            uf[p] = root;
            p = x;
        }
        return root;
    }

    bool uunion(int a, int b) {
        int r1 = find(a);
        int r2 = find(b);
        if (r1 == r2) return false;

        cnt--;
        if (rank[r1] < rank[r2]) {
            uf[r1] = r2;
            rank[r2] += rank[r1];
        } else {
            uf[r2] = r1;
            rank[r1] += rank[r2];
        }
        return true;
    }

    int getRank(int i) {
        return rank[find(i)];
    }

    int count() {
        return cnt;
    }

    bool connected(int x, int y) {
        return find(x) == find(y);
    }
};

class Solution {
public:
    string smallestStringWithSwaps(string s, vector <vector<int>> &pairs) {
        UnionFind uf(s.size());
        for (int i = 0; i < pairs.size(); ++i)
            uf.uunion(pairs[i][0], pairs[i][1]);

        map<int, vector<char>> HT;
        for (int i = 0; i < s.size(); ++i)
            HT[uf.find(i)].push_back(s[i]);

        for (map < int, vector < char >> ::iterator Iter = HT.begin(); Iter != HT.end(); ++Iter)
            sort((*Iter).second.begin(), (*Iter).second.end(), greater<char>());
        string ans = "";
        for (int i = 0; i < s.size(); ++i) {
            ans += HT[uf.find(i)].back();
            HT[uf.find(i)].pop_back();
        }
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
