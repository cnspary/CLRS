//In a row of seats, 1 represents a person sitting in that seat, and 0 represent
//s that the seat is empty. 
//
// There is at least one empty seat, and at least one person sitting. 
//
// Alex wants to sit in the seat such that the distance between him and the clos
//est person to him is maximized. 
//
// Return that maximum distance to closest person. 
//
// 
// Example 1: 
//
// 
//Input: [1,0,0,0,1,0,1]
//Output: 2
//Explanation: 
//If Alex sits in the second open seat (seats[2]), then the closest person has d
//istance 2.
//If Alex sits in any other open seat, the closest person has distance 1.
//Thus, the maximum distance to the closest person is 2. 
//
// 
// Example 2: 
//
// 
//Input: [1,0,0,0]
//Output: 3
//Explanation: 
//If Alex sits in the last seat, the closest person is 3 seats away.
//This is the maximum distance possible, so the answer is 3.
// 
//
// Note: 
//
// 
// 1 <= seats.length <= 20000 
// seats contains only 0s or 1s, at least one 0, and at least one 1. 
// 
// 
// 
// Related Topics 数组


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
public:
    int maxDistToClosest(vector<int> &seats) {
        int maxGap = -1;
        int leftMargin = 0, rightMargin = 0;
        int i = 0, j = 1;
        while (j < seats.size()) {
            if (seats[j] == 1) {
                if (seats[i] != 1)
                    leftMargin = j - i;
                else
                    maxGap = max(maxGap, j - i - 1);
                i = j;
            }
            j++;
        }

        rightMargin = j - i - 1;
        return max((maxGap + 1) / 2, max(leftMargin, rightMargin));
    }
};
//leetcode submit region end(Prohibit modification and deletion)
