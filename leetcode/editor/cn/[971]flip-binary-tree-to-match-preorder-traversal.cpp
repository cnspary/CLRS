//Given a binary tree with N nodes, each node has a different value from {1, ...
//, N}. 
//
// A node in this binary tree can be flipped by swapping the left child and the 
//right child of that node. 
//
// Consider the sequence of N values reported by a preorder traversal starting f
//rom the root. Call such a sequence of N values the voyage of the tree. 
//
// (Recall that a preorder traversal of a node means we report the current node'
//s value, then preorder-traverse the left child, then preorder-traverse the right
// child.) 
//
// Our goal is to flip the least number of nodes in the tree so that the voyage 
//of the tree matches the voyage we are given. 
//
// If we can do so, then return a list of the values of all nodes flipped. You m
//ay return the answer in any order. 
//
// If we cannot do so, then return the list [-1]. 
//
// 
//
// 
// Example 1: 
//
// 
//
// 
//Input: root = [1,2], voyage = [2,1]
//Output: [-1]
// 
//
// 
// Example 2: 
//
// 
//
// 
//Input: root = [1,2,3], voyage = [1,3,2]
//Output: [1]
// 
//
// 
// Example 3: 
//
// 
//
// 
//Input: root = [1,2,3], voyage = [1,2,3]
//Output: []
// 
//
// 
//
// Note: 
//
// 
// 1 <= N <= 100 
// 
// 
// 
// 
// Related Topics 树 深度优先搜索


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    int index = 0;
    vector<int> ans;

    bool solver(TreeNode *root, vector<int> &voyage) {
        if (!root || index == voyage.size()) return true;

        if (root->val != voyage[index++]) {
            ans.clear();
            ans.push_back(-1);
            return false;
        }

        if (root->left && root->left->val != voyage[index]) {
            ans.push_back(root->val);
            if (!solver(root->right, voyage)) return false;
            if (!solver(root->left, voyage)) return false;
        } else {
            if (!solver(root->left, voyage)) return false;
            if (!solver(root->right, voyage)) return false;
        }

        return true;
    }

public:
    vector<int> flipMatchVoyage(TreeNode *root, vector<int> &voyage) {
        solver(root, voyage);
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
