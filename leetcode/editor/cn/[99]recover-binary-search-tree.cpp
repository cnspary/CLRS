//Two elements of a binary search tree (BST) are swapped by mistake. 
//
// Recover the tree without changing its structure. 
//
// Example 1: 
//
// 
//Input: [1,3,null,null,2]
//
//   1
//  /
// 3
//  \
//   2
//
//Output: [3,1,null,null,2]
//
//   3
//  /
// 1
//  \
//   2
// 
//
// Example 2: 
//
// 
//Input: [3,1,4,null,null,2]
//
//  3
// / \
//1   4
//   /
//  2
//
//Output: [2,1,4,null,null,3]
//
//  2
// / \
//1   4
//   /
//  3
// 
//
// Follow up: 
//
// 
// A solution using O(n) space is pretty straight forward. 
// Could you devise a constant space solution? 
// 
// Related Topics 树 深度优先搜索


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    void recoverTree(TreeNode *root) {
        TreeNode *x = nullptr, *y = nullptr;
        TreeNode *p = root, *pred = nullptr, *predecessor = nullptr;
        while (p) {
            if (p->left) {
                predecessor = p->left;
                while (predecessor->right && predecessor->right != p)
                    predecessor = predecessor->right;
                if (!predecessor->right) {
                    predecessor->right = p;
                    p = p->left;
                } else {
                    if (pred && pred->val > p->val) {
                        y = p;
                        if (!x) x = pred;
                    }

                    predecessor->right = nullptr;
                    pred = p;
                    p = p->right;
                }
            } else {
                if (pred && pred->val > p->val) {
                    y = p;
                    if (!x) x = pred;
                }
                pred = p;
                p = p->right;
            }
        }

        swap(x->val, y->val);
        return;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
