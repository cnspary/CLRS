//Implement FreqStack, a class which simulates the operation of a stack-like dat
//a structure. 
//
// FreqStack has two functions: 
//
// 
// push(int x), which pushes an integer x onto the stack. 
// pop(), which removes and returns the most frequent element in the stack.
// 
// If there is a tie for most frequent element, the element closest to the top o
//f the stack is removed and returned. 
// 
// 
// 
//
// 
//
// Example 1: 
//
// 
//Input: 
//["FreqStack","push","push","push","push","push","push","pop","pop","pop","pop"
//],
//[[],[5],[7],[5],[7],[4],[5],[],[],[],[]]
//Output: [null,null,null,null,null,null,null,5,7,5,4]
//Explanation:
//After making six .push operations, the stack is [5,7,5,7,4,5] from bottom to t
//op.  Then:
//
//pop() -> returns 5, as 5 is the most frequent.
//The stack becomes [5,7,5,7,4].
//
//pop() -> returns 7, as 5 and 7 is the most frequent, but 7 is closest to the t
//op.
//The stack becomes [5,7,5,4].
//
//pop() -> returns 5.
//The stack becomes [5,7,4].
//
//pop() -> returns 4.
//The stack becomes [5,7].
// 
//
// 
//
// Note: 
//
// 
// Calls to FreqStack.push(int x) will be such that 0 <= x <= 10^9. 
// It is guaranteed that FreqStack.pop() won't be called if the stack has zero e
//lements. 
// The total number of FreqStack.push calls will not exceed 10000 in a single te
//st case. 
// The total number of FreqStack.pop calls will not exceed 10000 in a single tes
//t case. 
// The total number of FreqStack.push and FreqStack.pop calls will not exceed 15
//0000 across all test cases. 
// 
//
// 
// 
// 
// Related Topics 栈 哈希表


//leetcode submit region begin(Prohibit modification and deletion)
class FreqStack {
private:
    unordered_map<int, int> Freq;
    unordered_map<int, stack<int>> Group;
    int maxFreq = 0;

public:
    FreqStack() {

    }

    void push(int x) {
        Freq[x]++;
        maxFreq = max(maxFreq, Freq[x]);
        Group[Freq[x]].push(x);
    }

    int pop() {
        int x = Group[maxFreq].top();
        Group[maxFreq].pop();
        Freq[x]--;
        if (Group[maxFreq].size() == 0) maxFreq--;
        return x;
    }
};

/**
 * Your FreqStack object will be instantiated and called as such:
 * FreqStack* obj = new FreqStack();
 * obj->push(x);
 * int param_2 = obj->pop();
 */
//leetcode submit region end(Prohibit modification and deletion)
