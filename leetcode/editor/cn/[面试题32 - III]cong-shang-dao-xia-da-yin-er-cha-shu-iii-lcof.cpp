//English description is not available for the problem. Please switch to Chinese
//. Related Topics 树 广度优先搜索


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector <vector<int>> levelOrder(TreeNode *root) {
        if (!root) return {};
        vector <vector<int>> ans;
        vector<int> num;
        queue <pair<TreeNode *, int>> Q;
        int curDeep = 0;
        Q.push({root, 0});
        while (!Q.empty()) {
            TreeNode *r = Q.front().first;
            int deep = Q.front().second;

            Q.pop();

            if (deep != curDeep) {
                if (deep % 2 == 0)
                    reverse(num.begin(), num.end());
                ans.push_back(num);
                curDeep = deep;
                num.clear();
            }
            num.push_back(r->val);

            if (r->left) Q.push({r->left, deep + 1});
            if (r->right) Q.push({r->right, deep + 1});
        }
        if(ans.size() % 2 == 1)
            reverse(num.begin(), num.end());
        ans.push_back(num);
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
