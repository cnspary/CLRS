//Given a binary tree, return the zigzag level order traversal of its nodes' val
//ues. (ie, from left to right, then right to left for the next level and alternat
//e between). 
//
// 
//For example: 
//Given binary tree [3,9,20,null,null,15,7], 
// 
//    3
//   / \
//  9  20
//    /  \
//   15   7
// 
// 
// 
//return its zigzag level order traversal as: 
// 
//[
//  [3],
//  [20,9],
//  [15,7]
//]
// 
// Related Topics 栈 树 广度优先搜索


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector <vector<int>> zigzagLevelOrder(TreeNode *root) {
        vector <vector<int>> ans;
        if (!root) return ans;

        stack < TreeNode * > S1, S2;

        S1.push(root);
        vector<int> seq;

        while (!S1.empty() || !S2.empty()) {
            vector<int> seq1;
            if (!S1.empty()) {
                while (!S1.empty()) {
                    TreeNode *x = S1.top();
                    S1.pop();
                    seq1.push_back(x->val);
                    if (x->left) S2.push(x->left);
                    if (x->right) S2.push(x->right);
                }
                ans.push_back(seq1);
            }
            if (!S2.empty()) {
                vector<int> seq2;
                while (!S2.empty()) {
                    TreeNode *x = S2.top();
                    S2.pop();
                    seq2.push_back(x->val);
                    if (x->right) S1.push(x->right);
                    if (x->left) S1.push(x->left);
                }
                ans.push_back(seq2);
            }
        }

        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
