//
//In this problem, a tree is an undirected graph that is connected and has no cy
//cles.
// 
//The given input is a graph that started as a tree with N nodes (with distinct 
//values 1, 2, ..., N), with one additional edge added. The added edge has two dif
//ferent vertices chosen from 1 to N, and was not an edge that already existed.
// 
//The resulting graph is given as a 2D-array of edges. Each element of edges is 
//a pair [u, v] with u < v, that represents an undirected edge connecting nodes u 
//and v.
// 
//Return an edge that can be removed so that the resulting graph is a tree of N 
//nodes. If there are multiple answers, return the answer that occurs last in the 
//given 2D-array. The answer edge [u, v] should be in the same format, with u < v.
//
// Example 1: 
// 
//Input: [[1,2], [1,3], [2,3]]
//Output: [2,3]
//Explanation: The given undirected graph will be like this:
//  1
// / \
//2 - 3
// 
// 
// Example 2: 
// 
//Input: [[1,2], [2,3], [3,4], [1,4], [1,5]]
//Output: [1,4]
//Explanation: The given undirected graph will be like this:
//5 - 1 - 2
//    |   |
//    4 - 3
// 
// 
// Note: 
// The size of the input 2D-array will be between 3 and 1000. 
// Every integer represented in the 2D-array will be between 1 and N, where N is
// the size of the input array. 
// 
//
// 
//
// 
//Update (2017-09-26): 
//We have overhauled the problem description + test cases and specified clearly 
//the graph is an undirected graph. For the directed graph follow up please see Re
//dundant Connection II). We apologize for any inconvenience caused.
// Related Topics 树 并查集 图


//leetcode submit region begin(Prohibit modification and deletion)

class UnionFind {
private:
    vector<int> uf;
    vector<int> rank;
public:
    UnionFind(int n) {
        uf.resize(n);
        rank.resize(n);
        for (int i = 0; i < n; ++i) {
            uf[i] = i;
            rank[i] = 1;
        }
    }

    int find(int a) {
        int r = a;
        while (uf[r] != r) r = uf[r];

        while (uf[a] != r) {
            int t = uf[a];
            uf[a] = r;
            a = t;
        }
        return r;
    }

    void uunion(int a, int b) {
        int r1 = find(a);
        int r2 = find(b);

        if (r1 == r2) return;

        if (rank[r1] > rank[r2]) {
            uf[r2] = r1;
            rank[r1] += rank[r2];
        } else {
            uf[r1] = r2;
            rank[r2] += rank[r1];
        }
    }

    bool connected(int a, int b) {
        return find(a) == find(b);
    }
};

class Solution {
public:
    vector<int> findRedundantConnection(vector <vector<int>> &edges) {
        UnionFind uf(edges.size() + 1);

        for (int i = 0; i < edges.size(); ++i) {
            if (!uf.connected(edges[i][0], edges[i][1]))
                uf.uunion(edges[i][0], edges[i][1]);
            else
                return edges[i];
        }

        return {};
    }
};
//leetcode submit region end(Prohibit modification and deletion)
