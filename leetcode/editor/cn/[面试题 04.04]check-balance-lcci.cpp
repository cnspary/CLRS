//Implement a function to check if a binary tree is balanced. For the purposes o
//f this question, a balanced tree is defined to be a tree such that the heights o
//f the two subtrees of any node never differ by more than one. 
//
// 
//Example 1: 
//
// 
//Given tree [3,9,20,null,null,15,7]
//    3
//   / \
//  9  20
//    /  \
//   15   7
//return true. 
//
// Example 2: 
//
// 
//Given [1,2,2,3,3,null,null,4,4]
//      1
//     / \
//    2   2
//   / \
//  3   3
// / \
//4   4
//return false. 
//
// 
// Related Topics 树 深度优先搜索


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    bool isBalanced(TreeNode *root, int &deep) {
        if (!root) {
            deep = 0;
            return true;
        }

        int ld, rd;
        if (!isBalanced(root->left, ld)) return false;
        if (!isBalanced(root->right, rd)) return false;
        if (abs(ld - rd) > 1) return false;

        deep = max(ld, rd) + 1;
        return true;
    }

public:
    bool isBalanced(TreeNode *root) {
        int deep = 0;
        return isBalanced(root, deep);
    }
};
//leetcode submit region end(Prohibit modification and deletion)
