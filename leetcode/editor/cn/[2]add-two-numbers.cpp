//You are given two non-empty linked lists representing two non-negative integer
//s. The digits are stored in reverse order and each of their nodes contain a sing
//le digit. Add the two numbers and return it as a linked list. 
//
// You may assume the two numbers do not contain any leading zero, except the nu
//mber 0 itself. 
//
// Example: 
//
// 
//Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
//Output: 7 -> 0 -> 8
//Explanation: 342 + 465 = 807.
// 
// Related Topics 链表 数学


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *addTwoNumbers(ListNode *l1, ListNode *l2) {
        ListNode *ans = new ListNode(-1);
        ListNode *tail = ans, *p = l1, *q = l2;

        int carryBit = 0;

        while (p != nullptr && q != nullptr) {
            int s = p->val + q->val + carryBit;
            carryBit = s / 10;
            tail->next = new ListNode(s % 10);
            tail = tail->next;
            p = p->next;
            q = q->next;
        }

        while (p != nullptr) {
            int s = p->val + carryBit;
            carryBit = s / 10;
            tail->next = new ListNode(s % 10);
            tail = tail->next;
            p = p->next;
        }

        while (q != nullptr) {
            int s = q->val + carryBit;
            carryBit = s / 10;
            tail->next = new ListNode(s % 10);
            tail = tail->next;
            q = q->next;
        }

        if (carryBit != 0)
            tail->next = new ListNode(carryBit);

        return ans->next;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
