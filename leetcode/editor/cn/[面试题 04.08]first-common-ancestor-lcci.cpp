//Design an algorithm and write code to find the first common ancestor of two no
//des in a binary tree. Avoid storing additional nodes in a data structure. NOTE: 
//This is not necessarily a binary search tree. 
//
// For example, Given the following tree: root = [3,5,1,6,2,0,8,null,null,7,4] 
//
// 
//    3
//   / \
//  5   1
// / \ / \
//6  2 0  8
//  / \
// 7   4
// 
//
// Example 1: 
//
// 
//Input: root = [3,5,1,6,2,0,8,null,null,7,4], p = 5, q = 1
//Input: 3
//Explanation: The first common ancestor of node 5 and node 1 is node 3. 
//
// Example 2: 
//
// 
//Input: root = [3,5,1,6,2,0,8,null,null,7,4], p = 5, q = 4
//Output: 5
//Explanation: The first common ancestor of node 5 and node 4 is node 5. 
//
// Notes: 
//
// 
// All node values are pairwise distinct. 
// p, q are different node and both can be found in the given tree. 
// 
// Related Topics 树


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    vector<TreeNode *> S1, S2;

    bool dfs(TreeNode *root, TreeNode *target, vector<TreeNode *> &S) {
        if (!root) return false;

        S.push_back(root);

        if (root == target) return true;

        if (dfs(root->left, target, S)) return true;
        if (dfs(root->right, target, S)) return true;

        S.pop_back();

        return false;
    }

public:
    TreeNode *lowestCommonAncestor(TreeNode *root, TreeNode *p, TreeNode *q) {
        dfs(root, p, S1);
        dfs(root, q, S2);
        int i;
        for (i = 0; i < S1.size() && i < S2.size(); ++i)
            if (S1[i] != S2[i]) break;
        return S1[i - 1];
    }
};
//leetcode submit region end(Prohibit modification and deletion)
