//Given a non-empty special binary tree consisting of nodes with the non-negativ
//e value, where each node in this tree has exactly two or zero sub-node. If the n
//ode has two sub-nodes, then this node's value is the smaller value among its two
// sub-nodes. More formally, the property root.val = min(root.left.val, root.right
//.val) always holds.
//
// Given such a binary tree, you need to output the second minimum value in the
//set made of all the nodes' value in the whole tree.
//
// If no such second minimum value exists, output -1 instead.
//
// Example 1:
//
//
//Input:
//    2
//   / \
//  2   5
//     / \
//    5   7
//
//Output: 5
//Explanation: The smallest value is 2, the second smallest value is 5.
//
//
//
//
// Example 2:
//
//
//Input:
//    2
//   / \
//  2   2
//
//Output: -1
//Explanation: The smallest value is 2, but there isn't any second smallest valu
//e.
//
//
//
// Related Topics 树


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    int Min, secMin;

    void travel(TreeNode *root) {
        if (!root) return;

        if (root->val != Min && (secMin == Min || secMin > root->val))
            secMin = root->val;
        travel(root->left);
        travel(root->right);
    }

public:
    int findSecondMinimumValue(TreeNode *root) {
        if (!root) return -1;
        Min = root->val;
        secMin = root->val;
        travel(root);

        if (secMin == Min) return -1;
        else return secMin;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
