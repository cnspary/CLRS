//Given n nodes labeled from 0 to n-1 and a list of undirected edges (each edge 
//is a pair of nodes), write a function to check whether these edges make up a val
//id tree. 
//
// Example 1: 
//
// 
//Input: n = 5, and edges = [[0,1], [0,2], [0,3], [1,4]]
//Output: true 
//
// Example 2: 
//
// 
//Input: n = 5, and edges = [[0,1], [1,2], [2,3], [1,3], [1,4]]
//Output: false 
//
// Note: you can assume that no duplicate edges will appear in edges. Since all 
//edges are undirected, [0,1] is the same as [1,0] and thus will not appear togeth
//er in edges. 
// Related Topics 深度优先搜索 广度优先搜索 并查集 图


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
private:
    vector<int> uf;
    vector<int> rank;
    int component;

    int find(int x) {
        int root = x;
        while (uf[root] != root)
            root = uf[root];

        while (uf[x] != root) {
            int t = uf[x];
            uf[x] = root;
            x = t;
        }
        return root;
    }

    void unionn(int x, int y) {
        int r1 = find(x);
        int r2 = find(y);

        if (r1 == r2) return;

        component--;
        if (rank[r1] > rank[r2]) {
            uf[r2] = r1;
            rank[r1] += rank[r2];
        } else {
            uf[r1] = r2;
            rank[r2] += rank[r1];
        }
        return;
    }

    bool connected(int x, int y) {
        return find(x) == find(y);
    }

public:
    bool validTree(int n, vector <vector<int>> &edges) {
        if (n <= edges.size()) return false;

        uf.resize(n);
        rank.resize(n, 1);
        component = n;

        for (int i = 0; i < uf.size(); ++i)
            uf[i] = i;

        for (int i = 0; i < edges.size(); ++i) {
            int a = edges[i][0];
            int b = edges[i][1];

            if (connected(a, b)) return false;

            unionn(a, b);
        }
        return component == 1;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
