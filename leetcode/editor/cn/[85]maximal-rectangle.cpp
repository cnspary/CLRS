//Given a 2D binary matrix filled with 0's and 1's, find the largest rectangle c
//ontaining only 1's and return its area.
//
// Example:
//
//
//Input:
//[
//  ["1","0","1","0","0"],
//  ["1","0","1","1","1"],
//  ["1","1","1","1","1"],
//  ["1","0","0","1","0"]
//]
//Output: 6
//
// Related Topics 栈 数组 哈希表 动态规划


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
private:
    vector<int> heights;
    vector<int> leftBoundary;
    vector<int> rightBoundary;
    int maxArea = 0;
public:
    int maximalRectangle(vector <vector<char>> &matrix) {
        if (matrix.size() == 0) return 0;

        int M = matrix.size(), N = matrix[0].size();

        heights.resize(N, 0);
        leftBoundary.resize(N, 0);
        rightBoundary.resize(N, N);

        for (int row = 0; row < M; ++row) {
            for (int col = 0; col < N; ++col) {
                if (matrix[row][col] == '1') heights[col] += 1;
                else heights[col] = 0;
            }

            int left = 0;
            for (int col = 0; col < N; ++col) {
                if (matrix[row][col] == '1')
                    leftBoundary[col] = max(leftBoundary[col], left);
                else {
                    leftBoundary[col] = 0;
                    left = col + 1;
                }
            }

            int right = N;
            for (int col = N - 1; -1 < col; --col) {
                if (matrix[row][col] == '1')
                    rightBoundary[col] = min(rightBoundary[col], right);
                else {
                    rightBoundary[col] = N;
                    right = col;
                }
            }

            for (int i = 0; i < heights.size(); ++i)
                maxArea = max(maxArea, heights[i] * (rightBoundary[i] - leftBoundary[i]));
        }
        return maxArea;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
