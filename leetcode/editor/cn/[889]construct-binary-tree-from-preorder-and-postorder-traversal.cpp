//Return any binary tree that matches the given preorder and postorder traversal
//s. 
//
// Values in the traversals pre and post are distinct positive integers. 
//
// 
//
// 
// Example 1: 
//
// 
//Input: pre = [1,2,4,5,3,6,7], post = [4,5,2,6,7,3,1]
//Output: [1,2,3,4,5,6,7]
// 
//
// 
//
// Note: 
//
// 
// 1 <= pre.length == post.length <= 30 
// pre[] and post[] are both permutations of 1, 2, ..., pre.length. 
// It is guaranteed an answer exists. If there exists multiple answers, you can 
//return any of them. 
// 
// 
// Related Topics 树


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    TreeNode *buildTree(vector<int> pre, int preL, int preR, vector<int> post, int postL, int postR) {
        if (preL > preR || postL > postR) return nullptr;

        TreeNode *root = new TreeNode(pre[preL]);
        if (preL == preR && postL == postR) return root;

        int mid = postL;
        while (post[mid] != pre[preL + 1]) mid++;

        root->left = buildTree(pre, preL + 1, preL + mid - postL + 1, post, postL, mid);
        root->right = buildTree(pre, preL + mid - postL + 2, preR, post, mid + 1, postR - 1);
        return root;
    }

public:
    TreeNode *constructFromPrePost(vector<int> &pre, vector<int> &post) {
        return buildTree(pre, 0, pre.size() - 1, post, 0, post.size() - 1);
    }
};
//leetcode submit region end(Prohibit modification and deletion)
