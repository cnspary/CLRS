//Implement a basic calculator to evaluate a simple expression string. 
//
// The expression string may contain open ( and closing parentheses ), the plus 
//+ or minus sign -, non-negative integers and empty spaces . 
//
// The expression string contains only non-negative integers, +, -, *, / operato
//rs , open ( and closing parentheses ) and empty spaces . The integer division sh
//ould truncate toward zero. 
//
// You may assume that the given expression is always valid. All intermediate re
//sults will be in the range of [-2147483648, 2147483647]. 
//
// Some examples: 
//
// 
//"1 + 1" = 2
//" 6-4 / 2 " = 4
//"2*(5+5*2)/3+(6/2+8)" = 21
//"(2+6* 3+5- (3*14/7+2)*5)+3"=-12
// 
//
// 
//
// Note: Do not use the eval built-in library function. 
// Related Topics 栈 字符串


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
private:
    long long cal(long long a, char op, long long b) {
        if (op == '+') return a + b;
        else if (op == '-') return a - b;
        else if (op == '*') return a * b;
        else return a / b;
    }

public:
    int calculate(string ss) {
        string s = "";
        for (int i = 0; i < ss.size(); ++i)
            if (ss[i] != ' ') s += ss[i];
        s += "#";

        vector<long long> NUM;
        vector<char> OP;
        for (int i = 0; i < s.size();) {
            if (s[i] == ' ') {
                i++;
            } else if (isdigit(s[i])) {
                int j = i + 1;
                while (isdigit(s[j])) j++;
                long long num = stol(s.substr(i, j - i));
                NUM.push_back(num);
                i = j;
            } else {
                if (s[i] == '(') {
                    OP.push_back(s[i]);
                    i++;
                } else if (s[i] == ')') {
                    while (OP.back() != '(') {
                        long long b = NUM.back();
                        NUM.pop_back();
                        long long a = NUM.back();
                        NUM.pop_back();

                        long long res = cal(a, OP.back(), b);
                        OP.pop_back();
                        NUM.push_back(res);
                    }
                    OP.pop_back();
                    i++;
                } else if (s[i] == '#' || s[i] == '+' || s[i] == '-') {
                    if (s[i] == '+' || s[i] == '-') {
                        if (i - 1 < 0 || s[i - 1] == '(') {
                            NUM.push_back(0);
                        }
                    }
                    while (!OP.empty() && OP.back() != '(') {
                        long long b = NUM.back();
                        NUM.pop_back();
                        long long a = NUM.back();
                        NUM.pop_back();

                        long long res = cal(a, OP.back(), b);
                        OP.pop_back();
                        NUM.push_back(res);
                    }
                    OP.push_back(s[i]);
                    i++;
                } else if (s[i] == '*' || s[i] == '/') {
                    while (!OP.empty() && (OP.back() == '*' || OP.back() == '/')) {
                        long long b = NUM.back();
                        NUM.pop_back();
                        long long a = NUM.back();
                        NUM.pop_back();

                        long long res = cal(a, OP.back(), b);
                        OP.pop_back();
                        NUM.push_back(res);
                    }
                    OP.push_back(s[i]);
                    i++;
                }
            }
        }
        return NUM.back();
    }
};
//leetcode submit region end(Prohibit modification and deletion)
