//Given inorder and postorder traversal of a tree, construct the binary tree. 
//
// Note: 
//You may assume that duplicates do not exist in the tree. 
//
// For example, given 
//
// 
//inorder = [9,3,15,20,7]
//postorder = [9,15,7,20,3] 
//
// Return the following binary tree: 
//
// 
//    3
//   / \
//  9  20
//    /  \
//   15   7
// 
// Related Topics 树 深度优先搜索 数组


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    vector<int> post;
    vector<int> in;

public:
    TreeNode *constructTree(int postL, int postR, int inL, int inR) {

        int root_index;
        for (root_index = inL; root_index <= inR; ++root_index)
            if (in[root_index] == post[postR]) break;

        TreeNode *x = new TreeNode(post[postR]);

        if (root_index - inL > 0)
            x->left = constructTree(postL, postL + root_index - inL - 1,
                                    inL, root_index - 1);
        if (inR - root_index > 0)
            x->right = constructTree(postL + root_index - inL, postR - 1,
                                     root_index + 1, inR);
        return x;
    }

    TreeNode *buildTree(vector<int> &inorder, vector<int> &postorder) {
        if (postorder.size() == 0 && inorder.size() == 0) return NULL;

        for (int i = 0; i < postorder.size(); ++i) {
            post.push_back(postorder[i]);
            in.push_back(inorder[i]);
        }

        return constructTree(0, post.size() - 1, 0, in.size() - 1);
    }
};
//leetcode submit region end(Prohibit modification and deletion)
