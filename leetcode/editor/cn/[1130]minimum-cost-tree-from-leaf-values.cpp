//Given an array arr of positive integers, consider all binary trees such that: 
//
//
// 
// Each node has either 0 or 2 children; 
// The values of arr correspond to the values of each leaf in an in-order traver
//sal of the tree. (Recall that a node is a leaf if and only if it has 0 children.
//) 
// The value of each non-leaf node is equal to the product of the largest leaf v
//alue in its left and right subtree respectively. 
// 
//
// Among all possible binary trees considered, return the smallest possible sum 
//of the values of each non-leaf node. It is guaranteed this sum fits into a 32-bi
//t integer. 
//
// 
// Example 1: 
//
// 
//Input: arr = [6,2,4]
//Output: 32
//Explanation:
//There are two possible trees.  The first has non-leaf node sum 36, and the sec
//ond has non-leaf node sum 32.
//
//    24            24
//   /  \          /  \
//  12   4        6    8
// /  \               / \
//6    2             2   4
// 
//
// 
// Constraints: 
//
// 
// 2 <= arr.length <= 40 
// 1 <= arr[i] <= 15 
// It is guaranteed that the answer fits into a 32-bit signed integer (ie. it is
// less than 2^31). 
// Related Topics 栈 树 动态规划


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
public:
    int mctFromLeafValues(vector<int> &arr) {
        vector <vector<int>> dp(arr.size(), vector<int>(arr.size(), INT_MAX));
        for (int i = 0; i < arr.size(); ++i)
            dp[i][i] = 0;

        for (int indent = 1; indent < arr.size(); ++indent) {
            for (int i = 0, j = indent; i < arr.size() && j < arr.size(); i = i + 1, j = i + indent) {
                for (int k = i; k < j; ++k) {
                    int maxL = arr[i], maxR = arr[j];
                    for (int p = i; p <= k; ++p)
                        maxL = max(maxL, arr[p]);
                    for (int p = k + 1; p <= j; ++p)
                        maxR = max(maxR, arr[p]);

                    dp[i][j] = min(dp[i][j], dp[i][k] + dp[k + 1][j] + maxL * maxR);
                }
            }
        }
        return dp[0][arr.size() - 1];
    }
};
//leetcode submit region end(Prohibit modification and deletion)
