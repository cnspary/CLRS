//Given an array of non-negative integers, you are initially positioned at the f
//irst index of the array. 
//
// Each element in the array represents your maximum jump length at that positio
//n. 
//
// Your goal is to reach the last index in the minimum number of jumps. 
//
// Example: 
//
// 
//Input: [2,3,1,1,4]
//Output: 2
//Explanation: The minimum number of jumps to reach the last index is 2.
//    Jump 1 step from index 0 to 1, then 3 steps to the last index. 
//
// Note: 
//
// You can assume that you can always reach the last index. 
// Related Topics 贪心算法 数组


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
public:
    int jump(vector<int> &nums) {
        int ans = 0;
        int end = 0;
        int rightMost = 0;

        for (int i = 0; i < nums.size() - 1; ++i) {
            if (i <= rightMost) {
                rightMost = max(rightMost, i + nums[i]);

                if (i == end) {
                    end = rightMost;
                    ans++;
                }
            }
        }

        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
