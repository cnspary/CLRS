//The data structure TreeNode is used for binary tree, but it can also used to r
//epresent a single linked list (where left is null, and right is the next node in
// the list). Implement a method to convert a binary search tree (implemented with
// TreeNode) into a single linked list. The values should be kept in order and the
// operation should be performed in place (that is, on the original data structure
//). 
//
// Return the head node of the linked list after converting. 
//
// Note: This problem is slightly different from the original one in the book. 
//
// 
//
// Example: 
//
// 
//Input:  [4,2,5,1,3,null,6,0]
//Output:  [0,null,1,null,2,null,3,null,4,null,5,null,6]
// 
//
// Note: 
//
// 
// The number of nodes will not exceed 100000. 
// 
// Related Topics 树 二叉搜索树 递归


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    stack<TreeNode *> S;

    void goAlongLeftBranch(TreeNode *x) {
        while (x) {
            S.push(x);
            x = x->left;
        }
    }

public:
    TreeNode *convertBiNode(TreeNode *root) {
        TreeNode *head = new TreeNode(-1);
        TreeNode *x = root, *pre = head;

        while (true) {
            goAlongLeftBranch(x);
            if (S.empty()) break;

            x = S.top();
            S.pop();

            pre->right = x;
            x->left = nullptr;
            pre = x;
            x = x->right;
        }
        return head->right;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
