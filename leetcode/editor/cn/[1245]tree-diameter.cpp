//Given an undirected tree, return its diameter: the number of edges in a longes
//t path in that tree. 
//
// The tree is given as an array of edges where edges[i] = [u, v] is a bidirecti
//onal edge between nodes u and v. Each node has labels in the set {0, 1, ..., edg
//es.length}. 
//
// 
// Example 1: 
//
// 
//
// 
//Input: edges = [[0,1],[0,2]]
//Output: 2
//Explanation: 
//A longest path of the tree is the path 1 - 0 - 2.
// 
//
// Example 2: 
//
// 
//
// 
//Input: edges = [[0,1],[1,2],[2,3],[1,4],[4,5]]
//Output: 4
//Explanation: 
//A longest path of the tree is the path 3 - 2 - 1 - 4 - 5.
// 
//
// 
// Constraints: 
//
// 
// 0 <= edges.length < 10^4 
// edges[i][0] != edges[i][1] 
// 0 <= edges[i][j] <= edges.length 
// The given edges form an undirected tree. 
// 
// Related Topics 树 深度优先搜索 广度优先搜索


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
private:
    vector <vector<int>> Graph;

    vector<int> bfs(int start) {
        queue <vector<int>> Q;
        Q.push({start, 0, -1});
        int DeepestNode = -1;
        int Deepest = -1;

        while (!Q.empty()) {
            int r = Q.front()[0];
            int d = Q.front()[1];
            int p = Q.front()[2];

            Q.pop();

            DeepestNode = r;
            Deepest = d;

            for (int next : Graph[r])
                if (next != p)
                    Q.push({next, d + 1, r});
        }

        return {DeepestNode, Deepest};
    }

public:
    int treeDiameter(vector <vector<int>> &edges) {
        Graph.resize(edges.size() + 1);
        for (int i = 0; i < edges.size(); ++i) {
            int a = edges[i][0];
            int b = edges[i][1];
            Graph[a].push_back(b);
            Graph[b].push_back(a);
        }

        auto x = bfs(0);
        auto y = bfs(x[0]);
        return y[1];
    }
};
//leetcode submit region end(Prohibit modification and deletion)
