//Given a non-empty binary search tree and a target value, find the value in the
// BST that is closest to the target. 
//
// Note: 
//
// 
// Given target value is a floating point. 
// You are guaranteed to have only one unique value in the BST that is closest t
//o the target. 
// 
//
// Example: 
//
// 
//Input: root = [4,2,5,1,3], target = 3.714286
//
//    4
//   / \
//  2   5
// / \
//1   3
//
//Output: 4
// 
// Related Topics 树 二分查找


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    int closestValue(TreeNode *root, double target) {
        double l = root->val, r = root->val;
        while (root) {
            if (target == root->val) return root->val;
            else if (target < root->val) {
                r = root->val;
                root = root->left;
            } else {
                l = root->val;
                root = root->right;
            }
        }

        if (abs(target - l) < abs(r - target)) return l;
        else return r;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
