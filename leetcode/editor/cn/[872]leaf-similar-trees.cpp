//Consider all the leaves of a binary tree. From left to right order, the values
// of those leaves form a leaf value sequence. 
//
// 
//
// For example, in the given tree above, the leaf value sequence is (6, 7, 4, 9,
// 8). 
//
// Two binary trees are considered leaf-similar if their leaf value sequence is 
//the same. 
//
// Return true if and only if the two given trees with head nodes root1 and root
//2 are leaf-similar. 
//
// 
// Constraints: 
//
// 
// Both of the given trees will have between 1 and 200 nodes. 
// Both of the given trees will have values between 0 and 200 
// 
// Related Topics 树 深度优先搜索


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    void DFS(TreeNode *root, vector<int> &leaf) {
        if (!root) return;

        if (!root->left && !root->right) {
            leaf.push_back(root->val);
            return;
        }

        DFS(root->left, leaf);
        DFS(root->right, leaf);
        return;
    }

public:
    bool leafSimilar(TreeNode *root1, TreeNode *root2) {
        vector<int> leaf1, leaf2;
        DFS(root1, leaf1);
        DFS(root2, leaf2);
        if (leaf1 == leaf2) return true;
        else return false;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
