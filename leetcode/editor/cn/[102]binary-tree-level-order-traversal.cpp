//Given a binary tree, return the level order traversal of its nodes' values. (i
//e, from left to right, level by level). 
//
// 
//For example: 
//Given binary tree [3,9,20,null,null,15,7], 
// 
//    3
//   / \
//  9  20
//    /  \
//   15   7
// 
// 
// 
//return its level order traversal as: 
// 
//[
//  [3],
//  [9,20],
//  [15,7]
//]
// 
// Related Topics 树 广度优先搜索


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector <vector<int>> levelOrder(TreeNode *root) {
        if (!root) return {};
        vector <vector<int>> ans;
        queue <pair<TreeNode *, int>> Q;
        int level = -1;
        Q.push({root, 0});
        while (!Q.empty()) {
            TreeNode *c = Q.front().first;
            int l = Q.front().second;
            Q.pop();

            if (level != l) {
                level = l;
                ans.push_back(vector<int>());
            }

            ans.back().push_back(c->val);
            if (c->left) Q.push({c->left, l + 1});
            if (c->right) Q.push({c->right, l + 1});
        }
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
