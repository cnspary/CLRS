//Given a binary tree with the following rules: 
//
// 
// root.val == 0 
// If treeNode.val == x and treeNode.left != null, then treeNode.left.val == 2 *
// x + 1 
// If treeNode.val == x and treeNode.right != null, then treeNode.right.val == 2
// * x + 2 
// 
//
// Now the binary tree is contaminated, which means all treeNode.val have been c
//hanged to -1. 
//
// You need to first recover the binary tree and then implement the FindElements
// class: 
//
// 
// FindElements(TreeNode* root) Initializes the object with a contamined binary 
//tree, you need to recover it first. 
// bool find(int target) Return if the target value exists in the recovered bina
//ry tree. 
// 
//
// 
// Example 1: 
//
// 
//
// 
//Input
//["FindElements","find","find"]
//[[[-1,null,-1]],[1],[2]]
//Output
//[null,false,true]
//Explanation
//FindElements findElements = new FindElements([-1,null,-1]); 
//findElements.find(1); // return False 
//findElements.find(2); // return True 
//
// Example 2: 
//
// 
//
// 
//Input
//["FindElements","find","find","find"]
//[[[-1,-1,-1,-1,-1]],[1],[3],[5]]
//Output
//[null,true,true,false]
//Explanation
//FindElements findElements = new FindElements([-1,-1,-1,-1,-1]);
//findElements.find(1); // return True
//findElements.find(3); // return True
//findElements.find(5); // return False 
//
// Example 3: 
//
// 
//
// 
//Input
//["FindElements","find","find","find","find"]
//[[[-1,null,-1,-1,null,-1]],[2],[3],[4],[5]]
//Output
//[null,true,false,false,true]
//Explanation
//FindElements findElements = new FindElements([-1,null,-1,-1,null,-1]);
//findElements.find(2); // return True
//findElements.find(3); // return False
//findElements.find(4); // return False
//findElements.find(5); // return True
// 
//
// 
// Constraints: 
//
// 
// TreeNode.val == -1 
// The height of the binary tree is less than or equal to 20 
// The total number of nodes is between [1, 10^4] 
// Total calls of find() is between [1, 10^4] 
// 0 <= target <= 10^6 
// 
// Related Topics 树 哈希表


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class FindElements {
private:
    TreeNode *R;

    void recover(TreeNode *root) {
        if (!root) return;

        if (root->left) {
            root->left->val = root->val * 2 + 1;
            recover(root->left);
        }

        if (root->right) {
            root->right->val = root->val * 2 + 2;
            recover(root->right);
        }
    }

public:
    FindElements(TreeNode *root) {
        root->val = 0;
        recover(root);
        R = root;
    }

    bool find(int target) {
        TreeNode *s = R;
        vector<int> path;

        while (target != 0) {
            path.push_back(target);
            target = (target - 1) / 2;
        }
        reverse(path.begin(), path.end());

        for (int e : path) {
            if (s->left && e == s->val * 2 + 1)
                s = s->left;
            else if (s->right && e == s->val * 2 + 2)
                s = s->right;
            else return false;
        }
        return true;
    }
};

/**
 * Your FindElements object will be instantiated and called as such:
 * FindElements* obj = new FindElements(root);
 * bool param_1 = obj->find(target);
 */
//leetcode submit region end(Prohibit modification and deletion)
