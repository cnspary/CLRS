//Given a binary tree, determine if it is height-balanced. 
//
// For this problem, a height-balanced binary tree is defined as: 
//
// 
// a binary tree in which the left and right subtrees of every node differ in he
//ight by no more than 1. 
// 
//
// 
//
// Example 1: 
//
// Given the following tree [3,9,20,null,null,15,7]: 
//
// 
//    3
//   / \
//  9  20
//    /  \
//   15   7 
//
// Return true. 
// 
//Example 2: 
//
// Given the following tree [1,2,2,3,3,null,null,4,4]: 
//
// 
//       1
//      / \
//     2   2
//    / \
//   3   3
//  / \
// 4   4
// 
//
// Return false. 
// Related Topics 树 深度优先搜索


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    int BalanceFactor(TreeNode *x) {
        if (!x) return 0;

        int LH = BalanceFactor(x->left);
        if (LH < 0) return -1;

        int RH = BalanceFactor(x->right);
        if (RH < 0) return -1;

        if (abs(LH - RH) < 2)
            return max(LH, RH) + 1;
        else
            return -1;

    }

    bool isBalanced(TreeNode *root) {
        return !(BalanceFactor(root) < 0);
    }
};
//leetcode submit region end(Prohibit modification and deletion)
