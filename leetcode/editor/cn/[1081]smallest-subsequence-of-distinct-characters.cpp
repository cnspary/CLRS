//Return the lexicographically smallest subsequence of text that contains all th
//e distinct characters of text exactly once. 
//
// Example 1: 
//
// 
//Input: "cdadabcc"
//Output: "adbc"
// 
//
// 
// Example 2: 
//
// 
//Input: "abcd"
//Output: "abcd"
// 
//
// 
// Example 3: 
//
// 
//Input: "ecbacba"
//Output: "eacb"
// 
//
// 
// Example 4: 
//
// 
//Input: "leetcode"
//Output: "letcod"
// 
//
// 
//
// Constraints: 
//
// 
// 1 <= text.length <= 1000 
// text consists of lowercase English letters. 
// 
//
// Note: This question is the same as 316: https://leetcode.com/problems/remove-
//duplicate-letters/ 
// 
// 
// 
// Related Topics 字符串


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
public:
    string smallestSubsequence(string s) {
        vector<int> count(26, 0);
        vector<char> S;
        for (int i = 0; i < s.size(); ++i)
            count[s[i] - 'a']++;

        vector<bool> exist(26, false);
        for (int i = 0; i < s.size(); ++i) {
            count[s[i] - 'a']--;

            if (exist[s[i] - 'a']) continue;

            while (!S.empty() && S.back() > s[i] && count[S.back() - 'a'] > 0) {
                exist[S.back() - 'a'] = false;
                S.pop_back();
            }
            S.push_back(s[i]);
            exist[s[i] - 'a'] = true;
        }

        string ans = "";
        for (int i = 0; i < S.size(); ++i)
            ans += S[i];

        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
