//You are given the array paths, where paths[i] = [cityAi, cityBi] means there e
//xists a direct path going from cityAi to cityBi. Return the destination city, th
//at is, the city without any path outgoing to another city. 
//
// It is guaranteed that the graph of paths forms a line without any loop, there
//fore, there will be exactly one destination city. 
//
// 
// Example 1: 
//
// 
//Input: paths = [["London","New York"],["New York","Lima"],["Lima","Sao Paulo"]
//]
//Output: "Sao Paulo" 
//Explanation: Starting at "London" city you will reach "Sao Paulo" city which i
//s the destination city. Your trip consist of: "London" -> "New York" -> "Lima" -
//> "Sao Paulo".
// 
//
// Example 2: 
//
// 
//Input: paths = [["B","C"],["D","B"],["C","A"]]
//Output: "A"
//Explanation: All possible trips are: 
//"D" -> "B" -> "C" -> "A". 
//"B" -> "C" -> "A". 
//"C" -> "A". 
//"A". 
//Clearly the destination city is "A".
// 
//
// Example 3: 
//
// 
//Input: paths = [["A","Z"]]
//Output: "Z"
// 
//
// 
// Constraints: 
//
// 
// 1 <= paths.length <= 100 
// paths[i].length == 2 
// 1 <= cityAi.length, cityBi.length <= 10 
// cityAi != cityBi 
// All strings consist of lowercase and uppercase English letters and the space 
//character. 
// 
// Related Topics 字符串


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
public:
    string destCity(vector <vector<string>> &paths) {
        map <string, string> Map;
        for (int i = 0; i < paths.size(); ++i)
            Map.insert({paths[i][0], paths[i][1]});

        string start = paths[0][0];
        while (true) {
            if (Map.find(start) == Map.end()) break;
            else start = Map[start];
        }
        return start;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
