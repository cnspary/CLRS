//We are given a binary tree (with root node root), a target node, and an intege
//r value K. 
//
// Return a list of the values of all nodes that have a distance K from the targ
//et node. The answer can be returned in any order. 
//
// 
//
// 
// 
//
// 
// Example 1: 
//
// 
//Input: root = [3,5,1,6,2,0,8,null,null,7,4], target = 5, K = 2
//
//Output: [7,4,1]
//
//Explanation: 
//The nodes that are a distance 2 from the target node (with value 5)
//have values 7, 4, and 1.
//
//
//
//Note that the inputs "root" and "target" are actually TreeNodes.
//The descriptions of the inputs above are just serializations of these objects.
//
// 
//
// 
//
// Note: 
//
// 
// The given tree is non-empty. 
// Each node in the tree has unique values 0 <= node.val <= 500. 
// The target node is a node in the tree. 
// 0 <= K <= 1000. 
// 
// 
// Related Topics 树 深度优先搜索 广度优先搜索


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    vector<TreeNode *> S;
    vector<int> ans;

    bool search(TreeNode *root, TreeNode *target) {
        if (!root) return false;

        S.push_back(root);
        if (root == target) return true;
        if (search(root->left, target)) return true;
        if (search(root->right, target))return true;
        S.pop_back();
        return false;
    }

    void disK(TreeNode *root, int K) {
        if (!root) return;

        if (K == 0) {
            ans.push_back(root->val);
            return;
        }

        disK(root->left, K - 1);
        disK(root->right, K - 1);
        return;
    }

public:
    vector<int> distanceK(TreeNode *root, TreeNode *target, int K) {
        if (K == 0) return {target->val};

        search(root, target);

        TreeNode *pre = nullptr;
        int lifting = 0;
        while (!S.empty()) {
            TreeNode *root = S.back();
            S.pop_back();

            if (lifting == 0) {
                disK(root->left, K - 1);
                disK(root->right, K - 1);
            } else {
                if (K - lifting >= 0 && root->left == pre)
                    if (K - lifting > 0) disK(root->right, K - lifting - 1);
                    else ans.push_back(root->val);
                else if (K - lifting >= 0 && root->right == pre) {
                    if (K - lifting > 0) disK(root->left, K - lifting - 1);
                    else ans.push_back(root->val);
                }
            }
            pre = root;
            lifting++;
        }
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
