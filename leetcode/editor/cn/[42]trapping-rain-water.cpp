//Given n non-negative integers representing an elevation map where the width of
// each bar is 1, compute how much water it is able to trap after raining. 
//
// 
//The above elevation map is represented by array [0,1,0,2,1,0,1,3,2,1,2,1]. In 
//this case, 6 units of rain water (blue section) are being trapped. Thanks Marcos
// for contributing this image! 
//
// Example: 
//
// 
//Input: [0,1,0,2,1,0,1,3,2,1,2,1]
//Output: 6 
// Related Topics 栈 数组 双指针


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
public:
    int trap(vector<int> &height) {
        if (height.size() <= 2) return 0;

        vector<int> preMax(height.size(), -1), sufMax(height.size(), -1);
        preMax[0] = height[0];
        for (int i = 1; i < height.size(); ++i)
            preMax[i] = max(preMax[i - 1], height[i]);

        sufMax.back() = height.back();
        for (int i = height.size() - 2; 0 <= i; --i)
            sufMax[i] = max(sufMax[i + 1], height[i]);

        int maxTrap = 0;
        for (int i = 0; i < height.size(); ++i) {
            int left_max, right_max;

            if (i - 1 < 0) left_max = height[i];
            else left_max = preMax[i - 1];

            if (height.size() <= i + 1) right_max = height[i];
            else right_max = sufMax[i + 1];

            maxTrap += max(min(left_max, right_max), height[i]) - height[i];
        }

        return maxTrap;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
