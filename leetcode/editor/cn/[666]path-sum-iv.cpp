//If the depth of a tree is smaller than 5, then this tree can be represented by
// a list of three-digits integers. 
//
// For each integer in this list: 
//
// 
// The hundreds digit represents the depth D of this node, 1 <= D <= 4. 
// The tens digit represents the position P of this node in the level it belongs
// to, 1 <= P <= 8. The position is the same as that in a full binary tree. 
// The units digit represents the value V of this node, 0 <= V <= 9. 
// 
//
// 
//
// Given a list of ascending three-digits integers representing a binary tree wi
//th the depth smaller than 5, you need to return the sum of all paths from the ro
//ot towards the leaves. 
//
// Example 1: 
//
// 
//Input: [113, 215, 221]
//Output: 12
//Explanation: 
//The tree that the list represents is:
//    3
//   / \
//  5   1
//
//The path sum is (3 + 5) + (3 + 1) = 12.
// 
//
// 
//
// Example 2: 
//
// 
//Input: [113, 221]
//Output: 4
//Explanation: 
//The tree that the list represents is: 
//    3
//     \
//      1
//
//The path sum is (3 + 1) = 4.
// 
//
// 
// Related Topics 树


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
private:
    static int getLevel(int x) {
        return x / 100;
    }

    static int getIndex(int x) {
        return (x % 100) / 10;
    }

    static int getValue(int x) {
        return x % 10;
    }

    static bool cmp(int a, int b) {
        return (getLevel(a) != getLevel(b)) ? (getLevel(a) < getLevel(b)) : (getIndex(a) < getIndex(b));
    }

public:
    int pathSum(vector<int> &nums) {
        if (nums.size() == 0) return 0;

        vector<int> Tree(16, -1);
        Tree[0] = 0;

        for (int node : nums)
            Tree[pow(2, getLevel(node) - 1) + getIndex(node) - 1] = getValue(node);
        int ans = 0;
        for (int i = 1; i <= 15; ++i) {
            if (Tree[i] != -1) {
                Tree[i] += Tree[i / 2];
                if (2 * i > 15 || Tree[2 * i] == -1)
                    if (2 * i + 1 > 15 || Tree[2 * i + 1] == -1) {
                        ans += Tree[i];
                    }
            }
        }
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
