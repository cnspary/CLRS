//Given two arrays arr1 and arr2, the elements of arr2 are distinct, and all ele
//ments in arr2 are also in arr1. 
//
// Sort the elements of arr1 such that the relative ordering of items in arr1 ar
//e the same as in arr2. Elements that don't appear in arr2 should be placed at th
//e end of arr1 in ascending order. 
//
// 
// Example 1: 
// Input: arr1 = [2,3,1,3,2,4,6,7,9,2,19], arr2 = [2,1,4,3,9,6]
//Output: [2,2,2,1,4,3,3,9,6,7,19]
// 
// 
// Constraints: 
//
// 
// arr1.length, arr2.length <= 1000 
// 0 <= arr1[i], arr2[i] <= 1000 
// Each arr2[i] is distinct. 
// Each arr2[i] is in arr1. 
// 
// Related Topics 排序 数组


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
public:
    vector<int> relativeSortArray(vector<int> &arr1, vector<int> &arr2) {
        map<int, int> weight;
        for (int i = 0; i < arr2.size(); ++i)
            weight.insert({arr2[i], i});

        int cnt = 0;
        for (int i = 0; i < arr1.size(); ++i) {
            if (weight.find(arr1[i]) == weight.end()) {
                cnt++;
                continue;
            }

            for (int j = i; 0 < j; --j) {
                if (weight.find(arr1[j - 1]) == weight.end() ||
                    weight[arr1[j - 1]] > weight[arr1[j]]) {
                    swap(arr1[j - 1], arr1[j]);
                } else
                    break;
            }
        }
        sort(arr1.begin() + arr1.size() - cnt, arr1.end());
        return arr1;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
