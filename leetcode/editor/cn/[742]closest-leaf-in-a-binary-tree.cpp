//Given a binary tree where every node has a unique value, and a target key k, f
//ind the value of the nearest leaf node to target k in the tree.
// 
//Here, nearest to a leaf means the least number of edges travelled on the binar
//y tree to reach any leaf of the tree. Also, a node is called a leaf if it has no
// children.
// 
//In the following examples, the input tree is represented in flattened form row
// by row.
//The actual root tree given will be a TreeNode object.
// 
//Example 1:
// 
//Input:
//root = [1, 3, 2], k = 1
//Diagram of binary tree:
//          1
//         / \
//        3   2
//
//Output: 2 (or 3)
//
//Explanation: Either 2 or 3 is the nearest leaf node to the target of 1.
// 
// 
//Example 2:
// 
//Input:
//root = [1], k = 1
//Output: 1
//
//Explanation: The nearest leaf node is the root node itself.
// 
// 
//
// 
//Example 3:
// 
//Input:
//root = [1,2,3,4,null,null,null,5,null,6], k = 2
//Diagram of binary tree:
//             1
//            / \
//           2   3
//          /
//         4
//        /
//       5
//      /
//     6
//
//Output: 3
//Explanation: The leaf node with value 3 (and not the leaf node with value 6) i
//s nearest to the node with value 2.
// 
// 
//
// Note: 
// 
// root represents a binary tree with at least 1 node and at most 1000 nodes. 
// Every node has a unique node.val in range [1, 1000]. 
// There exists some node in the given binary tree for which node.val == k. 
// 
// Related Topics 树


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    map<TreeNode *, pair < int, int>> H;
    stack<TreeNode *> S;

    bool getPath(TreeNode *root, int k) {
        if (!root) return false;

        S.push(root);
        if (root->val == k) return true;

        if (getPath(root->left, k)) return true;
        if (getPath(root->right, k)) return true;

        S.pop();
        return false;
    }

    pair<int, int> getClosestLeaf(TreeNode *root) {
        if (H.find(root) != H.end())
            return H[root];

        if (!root->left && !root->right) {
            H.insert({root, {0, root->val}});
            return H[root];
        }

        pair<int, int> L = {0x7fff, 0}, R = {0x7fff, 0};
        if (root->left) L = getClosestLeaf(root->left);
        if (root->right) R = getClosestLeaf(root->right);

        H[root] = {min(L.first, R.first) + 1, (L.first <= R.first) ? L.second : R.second};
        return H[root];
    }

public:
    int findClosestLeaf(TreeNode *root, int k) {
        getClosestLeaf(root);
        getPath(root, k);

        int lift = 0;
        pair<int, int> bestPath = {1003, 0};
        while (!S.empty()) {
            TreeNode *x = S.top();
            S.pop();
            pair<int, int> tmp = H[x];

            if (tmp.second != bestPath.second && tmp.first + lift < bestPath.first)
                bestPath = {tmp.first + lift, tmp.second};
            lift++;
        }
        return bestPath.second;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
