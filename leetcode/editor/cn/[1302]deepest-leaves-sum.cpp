//Given a binary tree, return the sum of values of its deepest leaves.
// 
// Example 1: 
//
// 
//
// 
//Input: root = [1,2,3,4,5,null,6,7,null,null,null,null,8]
//Output: 15
// 
//
// 
// Constraints: 
//
// 
// The number of nodes in the tree is between 1 and 10^4. 
// The value of nodes is between 1 and 100. 
// Related Topics 树 深度优先搜索


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    int sum = 0;

    void bfs(TreeNode *root) {
        queue <pair<TreeNode *, int>> Q;
        int curLevel = -1;
        Q.push({root, 0});

        while (!Q.empty()) {
            root = Q.front().first;
            int deep = Q.front().second;

            Q.pop();

            if (deep > curLevel) {
                sum = 0;
                curLevel = deep;
            }

            sum += root->val;

            if (root->left) Q.push({root->left, deep + 1});
            if (root->right) Q.push({root->right, deep + 1});
        }
    }

public:
    int deepestLeavesSum(TreeNode *root) {
        bfs(root);
        return sum;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
