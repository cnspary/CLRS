//Given a binary tree, find the length of the longest path where each node in th
//e path has the same value. This path may or may not pass through the root. 
//
// The length of path between two nodes is represented by the number of edges be
//tween them. 
//
// 
//
// Example 1: 
//
// Input: 
//
// 
//              5
//             / \
//            4   5
//           / \   \
//          1   1   5
// 
//
// Output: 2 
//
// 
//
// Example 2: 
//
// Input: 
//
// 
//              1
//             / \
//            4   5
//           / \   \
//          4   4   5
// 
//
// Output: 2 
//
// 
//
// Note: The given binary tree has not more than 10000 nodes. The height of the 
//tree is not more than 1000. 
// Related Topics 树 递归


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    int ans = 0;

    int longestPath(TreeNode *root) {
        if (!root)return 0;
        int dl = longestPath(root->left);
        int dr = longestPath(root->right);

        (root->left && root->val == root->left->val) ? dl += 1 : dl = 0;
        (root->right && root->val == root->right->val) ? dr += 1 : dr = 0;

        ans = max(ans, dl + dr);

        return max(dl, dr);
    }

public:
    int longestUnivaluePath(TreeNode *root) {
        longestPath(root);
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
