//Imagine a histogram (bar graph). Design an algorithm to compute the volume of 
//water it could hold if someone poured water across the top. You can assume that 
//each histogram bar has width 1. 
//
// 
//
// The above elevation map is represented by array [0,1,0,2,1,0,1,3,2,1,2,1]. In
// this case, 6 units of water (blue section) are being trapped. Thanks Marcos for
// contributing this image! 
//
// Example: 
//
// 
//Input: [0,1,0,2,1,0,1,3,2,1,2,1]
//Output: 6 
// Related Topics 栈 数组 双指针


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
public:
    int trap(vector<int> &height) {
        stack<int> ms;
        int rIndex = 0, sum = 0;
        while (rIndex < height.size()) {
            while (!ms.empty() && height[ms.top()] <= height[rIndex]) {
                int midIndex = ms.top();
                ms.pop();
                if (!ms.empty()) {
                    int lIndex = ms.top();
                    sum += (rIndex - lIndex - 1) * (min(height[lIndex], height[rIndex]) - height[midIndex]);
                }
            }
            ms.push(rIndex++);
        }
        return sum;
    }

};
//leetcode submit region end(Prohibit modification and deletion)
