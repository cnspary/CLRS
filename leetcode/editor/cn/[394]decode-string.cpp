//Given an encoded string, return its decoded string. 
//
// The encoding rule is: k[encoded_string], where the encoded_string inside the 
//square brackets is being repeated exactly k times. Note that k is guaranteed to 
//be a positive integer. 
//
// You may assume that the input string is always valid; No extra white spaces, 
//square brackets are well-formed, etc. 
//
// Furthermore, you may assume that the original data does not contain any digit
//s and that digits are only for those repeat numbers, k. For example, there won't
// be input like 3a or 2[4]. 
//
// Examples: 
//
// 
//s = "3[a]2[bc]", return "aaabcbc".
//s = "3[a2[c]]", return "accaccacc".
//s = "2[abc]3[cd]ef", return "abcabccdcdcdef".
// 
//
// 
// Related Topics 栈 深度优先搜索


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
public:
    string decodeString(string s) {
        string ans;
        stack<int> count;
        stack <string> substring;

        int i = 0;
        while (i < s.size()) {
            if (isdigit(s[i])) {
                int cnt = 0, j = i;
                while (j < s.size() && isdigit(s[j]))
                    cnt = cnt * 10 + (s[j++] - '0');
                count.push(cnt);
                i = j;
            } else if (s[i] == '[') {
                string ss;
                int j = i + 1;
                while (j < s.size() && (('a' <= s[j] && s[j] <= 'z') || ('A' <= s[j] && s[j] <= 'Z')))
                    ss += s[j++];
                substring.push(ss);
                i = j;
            } else if (s[i] == ']') {
                string add = "";
                for (int k = 0; k < count.top(); ++k)
                    add += substring.top();
                count.pop();
                substring.pop();
                if (substring.empty()) ans += add;
                else substring.top() += add;
                i++;
            } else {
                if (substring.empty()) ans += s[i];
                else substring.top() += s[i];
                i++;
            }
        }
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
