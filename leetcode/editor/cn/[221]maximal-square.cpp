//Given a 2D binary matrix filled with 0's and 1's, find the largest square cont
//aining only 1's and return its area. 
//
// Example: 
//
// 
//Input: 
//
//1 0 1 0 0
//1 0 1 1 1
//1 1 1 1 1
//1 0 0 1 0
//
//Output: 4
// Related Topics 动态规划


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
public:
    int maximalSquare(vector <vector<char>> &matrix) {
        int row = matrix.size();
        int col = row > 0 ? matrix[0].size() : 0;
        vector<int> dp(col + 1, 0);
        int prev = 0;
        int maxsqlen = 0;

        for (int i = 1; i <= row; ++i)
            for (int j = 1; j <= col; ++j) {
                int temp = dp[j];
                if (matrix[i - 1][j - 1] == '1') {
                    dp[j] = min(min(prev, dp[j - 1]), dp[j]) + 1;
                    maxsqlen = max(maxsqlen, dp[j]);
                } else
                    dp[j] = 0;
                prev = temp;
            }

        return maxsqlen * maxsqlen;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
