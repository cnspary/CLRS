//Implement the following operations of a stack using queues. 
//
// 
// push(x) -- Push element x onto stack. 
// pop() -- Removes the element on top of the stack. 
// top() -- Get the top element. 
// empty() -- Return whether the stack is empty. 
// 
//
// Example: 
//
// 
//MyStack stack = new MyStack();
//
//stack.push(1);
//stack.push(2);  
//stack.top();   // returns 2
//stack.pop();   // returns 2
//stack.empty(); // returns false 
//
// Notes: 
//
// 
// You must use only standard operations of a queue -- which means only push to 
//back, peek/pop from front, size, and is empty operations are valid. 
// Depending on your language, queue may not be supported natively. You may simu
//late a queue by using a list or deque (double-ended queue), as long as you use o
//nly standard operations of a queue. 
// You may assume that all operations are valid (for example, no pop or top oper
//ations will be called on an empty stack). 
// 
// Related Topics 栈 设计


//leetcode submit region begin(Prohibit modification and deletion)
class MyStack {
private:
    queue<int> Q;
    queue<int> tmp;
public:
    /** Initialize your data structure here. */
    MyStack() {

    }

    /** Push element x onto stack. */
    void push(int x) {
        Q.push(x);
    }

    /** Removes the element on top of the stack and returns that element. */
    int pop() {
        int e;
        while (!Q.empty()) {
            e = Q.front();
            Q.pop();
            if (!Q.empty())
                tmp.push(e);
        }

        while (!tmp.empty()) {
            Q.push(tmp.front());
            tmp.pop();
        }

        return e;
    }

    /** Get the top element. */
    int top() {
        int e;
        while (!Q.empty()) {
            e = Q.front();
            Q.pop();
            tmp.push(e);
        }

        while (!tmp.empty()) {
            Q.push(tmp.front());
            tmp.pop();
        }

        return e;
    }

    /** Returns whether the stack is empty. */
    bool empty() {
        return Q.empty();
    }
};

/**
 * Your MyStack object will be instantiated and called as such:
 * MyStack* obj = new MyStack();
 * obj->push(x);
 * int param_2 = obj->pop();
 * int param_3 = obj->top();
 * bool param_4 = obj->empty();
 */
//leetcode submit region end(Prohibit modification and deletion)
