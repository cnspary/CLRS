//Find the sum of all left leaves in a given binary tree. 
//
// Example:
// 
//    3
//   / \
//  9  20
//    /  \
//   15   7
//
//There are two left leaves in the binary tree, with values 9 and 15 respectivel
//y. Return 24.
// 
// Related Topics 树


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

#define isLChild(p, c) ((p && p->left == c) ? true : false)

class Solution {
private:
    int ans = 0;

    void DFS(TreeNode *c, TreeNode *p) {
        if (!c) return;

        DFS(c->left, c);
        DFS(c->right, c);

        if (!c->left && !c->right && isLChild(p, c))
            ans += c->val;
        return;
    }

public:
    int sumOfLeftLeaves(TreeNode *root) {
        DFS(root, nullptr);
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
