//A binary tree is univalued if every node in the tree has the same value. 
//
// Return true if and only if the given tree is univalued. 
//
// 
//
// Example 1: 
//
// 
//Input: [1,1,1,1,1,null,1]
//Output: true
// 
//
// 
// Example 2: 
//
// 
//Input: [2,2,2,5,2]
//Output: false
// 
// 
//
// 
//
// Note: 
//
// 
// The number of nodes in the given tree will be in the range [1, 100]. 
// Each node's value will be an integer in the range [0, 99]. 
// 
// Related Topics 树


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    int VALUE;
public:
    bool isUnivalTree(TreeNode *root) {
        VALUE = root->val;

        queue < TreeNode * > Q;
        Q.push(root);
        while (!Q.empty()) {
            root = Q.front();
            Q.pop();

            if (root->val != VALUE) return false;

            if (root->left) Q.push(root->left);
            if (root->right) Q.push(root->right);
        }
        return true;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
