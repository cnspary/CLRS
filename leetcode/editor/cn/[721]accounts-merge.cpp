//Given a list accounts, each element accounts[i] is a list of strings, where th
//e first element accounts[i][0] is a name, and the rest of the elements are email
//s representing emails of the account. 
//
// Now, we would like to merge these accounts. Two accounts definitely belong to
// the same person if there is some email that is common to both accounts. Note th
//at even if two accounts have the same name, they may belong to different people 
//as people could have the same name. A person can have any number of accounts ini
//tially, but all of their accounts definitely have the same name. 
//
// After merging the accounts, return the accounts in the following format: the 
//first element of each account is the name, and the rest of the elements are emai
//ls in sorted order. The accounts themselves can be returned in any order. 
//
// Example 1: 
// 
//Input: 
//accounts = [["John", "johnsmith@mail.com", "john00@mail.com"], ["John", "johnn
//ybravo@mail.com"], ["John", "johnsmith@mail.com", "john_newyork@mail.com"], ["Ma
//ry", "mary@mail.com"]]
//Output: [["John", 'john00@mail.com', 'john_newyork@mail.com', 'johnsmith@mail.
//com'],  ["John", "johnnybravo@mail.com"], ["Mary", "mary@mail.com"]]
//Explanation: 
//The first and third John's are the same person as they have the common email "
//johnsmith@mail.com".
//The second John and Mary are different people as none of their email addresses
// are used by other accounts.
//We could return these lists in any order, for example the answer [['Mary', 'ma
//ry@mail.com'], ['John', 'johnnybravo@mail.com'], 
//['John', 'john00@mail.com', 'john_newyork@mail.com', 'johnsmith@mail.com']] wo
//uld still be accepted.
// 
// 
//
// Note:
// The length of accounts will be in the range [1, 1000]. 
// The length of accounts[i] will be in the range [1, 10]. 
// The length of accounts[i][j] will be in the range [1, 30]. 
// Related Topics 深度优先搜索 并查集


//leetcode submit region begin(Prohibit modification and deletion)
class UnionFind {
private:
    int cnt;
    vector<int> uf;
    vector<int> rank;

public:
    UnionFind(int n) {
        cnt = n;
        uf = vector<int>(n);
        rank = vector<int>(n, 1);
        for (int i = 0; i < n; ++i)
            uf[i] = i;
    }

    int find(int a) {
        int p = a;
        while (uf[a] != a) a = uf[a];
        int root = uf[a];
        while (uf[p] != root) {
            int x = uf[p];
            uf[p] = root;
            p = x;
        }
        return root;
    }

    bool uunion(int a, int b) {
        int r1 = find(a);
        int r2 = find(b);
        if (r1 == r2) return false;

        cnt--;
        if (rank[r1] < rank[r2]) {
            uf[r1] = r2;
            rank[r2] += rank[r1];
        } else {
            uf[r2] = r1;
            rank[r1] += rank[r2];
        }
        return true;
    }

    int getRank(int i) {
        return rank[find(i)];
    }

    int count() {
        return cnt;
    }

    bool connected(int x, int y) {
        return find(x) == find(y);
    }
};

class Solution {
public:
    vector <vector<string>> accountsMerge(vector <vector<string>> &accounts) {
        map<string, int> ht;
        map <string, string> name;
        int cnt = 0;
        for (int i = 0; i < accounts.size(); ++i) {
            for (int j = 1; j < accounts[i].size(); ++j) {
                string email = accounts[i][j];
                if (ht.find(email) == ht.end())
                    ht.insert({email, cnt++});
                name[email] = accounts[i][0];
            }
        }
        UnionFind uf(ht.size());
        for (int i = 0; i < accounts.size(); ++i)
            for (int j = 2; j < accounts[i].size(); ++j) {
                int email1_id = ht[accounts[i][j - 1]];
                int email2_id = ht[accounts[i][j]];
                uf.uunion(email1_id, email2_id);
            }

        map<int, vector<string>> Emails;
        for (auto x : ht)
            Emails[uf.find(x.second)].push_back(x.first);

        vector <vector<string>> ans;
        for (auto x : Emails) {
            ans.push_back(x.second);
            sort(ans.back().begin(), ans.back().end());
        }

        for (int i = 0; i < ans.size(); ++i)
            ans[i].insert(ans[i].begin(), name[ans[i][0]]);
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
