//Each year, the government releases a list of the 10000 most common baby names 
//and their frequencies (the number of babies with that name). The only problem wi
//th this is that some names have multiple spellings. For example,"John" and ''Jon
//" are essentially the same name but would be listed separately in the list. Give
//n two lists, one of names/frequencies and the other of pairs of equivalent names
//, write an algorithm to print a new list of the true frequency of each name. Not
//e that if John and Jon are synonyms, and Jon and Johnny are synonyms, then John 
//and Johnny are synonyms. (It is both transitive and symmetric.) In the final lis
//t, choose the name that are lexicographically smallest as the "real" name. 
//
// Example: 
//
// 
//Input: names = ["John(15)","Jon(12)","Chris(13)","Kris(4)","Christopher(19)"],
// synonyms = ["(Jon,John)","(John,Johnny)","(Chris,Kris)","(Chris,Christopher)"]
//Output: ["John(27)","Chris(36)"] 
//
// Note: 
//
// 
// names.length <= 100000 
// 
// Related Topics 深度优先搜索 广度优先搜索 并查集


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
private:
    map<string, int> mpNameToIndex;
    vector<int> uf;
    vector<string> Name;
    vector<int> NameTimes;

    int find(string s) {
        int a = mpNameToIndex[s];
        int p = a;
        while (uf[a] != a) a = uf[a];
        int root = uf[a];
        while (uf[p] != root) {
            int x = uf[p];
            uf[p] = root;
            p = x;
        }
        return root;
    }

    bool uunion(string p, string q) {
        if (mpNameToIndex.find(p) == mpNameToIndex.end())
            return false;
        if (mpNameToIndex.find(q) == mpNameToIndex.end())
            return false;

        int a = mpNameToIndex[p];
        int b = mpNameToIndex[q];

        int r1 = find(p);
        int r2 = find(q);
        if (r1 == r2) return false;

        if (Name[r1].compare(Name[r2]) < 0) {
            uf[r2] = r1;
            NameTimes[r1] += NameTimes[r2];
        } else {
            uf[r1] = r2;
            NameTimes[r2] += NameTimes[r1];
        }
        return true;
    }
public:
    vector<string> trulyMostPopular(vector<string> &names, vector<string> &synonyms) {
        for (int i = 0; i < names.size(); ++i) {
            int split = names[i].find('(');
            string name = names[i].substr(0, split);
            int times = stoi(names[i].substr(split + 1, names[i].size() - split - 2));

            mpNameToIndex[name] = i;
            uf.push_back(i);
            Name.push_back(name);
            NameTimes.push_back(times);
        }

        for (int i = 0; i < synonyms.size(); ++i) {
            int split = synonyms[i].find(',');
            string ps = synonyms[i].substr(1, split - 1);
            string qs = synonyms[i].substr(split + 1, synonyms[i].size() - split - 2);
            uunion(ps, qs);
        }

        vector<string> ans;
        for (int i = 0; i < uf.size(); ++i) {
            if (uf[i] == i) {
                string s = "";
                s.append(Name[i]);
                s.append("(");
                s.append(to_string(NameTimes[i]));
                s.append(")");
                ans.push_back(s);
            }
        }
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
