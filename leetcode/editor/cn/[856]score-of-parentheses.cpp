//Given a balanced parentheses string S, compute the score of the string based o
//n the following rule: 
//
// 
// () has score 1 
// AB has score A + B, where A and B are balanced parentheses strings. 
// (A) has score 2 * A, where A is a balanced parentheses string. 
// 
//
// 
//
// 
// Example 1: 
//
// 
//Input: "()"
//Output: 1
// 
//
// 
// Example 2: 
//
// 
//Input: "(())"
//Output: 2
// 
//
// 
// Example 3: 
//
// 
//Input: "()()"
//Output: 2
// 
//
// 
// Example 4:
//
//
//Input: "(()(()))"
//Output: 6
// 
//
// 
//
// Note: 
//
// 
// S is a balanced parentheses string, containing only ( and ). 
// 2 <= S.length <= 50 
// 
// 
// 
// 
// 
// Related Topics 栈 字符串


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
public:
    int scoreOfParentheses(string S) {
        stack<char> stk;
        int base = -1;
        int ans = 0;
        for (int i = 0; i < S.size(); ++i) {
            if (S[i] == '(') {
                base++;
                stk.push(S[i]);
            } else {
                if(S[i-1] == '(')
                    ans += pow(2, base);
                base--;
                stk.pop();
            }
        }
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
