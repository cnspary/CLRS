//Given a binary tree, find the largest subtree which is a Binary Search Tree (B
//ST), where largest means subtree with largest number of nodes in it. 
//
// Note: 
//A subtree must include all of its descendants. 
//
// Example: 
//
// 
//Input: [10,5,15,1,8,null,7]
//
//   10 
//   / \ 
//  5  15 
// / \   \ 
//1   8   7
//
//Output: 3
//Explanation: The Largest BST Subtree in this case is the highlighted one.
//             The return value is the subtree's size, which is 3.
// 
//
// Follow up: 
//Can you figure out ways to solve it with O(n) time complexity? 
// Related Topics 树


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    int ans = 0;

    int solver(TreeNode *root, int &Min, int &Max) {
        if (!root) return 0;

        int sub1 = 0, sub2 = 0;
        int lmin = root->val, lmax = root->val;
        int rmin = root->val, rmax = root->val;

        sub1 = solver(root->left, lmin, lmax);
        sub2 = solver(root->right, rmin, rmax);

        if (sub1 == -1 || sub2 == -1) return -1;
        if ((sub1 != 0 && root->val <= lmax) || (sub2 != 0 && root->val >= rmin)) return -1;

        Min = lmin;
        Max = rmax;

        ans = max(ans, 1 + sub1 + sub2);
        return 1 + sub1 + sub2;
    }

public:
    int largestBSTSubtree(TreeNode *root) {
        if (!root) return 0;

        int Min, Max;
        solver(root, Min, Max);
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
