//Given a binary tree, flatten it to a linked list in-place. 
//
// For example, given the following tree: 
//
// 
//    1
//   / \
//  2   5
// / \   \
//3   4   6
// 
//
// The flattened tree should look like: 
//
// 
//1
// \
//  2
//   \
//    3
//     \
//      4
//       \
//        5
//         \
//          6
// 
// Related Topics 树 深度优先搜索


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    stack<TreeNode *> S;

    void goAlongLeftBranch(TreeNode *p) {
        while (p) {
            S.push(p);
            p = p->left;
        }
        return;
    }

public:
    void flatten(TreeNode *root) {
        TreeNode *t;
        while (true) {
            goAlongLeftBranch(root);
            if (S.empty()) break;

            root = S.top();
            S.pop();

            if (root->left) {
                t = root->left;
                while (t->right) t = t->right;

                t->right = root->right;
                root->right = root->left;
                root->left = nullptr;

                root = t->right;
            } else root = root->right;
        }

    }
};
//leetcode submit region end(Prohibit modification and deletion)
