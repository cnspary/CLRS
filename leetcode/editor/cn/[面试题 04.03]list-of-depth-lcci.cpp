//Given a binary tree, design an algorithm which creates a linked list of all th
//e nodes at each depth (e.g., if you have a tree with depth D, you'll have D link
//ed lists). Return a array containing all the linked lists. 
//
// 
//
// Example: 
//
// 
//Input: [1,2,3,4,5,null,7,8]
//
//        1
//       /  \ 
//      2    3
//     / \    \ 
//    4   5    7
//   /
//  8
//
//Output: [[1],[2,3],[4,5,7],[8]]
// 
// Related Topics 树 广度优先搜索


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
private:
    vector<ListNode *> ans;

    ListNode *createList(vector<int> nums) {
        ListNode *node;
        ListNode *head = new ListNode(nums.back());
        for (int i = nums.size() - 2; 0 <= i; --i) {
            node = new ListNode(nums[i]);
            node->next = head;
            head = node;
        }
        return head;
    }

public:
    vector<ListNode *> listOfDepth(TreeNode *tree) {
        queue <pair<TreeNode *, int>> Q;
        int curDeep = 0;
        vector<int> nums;

        Q.push({tree, 0});
        while (!Q.empty()) {
            TreeNode *root = Q.front().first;
            int Deep = Q.front().second;
            Q.pop();

            if (Deep != curDeep) {
                ans.push_back(createList(nums));
                curDeep = Deep;
                nums.clear();
            }
            nums.push_back(root->val);

            if (root->left) Q.push({root->left, Deep + 1});
            if (root->right) Q.push({root->right, Deep + 1});
        }
        ans.push_back(createList(nums));
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
