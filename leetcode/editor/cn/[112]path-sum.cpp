//Given a binary tree and a sum, determine if the tree has a root-to-leaf path s
//uch that adding up all the values along the path equals the given sum. 
//
// Note: A leaf is a node with no children. 
//
// Example: 
//
// Given the below binary tree and sum = 22, 
//
// 
//      5
//     / \
//    4   8
//   /   / \
//  11  13  4
// /  \      \
//7    2      1
// 
//
// return true, as there exist a root-to-leaf path 5->4->11->2 which sum is 22. 
//
// Related Topics 树 深度优先搜索


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    bool hasPathSum(TreeNode *root, int sum) {
        if(!root) return false;
        queue < TreeNode * > Q;
        Q.push(root);
        while (!Q.empty()) {
            TreeNode *p = Q.front();
            Q.pop();

            if (p->left || p->right) {
                if (p->left) {
                    p->left->val += p->val;
                    Q.push(p->left);
                }

                if (p->right) {
                    p->right->val += p->val;
                    Q.push(p->right);
                }

            } else if (p->val == sum) return true;
        }
        return false;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
