//Given a binary tree, you need to find the length of Longest Consecutive Path i
//n Binary Tree. 
//
// Especially, this path can be either increasing or decreasing. For example, [1
//,2,3,4] and [4,3,2,1] are both considered valid, but the path [1,2,4,3] is not v
//alid. On the other hand, the path can be in the child-Parent-child order, where 
//not necessarily be parent-child order. 
//
// Example 1: 
//
// 
//Input:
//        1
//       / \
//      2   3
//Output: 2
//Explanation: The longest consecutive path is [1, 2] or [2, 1].
// 
//
// 
//
// Example 2: 
//
// 
//Input:
//        2
//       / \
//      1   3
//Output: 3
//Explanation: The longest consecutive path is [1, 2, 3] or [3, 2, 1].
// 
//
// 
//
// Note: All the values of tree nodes are in the range of [-1e7, 1e7]. 
// Related Topics 树


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    int ans = 0;

    vector<int> postOrder(TreeNode *root) {
        if (!root) return {0, 0};

        vector<int> cur = {1, 1};

        vector<int> l = postOrder(root->left);
        vector<int> r = postOrder(root->right);

        if (root->left && root->left->val + 1 == root->val)
            cur[1] += l[1];
        else if (root->left && root->left->val - 1 == root->val)
            cur[0] += l[0];

        if (root->right && root->right->val - 1 == root->val)
            cur[0] = max(cur[0], r[0] + 1);
        else if (root->right && root->right->val + 1 == root->val)
            cur[1] = max(cur[1], r[1] + 1);

        ans = max(ans, cur[0] + cur[1] - 1);
        return cur;
    }

public:
    int longestConsecutive(TreeNode *root) {
        postOrder(root);
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
