//Given a binary tree, return the tilt of the whole tree. 
//
// The tilt of a tree node is defined as the absolute difference between the sum
// of all left subtree node values and the sum of all right subtree node values. N
//ull node has tilt 0. 
//
// The tilt of the whole tree is defined as the sum of all nodes' tilt. 
//
// Example: 
// 
//Input: 
//         1
//       /   \
//      2     3
//Output: 1
//Explanation: 
//Tilt of node 2 : 0
//Tilt of node 3 : 0
//Tilt of node 1 : |2-3| = 1
//Tilt of binary tree : 0 + 0 + 1 = 1
// 
// 
//
// Note:
// 
// The sum of node values in any subtree won't exceed the range of 32-bit intege
//r. 
// All the tilt values won't exceed the range of 32-bit integer. 
// 
// Related Topics 树


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    int ans = 0;

    int solver(TreeNode *root) {
        if (!root) return 0;

        int lsum = solver(root->left);
        int rsum = solver(root->right);

        ans += abs(lsum - rsum);

        return root->val + lsum + rsum;
    }

public:
    int findTilt(TreeNode *root) {
        solver(root);
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
