//In a N x N grid composed of 1 x 1 squares, each 1 x 1 square consists of a /, 
//\, or blank space. These characters divide the square into contiguous regions. 
//
// (Note that backslash characters are escaped, so a \ is represented as "\\".) 
//
//
// Return the number of regions. 
//
// 
//
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
//
// 
// Example 1: 
//
// 
//Input:
//[
//  " /",
//  "/ "
//]
//Output: 2
//Explanation: The 2x2 grid is as follows:
//
// 
//
// 
// Example 2: 
//
// 
//Input:
//[
//  " /",
//  "  "
//]
//Output: 1
//Explanation: The 2x2 grid is as follows:
//
// 
//
// 
// Example 3: 
//
// 
//Input:
//[
//  "\\/",
//  "/\\"
//]
//Output: 4
//Explanation: (Recall that because \ characters are escaped, "\\/" refers to \/
//, and "/\\" refers to /\.)
//The 2x2 grid is as follows:
//
// 
//
// 
// Example 4: 
//
// 
//Input:
//[
//  "/\\",
//  "\\/"
//]
//Output: 5
//Explanation: (Recall that because \ characters are escaped, "/\\" refers to /\
//, and "\\/" refers to \/.)
//The 2x2 grid is as follows:
//
// 
//
// 
// Example 5: 
//
// 
//Input:
//[
//  "//",
//  "/ "
//]
//Output: 3
//Explanation: The 2x2 grid is as follows:
//
// 
//
// 
//
// Note: 
//
// 
// 1 <= grid.length == grid[0].length <= 30 
// grid[i][j] is either '/', '\', or ' '. 
// 
// 
// 
// 
// 
// Related Topics 深度优先搜索 并查集 图


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    class UnionFind {
    private:
        vector<int> uf;
        vector<int> rank;

    public:
        UnionFind(int n) {
            uf = vector<int>(n);
            rank = vector<int>(n, 1);
            for (int i = 0; i < n; ++i)
                uf[i] = i;
        }

        int find(int a) {
            int p = a;
            while (uf[a] != a) a = uf[a];
            int root = uf[a];
            while (uf[p] != root) {
                int x = uf[p];
                uf[p] = root;
                p = x;
            }
            return root;
        }

        bool uunion(int a, int b) {
            int r1 = find(a);
            int r2 = find(b);
            if (r1 == r2) return false;

            if (rank[r1] < rank[r2]) {
                uf[r1] = r2;
                rank[r2] += rank[r1];
            } else {
                uf[r2] = r1;
                rank[r1] += rank[r2];
            }
            return true;
        }

        int count() {
            int cnt = 0;
            for (int i = 0; i < uf.size(); ++i)
                if (uf[i] == i) cnt++;
            return cnt;
        }
    };

    class Solution {
    public:
        int regionsBySlashes(vector <string> &grid) {
            int N = grid.size();
            UnionFind uf = UnionFind(N * N * 4);

            for (int row = 0; row < grid.size(); ++row) {
                for (int col = 0; col < grid[row].size(); ++col) {
                    int indexB = (row * N + col) * 4;
                    int indexR = (row * N + col) * 4 + 1;
                    int indexT = (row * N + col) * 4 + 2;
                    int indexL = (row * N + col) * 4 + 3;

                    if (grid[row][col] == ' ') {
                        uf.uunion(indexB, indexR);
                        uf.uunion(indexT, indexL);
                        uf.uunion(indexB, indexT);
                    } else if (grid[row][col] == '/') {
                        uf.uunion(indexB, indexR);
                        uf.uunion(indexT, indexL);
                    } else if (grid[row][col] == '\\') {
                        uf.uunion(indexB, indexL);
                        uf.uunion(indexT, indexR);
                    }


                    if (row + 1 < N)
                        uf.uunion(indexB, ((row + 1) * N + col) * 4 + 2);

                    if (col + 1 < N)
                        uf.uunion(indexR, (row * N + col + 1) * 4 + 3);

                    if (-1 < row - 1)
                        uf.uunion(indexT, ((row - 1) * N + col) * 4);

                    if (-1 < col - 1)
                        uf.uunion(indexL, (row * N + col - 1) * 4 + 1);
                }
            }

            return uf.count();
        }
    };
//leetcode submit region end(Prohibit modification and deletion)
