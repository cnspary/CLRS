//A 2d grid map of m rows and n columns is initially filled with water. We may p
//erform an addLand operation which turns the water at position (row, col) into a 
//land. Given a list of positions to operate, count the number of islands after ea
//ch addLand operation. An island is surrounded by water and is formed by connecti
//ng adjacent lands horizontally or vertically. You may assume all four edges of t
//he grid are all surrounded by water. 
//
// Example: 
//
// 
//Input: m = 3, n = 3, positions = [[0,0], [0,1], [1,2], [2,1]]
//Output: [1,1,2,3]
// 
//
// Explanation: 
//
// Initially, the 2d grid grid is filled with water. (Assume 0 represents water 
//and 1 represents land). 
//
// 
//0 0 0
//0 0 0
//0 0 0
// 
//
// Operation #1: addLand(0, 0) turns the water at grid[0][0] into a land. 
//
// 
//1 0 0
//0 0 0   Number of islands = 1
//0 0 0
// 
//
// Operation #2: addLand(0, 1) turns the water at grid[0][1] into a land. 
//
// 
//1 1 0
//0 0 0   Number of islands = 1
//0 0 0
// 
//
// Operation #3: addLand(1, 2) turns the water at grid[1][2] into a land. 
//
// 
//1 1 0
//0 0 1   Number of islands = 2
//0 0 0
// 
//
// Operation #4: addLand(2, 1) turns the water at grid[2][1] into a land. 
//
// 
//1 1 0
//0 0 1   Number of islands = 3
//0 1 0
// 
//
// Follow up: 
//
// Can you do it in time complexity O(k log mn), where k is the length of the po
//sitions? 
// Related Topics 并查集


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
private:
    vector<int> uf;
    vector<int> rank;

    int find(int x) {
        int root = x;
        while (uf[root] != root)
            root = uf[root];

        while (uf[x] != root) {
            int t = uf[x];
            uf[x] = root;
            x = t;
        }
        return root;
    }

    void unionn(int x, int y) {
        int r1 = find(x);
        int r2 = find(y);

        if (r1 == r2) return;

        if (rank[r1] > rank[r2]) {
            uf[r2] = r1;
            rank[r1] += rank[r2];
        } else {
            uf[r1] = r2;
            rank[r2] += rank[r1];
        }
        return;
    }

    bool connected(int x, int y) {
        return find(x) == find(y);
    }

public:
    vector<int> numIslands2(int m, int n, vector <vector<int>> &positions) {
        uf.resize(m * n);
        rank.resize(m * n, 1);

        for (int i = 0; i < uf.size(); ++i)
            uf[i] = i;

        vector <vector<int>> pos = {{1,  0},
                                    {0,  1},
                                    {-1, 0},
                                    {0,  -1}};
        set<int> WaterPos;
        vector<int> ans;
        int islands = 0;

        for (int i = 0; i < positions.size(); ++i) {
            int r = positions[i][0];
            int c = positions[i][1];

            if (WaterPos.find(r * n + c) != WaterPos.end()) {
                ans.push_back(islands);
                continue;
            }

            WaterPos.insert(r * n + c);

            int cnt = 0;
            for (int i = 0; i < pos.size(); ++i) {
                int nx = r + pos[i][0];
                int ny = c + pos[i][1];

                if (0 <= nx && nx < m && 0 <= ny && ny < n &&
                    WaterPos.find(nx * n + ny) != WaterPos.end()) {
                    if(!connected(r * n + c, nx * n + ny)) {
                        cnt++;
                        unionn(r * n + c, nx * n + ny);
                    }
                }
            }
            islands = islands - (cnt - 1);
            ans.push_back(islands);
        }
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
