//Given an input string, reverse the string word by word. 
//
// 
//
// Example 1: 
//
// 
//Input: "the sky is blue"
//Output: "blue is sky the"
// 
//
// Example 2: 
//
// 
//Input: "  hello world!  "
//Output: "world! hello"
//Explanation: Your reversed string should not contain leading or trailing space
//s.
// 
//
// Example 3: 
//
// 
//Input: "a good   example"
//Output: "example good a"
//Explanation: You need to reduce multiple spaces between two words to a single 
//space in the reversed string.
// 
//
// 
//
// Note: 
//
// 
// A word is defined as a sequence of non-space characters. 
// Input string may contain leading or trailing spaces. However, your reversed s
//tring should not contain leading or trailing spaces. 
// You need to reduce multiple spaces between two words to a single space in the
// reversed string. 
// 
//
// 
//
// Follow up: 
//
// For C programmers, try to solve it in-place in O(1) extra space. Related Topi
//cs 字符串


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
public:
    string reverseWords(string s) {
        vector <string> splits;

        int start = 0, end = s.size() - 1;
        while (start <= end)
            if (s[start] == ' ') start++;
            else break;
        while (start <= end)
            if (s[end] == ' ') end--;
            else break;

        int i, j;
        for (i = start, j = start; j <= end; j++) {
            if (s[j] == ' ') {
                if (i == j) {
                    i++;
                    continue;
                }
                splits.push_back(s.substr(i, j - i));
                i = j + 1;
            }
        }
        splits.push_back(s.substr(i, j - i));
        string ans = "";
        for (int i = splits.size() - 1; -1 < i; --i)
            ans += splits[i] + " ";
        return ans.substr(0, ans.size() - 1);
    }
};
//leetcode submit region end(Prohibit modification and deletion)
