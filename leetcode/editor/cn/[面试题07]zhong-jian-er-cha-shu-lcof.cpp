//English description is not available for the problem. Please switch to Chinese
//. Related Topics 树 递归


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    TreeNode *buildTree(vector<int> &preorder, int PL, int PR, vector<int> &inorder, int IL, int IR) {
        if (PL > PR || IL > IR) return nullptr;
        TreeNode *root = new TreeNode(preorder[PL]);

        int mid = IL;
        while (inorder[mid] != root->val) mid++;

        root->left = buildTree(preorder, PL + 1, PL + mid - IL, inorder, IL, mid - 1);
        root->right = buildTree(preorder, PL + mid - IL + 1, PR, inorder, mid + 1, IR);
        return root;
    }

public:
    TreeNode *buildTree(vector<int> &preorder, vector<int> &inorder) {
        return buildTree(preorder, 0, preorder.size() - 1, inorder, 0, inorder.size() - 1);
    }
};
//leetcode submit region end(Prohibit modification and deletion)
