//Given two binary search trees root1 and root2. 
//
// Return a list containing all the integers from both trees sorted in ascending
// order. 
//
// 
// Example 1: 
//
// 
//Input: root1 = [2,1,4], root2 = [1,0,3]
//Output: [0,1,1,2,3,4]
// 
//
// Example 2: 
//
// 
//Input: root1 = [0,-10,10], root2 = [5,1,7,0,2]
//Output: [-10,0,0,1,2,5,7,10]
// 
//
// Example 3: 
//
// 
//Input: root1 = [], root2 = [5,1,7,0,2]
//Output: [0,1,2,5,7]
// 
//
// Example 4: 
//
// 
//Input: root1 = [0,-10,10], root2 = []
//Output: [-10,0,10]
// 
//
// Example 5: 
//
// 
//Input: root1 = [1,null,8], root2 = [8,1]
//Output: [1,1,8,8]
// 
//
// 
// Constraints: 
//
// 
// Each tree has at most 5000 nodes. 
// Each node's value is between [-10^5, 10^5]. 
// 
// Related Topics 排序 树


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    void inOrder(TreeNode *root, vector<int> &seq) {
        if (root == NULL) return;

        inOrder(root->left, seq);
        seq.push_back(root->val);
        inOrder(root->right, seq);
    }

    vector<int> merge(vector<int> r1, vector<int> r2) {
        vector<int> ans;
        int i = 0, j = 0;
        while (i < r1.size() && j < r2.size()) {
            if (r1[i] < r2[j]) ans.push_back(r1[i++]);
            else ans.push_back(r2[j++]);
        }

        while (i < r1.size()) ans.push_back(r1[i++]);
        while (j < r2.size()) ans.push_back(r2[j++]);
        return ans;
    }

    vector<int> getAllElements(TreeNode *root1, TreeNode *root2) {
        vector<int> r1, r2;
        inOrder(root1, r1);
        inOrder(root2, r2);
        return merge(r1, r2);
    }
};
//leetcode submit region end(Prohibit modification and deletion)
