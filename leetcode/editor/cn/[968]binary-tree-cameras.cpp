//Given a binary tree, we install cameras on the nodes of the tree. 
//
// Each camera at a node can monitor its parent, itself, and its immediate child
//ren. 
//
// Calculate the minimum number of cameras needed to monitor all nodes of the tr
//ee. 
//
// 
//
// Example 1: 
//
// 
// 
//Input: [0,0,null,0,0]
//Output: 1
//Explanation: One camera is enough to monitor all nodes if placed as shown.
// 
//
// 
// Example 2: 
//
// 
//Input: [0,0,null,0,null,0,null,null,0]
//Output: 2
//Explanation: At least two cameras are needed to monitor all nodes of the tree.
// The above image shows one of the valid configurations of camera placement.
// 
//
// 
//Note: 
//
// 
// The number of nodes in the given tree will be in the range [1, 1000]. 
// Every node has value 0. 
// 
// 
// 
// Related Topics 树 深度优先搜索 动态规划


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    int ans = 0;
    set<TreeNode *> covered;

    void DFS(TreeNode *root, TreeNode *parent) {
        if (!root) return;

        DFS(root->left, root);
        DFS(root->right, root);

        if ((!parent && !covered.count(root) ||
             !covered.count(root->left) ||
             !covered.count(root->right))) {
            ans++;
            covered.insert(root);
            covered.insert(parent);
            covered.insert(root->left);
            covered.insert(root->right);
        }
    }

public:
    int minCameraCover(TreeNode *root) {
        covered.insert(nullptr);
        DFS(root, nullptr);
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
