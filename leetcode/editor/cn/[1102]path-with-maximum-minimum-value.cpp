//Given a matrix of integers A with R rows and C columns, find the maximum score
// of a path starting at [0,0] and ending at [R-1,C-1]. 
//
// The score of a path is the minimum value in that path. For example, the value
// of the path 8 → 4 → 5 → 9 is 4. 
//
// A path moves some number of times from one visited cell to any neighbouring u
//nvisited cell in one of the 4 cardinal directions (north, east, west, south). 
//
// 
//
// Example 1: 
//
// 
//
// 
//Input: [[5,4,5],[1,2,6],[7,4,6]]
//Output: 4
//Explanation: 
//The path with the maximum score is highlighted in yellow. 
// 
//
// Example 2: 
//
// 
//
// 
//Input: [[2,2,1,2,2,2],[1,2,2,2,1,2]]
//Output: 2 
//
// Example 3: 
//
// 
//
// 
//Input: [[3,4,6,3,4],[0,2,1,1,7],[8,8,3,2,7],[3,2,4,9,8],[4,1,2,0,0],[4,6,5,4,3
//]]
//Output: 3 
//
// 
//
// Note: 
//
// 
// 1 <= R, C <= 100 
// 0 <= A[i][j] <= 10^9 
// 
// Related Topics 深度优先搜索 并查集 图


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
public:
    int maximumMinimumPath(vector <vector<int>> &A) {
        int r = A.size();
        int c = A[0].size();
        int d[4][2] = {{1,  0},
                       {-1, 0},
                       {0,  1},
                       {0,  -1}};
        int res = INT_MAX;
        vector <vector<bool>> visit(100, vector<bool>(100, false));

        auto cmp = [](pair<int, int> a, pair<int, int> b) -> bool { return a.first <= b.first; };
        priority_queue < pair < int, int >, vector < pair < int, int >>, decltype(cmp) > pq(cmp);

        pq.push(make_pair(A[0][0], 0));
        visit[0][0] = true;

        while (!pq.empty()) {
            pair<int, int> curr = pq.top();
            pq.pop();

            int score = curr.first;
            int x = curr.second / c;
            int y = curr.second % c;
            res = min(res, score);

            if (x == r - 1 && y == c - 1) {
                return res;
            }

            for (int i = 0; i < 4; ++i) {
                int x1 = x + d[i][0];
                int y1 = y + d[i][1];

                if (x1 >= 0 && x1 < r && y1 >= 0 && y1 < c) {
                    if (!visit[x1][y1]) {
                        pq.push(make_pair(A[x1][y1], x1 * c + y1));
                        visit[x1][y1] = true;
                    }
                }
            }
        }

        return -1;
    }

};
//leetcode submit region end(Prohibit modification and deletion)
