//
//In this problem, a rooted tree is a directed graph such that, there is exactly
// one node (the root) for which all other nodes are descendants of this node, plu
//s every node has exactly one parent, except for the root node which has no paren
//ts.
// 
//The given input is a directed graph that started as a rooted tree with N nodes
// (with distinct values 1, 2, ..., N), with one additional directed edge added. T
//he added edge has two different vertices chosen from 1 to N, and was not an edge
// that already existed.
// 
//The resulting graph is given as a 2D-array of edges. Each element of edges is 
//a pair [u, v] that represents a directed edge connecting nodes u and v, where u 
//is a parent of child v.
// 
//Return an edge that can be removed so that the resulting graph is a rooted tre
//e of N nodes. If there are multiple answers, return the answer that occurs last 
//in the given 2D-array.
// Example 1: 
// 
//Input: [[1,2], [1,3], [2,3]]
//Output: [2,3]
//Explanation: The given directed graph will be like this:
//  1
// / \
//v   v
//2-->3
// 
// 
// Example 2: 
// 
//Input: [[1,2], [2,3], [3,4], [4,1], [1,5]]
//Output: [4,1]
//Explanation: The given directed graph will be like this:
//5 <- 1 -> 2
//     ^    |
//     |    v
//     4 <- 3
// 
// 
// Note: 
// The size of the input 2D-array will be between 3 and 1000. 
// Every integer represented in the 2D-array will be between 1 and N, where N is
// the size of the input array. 
// Related Topics 树 深度优先搜索 并查集 图


//leetcode submit region begin(Prohibit modification and deletion)

class UnionFind {
private:
    vector<int> uf;
    vector<int> rank;
    int _component;
public:
    UnionFind(int n) {
        uf.resize(n);
        rank.resize(n);
        _component = n;
        for (int i = 0; i < n; ++i) {
            uf[i] = i;
            rank[i] = 1;
        }
    }

    int find(int a) {
        int r = a;
        while (uf[r] != r) r = uf[r];

        while (uf[a] != r) {
            int t = uf[a];
            uf[a] = r;
            a = t;
        }
        return r;
    }

    void uunion(int a, int b) {
        int r1 = find(a);
        int r2 = find(b);

        if (r1 == r2) return;

        _component--;
        if (rank[r1] > rank[r2]) {
            uf[r2] = r1;
            rank[r1] += rank[r2];
        } else {
            uf[r1] = r2;
            rank[r2] += rank[r1];
        }
    }

    bool connected(int a, int b) {
        return find(a) == find(b);
    }

    int component() {
        return _component;
    }
};

class Solution {
private:
    vector<int> solverDoubleParent(vector <vector<int>> &edges, int Parent1, int Parent2, int child) {
        vector<int> Parents = {Parent2, Parent1};
        vector<int> ans;
        for (int parent : Parents) {
            UnionFind uf(edges.size());
            for (int i = 0; i < edges.size(); ++i) {
                int p = edges[i][0] - 1, c = edges[i][1] - 1;
                if (p == parent && c == child) continue;
                uf.uunion(p, c);
            }
            if (uf.component() == 1) return {parent + 1, child + 1};
        }
        return {};
    }

public:
    vector<int> findRedundantDirectedConnection(vector <vector<int>> &edges) {
        UnionFind uf(edges.size());
        vector<int> Parent(edges.size(), -1);
        vector<int> ans;
        for (int i = 0; i < edges.size(); ++i) {
            int p = edges[i][0] - 1, c = edges[i][1] - 1;

            if (Parent[c] != -1) return solverDoubleParent(edges, Parent[c], p, c);
            if (uf.connected(p, c)) ans = {p + 1, c + 1};

            uf.uunion(p, c);
            Parent[c] = p;
        }
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
