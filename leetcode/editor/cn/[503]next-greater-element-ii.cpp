//
//Given a circular array (the next element of the last element is the first elem
//ent of the array), print the Next Greater Number for every element. The Next Gre
//ater Number of a number x is the first greater number to its traversing-order ne
//xt in the array, which means you could search circularly to find its next greate
//r number. If it doesn't exist, output -1 for this number.
// 
//
// Example 1: 
// 
//Input: [1,2,1]
//Output: [2,-1,2]
//Explanation: The first 1's next greater number is 2; The number 2 can't find n
//ext greater number; The second 1's next greater number needs to search circularl
//y, which is also 2.
// 
// 
//
// Note:
//The length of given array won't exceed 10000.
// Related Topics 栈


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
public:
    vector<int> nextGreaterElements(vector<int> &nums) {
        stack<int> S;
        vector<int> ans(nums.size(), -1);
        int n = nums.size();

        for (int i = 2 * n - 1; -1 < i; --i) {
            while (!S.empty()) {
                if (S.top() <= nums[i % n]) S.pop();
                else break;
            }

            if (ans[i % n] == -1)
                if (S.empty()) ans[i % n] = -1;
                else ans[i % n] = S.top();

            S.push(nums[i % n]);
        }
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
