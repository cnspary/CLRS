//Given a binary tree, return the vertical order traversal of its nodes values. 
//
//
// For each node at position (X, Y), its left and right children respectively wi
//ll be at positions (X-1, Y-1) and (X+1, Y-1). 
//
// Running a vertical line from X = -infinity to X = +infinity, whenever the ver
//tical line touches some nodes, we report the values of the nodes in order from t
//op to bottom (decreasing Y coordinates). 
//
// If two nodes have the same position, then the value of the node that is repor
//ted first is the value that is smaller. 
//
// Return an list of non-empty reports in order of X coordinate. Every report wi
//ll have a list of values of nodes. 
//
// 
//
// Example 1: 
//
// 
//
// 
// 
//Input: [3,9,20,null,null,15,7]
//Output: [[9],[3,15],[20],[7]]
//Explanation: 
//Without loss of generality, we can assume the root node is at position (0, 0):
//
//Then, the node with value 9 occurs at position (-1, -1);
//The nodes with values 3 and 15 occur at positions (0, 0) and (0, -2);
//The node with value 20 occurs at position (1, -1);
//The node with value 7 occurs at position (2, -2).
// 
//
// 
// Example 2: 
//
// 
//
// 
//Input: [1,2,3,4,5,6,7]
//Output: [[4],[2],[1,5,6],[3],[7]]
//Explanation: 
//The node with value 5 and the node with value 6 have the same position accordi
//ng to the given scheme.
//However, in the report "[1,5,6]", the node value of 5 comes first since 5 is s
//maller than 6.
// 
//
// 
// 
//
// Note: 
//
// 
// The tree will have between 1 and 1000 nodes. 
// Each node's value will be between 0 and 1000. 
// 
// 
//
// 
// 
// 
// Related Topics 树 哈希表


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

struct Node {
    int val;
    int level;
    int pos;

    Node(int v, int l, int p) : val(v), level(l), pos(p) {};

    bool operator<(Node that) {
        if (pos < that.pos) return true;
        if (pos > that.pos) return false;

        if (level < that.level) return true;
        if (level > that.level) return false;

        if (val < that.val) return true;
        if (val > that.val) return false;

        return false;
    }
};

class Solution {
private:
    vector <Node> Index;

    void dfs(TreeNode *root, int level, int pos) {
        if (!root) return;

        Index.push_back({root->val, level, pos});
        dfs(root->left, level + 1, pos - 1);
        dfs(root->right, level + 1, pos + 1);
    }

public:
    vector <vector<int>> verticalTraversal(TreeNode *root) {
        dfs(root, 0, 0);
        sort(Index.begin(), Index.end());

        int curPos = Index[0].pos;
        vector <vector<int>> ans = {{}};
        for (Node node : Index) {
            if (node.pos != curPos) {
                ans.push_back({});
                curPos = node.pos;
            }
            ans.back().push_back(node.val);
        }
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
