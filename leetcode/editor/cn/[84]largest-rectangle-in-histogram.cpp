//Given n non-negative integers representing the histogram's bar height where th
//e width of each bar is 1, find the area of largest rectangle in the histogram. 
//
// 
//
// 
//Above is a histogram where width of each bar is 1, given height = [2,1,5,6,2,3
//]. 
//
// 
//
// 
//The largest rectangle is shown in the shaded area, which has area = 10 unit. 
//
// 
//
// Example: 
//
// 
//Input: [2,1,5,6,2,3]
//Output: 10
// 
// Related Topics 栈 数组


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
public:
    int largestRectangleArea(vector<int> &heights) {
        vector<int> S;
        int minH = INT_MAX;
        int maxArea = 0;
        for (int right = 0; right < heights.size(); ++right) {
            minH = min(minH, heights[right]);
            if (S.empty() || heights[S.back()] <= heights[right]) S.push_back(right);
            else {
                while (!S.empty() && heights[S.back()] > heights[right]) {
                    int left, mid = S.back();
                    S.pop_back();
                    if (S.empty()) left = -1;
                    else left = S.back();
                    maxArea = max(maxArea, (right - left - 1) * heights[mid]);
                }
                S.push_back(right);
            }
        }

        while (!S.empty()) {
            int left, mid = S.back();
            S.pop_back();
            if (S.empty()) left = -1;
            else left = S.back();
            maxArea = max(maxArea, ((int) heights.size() - left - 1) * heights[mid]);
        }

        return maxArea;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
