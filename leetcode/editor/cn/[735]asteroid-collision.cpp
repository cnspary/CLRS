//
//We are given an array asteroids of integers representing asteroids in a row.
// 
//For each asteroid, the absolute value represents its size, and the sign repres
//ents its direction (positive meaning right, negative meaning left). Each asteroi
//d moves at the same speed.
// 
//Find out the state of the asteroids after all collisions. If two asteroids mee
//t, the smaller one will explode. If both are the same size, both will explode. T
//wo asteroids moving in the same direction will never meet.
// 
//
// Example 1: 
// 
//Input: 
//asteroids = [5, 10, -5]
//Output: [5, 10]
//Explanation: 
//The 10 and -5 collide resulting in 10.  The 5 and 10 never collide.
// 
// 
//
// Example 2: 
// 
//Input: 
//asteroids = [8, -8]
//Output: []
//Explanation: 
//The 8 and -8 collide exploding each other.
// 
// 
//
// Example 3: 
// 
//Input: 
//asteroids = [10, 2, -5]
//Output: [10]
//Explanation: 
//The 2 and -5 collide resulting in -5.  The 10 and -5 collide resulting in 10.
// 
// 
//
// Example 4: 
// 
//Input: 
//asteroids = [-2, -1, 1, 2]
//Output: [-2, -1, 1, 2]
//Explanation: 
//The -2 and -1 are moving left, while the 1 and 2 are moving right.
//Asteroids moving the same direction never meet, so no asteroids will meet each
// other.
// 
// 
//
// Note:
// The length of asteroids will be at most 10000. 
// Each asteroid will be a non-zero integer in the range [-1000, 1000].. 
// Related Topics 栈


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
public:
    vector<int> asteroidCollision(vector<int> &asteroids) {
        stack<int> S;
        vector<int> ans;
        for (int i = 0; i < asteroids.size(); ++i) {
            if (asteroids[i] < 0) {
                bool dismiss = false;
                while (!S.empty()) {
                    if (S.top() == -asteroids[i]) {
                        S.pop();
                        ans.pop_back();
                        dismiss = true;
                        break;
                    } else if (S.top() > -asteroids[i]) {
                        dismiss = true;
                        break;
                    } else {
                        S.pop();
                        ans.pop_back();
                    }
                }
                if (!dismiss) ans.push_back(asteroids[i]);

            } else {
                S.push(asteroids[i]);
                ans.push_back(asteroids[i]);
            }
        }
        return ans;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
