//
// Created by cnspary on 2020/3/19.
//

#include "bits/stdc++.h"

using namespace std;

void solver(vector<int> Ropes) {
    sort(Ropes.begin(), Ropes.end());
    double sum = Ropes[0];
    for (int i = 0; i < Ropes.size(); ++i)
        sum = ((sum + Ropes[i]) * 1.0) / 2;

    printf("%d", int(sum));
    return;
}

int main() {
    int N;
    vector<int> Ropes;
    scanf("%d", &N);
    Ropes.resize(N);
    for (int i = 0; i < N; ++i)
        scanf("%d", &Ropes[i]);
    solver(Ropes);
    return 0;
}