//
// Created by cnspary on 2020/2/29.
//
#include "bits/stdc++.h"

using namespace std;

struct Course {
    int id;
    vector<string> students;
};

void solver(vector<Course> info, vector<string> query) {
    map<string, vector<int>> HT;
    int id;
    string name;
    for (int i = 0; i < info.size(); ++i) {
        id = info[i].id;
        for (int j = 0; j < info[i].students.size(); ++j) {
            name = info[i].students[j];
            HT[name].push_back(id);
        }
    }

    for (int i = 0; i < query.size(); ++i) {
        name = query[i];
        if (HT.find(name) == HT.end()) {
            cout << name << " " << 0;
        } else {
            cout << name << " " << HT[name].size();
            sort(HT[name].begin(), HT[name].end(), less<int>());
            for (int j = 0; j < HT[name].size(); ++j)
                cout << " " << HT[name][j];
        }
        if (i != query.size() - 1)
            cout << endl;
    }
    return;
}

int main() {
    ios::sync_with_stdio(false);
    int N, K;
    cin >> N >> K;
    vector<Course> info;
    vector<string> query;
    int id, Ni;
    string name;
    for (int i = 0; i < K; ++i) {
        cin >> id >> Ni;
        Course c;
        c.id = id;
        for (int j = 0; j < Ni; ++j) {
            cin >> name;
            c.students.push_back(name);
        }
        info.push_back(c);
    }

    for (int i = 0; i < N; ++i) {
        cin >> name;
        query.push_back(name);
    }

    solver(info, query);
    return 0;
}