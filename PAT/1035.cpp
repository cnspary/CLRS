//
// Created by cnspary on 2020/2/28.
//

#include "bits/stdc++.h"

using namespace std;

struct Password {
    string user;
    string password;

    Password(string u, string p) {
        user = u;
        password = p;
    }
};

bool modify(string &s) {
    bool modify = false;
    for (int i = 0; i < s.size(); ++i) {
        if (s[i] == '1') {
            s[i] = '@';
            modify = true;
        } else if (s[i] == '0') {
            s[i] = '%';
            modify = true;
        } else if (s[i] == 'l') {
            s[i] = 'L';
            modify = true;
        } else if (s[i] == 'O') {
            s[i] = 'o';
            modify = true;
        }
    }
    return modify;
}

void solver(vector<Password> pwds) {
    int cnt = 0;
    vector<Password> ans;
    for (int i = 0; i < pwds.size(); ++i) {
        if (modify(pwds[i].password))
            ans.push_back(pwds[i]);
        else
            cnt++;
    }

    if (ans.size() != 0) {
        cout << ans.size();
        for (int i = 0; i < ans.size(); ++i)
            cout << endl << ans[i].user << " " << ans[i].password;
    } else if (cnt == 1)
        cout << "There is 1 account and no account is modified";
    else
        cout << "There are " << cnt << " accounts and no account is modified";

    return;
}

int main() {
    ios::sync_with_stdio(false);
    int N;
    cin >> N;
    vector<Password> pwds;
    string u, p;
    for (int i = 0; i < N; ++i) {
        cin >> u >> p;
        pwds.push_back(Password(u, p));
    }

    solver(pwds);
    return 0;
}