//
// Created by cnspary on 2020/2/24.
//

#include "bits/stdc++.h"

using namespace std;

void solver(vector<vector<double>> odds) {
    double profit = 1;
    for (int i = 0; i < 3; ++i) {
        int best = 0;
        for (int j = 1; j < 3; ++j) {
            if (odds[i][j] > odds[i][best])
                best = j;
        }
        profit *= odds[i][best];
        switch (best) {
            case 0:
                printf("W ");
                break;
            case 1:
                printf("T ");
                break;
            case 2:
                printf("L ");
                break;
        }
    }
    profit = (profit * 0.65 - 1) * 2;
    printf("%.2lf", profit);
    return;
}

int main() {
    vector<vector<double>> odds;
    for (int i = 0; i < 3; ++i) {
        double w, t, l;
        scanf("%lf %lf %lf", &w, &t, &l);
        odds.push_back({w, t, l});
    }

    solver(odds);
    return 0;
}