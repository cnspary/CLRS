//
// Created by cnspary on 2020/2/27.
//

#include "bits/stdc++.h"

using namespace std;

struct Edge {
    int from;
    int to;
    int weight;
    int cost;

    Edge(int f, int t, int w, int c) :
            from(f), to(t), weight(w), cost(c) {};
};

vector<vector<Edge>> Graph;
vector<int> visited;

string miniPath = "";
int miniDis = INT_MAX;
int miniCost = INT_MAX;

void dfs(int c, int D, int dis, int cost, string path) {
    if (c == D && (dis < miniDis || (dis == miniDis && cost < miniCost))) {
        miniDis = dis;
        miniCost = cost;
        miniPath = path;
        return;
    }

    for (int i = 0; i < Graph[c].size() && dis < miniDis; ++i) {
        int next = Graph[c][i].to;
        if (visited[next] != 1) {
            visited[next] = 1;
            dfs(next, D, dis + Graph[c][i].weight, cost + Graph[c][i].cost, path + " " + to_string(next));
            visited[next] = 0;
        }
    }
    return;

}

int main() {
    int N, M, S, D;
    scanf("%d %d %d %d", &N, &M, &S, &D);
    Graph.resize(N);
    visited.resize(N, 0);
    for (int i = 0; i < M; ++i) {
        int f, t, w, c;
        scanf("%d %d %d %d", &f, &t, &w, &c);
        Graph[f].push_back(Edge(f, t, w, c));
        Graph[t].push_back(Edge(t, f, w, c));
    }
    dfs(S, D, 0, 0, to_string(S));
    cout << miniPath << " " << miniDis << " " << miniCost;
    return 0;
}