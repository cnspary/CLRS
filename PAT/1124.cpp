//
// Created by cnspary on 2020/3/19.
//

#include "bits/stdc++.h"

using namespace std;
int M, N, S;
vector<string> Forwards;

void solver() {
    set<string> winner;
    bool hasWinner = false;
    for (int i = S; i < Forwards.size(); i += N) {
        while (i < Forwards.size() && winner.find(Forwards[i]) != winner.end())
            i++;
        if (i < Forwards.size()) {
            hasWinner = true;
            cout << Forwards[i] << "\n";
            winner.insert(Forwards[i]);
        }
    }

    if (!hasWinner) printf("Keep going...");
    return;
}

int main() {
    cin >> M >> N >> S;
    S--;
    string s;
    for (int i = 0; i < M; ++i) {
        cin >> s;
        Forwards.push_back(s);
    }
    solver();
    return 0;
}