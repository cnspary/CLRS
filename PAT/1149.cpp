//
// Created by cnspary on 2020/3/17.
//

#include "bits/stdc++.h"

using namespace std;

int M, N;
map<int, set<int>> InCompatible;
vector<vector<int>> Containers;

void solver() {
    for (auto container : Containers) {
        bool isOk = true;
        for (int i = 0; i < container.size(); ++i) {
            if (InCompatible.find(container[i]) == InCompatible.end())
                continue;
            for (int j = i + 1; j < container.size(); ++j)
                if (InCompatible[container[i]].find(container[j]) != InCompatible[container[i]].end()) {
                    isOk = false;
                    break;
                }
            if (!isOk) break;
        }

        if (isOk) printf("Yes\n");
        else printf("No\n");
    }
    return;
}

int main() {
    scanf("%d %d", &M, &N);
    for (int i = 0; i < M; ++i) {
        int a, b;
        scanf("%d %d", &a, &b);
        InCompatible[a].insert(b);
        InCompatible[b].insert(a);
    }
    Containers.resize(N);
    for (int i = 0; i < N; ++i) {
        int k;
        scanf("%d", &k);
        Containers[i].resize(k);
        for (int j = 0; j < k; ++j)
            scanf("%d", &Containers[i][j]);
    }
    solver();
    return 0;
}