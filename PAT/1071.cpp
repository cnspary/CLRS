//
// Created by cnspary on 2020/3/10.
//

#include "bits/stdc++.h"

using namespace std;

bool isAlphanumerical(char c) {
    if ('0' <= c && c <= '9') return true;
    else if ('a' <= c && c <= 'z') return true;
    return false;
}

void solver(string s) {
    transform(s.begin(), s.end(), s.begin(), ::tolower);
    vector<string> words;
    int l = 0, r = 0;
    while (r < s.size()) {
        if (isAlphanumerical(s[r])) r++;
        else {
            words.push_back(s.substr(l, r - l));
            while (r < s.size() && !isAlphanumerical(s[r])) r++;
            l = r;
        }
    }
    if (l != r) words.push_back(s.substr(l, r - l));

    map<string, int> ht;
    for (string s : words) {
        if (ht.find(s) == ht.end()) ht[s] = 1;
        else ht[s]++;
    }

    int maxCount = 0;
    string maxString = "";
    for (auto x: ht)
        if (x.second > maxCount) {
            maxString = x.first;
            maxCount = x.second;
        }

    cout << maxString << " " << maxCount;
    return;
}

int main() {
    string s;
    getline(cin, s);
    solver(s);
    return 0;
}