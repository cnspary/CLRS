//
// Created by cnspary on 2020/3/20.
//

#include "bits/stdc++.h"

using namespace std;

string getMirror(string s) {
    string ans;
    for (int i = s.size() - 1; -1 < i; --i)
        ans += s[i];
    return ans;
}

bool isPalindrome(string s) {
    int length = s.size();
    for (int i = 0; i < length / 2; ++i)
        if (s[i] != s[length - 1 - i]) return false;
    return true;
}

string add(string s1, string s2) {
    int bitCarry = 0;
    int sum;
    string ans = "";
    for (int i = s1.size() - 1; -1 < i; --i) {
        sum = s1[i] - '0' + s2[i] - '0' + bitCarry;
        bitCarry = sum / 10;
        sum = sum % 10;
        ans.insert(0, 1, '0' + sum);
    }
    if (bitCarry != 0)
        ans.insert(0, 1, '0' + bitCarry);

    return ans;
}


void solver(string Digit) {
    if (isPalindrome(Digit)) {
        cout << Digit << " is a palindromic number.";
        return;
    }

    for (int i = 0; i < 10; ++i) {
        string mDigit = getMirror(Digit);
        cout << Digit << " + " << mDigit << " = ";
        Digit = add(Digit, mDigit);
        cout << Digit << "\n";
        if (isPalindrome(Digit)) {
            cout << Digit << " is a palindromic number.";
            return;
        }
    }
    cout << "Not found in 10 iterations.";
    return;
}

int main() {
    string Digit;
    cin >> Digit;
    solver(Digit);
    return 0;
}