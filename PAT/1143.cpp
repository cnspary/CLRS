//
// Created by cnspary on 2020/3/17.
//

#include "bits/stdc++.h"

using namespace std;

int N, M;
set<int> T;
vector<int> pre;
vector<pair<int, int>> query;

void LCA(int a, int b) {
    for (int rid = 0; rid < pre.size();) {
        if (a < pre[rid] && b > pre[rid]) {
            printf("LCA of %d and %d is %d.\n", a, b, pre[rid]);
            return;
        } else if (a > pre[rid] && b < pre[rid]) {
            printf("LCA of %d and %d is %d.\n", a, b, pre[rid]);
            return;
        } else if (a < pre[rid] && b < pre[rid])
            rid++;
        else if (a > pre[rid] && b > pre[rid]) {
            int root = pre[rid];
            while (root >= pre[rid]) rid++;
        } else if (a == pre[rid]) {
            printf("%d is an ancestor of %d.\n", a, b);
            return;
        } else if (b == pre[rid]) {
            printf("%d is an ancestor of %d.\n", b, a);
            return;
        }
    }
    return;
}

void solver() {
    for (int x : pre)
        T.insert(x);

    for (int i = 0; i < query.size(); ++i) {
        int a = query[i].first, b = query[i].second;
        if (T.find(a) == T.end() && T.find(b) == T.end())
            printf("ERROR: %d and %d are not found.\n", a, b);
        else if (T.find(a) == T.end())
            printf("ERROR: %d is not found.\n", a);
        else if (T.find(b) == T.end())
            printf("ERROR: %d is not found.\n", b);
        else LCA(a, b);
    }

    return;
}

int main() {
    scanf("%d %d", &N, &M);
    pre.resize(M);
    for (int i = 0; i < M; ++i)
        scanf("%d", &pre[i]);

    for (int i = 0; i < N; ++i) {
        int a, b;
        scanf("%d %d", &a, &b);
        query.push_back({a, b});
    }
    solver();
    return 0;
}