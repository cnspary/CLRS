//
// Created by cnspary on 2020/3/18.
//

#include "bits/stdc++.h"

using namespace std;

int N;
vector<int> nums;

void find(int &m, int &n) {
    int diff = INT_MAX;
    for (int i = 1; i <= sqrt(N); ++i) {
        if (N % i == 0) {
            int a = i, b = N / i;
            if (b - a < diff) {
                m = b;
                n = a;
                diff = b - a;
            }
        }
    }
    return;
}

void solver() {
    int m, n;
    find(m, n);
    sort(nums.begin(), nums.end(), greater<int>());
    vector<vector<int>> SM(m, vector<int>(n));

    int ROW1 = 0, ROW2 = m - 1, COL1 = 0, COL2 = n - 1;
    int i = 0, r = 0, c = 0;

    while (true) {
        r = ROW1;
        c = COL1;
        for (; c <= COL2; ++c)
            SM[r][c] = nums[i++];
        ROW1++;
        if (i == N) break;

        r = ROW1;
        c = COL2;
        for (; r <= ROW2; ++r)
            SM[r][c] = nums[i++];
        COL2--;
        if (i == N) break;

        r = ROW2;
        c = COL2;
        for (; COL1 <= c; --c)
            SM[r][c] = nums[i++];
        ROW2--;
        if (i == N) break;

        r = ROW2;
        c = COL1;
        for (; ROW1 <= r; --r)
            SM[r][c] = nums[i++];
        COL1++;
        if (i == N) break;
    }

    for (int i = 0; i < m; ++i) {
        for (int j = 0; j < n; ++j) {
            printf("%d", SM[i][j]);
            if (j != n - 1) printf(" ");
        }
        printf("\n");
    }

    return;
}

int main() {
    scanf("%d", &N);
    nums.resize(N);
    for (int i = 0; i < N; ++i)
        scanf("%d", &nums[i]);
    solver();
    return 0;
}