//
// Created by cnspary on 2020/2/24.
//

#include "bits/stdc++.h"

using namespace std;

void solve(int N, vector<vector<int>> param) {
    vector<int> degree(N, 0);
    vector<int> level(N, 0);
    int maxLevel = 0;
    for (int p = 0; p < N; ++p) {
        int d = param[p].size();
        degree[p] = d;
        for (int j = 0; j < param[p].size(); ++j) {
            int s = param[p][j];
            level[s] = level[p] + 1;
            maxLevel = max(maxLevel, level[s]);
        }
    }

    vector<int> ans(maxLevel + 1, 0);
    for (int i = 0; i < N; ++i)
        if (degree[i] == 0)
            ans[level[i]]++;

    for (int i = 0; i < ans.size() - 1; ++i)
        printf("%d ", ans[i]);
    printf("%d", ans.back());
    return;
}

int main() {
    int N, M;
    scanf("%d %d", &N, &M);
    vector<vector<int>> param(N);

    for (int i = 0; i < M; ++i) {
        int p, d, s;
        scanf("%d %d", &p, &d);
        for (int j = 0; j < d; ++j) {
            scanf("%d", &s);
            param[p - 1].push_back(s - 1);
        }
    }
    solve(N, param);
    return 0;
}