//
// Created by cnspary on 2020/2/27.
//

#include "bits/stdc++.h"

using namespace std;

struct Book {
    string id;
    string title;
    string author;
    string key_words;
    string publisher;
    string year;

    Book(string a, string b, string c,
         string d, string e, string f) :
            id(a), title(b), author(c),
            key_words(d), publisher(e), year(f) {}
};

map<string, set<string>> TitleMap;
map<string, set<string>> AuthorMap;
map<string, set<string>> KeyWordMap;
map<string, set<string>> PublisherMap;
map<string, set<string>> YearMap;

void buildIndex(vector<Book> books) {
    for (Book b : books) {
        TitleMap[b.title].insert(b.id);
        AuthorMap[b.author].insert(b.id);

        string keywords = b.key_words;
        while (keywords.find(" ") != -1) {
            int index = keywords.find(" ");
            KeyWordMap[keywords.substr(0, index)].insert(b.id);
            keywords = keywords.substr(index + 1, keywords.length());
        }
        KeyWordMap[keywords].insert(b.id);

        PublisherMap[b.publisher].insert(b.id);
        YearMap[b.year].insert(b.id);
    }
    return;
}

void printAns(set<string> searchSet) {
    set<string>::iterator Iter;
    Iter = searchSet.begin();
    printf("%s", (*Iter).c_str());
    for (Iter = ++searchSet.begin(); Iter != searchSet.end(); ++Iter)
        printf("\n%s", (*Iter).c_str());
}

void solver(vector<Book> books, vector<pair<int, string>> query) {
    buildIndex(books);
    set<string> searchSet;
    for (int i = 0; i < query.size(); ++i) {
        printf("%d: %s\n", query[i].first, query[i].second.c_str());
        switch (query[i].first) {
            case 1:
                if (TitleMap.find(query[i].second) != TitleMap.end()) {
                    searchSet = TitleMap[query[i].second];
                    printAns(searchSet);
                } else printf("Not Found");
                break;
            case 2:
                if (AuthorMap.find(query[i].second) != AuthorMap.end()) {
                    searchSet = AuthorMap[query[i].second];
                    printAns(searchSet);
                } else printf("Not Found");
                break;
            case 3:
                if (KeyWordMap.find(query[i].second) != KeyWordMap.end()) {
                    searchSet = KeyWordMap[query[i].second];
                    printAns(searchSet);
                } else printf("Not Found");
                break;
            case 4:
                if (PublisherMap.find(query[i].second) != PublisherMap.end()) {
                    searchSet = PublisherMap[query[i].second];
                    printAns(searchSet);
                } else printf("Not Found");
                break;
            case 5:
                if (YearMap.find(query[i].second) != YearMap.end()) {
                    searchSet = YearMap[query[i].second];
                    printAns(searchSet);
                } else printf("Not Found");
                break;
        }
        if (i < query.size() - 1)
            printf("\n");
    }
    return;
}

int main() {
    int N, M;
    cin >> N;
    vector<Book> books;
    vector<pair<int, string>> query;

    string id, title, author, key_words, publisher, year;
    getchar();
    for (int i = 0; i < N; ++i) {
        getline(cin, id);
        getline(cin, title);
        getline(cin, author);
        getline(cin, key_words);
        getline(cin, publisher);
        getline(cin, year);
        books.push_back(Book(id, title, author, key_words, publisher, year));
    }

    cin >> M;
    for (int i = 0; i < M; ++i) {
        int type;
        string condition;
        scanf("%d: ", &type);
        getline(cin, condition);
        query.push_back({type, condition});
    }
    solver(books, query);
    return 0;
}