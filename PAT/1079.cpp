//
// Created by cnspary on 2020/3/12.
//

#include "bits/stdc++.h"

using namespace std;

int N;
double P, r;
vector<vector<int>> DirectGaph;
map<int, pair<int, int>> Query;

void solver() {
    queue<pair<int, int>> Q;
    Q.push({0, 0});

    while (!Q.empty()) {
        int p = Q.front().first;
        int d = Q.front().second;
        Q.pop();

        if (DirectGaph[p].size() == 0)
            Query[p].second = d;

        for (int c : DirectGaph[p])
            Q.push({c, d + 1});
    }

    double totalSales = 0;
    for (auto retailer : Query) {
        int products = retailer.second.first;
        int level = retailer.second.second;
        totalSales += products * pow(1 + r / 100, level) * P;
    }

    printf("%.1lf", totalSales);
    return;
}

int main() {
    scanf("%d %lf %lf", &N, &P, &r);
    DirectGaph.resize(N);
    for (int i = 0; i < N; ++i) {
        int c, n;
        scanf("%d", &c);

        if (c == 0) {
            scanf("%d", &n);
            Query.insert({i, {n, 0}});
        } else {
            for (int j = 0; j < c; ++j) {
                scanf("%d", &n);
                DirectGaph[i].push_back(n);
            }
        }
    }
    solver();
    return 0;
}