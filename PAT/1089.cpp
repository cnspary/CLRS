//
// Created by cnspary on 2020/3/16.
//

#include "bits/stdc++.h"

using namespace std;

vector<int> init;
vector<int> process;

void solver() {
    bool isMerge = false;
    int i;
    for (i = 0; i < process.size() - 1; ++i)
        if (process[i] > process[i + 1]) break;
    for (i = i + 1; i < process.size(); ++i)
        if (init[i] != process[i]) {
            isMerge = true;
            break;
        }

    if (!isMerge) {
        printf("Insertion Sort\n");
        int i, tmp;
        for (i = 1; i < process.size(); ++i)
            if (process[i] < process[i - 1]) break;
        tmp = process[i];
        while (0 < i && tmp < process[i - 1]) {
            process[i] = process[i - 1];
            i--;
        }
        process[i] = tmp;
    } else {
        printf("Merge Sort\n");
        int mgI = 1;
        bool ContinueSort = true;
        while (ContinueSort) {
            for (int i = 0; i < init.size(); i += mgI) {
                if (i + mgI < init.size()) sort(init.begin() + i, init.begin() + i + mgI);
                else sort(init.begin() + i, init.end());
            }
            ContinueSort = false;
            for (int i = 0; i < init.size(); ++i) {
                if (init[i] != process[i]) {
                    ContinueSort = true;
                    mgI *= 2;
                    break;
                }
            }
        }
        mgI *= 2;
        for (int i = 0; i < process.size(); i += mgI) {
            if (i + mgI < process.size()) sort(process.begin() + i, process.begin() + i + mgI);
            else sort(process.begin() + i, process.end());
        }
    }

    for (int i = 0; i < process.size(); ++i) {
        printf("%d", process[i]);
        if (i != process.size() - 1) printf(" ");
    }
    return;
}

int main() {
    int N;
    scanf("%d", &N);

    init.resize(N);
    process.resize(N);

    for (int i = 0; i < N; ++i)
        scanf("%d", &init[i]);

    for (int i = 0; i < N; ++i)
        scanf("%d", &process[i]);

    solver();
    return 0;
}