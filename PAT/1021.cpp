//
// Created by cnspary on 2020/2/26.
//

#include "bits/stdc++.h"

using namespace std;

class UnionFind {
private:
    int cnt;
    vector<int> uf;
    vector<int> rank;

public:
    UnionFind(int n) {
        cnt = n;
        uf = vector<int>(n);
        rank = vector<int>(n, 1);
        for (int i = 0; i < n; ++i)
            uf[i] = i;
    }

    int find(int a) {
        int p = a;
        while (uf[a] != a) a = uf[a];
        int root = uf[a];
        while (uf[p] != root) {
            int x = uf[p];
            uf[p] = root;
            p = x;
        }
        return root;
    }

    bool uunion(int a, int b) {
        int r1 = find(a);
        int r2 = find(b);
        if (r1 == r2) return false;

        cnt--;
        if (rank[r1] < rank[r2]) {
            uf[r1] = r2;
            rank[r2] += rank[r1];
        } else {
            uf[r2] = r1;
            rank[r1] += rank[r2];
        }
        return true;
    }

    int getRank(int i) {
        return rank[find(i)];
    }

    int count() {
        return cnt;
    }

    bool connected(int x, int y) {
        return find(x) == find(y);
    }
};

struct BFSearchNode {
    int id;
    int deep;

    BFSearchNode(int i, int d) : id(i), deep(d) {}
};

vector<vector<int>> Graph;
vector<int> visited;
vector<int> Deep;
int maxDeep;
queue<BFSearchNode> Q;

void BFS(int c) {
    visited[c] = 1;
    Q.push(BFSearchNode(c, 0));

    while (!Q.empty()) {
        auto x = Q.front();
        Q.pop();
        if (x.deep > maxDeep)
            maxDeep = x.deep;
        for (int i = 0; i < Graph[x.id].size(); ++i) {
            int next = Graph[x.id][i];
            if (visited[next] == 0) {
                visited[next] = 1;
                Deep[next] = x.deep + 1;
                Q.push(BFSearchNode{next, x.deep + 1});
            }
        }
    }
}

void solver(int N) {
    UnionFind uf(N);

    for (int i = 0; i < Graph.size(); ++i)
        for (int j = 0; j < Graph[i].size(); ++j)
            uf.uunion(i, Graph[i][j]);

    if (uf.count() > 1) {
        printf("Error: %d components", uf.count());
        return;
    }

    set<int> ans;

    visited.resize(N, 0);
    Deep.resize(N, 0);
    maxDeep = -1;
    BFS(0);
    for (int i = 0; i < Deep.size(); ++i)
        if (Deep[i] == maxDeep)
            ans.insert(i);

    visited.clear();
    Deep.clear();
    visited.resize(N, 0);
    Deep.resize(N, 0);
    maxDeep = -1;
    BFS(*ans.begin());
    for (int i = 0; i < Deep.size(); ++i)
        if (Deep[i] == maxDeep)
            ans.insert(i);

    set<int>::iterator Iter = ans.begin();
    cout << *Iter + 1;
    for (Iter = ++ans.begin(); Iter != ans.end(); ++Iter)
        cout << endl << *Iter + 1;
    return;
}

int main() {
    int N;
    scanf("%d", &N);

    Graph.resize(N);

    for (int i = 1; i < N; ++i) {
        int a, b;
        scanf("%d %d", &a, &b);
        Graph[a - 1].push_back(b - 1);
        Graph[b - 1].push_back(a - 1);
    }
    solver(N);
    return 0;
}