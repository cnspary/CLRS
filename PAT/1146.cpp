//
// Created by cnspary on 2020/3/17.
//

#include "bits/stdc++.h"

using namespace std;

int N, M, K;
vector<vector<int>> Graph;
vector<int> Degree;
vector<vector<int>> Tops;

void solver() {
    vector<int> ans;
    for (int n = 0; n < Tops.size(); ++n) {
        vector<int> D = Degree;
        for (int x : Tops[n]) {
            if (D[x] != 0) {
                ans.push_back(n);
                break;
            } else {
                for (int neg : Graph[x])
                    D[neg]--;
            }
        }
    }
    for (int i = 0; i < ans.size(); ++i) {
        printf("%d", ans[i]);
        if (i != ans.size() - 1) printf(" ");
    }
    return;
}

int main() {
    scanf("%d %d", &N, &M);
    Graph.resize(N + 1);
    Degree.resize(N + 1, 0);
    for (int i = 0; i < M; ++i) {
        int f, t;
        scanf("%d %d", &f, &t);
        Graph[f].push_back(t);
        Degree[t]++;
    }

    scanf("%d", &K);
    Tops.resize(K, vector<int>(N));
    for (int i = 0; i < K; ++i) {
        for (int j = 0; j < N; ++j)
            scanf("%d", &Tops[i][j]);
    }
    solver();
    return 0;
}