//
// Created by cnspary on 2020/2/24.
//

#include "bits/stdc++.h"

using namespace std;

void solver(vector<int> num) {
    int l = 0, r = 0;
    int maxSum = -1;
    int sum = 0;
    for (int i = 0, j = 0; j < num.size();) {
        sum += num[j];
        if (sum < 0) {
            i = j + 1;
            j = j + 1;
            sum = 0;
        } else {
            if (sum > maxSum) {
                l = i;
                r = j;
                maxSum = sum;
            }
            j++;
        }
    }

    if (maxSum < 0)
        cout << 0 << " " << num[0] << " " << num.back();
    else
        cout << maxSum << " " << num[l] << " " << num[r];
    return;
}

int main() {
    int N, x;
    scanf("%d", &N);
    vector<int> num;
    for (int i = 0; i < N; ++i) {
        scanf("%d", &x);
        num.push_back(x);
    }

    solver(num);

    return 0;
}