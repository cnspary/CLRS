//
// Created by cnspary on 2020/3/3.
//

#include "bits/stdc++.h"

using namespace std;

void solver(vector<int> diamonds, int M) {
    int minOffset = INT_MAX;
    int sum = 0;
    vector<pair<int, int>> ans;
    for (int l = 0, r = 0; r < diamonds.size();) {
        while (r < diamonds.size() && sum < M) sum += diamonds[r++];
        while (sum - diamonds[l] >= M) sum -= diamonds[l++];

        if (sum - M < minOffset) {
            minOffset = sum - M;
            ans.clear();
            ans.push_back({l + 1, r});
        } else if (sum - M == minOffset)
            ans.push_back({l + 1, r});

        sum -= diamonds[l++];
    }

    printf("%d-%d", ans[0].first, ans[0].second);
    for (int i = 1; i < ans.size(); ++i)
        printf("\n%d-%d", ans[i].first, ans[i].second);
    return;
}

int main() {
    int N, M;
    scanf("%d %d", &N, &M);
    vector<int> diamonds(N);
    for (int i = 0; i < N; ++i)
        scanf("%d", &diamonds[i]);

    solver(diamonds, M);
    return 0;
}