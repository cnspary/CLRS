//
// Created by cnspary on 2020/3/3.
//

#include "bits/stdc++.h"

using namespace std;

void solver(vector<int> f, vector<int> c) {
    map<int, int> fs;
    fs.insert({-1, -1});
    for (int i = 0; i < f.size(); ++i)
        fs.insert({f[i], i});

    vector<int> s;
    s.push_back(-1);
    for (int x:c)
        if (fs.find(x) != fs.end())
            s.push_back(x);

    vector<int> dp(s.size(), 0);
    int maxLength = 0;
    for (int i = 1; i < s.size(); ++i) {
        int j = i - 1;
        while (fs[s[j]] > fs[s[i]]) j--;
        dp[i] = dp[j] + 1;
        maxLength = max(maxLength, dp[i]);
    }

    printf("%d", maxLength);
    return;
}

int main() {
    int N;
    scanf("%d", &N);

    int K;
    scanf("%d", &K);
    vector<int> f(K);
    for (int i = 0; i < K; ++i)
        scanf("%d", &f[i]);

    scanf("%d", &K);
    vector<int> c(K);
    for (int i = 0; i < K; ++i)
        scanf("%d", &c[i]);


    solver(f, c);
    return 0;
}