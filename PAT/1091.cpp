//
// Created by cnspary on 2020/3/16.
//

#include "bits/stdc++.h"

using namespace std;

int M, N, L, K;
vector<vector<vector<int>>> Image;
vector<vector<int>> pos = {{1,  0,  0},
                           {-1, 0,  0},
                           {0,  1,  0},
                           {0,  0,  1},
                           {0,  -1, 0},
                           {0,  0,  -1}};

int BFS(int l, int m, int n) {
    int cnt = 0;

    queue<vector<int>> Q;
    Image[l][m][n] = 0;
    Q.push({l, m, n});

    while (!Q.empty()) {
        cnt++;
        int l = Q.front()[0];
        int m = Q.front()[1];
        int n = Q.front()[2];
        Q.pop();

        for (auto p : pos) {
            int ll = l + p[0];
            int mm = m + p[1];
            int nn = n + p[2];

            if (-1 < ll && ll < L && -1 < mm && mm < M &&
                -1 < nn && nn < N && Image[ll][mm][nn] == 1) {
                Image[ll][mm][nn] = 0;
                Q.push({ll, mm, nn});
            }
        }
    }
    return cnt;
}

void solver() {
    int cnt = 0;
    for (int l = 0; l < L; ++l)
        for (int m = 0; m < M; ++m)
            for (int n = 0; n < N; ++n) {
                if (Image[l][m][n] == 1) {
                    int stroke = 0;
                    stroke = BFS(l, m, n);
                    if (stroke >= K) cnt += stroke;
                }
            }
    printf("%d", cnt);
    return;
}

int main() {
    scanf("%d %d %d %d", &M, &N, &L, &K);
    Image = vector<vector<vector<int>>>(L, vector<vector<int>>(M, vector<int>(N)));
    for (int l = 0; l < L; ++l)
        for (int m = 0; m < M; ++m)
            for (int n = 0; n < N; ++n)
                scanf("%d", &Image[l][m][n]);
    solver();
    return 0;
}