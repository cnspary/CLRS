//
// Created by cnspary on 2020/3/19.
//

#include "bits/stdc++.h"

using namespace std;

int N;
map<int, int> PreIndex, PostIndex;
vector<int> PreOrder, PostOrder, InOrder;

bool UNIQUE = true;

struct TreeNode {
    int val;
    TreeNode *lc, *rc;

    TreeNode(int v) : val(v), lc(nullptr), rc(nullptr) {}
};

void buildTree(TreeNode *root, int preRootIndex, int postRootIndex, int n) {
    if (n == 1) return;

    if (PreOrder[preRootIndex + 1] == PostOrder[postRootIndex - 1]) {
        UNIQUE = false;

        int LVal = PreOrder[preRootIndex + 1];
        root->lc = new TreeNode(LVal);
        buildTree(root->lc, PreIndex[LVal], PostIndex[LVal], n - 1);
    } else {
        int LVal = PreOrder[preRootIndex + 1];
        int RVal = PostOrder[postRootIndex - 1];

        int LSize = PreIndex[RVal] - PreIndex[LVal];
        int RSize = PostIndex[RVal] - PostIndex[LVal];

        root->lc = new TreeNode(LVal);
        root->rc = new TreeNode(RVal);

        buildTree(root->lc, PreIndex[LVal], PostIndex[LVal], LSize);
        buildTree(root->rc, PreIndex[RVal], PostIndex[RVal], RSize);
    }
    return;
}

void InTravel(TreeNode *root) {
    if (root->lc) InTravel(root->lc);
    InOrder.push_back(root->val);
    if (root->rc) InTravel(root->rc);
    return;
}

void solver() {
    TreeNode *root = new TreeNode(PreOrder[0]);
    buildTree(root, 0, PostOrder.size() - 1, N);
    InTravel(root);

    if (UNIQUE) printf("Yes\n");
    else printf("No\n");
    for (int i = 0; i < InOrder.size(); ++i) {
        printf("%d", InOrder[i]);
        if (i != InOrder.size() - 1) printf(" ");
    }
    printf("\n");
    return;
}

int main() {
    scanf("%d", &N);
    PreOrder.resize(N);
    PostOrder.resize(N);
    for (int i = 0; i < N; ++i) {
        scanf("%d", &PreOrder[i]);
        PreIndex.insert({PreOrder[i], i});
    }
    for (int i = 0; i < N; ++i) {
        scanf("%d", &PostOrder[i]);
        PostIndex.insert({PostOrder[i], i});
    }
    solver();
    return 0;
}