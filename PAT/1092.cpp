//
// Created by cnspary on 2020/3/16.
//

#include "bits/stdc++.h"

using namespace std;

void solver(string s1, string s2) {
    int extra = 0, miss = 0;
    map<char, int> Need;
    for (char c : s2)
        Need[c] += 1;

    for (char c : s1) {
        if (Need.find(c) == Need.end() || Need[c] == 0) extra++;
        else Need[c]--;
    }

    for (auto x : Need)
        if (x.second > 0) miss += x.second;

    if (miss > 0) printf("No %d", miss);
    else printf("Yes %d", extra);
    return;
}

int main() {
    string s1, s2;
    cin >> s1 >> s2;
    solver(s1, s2);
    return 0;
}