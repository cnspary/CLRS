//
// Created by cnspary on 2020/3/19.
//

#include "bits/stdc++.h"

using namespace std;

struct Institution {
    int rank;
    string name;
    int scoreB;
    int scoreA;
    int scoreT;
    int TWS;
    int testee;

    Institution(string n) {
        name = n;
        scoreB = 0;
        scoreA = 0;
        scoreT = 0;
        TWS = 0;
        testee = 0;
    }

    void calculate() {
        TWS = int((scoreB * 1.0) / 1.5 + scoreA * 1.0 + scoreT * 1.5);
    }
};

map<string, int> InsIndex;
vector<Institution> ins;

bool cmp(Institution a, Institution b) {
    int TWSa = int((a.scoreB * 1.0) / 1.5 + a.scoreA * 1.0 + a.scoreT * 1.5);
    int TWSb = int((b.scoreB * 1.0) / 1.5 + b.scoreA * 1.0 + b.scoreT * 1.5);

    if (TWSa > TWSb) return true;
    if (TWSa < TWSb) return false;

    if (a.testee < b.testee) return true;
    if (a.testee > b.testee) return false;

    return a.name < b.name;
}

void solver() {
    sort(ins.begin(), ins.end(), cmp);
    ins[0].rank = 1;
    ins[0].calculate();
    for (int i = 1; i < ins.size(); ++i) {
        ins[i].calculate();
        if (ins[i].TWS == ins[i - 1].TWS)
            ins[i].rank = ins[i - 1].rank;
        else ins[i].rank = i + 1;
    }

    cout << ins.size() << "\n";
    for (Institution i : ins)
        cout << i.rank << " "
             << i.name << " "
             << i.TWS << " "
             << i.testee << "\n";
    return;
}

int main() {
    int N;
    scanf("%d", &N);

    string ID, insName;
    int grade;
    for (int i = 0; i < N; ++i) {
        cin >> ID >> grade >> insName;
        transform(insName.begin(), insName.end(), insName.begin(), ::tolower);

        if (InsIndex.find(insName) == InsIndex.end()) {
            ins.push_back({insName});
            InsIndex.insert({insName, ins.size() - 1});
        }

        int index = InsIndex[insName];

        ins[index].testee++;
        if (ID[0] == 'B') ins[index].scoreB += grade;
        else if (ID[0] == 'A') ins[index].scoreA += grade;
        else if (ID[0] == 'T') ins[index].scoreT += grade;
    }
    solver();
    return 0;
}