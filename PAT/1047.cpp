//
// Created by cnspary on 2020/3/5.
//

#include "bits/stdc++.h"

using namespace std;

int main() {
    ios::sync_with_stdio(true);

    int N, M;
    cin >> N >> M;

    vector<vector<string>> course(M + 1);
    string name;
    int k, courseid;

    for (int i = 0; i < N; ++i) {
        cin >> name >> k;
        for (int j = 0; j < k; ++j) {
            cin >> courseid;
            course[courseid].push_back(name);
        }
    }

    for (int i = 1; i <= M; ++i) {
        sort(course[i].begin(), course[i].end());
        cout << to_string(i) + " " + to_string(course[i].size()) + "\n";
        for (string n:course[i])
            cout << n + "\n";
    }

    return 0;
}