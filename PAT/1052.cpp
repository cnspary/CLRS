//
// Created by cnspary on 2020/3/6.
//

#include "bits/stdc++.h"

using namespace std;

struct LinkNode {
    int val;
    int start;
    int next;

    LinkNode(int v, int s, int n) :
            val(v), start(s), next(n) {}
};

bool cmp(LinkNode a, LinkNode b) {
    return a.val < b.val;
}

map<int, int> HT;

void solver(vector<LinkNode> lists, int Si) {
    vector<LinkNode> ls;
    while (true) {
        ls.push_back(lists[Si]);
        if (lists[Si].next == -1) break;
        Si = HT[lists[Si].next];
    }
    sort(ls.begin(), ls.end(), cmp);
    for (int i = 0; i < ls.size() - 1; ++i)
        ls[i].next = ls[i + 1].start;
    ls.back().next = -1;

    printf("%d %05d\n", ls.size(), ls[0].start);
    for (int i = 0; i < ls.size() - 1; ++i)
        printf("%05d %d %05d\n", ls[i].start, ls[i].val, ls[i].next);
    printf("%05d %d %d\n", ls.back().start, ls.back().val, ls.back().next);
    return;
}

int main() {
    int N, S, Si;
    scanf("%d %d", &N, &S);

    if (S == -1) {
        printf("0 -1\n");
        return 0;
    }

    vector<LinkNode> lists;
    int s, v, n;
    for (int i = 0; i < N; ++i) {
        scanf("%d %d %d", &s, &v, &n);
        if (s == S) Si = i;
        HT.insert({s, i});
        lists.push_back({v, s, n});
    }

    solver(lists, Si);
    return 0;
}