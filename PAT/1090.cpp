//
// Created by cnspary on 2020/3/16.
//

#include "bits/stdc++.h"

using namespace std;
int N;
double P, r;
vector<int> connection;

void solver() {
    vector<vector<int>> T(N + 1);
    vector<int> Deepest;
    for (int i = 0; i < connection.size(); ++i) {
        int nid = i + 1;
        int pid = connection[i] + 1;
        T[pid].push_back(nid);
    }

    queue<pair<int, int>> Q;
    Q.push({0, -1});
    while (!Q.empty()) {
        int p = Q.front().first;
        int l = Q.front().second;
        Q.pop();

        if (T[p].size() == 0) Deepest.push_back(l);

        for (int i = 0; i < T[p].size(); ++i)
            Q.push({T[p][i], l + 1});
    }

    int cnt = 0;
    int deep = 0;
    for (int i = 0; i < Deepest.size(); ++i) {
        if (deep < Deepest[i]) {
            cnt = 1;
            deep = Deepest[i];
        } else if (deep == Deepest[i]) cnt++;
    }

    printf("%0.2lf %d", P * pow(1 + r / 100, deep), cnt);

    return;
}

int main() {
    scanf("%d %lf %lf", &N, &P, &r);
    connection.resize(N);
    for (int i = 0; i < N; ++i)
        scanf("%d", &connection[i]);
    solver();
    return 0;
}