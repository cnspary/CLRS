//
// Created by cnspary on 2020/3/15.
//

#include "bits/stdc++.h"

using namespace std;

void solver(int N) {
    int maxLength = 0, start = 0;
    for (int i = 2; i <= sqrt(N); ++i) {
        int j = i, count = 0;
        while (N % j == 0) {
            count++;
            j = j * (i + count);
        }
        if (maxLength < count) {
            maxLength = count;
            start = i;
        }
    }

    if (maxLength == 0) printf("1\n%d", N);
    else {
        printf("%d\n", maxLength);
        for (int i = 0; i < maxLength; ++i) {
            printf("%d", start + i);
            if (i != maxLength - 1) printf("*");
        }
    }
    return;
}

int main() {
    int N;
    scanf("%d", &N);
    solver(N);
    return 0;
}