//
// Created by cnspary on 2020/3/15.
//

#include "bits/stdc++.h"

using namespace std;

struct Grade {
    string name;
    string course;
    int grade;
    int needRank;

    Grade(string n, string c, int g) {
        name = n;
        course = c;
        grade = g;
        needRank = 0;
    }
};

bool cmp(Grade a, Grade b) {
    if (a.needRank > b.needRank) return true;
    if (a.needRank < b.needRank) return false;

    if (a.grade > b.grade) return true;
    return false;
}

void solver(vector<Grade> grades, int low, int high) {
    for (int i = 0; i < grades.size(); ++i)
        if (low <= grades[i].grade && grades[i].grade <= high)
            grades[i].needRank = 1;

    sort(grades.begin(), grades.end(), cmp);

    int i;
    for (i = 0; i < grades.size(); ++i) {
        if (grades[i].needRank == 0) break;
        else {
            cout << grades[i].name << " "
                 << grades[i].course << "\n";
        }
    }

    if (i == 0) cout << "NONE";
    return;
}

int main() {
    ios::sync_with_stdio(false);

    int N;
    cin >> N;

    vector<Grade> grades;
    string name, course;
    int grade;
    for (int i = 0; i < N; ++i) {
        cin >> name >> course >> grade;
        grades.push_back({name, course, grade});
    }

    int low, high;
    cin >> low >> high;
    solver(grades, low, high);
    return 0;
}