//
// Created by cnspary on 2020/3/19.
//

#include "bits/stdc++.h"

using namespace std;

class UnionFind {
private:
    vector<int> uf;
    vector<int> rank;
    int component;
public:
    UnionFind(int N) {
        component = N;
        uf = vector<int>(N);
        rank = vector<int>(N, 1);
        for (int i = 0; i < N; ++i)
            uf[i] = i;
    }

    int find(int x) {
        int root = x;
        while (uf[root] != root)
            root = uf[root];

        while (uf[x] != root) {
            int t = uf[x];
            uf[x] = root;
            x = t;
        }
        return root;
    }

    void uunion(int a, int b) {
        int r1 = find(a);
        int r2 = find(b);
        if (r1 == r2) return;

        component--;

        if (rank[r1] > rank[r2]) {
            uf[r2] = r1;
            rank[r1] += rank[r2];
        } else {
            uf[r1] = r2;
            rank[r2] += rank[r1];
        }
    }

    bool isConnect(int a, int b) {
        return find(a) == find(b);
    }

    int getComponent() {
        return component;
    }
};

int cnt = 0;
vector<vector<int>> pictures;
vector<pair<int, int>> query;

void solver() {
    UnionFind uf(cnt);
    for (int i = 0; i < pictures.size(); ++i)
        for (int j = 1; j < pictures[i].size(); ++j)
            uf.uunion(pictures[i][j - 1] - 1, pictures[i][j] - 1);
    printf("%d %d\n", uf.getComponent(), cnt);
    for (int i = 0; i < query.size(); ++i) {
        int a = query[i].first;
        int b = query[i].second;
        if (uf.isConnect(a - 1, b - 1)) printf("Yes\n");
        else printf("No\n");
    }
    return;
}

int main() {
    int N;
    scanf("%d", &N);
    pictures.resize(N);
    for (int i = 0; i < N; ++i) {
        int c;
        scanf("%d", &c);
        pictures[i].resize(c);
        for (int j = 0; j < c; ++j) {
            scanf("%d", &pictures[i][j]);
            cnt = max(cnt, pictures[i][j]);
        }
    }
    scanf("%d", &N);
    for (int i = 0; i < N; ++i) {
        int a, b;
        scanf("%d %d", &a, &b);
        query.push_back({a, b});
    }
    solver();
    return 0;
}