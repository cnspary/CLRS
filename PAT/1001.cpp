//
// Created by cnspary on 2020/2/24.
//

#include<bits/stdc++.h>

using namespace std;

int solve(int a, int b) {
    int sum = a + b;
    bool neg = (sum < 0);
    vector<char> S;

    if (neg) sum = abs(sum);

    int count = 0;
    do {
        S.push_back('0' + sum % 10);
        sum /= 10;
        count++;
        if (count % 3 == 0 && sum != 0)
            S.push_back(',');
    } while (sum != 0);

    if (neg) printf("-");
    for (int i = S.size() - 1; -1 < i; --i)
        printf("%c", S[i]);

    return 0;
}

int main() {
    int a, b;
    scanf("%d %d", &a, &b);
    solve(a, b);
    return 0;
}
