//
// Created by cnspary on 2020/3/6.
//

#include "bits/stdc++.h"

using namespace std;

struct Info {
    string name;
    int age;
    int worth;

    Info(string n, int a, int w) {
        name = n;
        age = a;
        worth = w;
    }
};

struct Query {
    int count;
    int lo, hi;

    Query(int c, int l, int h) :
            count(c), lo(l), hi(h) {}
};

bool cmp(Info a, Info b) {
    if (a.worth > b.worth) return true;
    else if (a.worth == b.worth && a.age < b.age) return true;
    else if (a.worth == b.worth && a.age == b.age && a.name < b.name) return true;
    else return false;
}

vector<Info> info;
vector<Query> query;

void solver() {
    sort(info.begin(), info.end(), cmp);
    for (int i = 0; i < query.size(); ++i) {
        int count = query[i].count;
        int lo = query[i].lo;
        int hi = query[i].hi;
        bool noone = true;
        printf("Case #%d:\n", i + 1);
        for (int i = 0; i < info.size() && count > 0; ++i) {
            if (lo <= info[i].age && info[i].age <= hi) {
                noone = false;
                count--;
                printf("%s %d %d\n", info[i].name.c_str(), info[i].age, info[i].worth);
            }
        }
        if (noone)
            printf("None\n");
    }
    return;
}

int main() {
    ios::sync_with_stdio(false);

    int N, S;
    cin >> N >> S;

    string name;
    int age, worth;
    for (int i = 0; i < N; ++i) {
        cin >> name >> age >> worth;
        info.push_back({name, age, worth});
    }

    int count, lo, hi;
    for (int i = 0; i < S; ++i) {
        cin >> count >> lo >> hi;
        query.push_back({count, lo, hi});
    }
    solver();
    return 0;
};