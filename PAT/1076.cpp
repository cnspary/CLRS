//
// Created by cnspary on 2020/3/12.
//

#include "bits/stdc++.h"

using namespace std;

int K, MaxLevel;
vector<int> Visited;
vector<vector<int>> DirectGraph;
vector<int> Query;

void BFS(int start) {
    queue<pair<int, int>> Q;
    Q.push({start, 0});
    Visited[start] = 1;
    int count = -1;

    while (!Q.empty()) {
        int p = Q.front().first;
        int l = Q.front().second;
        count++;
        Q.pop();

        for (int c : DirectGraph[p]) {
            if (Visited[c] == 0 && l + 1 <= MaxLevel) {
                Visited[c] = 1;
                Q.push({c, l + 1});
            }
        }
    }

    printf("%d\n", count);
    return;
}

void solver() {
    for (int q : Query) {
        Visited.clear();
        Visited.resize(K + 1, 0);
        BFS(q);
    }
    return;
}

int main() {
    scanf("%d %d", &K, &MaxLevel);

    DirectGraph.resize(K + 1);
    for (int i = 1; i <= K; ++i) {
        int c, follower;
        scanf("%d", &c);
        for (int j = 0; j < c; ++j) {
            scanf("%d", &follower);
            DirectGraph[follower].push_back(i);
        }
    }

    int q;
    scanf("%d", &q);
    Query.resize(q);
    for (int i = 0; i < q; ++i)
        scanf("%d", &Query[i]);

    solver();
    return 0;
}