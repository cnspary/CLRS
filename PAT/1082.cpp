//
// Created by cnspary on 2020/3/15.
//

#include "bits/stdc++.h"

using namespace std;

vector<string> numToC = {"ling ", "yi ", "er ", "san ", "si ", "wu ", "liu ", "qi ", "ba ", "jiu "};
vector<string> unitToC = {"Qian ", "Bai ", "Shi ", ""};

string helper(int num) {
    string ans;
    string ns = to_string(num);
    ns.insert(0, 4 - ns.length(), '0');
    bool suffixNum = false;
    for (int i = 3; -1 < i;) {
        if (ns[i] == '0' && !suffixNum) --i;
        else if (ns[i] == '0' && suffixNum) {
            while (-1 < i && ns[i] == '0') --i;
            if (-1 < i) ans = "ling " + ans;
        } else {
            ans = numToC[ns[i] - '0'] + unitToC[i] + ans;
            suffixNum = true;
            --i;
        }
    }

    ans = ans.substr(0, ans.size() - 1);
    if (ans == "") return ans;
    else return ans + " ";
}

void solver(int num) {
    if (num == 0) {
        cout << "ling";
        return;
    }

    string ans;

    bool neg = false;
    if (num < 0) {
        num = -num;
        neg = true;
    }

    int cnt = 0;
    while (num != 0) {
        ans = helper(num % 10000) + ans;

        if (num / 10000 != 0) {
            cnt++;
            if (0 < num % 10000 && num % 10000 < 1000) ans = "ling " + ans;

            if (cnt == 1 && (num / 10000) % 10000 != 0) ans = "Wan " + ans;
            else if (cnt == 2) ans = "Yi " + ans;
        }

        num /= 10000;
    }

    if (neg) ans = "Fu " + ans;
    cout << ans.substr(0, ans.size() - 1);
    return;
}

int main() {
    int num;
    cin >> num;
    solver(num);
    return 0;
}