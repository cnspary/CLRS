//
// Created by cnspary on 2020/3/18.
//

#include "bits/stdc++.h"

using namespace std;

int N;
vector<double> nums;

void solver() {
    double sum = 0.0;
    for (int i = 0; i < N; ++i)
        sum += nums[i] * (N - i) * (i + 1);
    printf("%.2lf", sum);
    return;
}

int main() {
    scanf("%d", &N);
    nums.resize(N);
    for (int i = 0; i < N; ++i)
        scanf("%lf", &nums[i]);
    solver();
    return 0;
}