//
// Created by cnspary on 2020/3/6.
//

#include "bits/stdc++.h"

using namespace std;

vector<int> weight;
vector<vector<int>> Tree;
int pathWeightSum = 0;
vector<pair<int, int>> S;
vector<vector<pair<int, int>>> ans;

void dfs(int cur, int Target) {
    S.push_back({weight[cur], cur});
    pathWeightSum += weight[cur];

    if (Tree[cur].size() == 0 && pathWeightSum == Target) {
        ans.push_back(S);
    } else if (Tree[cur].size() != 0 && pathWeightSum < Target) {
        for (int next : Tree[cur])
            dfs(next, Target);
    }
    pathWeightSum -= weight[cur];
    S.pop_back();
    return;
}

void solver(int Target) {
    dfs(0, Target);

    sort(ans.begin(), ans.end(), greater<vector<pair<int, int>>>());

    for (int i = 0; i < ans.size(); ++i) {
        printf("%d", ans[i][0].first);
        for (int j = 1; j < ans[i].size(); ++j)
            printf(" %d", ans[i][j].first);
        printf("\n");
    }
    return;
}

int main() {
    int N, M, Target;
    scanf("%d %d %d", &N, &M, &Target);

    weight.resize(N);
    Tree.resize(N);
    for (int i = 0; i < N; ++i)
        scanf("%d", &weight[i]);

    for (int i = 0; i < M; ++i) {
        int node, childNum;
        scanf("%d %d", &node, &childNum);
        Tree[node].resize(childNum);
        for (int j = 0; j < childNum; ++j)
            scanf("%d", &Tree[node][j]);
    }

    solver(Target);
    return 0;
}