//
// Created by cnspary on 2020/3/16.
//

#include "bits/stdc++.h"

using namespace std;

struct TreeNode {
    int id;
    int lc, rc;

    TreeNode(int i) : id(i), lc(-1), rc(-1) {}
};

int N;
vector<pair<string, int>> Ops;
vector<TreeNode> T;
vector<int> ans;

int buildTree() {
    for (int i = 0; i < N + 1; ++i)
        T.push_back({i});

    stack<int> S;

    bool toLeft = true;
    int root, pid, nid;
    root = Ops[0].second;

    for (auto op : Ops) {
        if (toLeft && op.first == "Push") {
            nid = op.second;
            if (!S.empty()) {
                pid = S.top();
                T[pid].lc = nid;
            }
            S.push(nid);
        } else if (!toLeft && op.first == "Push") {
            nid = op.second;
            T[pid].rc = nid;
            S.push(nid);
            toLeft = true;
        } else if (op.first == "Pop") {
            pid = S.top();
            S.pop();
            toLeft = false;
        }
    }
    return root;
}

void Travel(int root) {
    if (root == -1) return;

    Travel(T[root].lc);
    Travel(T[root].rc);
    ans.push_back(T[root].id);
}

void solver() {
    int root = buildTree();
    Travel(root);
    for (int i = 0; i < ans.size(); ++i) {
        cout << ans[i];
        if (i != ans.size() - 1) cout << " ";
    }
    return;
}

int main() {
    cin >> N;
    for (int i = 0; i < 2 * N; ++i) {
        string op;
        int nid;
        cin >> op;
        if (op == "Push") {
            cin >> nid;
            Ops.push_back({op, nid});
        } else
            Ops.push_back({op, -1});
    }
    solver();
    return 0;
}