//
// Created by cnspary on 2020/3/7.
//

#include "bits/stdc++.h"

using namespace std;

void solver(int a1, int a2, int a3, int b1, int b2, int b3) {
    int carrybit = 0;
    int c1, c2, c3;

    c3 = a3 + b3 + carrybit;
    carrybit = c3 / 29;
    c3 = c3 % 29;

    c2 = a2 + b2 + carrybit;
    carrybit = c2 / 17;
    c2 = c2 % 17;

    c1 = a1 + b1 + carrybit;

    printf("%d.%d.%d", c1, c2, c3);
    return;
}

int main() {
    int a1, a2, a3;
    int b1, b2, b3;
    scanf("%d.%d.%d", &a1, &a2, &a3);
    scanf("%d.%d.%d", &b1, &b2, &b3);
    solver(a1, a2, a3, b1, b2, b3);
    return 0;

}