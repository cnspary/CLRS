//
// Created by cnspary on 2020/2/27.
//

#include "bits/stdc++.h"

using namespace std;

vector<char> radix13 = {'0', '1', '2', '3', '4',
                        '5', '6', '7', '8', '9',
                        'A', 'B', 'C'};

string trans(int x) {
    string r13 = "";
    do {
        r13 = radix13[x % 13] + r13;
        x /= 13;
    } while (x != 0);

    if (r13.size() == 1)
        r13 = "0" + r13;

    return r13;
}

void solver(int r, int g, int b) {
    printf("#%s%s%s", trans(r).c_str(),
           trans(g).c_str(),
           trans(b).c_str());
    return;
}

int main() {
    int R, G, B;
    scanf("%d %d %d", &R, &G, &B);
    solver(R, G, B);
    return 0;
}