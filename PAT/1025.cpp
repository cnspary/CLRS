//
// Created by cnspary on 2020/2/27.
//

#include "bits/stdc++.h"

using namespace std;

struct Rank {
    string registration_number;
    int score;
    int all_rank;
    int location_number;
    int location_rank;

    Rank(string rn, int s, int ln) :
            registration_number(rn), score(s), location_number(ln) {}
};

bool operator>(Rank a, Rank b) {
    if (a.score > b.score) return true;
    if (a.score == b.score && a.registration_number < b.registration_number) return true;
    return false;
}

void solver(vector<vector<Rank>> ranks) {
    vector<Rank> all;

    for (int i = 1; i < ranks.size(); ++i) {
        sort(ranks[i].begin(), ranks[i].end(), greater<Rank>());
        int count = 1;
        ranks[i][0].location_rank = count;
        for (int j = 1; j < ranks[i].size(); ++j) {
            count++;
            if (ranks[i][j].score != ranks[i][j - 1].score) ranks[i][j].location_rank = count;
            else ranks[i][j].location_rank = ranks[i][j - 1].location_rank;
        }
        all.insert(all.end(), ranks[i].begin(), ranks[i].end());
    }

    sort(all.begin(), all.end(), greater<Rank>());
    int count = 1;
    all[0].all_rank = count;
    for (int i = 1; i < all.size(); ++i) {
        count++;
        if (all[i].score != all[i - 1].score) all[i].all_rank = count;
        else all[i].all_rank = all[i - 1].all_rank;
    }

    printf("%d\n", all.size());
    printf("%s %d %d %d", all[0].registration_number.c_str(),
           all[0].all_rank, all[0].location_number, all[0].location_rank);

    for (int i = 1; i < all.size(); ++i)
        printf("\n%s %d %d %d", all[i].registration_number.c_str(),
               all[i].all_rank, all[i].location_number, all[i].location_rank);
    return;
}


int main() {
    int N, K;
    string rn;
    int score;
    scanf("%d", &N);

    vector<vector<Rank>> r(N + 1);

    for (int localNum = 1; localNum <= N; ++localNum) {
        scanf("%d", &K);
        for (int j = 0; j < K; ++j) {
            cin >> rn >> score;
            r[localNum].push_back(Rank(rn, score, localNum));
        }
    }

    solver(r);
    return 0;
}