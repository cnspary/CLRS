//
// Created by cnspary on 2020/3/19.
//

#include "bits/stdc++.h"

using namespace std;

int N, M;
vector<int> Degree;

class UnionFind {
private:
    vector<int> uf;
    vector<int> rank;
    int component;
public:
    UnionFind(int N) {
        component = N;
        uf = vector<int>(N);
        rank = vector<int>(N, 1);
        for (int i = 0; i < N; ++i)
            uf[i] = i;
    }

    int find(int x) {
        int root = x;
        while (uf[root] != root)
            root = uf[root];

        while (uf[x] != root) {
            int t = uf[x];
            uf[x] = root;
            x = t;
        }
        return root;
    }

    void uunion(int a, int b) {
        int r1 = find(a);
        int r2 = find(b);
        if (r1 == r2) return;

        component--;

        if (rank[r1] > rank[r2]) {
            uf[r2] = r1;
            rank[r1] += rank[r2];
        } else {
            uf[r1] = r2;
            rank[r2] += rank[r1];
        }
    }

    bool isConnect(int a, int b) {
        return find(a) == find(b);
    }

    int getComponent() {
        return component;
    }
};

void solver(UnionFind uf) {
    int oddCnt = 0;

    for (int i = 1; i <= N; ++i) {
        if (Degree[i] % 2 == 1) oddCnt++;
        printf("%d", Degree[i]);
        if (i != N) printf(" ");
    }

    if (uf.getComponent() == 1 && oddCnt == 0) printf("\nEulerian");
    else if (uf.getComponent() == 1 && oddCnt == 2) printf("\nSemi-Eulerian");
    else printf("\nNon-Eulerian");

    return;
}

int main() {
    scanf("%d %d", &N, &M);
    UnionFind uf(N);
    Degree.resize(N + 1, 0);
    for (int i = 0; i < M; ++i) {
        int a, b;
        scanf("%d %d", &a, &b);
        uf.uunion(a - 1, b - 1);
        Degree[a]++;
        Degree[b]++;
    }
    solver(uf);
    return 0;
}