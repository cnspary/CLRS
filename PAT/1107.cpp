//
// Created by cnspary on 2020/3/18.
//

#include "bits/stdc++.h"

using namespace std;

int N;
map<int, int> HobbyToUser;
vector<vector<int>> Hobbies;

class UnionFind {
private:
    vector<int> uf;
    vector<int> rank;
    int component;
public:
    UnionFind(int N) {
        uf = vector<int>(N);
        rank = vector<int>(N, 1);
        component = N;
        for (int i = 0; i < N; ++i)
            uf[i] = i;
    }

    int find(int x) {
        int root = x;
        while (uf[root] != root)
            root = uf[root];

        while (uf[x] != root) {
            int t = uf[x];
            uf[x] = root;
            x = t;
        }

        return root;
    }

    void uunion(int x, int y) {
        int r1 = find(x);
        int r2 = find(y);

        if (r1 == r2) return;

        component--;
        if (rank[r1] > rank[r2]) {
            uf[r2] = r1;
            rank[r1] += rank[r2];
        } else {
            uf[r1] = r2;
            rank[r2] += rank[r1];
        }
        return;
    }

    void output() {
        printf("%d\n", component);
        vector<int> s;
        for (int i = 0; i < uf.size(); ++i)
            if (uf[i] == i) s.push_back(rank[i]);
        sort(s.begin(), s.end(), greater<int>());
        for (int i = 0; i < s.size(); ++i) {
            printf("%d", s[i]);
            if (i != s.size() - 1) printf(" ");
        }
        return;
    }
};

void solver() {
    UnionFind uf(N);
    for (int i = 0; i < N; ++i) {
        for (int h : Hobbies[i]) {
            if (HobbyToUser.find(h) != HobbyToUser.end())
                uf.uunion(i, HobbyToUser[h]);
            else HobbyToUser.insert({h, i});
        }
    }
    uf.output();
    return;
}

int main() {
    scanf("%d", &N);
    Hobbies.reserve(N);
    for (int i = 0; i < N; ++i) {
        int c;
        scanf("%d:", &c);
        Hobbies[i].resize(c);
        for (int j = 0; j < c; ++j)
            scanf("%d", &Hobbies[i][j]);
    }
    solver();
    return 0;
}