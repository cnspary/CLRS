//
// Created by cnspary on 2020/2/24.
//

#include "bits/stdc++.h"

using namespace std;

void solver(vector<int> requests) {
    int floor = 0;
    int time = 0;

    for (int i = 0; i < requests.size(); ++i) {

        if (requests[i] > floor)
            time += (requests[i] - floor) * 6;
        else
            time += (floor - requests[i]) * 4;

        floor = requests[i];
    }

    time += 5 * requests.size();

    printf("%d", time);
}

int main() {
    int N, f;
    vector<int> requests;
    scanf("%d", &N);

    for (int i = 0; i < N; ++i) {
        scanf("%d", &f);
        requests.push_back(f);
    }

    solver(requests);

    return 0;
}