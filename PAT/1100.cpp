//
// Created by cnspary on 2020/3/15.
//

#include "bits/stdc++.h"

using namespace std;

vector<string> ToMarsLower = {"tret", "jan", "feb", "mar", "apr", "may", "jun", "jly", "aug", "sep", "oct", "nov",
                              "dec"};
vector<string> ToMarsHigher = {"", "tam", "hel", "maa", "huh", "tou", "kes", "hei", "elo", "syy", "lok", "mer", "jou"};
map<string, int> ToEarthLower = {{"tret", 0},
                                 {"jan",  1},
                                 {"feb",  2},
                                 {"mar",  3},
                                 {"apr",  4},
                                 {"may",  5},
                                 {"jun",  6},
                                 {"jly",  7},
                                 {"aug",  8},
                                 {"sep",  9},
                                 {"oct",  10},
                                 {"nov",  11},
                                 {"dec",  12}};


map<string, int> ToEarthHigh = {{"tam", 1},
                                {"hel", 2},
                                {"maa", 3},
                                {"huh", 4},
                                {"tou", 5},
                                {"kes", 6},
                                {"hei", 7},
                                {"elo", 8},
                                {"syy", 9},
                                {"lok", 10},
                                {"mer", 11},
                                {"jou", 12}};

void transtoM(int num) {
    int higher = num / 13;
    int lower = num % 13;

    if (higher != 0 && lower != 0)
        cout << ToMarsHigher[higher] << " " << ToMarsLower[lower];
    else if (higher != 0 && lower == 0)
        cout << ToMarsHigher[higher];
    else if (higher == 0)
        cout << ToMarsLower[lower];
    cout << "\n";
    return;
}

void transtoE(string num) {
    int i = num.find(" ");
    string higher = "", lower = "";
    if (i == -1) {
        if (ToEarthHigh.find(num) != ToEarthHigh.end())
            higher = num;
        else
            lower = num;
    } else {
        higher = num.substr(0, i);
        lower = num.substr(i + 1);
    }

    int h, l;
    if (higher == "") h = 0;
    else h = ToEarthHigh[higher];

    if (lower == "") l = 0;
    else l = ToEarthLower[lower];
    cout << h * 13 + l << "\n";
}


void solver(vector<string> nums) {
    for (string x : nums) {
        if (isdigit(x[0])) transtoM(stoi(x));
        else transtoE(x);
    }
    return;
}

int main() {
    int N;
    cin >> N;

    vector<string> nums;
    string num;
    getchar();
    for (int i = 0; i < N; ++i) {
        getline(cin, num);
        nums.push_back(num);
    }

    solver(nums);
    return 0;
}