//
// Created by cnspary on 2020/2/29.
//

#include "bits/stdc++.h"

using namespace std;

struct Grade {
    string Name;
    string Subject;
    int grade;

    Grade(string n, string s, int g) {
        Name = n;
        Subject = s;
        grade = g;
    }
};

bool cmp(Grade a, Grade b) {
    return a.grade < b.grade;
}

void solver(vector<Grade> Female, vector<Grade> Male) {
    sort(Female.begin(), Female.end(), cmp);
    sort(Male.begin(), Male.end(), cmp);

    if (Female.size() == 0)
        cout << "Absent\n";
    else
        cout << Female.back().Name << " " << Female.back().Subject << endl;

    if (Male.size() == 0)
        cout << "Absent\n";
    else
        cout << Male[0].Name << " " << Male[0].Subject << endl;

    if (Female.size() != 0 && Male.size() != 0)
        cout << Female.back().grade - Male[0].grade;
    else
        cout << "NA";

    return;
}

int main() {
    ios::sync_with_stdio(false);
    int N;
    vector<Grade> Male;
    vector<Grade> Female;
    cin >> N;

    string Name;
    char gender;
    string Subject;
    int grade;
    for (int i = 0; i < N; ++i) {
        cin >> Name >> gender >> Subject >> grade;
        if (gender == 'M')
            Male.push_back(Grade(Name, Subject, grade));
        else
            Female.push_back(Grade(Name, Subject, grade));
    }

    solver(Female, Male);
    return 0;
}