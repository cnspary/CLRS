//
// Created by cnspary on 2020/3/20.
//

#include "bits/stdc++.h"

using namespace std;

int N, M;
map<int, vector<int>> Graph;
map<int, int> Edges;

vector<vector<int>> Lines;
vector<pair<int, int>> Query;

int MINDISTANCE, MINTRANS;
map<int, int> visited;
vector<int> tmpPath, ans;

void buildGraph() {
    int cnt = 1;
    for (auto line : Lines) {
        for (int i = 1; i < line.size(); ++i) {
            int a = line[i - 1];
            int b = line[i];
            Graph[a].push_back(b);
            Graph[b].push_back(a);
            Edges.insert({a * 10000 + b, cnt});
            Edges.insert({b * 10000 + a, cnt});
        }
        cnt++;
    }
}

int getTrans(vector<int> tmpPath) {
    int trans = 0, preLine = 0;
    for (int i = 1; i < tmpPath.size(); ++i) {
        int a = tmpPath[i - 1], b = tmpPath[i];
        if (Edges[a * 10000 + b] != preLine) {
            trans++;
            preLine = Edges[a * 10000 + b];
        }
    }
    return trans;
}

void DFS(int p, int dis, int END) {
    if (p == END) {
        if (dis < MINDISTANCE) {
            MINDISTANCE = dis;
            MINTRANS = getTrans(tmpPath);
            ans = tmpPath;
        } else if (dis == MINDISTANCE) {
            int trans = getTrans(tmpPath);
            if (trans < MINTRANS) {
                MINTRANS = trans;
                ans = tmpPath;
            }
        }
        return;
    }

    if (dis >= MINDISTANCE) return;

    for (int next : Graph[p]) {
        if (visited[next] == 0) {
            visited[next] = 1;
            tmpPath.push_back(next);
            DFS(next, dis + 1, END);
            tmpPath.pop_back();
            visited[next] = 0;
        }
    }
}

void solver() {
    buildGraph();
    tmpPath.clear();
    for (auto q : Query) {
        int a = q.first;
        int b = q.second;

        MINDISTANCE = INT_MAX;
        MINTRANS = INT_MAX;

        ans.clear();
        visited[a] = 1;
        tmpPath.push_back(a);
        DFS(a, 0, b);
        tmpPath.pop_back();
        visited[a] = 0;

        printf("%d\n", ans.size() - 1);
        int s = 0;
        int l = Edges[ans[s] * 10000 + ans[s + 1]];
        for (int i = 0; i < ans.size() - 1; ++i) {
            if (Edges[ans[i] * 10000 + ans[i + 1]] != l) {
                printf("Take Line#%d from %04d to %04d.\n", l, ans[s], ans[i]);
                s = i;
                l = Edges[ans[i] * 10000 + ans[i + 1]];
            }
        }
        printf("Take Line#%d from %04d to %04d.\n", l, ans[s], ans.back());
    }

    return;
}

int main() {
    scanf("%d", &N);
    Lines.resize(N);
    for (int i = 0; i < N; ++i) {
        int c;
        scanf("%d", &c);
        Lines[i].resize(c);
        for (int j = 0; j < c; ++j)
            scanf("%d", &Lines[i][j]);
    }

    scanf("%d", &M);
    for (int i = 0; i < M; ++i) {
        int a, b;
        scanf("%d %d", &a, &b);
        Query.push_back({a, b});
    }
    solver();
    return 0;
}
