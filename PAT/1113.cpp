//
// Created by cnspary on 2020/3/19.
//

#include "bits/stdc++.h"

using namespace std;

void solver(vector<int> nums) {
    sort(nums.begin(), nums.end());
    int m = nums.size() / 2 - 1;
    int sum1 = 0, sum2 = 0;
    for (int i = 0; i <= m; ++i)
        sum1 += nums[i];
    for (int i = m + 1; i < nums.size(); ++i)
        sum2 += nums[i];

    printf("%d %d", nums.size() % 2, sum2 - sum1);
    return;
}

int main() {
    int N;
    scanf("%d", &N);
    vector<int> nums(N);
    for (int i = 0; i < N; ++i)
        scanf("%d", &nums[i]);
    solver(nums);
    return 0;
}