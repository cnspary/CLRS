//
// Created by cnspary on 2020/2/26.
//

#include "bits/stdc++.h"

using namespace std;

struct Edge {
    int from;
    int to;
    int weight;

    Edge(int f, int t, int w) : from(f), to(t), weight(w) {};
};

int C, N, S, M;
vector<int> visited;
vector<int> numOfBike;
vector<vector<Edge>> Graph;
int miniSend, miniBack;
int miniDistance = INT_MAX;
string miniPath = "";

void dfs(int cur, int deep, int dis, string path, int send, int back) {
    if (dis > miniDistance) return;
    if (cur == S) {
        if ((dis < miniDistance) ||
            (dis == miniDistance && send < miniSend) ||
            (dis == miniDistance && send == miniSend && back < miniBack)) {
            miniDistance = dis;
            miniSend = send;
            miniBack = back;
            miniPath = path;
        }
        return;
    }

    for (int i = 0; i < Graph[cur].size(); ++i) {
        int next = Graph[cur][i].to;
        if (visited[next] != 1) {
            visited[next] = 1;
            if (numOfBike[next] > (C / 2))
                dfs(next, deep + 1,
                    dis + Graph[cur][i].weight, path + "->" + to_string(next),
                    send, back + (numOfBike[next] - (C / 2)));
            else if (numOfBike[next] < (C / 2)) {
                int need = (C / 2) - numOfBike[next];
                if (back >= need)
                    dfs(next, deep + 1,
                        dis + Graph[cur][i].weight, path + "->" + to_string(next),
                        send, back - need);
                else
                    dfs(next, deep + 1,
                        dis + Graph[cur][i].weight, path + "->" + to_string(next),
                        send + need - back, 0);

            }
            visited[next] = 0;
        }
    }

    return;
}

int main() {
    scanf("%d %d %d %d", &C, &N, &S, &M);
    visited.resize((N + 1), 0);
    numOfBike.resize(N + 1, 0);
    Graph.resize(N + 1);

    for (int i = 1; i <= N; ++i)
        scanf("%d", &numOfBike[i]);

    for (int i = 0; i < M; ++i) {
        int f, t, w;
        scanf("%d %d %d", &f, &t, &w);
        Graph[f].push_back(Edge(f, t, w));
        Graph[t].push_back(Edge(t, f, w));
    }

    dfs(0, 0, 0, "0", 0, 0);

    printf("%d %s %d", miniSend, miniPath.c_str(), miniBack);

    return 0;
}