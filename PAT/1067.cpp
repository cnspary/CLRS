//
// Created by cnspary on 2020/3/9.
//

#include "bits/stdc++.h"

using namespace std;

void solver(vector<int> num) {
    map<int, int> search;
    int indexOfZero;
    for (int i = 0; i < num.size(); i++) {
        if (num[i] == 0) indexOfZero = i;
        if (i != num[i] && num[i] != 0) search.insert({num[i], i});
    }
    int count = 0, tmp;
    while (!search.empty()) {
        if (indexOfZero == 0) {
            count++;
            indexOfZero = (*search.begin()).second;
            (*search.begin()).second = 0;
        } else {
            count++;
            tmp = search[indexOfZero];
            search.erase(indexOfZero);
            indexOfZero = tmp;
        }
    }
    cout << count;
    return;
}

int main() {
    int N;
    scanf("%d", &N);
    vector<int> num(N);
    for (int i = 0; i < N; i++)
        scanf("%d", &num[i]);
    solver(num);
    return 0;
}