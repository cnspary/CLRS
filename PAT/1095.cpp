//
// Created by cnspary on 2020/3/16.
//

#include "bits/stdc++.h"

using namespace std;

struct Record {
    string carNum;
    int time;
    int status;

    Record(string c, int t, int s) {
        carNum = c;
        time = t;
        status = s;
    }
};

bool carNumCMP(Record a, Record b) {
    if (a.carNum == b.carNum) return a.time < b.time;
    else return a.carNum < b.carNum;
}

bool timeCMP(Record a, Record b) {
    return a.time < b.time;
}

vector<Record> records;
vector<int> query;


void solver() {
    sort(records.begin(), records.end(), carNumCMP);
    vector<Record> vaildRecords;
    for (int i = 0; i < records.size() - 1; ++i) {
        if (records[i].carNum == records[i + 1].carNum &&
            records[i].status == 1 &&
            records[i + 1].status == 0) {
            vaildRecords.push_back(records[i]);
            vaildRecords.push_back(records[i + 1]);
        }
    }

    map<string, int> parkTime;

    for (int i = 0; i < vaildRecords.size() - 1; i += 2) {
        string carNum = vaildRecords[i].carNum;
        int inTime = vaildRecords[i].time;
        int outTime = vaildRecords[i + 1].time;
        parkTime[carNum] += outTime - inTime;
    }

    sort(vaildRecords.begin(), vaildRecords.end(), timeCMP);
    int cnt = 0, j = 0;
    for (int i = 0; i < query.size(); ++i) {
        int searchTime = query[i];
        while (j < vaildRecords.size() && vaildRecords[j].time <= searchTime) {
            if (vaildRecords[j].status == 1) cnt++;
            else cnt--;
            j++;
        }
        cout << cnt << "\n";
    }

    int maxTime = -1;
    for (auto pt : parkTime)
        maxTime = max(maxTime, pt.second);

    for (auto pt:parkTime)
        if (pt.second == maxTime)
            cout << pt.first << " ";

    printf("%02d:%02d:%02d", maxTime / 3600, (maxTime % 3600) / 60, maxTime % 60);
    return;
}

int main() {
    ios::sync_with_stdio(false);

    int N, K;
    cin >> N >> K;

    for (int i = 0; i < N; ++i) {
        string carNum, time, status;
        cin >> carNum >> time >> status;

        int h = stoi(time.substr(0, 2));
        int m = stoi(time.substr(3, 2));
        int s = stoi(time.substr(6, 2));

        records.push_back({carNum, h * 3600 + m * 60 + s, (status == "in") ? 1 : 0});
    }
    for (int i = 0; i < K; ++i) {
        string time;
        cin >> time;
        int h = stoi(time.substr(0, 2));
        int m = stoi(time.substr(3, 2));
        int s = stoi(time.substr(6, 2));
        query.push_back(h * 3600 + m * 60 + s);
    }

    solver();
    return 0;
}