//
// Created by cnspary on 2020/2/29.
//

#include "bits/stdc++.h"

using namespace std;

bool cmp(string a, string b) {
    return a + b < b + a;
}

void solver(vector<string> nums) {
    sort(nums.begin(), nums.end(), cmp);
    string ans = "";
    for (int i = 0; i < nums.size(); ++i)
        ans += nums[i];

    while (ans.size() != 0 && ans[0] == '0')
        ans.erase(ans.begin());

    if(ans.size() == 0) cout << 0;
    else cout << ans;
    return;
}

int main() {
    ios::sync_with_stdio(false);
    int N;
    cin >> N;

    vector<string> nums(N);
    for (int i = 0; i < N; ++i)
        cin >> nums[i];

    solver(nums);
    return 0;
}