//
// Created by cnspary on 2020/2/29.
//

#include "bits/stdc++.h"

using namespace std;

void solver(vector<int> coupons, vector<int> products) {
    sort(coupons.begin(), coupons.end(), less<int>());
    sort(products.begin(), products.end(), less<int>());
    int sum = 0;

    for (int i = 0, j = 0; i < coupons.size() && j < products.size(); ++i, ++j) {
        if (coupons[i] >= 0 || products[j] >= 0) break;
        sum += coupons[i] * products[j];
    }

    for (int i = coupons.size() - 1, j = products.size() - 1; -1 < i && -1 < j; --i, --j) {
        if (coupons[i] < 0 || products[j] < 0) break;
        sum += coupons[i] * products[j];
    }

    printf("%d", sum);
    return;
}

int main() {
    int N;

    scanf("%d", &N);
    vector<int> coupons(N);
    for (int i = 0; i < N; ++i)
        scanf("%d", &coupons[i]);

    scanf("%d", &N);
    vector<int> product(N);
    for (int i = 0; i < N; ++i)
        scanf("%d", &product[i]);

    solver(coupons, product);
    return 0;
}