//
// Created by cnspary on 2020/3/18.
//

#include "bits/stdc++.h"

using namespace std;

int N, K, P;
vector<int> POW;
int maximumFactorSum = -1;
vector<int> ans;
vector<int> tmp;

void DFS(int r, int sum) {
    if (tmp.size() == K && sum == N) {
        int ac = accumulate(tmp.begin(), tmp.end(), 0);
        if (ac > maximumFactorSum) {
            ans = tmp;
            maximumFactorSum = ac;
        }
        return;
    }

    for (int c = r; c < POW.size() && sum < N && tmp.size() < K; ++c) {
        int w = pow(POW[c], P);
        if (((sum + w) < N && (tmp.size() + 1) < K) || ((sum + w) == N && (tmp.size() + 1) == K)) {
            tmp.push_back(POW[c]);
            DFS(c, sum + w);
            tmp.pop_back();
        }
    }
    return;
}

void solver() {
    DFS(0, 0);
    if (ans.size() != 0) {
        printf("%d =", N);
        for (int i = 0; i < ans.size(); ++i) {
            printf(" %d^%d", ans[i], P);
            if (i != ans.size() - 1)
                printf(" +");
        }
    } else printf("Impossible");
    return;
}


int main() {
    scanf("%d %d %d", &N, &K, &P);

    int x = int(pow(N, 1.0 / P));

    while (x != 0) {
        POW.push_back(x);
        x--;
    }
    solver();
    return 0;
}