//
// Created by cnspary on 2020/3/8.
//

#include "bits/stdc++.h"

using namespace std;

int caculate(int l, int r) {
    int size = r - l + 1;
    int hight = 1;
    while (pow(2, hight) < size + 1) hight++;
    int complete = pow(2, hight - 1) - 1;
    int remain = size - complete;
    int half = (complete + 1) / 2;

    if (remain <= half)
        return l + complete / 2 + remain;
    else
        return l + complete / 2 + half;
}

void solver(vector<int> seq) {
    sort(seq.begin(), seq.end());
    vector<int> ans;
    queue<pair<int, int>> Q;

    Q.push({0, seq.size() - 1});
    while (!Q.empty()) {
        auto job = Q.front();
        Q.pop();
        int l = job.first, r = job.second;
        if (l != r) {
            int m = caculate(l, r);
            ans.push_back(seq[m]);
            if (l <= m - 1) Q.push({l, m - 1});
            if (m + 1 <= r) Q.push({m + 1, r});
        } else ans.push_back(seq[l]);
    }
    for (int i = 0; i < ans.size(); ++i) {
        printf("%d", ans[i]);
        if (i != ans.size() - 1) printf(" ");
    }
    return;
}


int main() {
    int N;
    scanf("%d", &N);
    vector<int> seq(N);
    for (int i = 0; i < N; ++i)
        scanf("%d", &seq[i]);
    solver(seq);
    return 0;
}