//
// Created by cnspary on 2020/2/26.
//

#include "bits/stdc++.h"

using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;

    TreeNode(int v) :
            val(v), left(nullptr), right(nullptr) {}
};

vector<int> post;
vector<int> in;

void level(TreeNode *root) {
    queue<TreeNode *> Q;
    vector<int> ans;
    Q.push(root);
    while (!Q.empty()) {
        auto x = Q.front();
        Q.pop();
        ans.push_back(x->val);
        if (x->left) Q.push(x->left);
        if (x->right) Q.push(x->right);
    }

    printf("%d", ans[0]);
    for (int i = 1; i < ans.size(); ++i)
        printf(" %d", ans[i]);
    return;
}

TreeNode *build(int inL, int inR, int pL, int pR) {
    if (inL > inR || pL > pR) return nullptr;

    int val = post[pR];
    int index;
    for (index = inL; index <= inR; ++index)
        if (in[index] == val) break;
    TreeNode *root = new TreeNode(val);
    root->left = build(inL, index - 1, pL, pL + index - inL - 1);
    root->right = build(index + 1, inR, pL + index - inL, pR - 1);
    return root;
}

void solver(int N) {
    TreeNode *root = build(0, N, 0, N);
    level(root);
    return;
}

int main() {
    int N;
    scanf("%d", &N);
    post.resize(N);
    in.resize(N);
    for (int i = 0; i < N; ++i)
        scanf("%d", &post[i]);
    for (int i = 0; i < N; ++i)
        scanf("%d", &in[i]);
    solver(N - 1);
    return 0;
}
