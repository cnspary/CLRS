//
// Created by cnspary on 2020/3/17.
//

#include "bits/stdc++.h"

using namespace std;

int N, M;
set<int> T;
vector<int> pre, in;
map<int, int> preIndex, inIndex;
vector<pair<int, int>> query;

void LCA(int p, int il, int ir, int a, int b) {
    if (il >= ir) return;

    int rootIndex = inIndex[pre[p]];
    int aIndex = inIndex[a], bIndex = inIndex[b];

    if (aIndex < rootIndex && bIndex < rootIndex)
        LCA(p + 1, il, rootIndex, a, b);
    else if (aIndex > rootIndex && bIndex > rootIndex)
        LCA(p + rootIndex - il + 1, rootIndex + 1, ir, a, b);
    else if (aIndex < rootIndex && bIndex > rootIndex)
        printf("LCA of %d and %d is %d.\n", a, b, in[rootIndex]);
    else if (aIndex > rootIndex && bIndex < rootIndex)
        printf("LCA of %d and %d is %d.\n", a, b, in[rootIndex]);
    else if (aIndex == rootIndex)
        printf("%d is an ancestor of %d.\n", a, b);
    else if (bIndex == rootIndex)
        printf("%d is an ancestor of %d.\n", b, a);
    return;
}

void solver() {
    for (int x : pre)
        T.insert(x);

    for (int i = 0; i < query.size(); ++i) {
        int a = query[i].first, b = query[i].second;
        if (T.find(a) == T.end() && T.find(b) == T.end())
            printf("ERROR: %d and %d are not found.\n", a, b);
        else if (T.find(a) == T.end())
            printf("ERROR: %d is not found.\n", a);
        else if (T.find(b) == T.end())
            printf("ERROR: %d is not found.\n", b);
        else LCA(0, 0, M, a, b);
    }
    return;
}

int main() {
    scanf("%d %d", &N, &M);
    in.resize(M);
    pre.resize(M);
    for (int i = 0; i < M; ++i) {
        scanf("%d", &in[i]);
        inIndex.insert({in[i], i});
    }
    for (int i = 0; i < M; ++i) {
        scanf("%d", &pre[i]);
        preIndex.insert({pre[i], i});
    }

    for (int i = 0; i < N; ++i) {
        int a, b;
        scanf("%d %d", &a, &b);
        query.push_back({a, b});
    }
    solver();
    return 0;
}