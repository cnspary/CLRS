//
// Created by cnspary on 2020/3/12.
//

#include "bits/stdc++.h"

using namespace std;

struct Application {
    int id;
    int Ge;
    int Gi;
    vector<int> admissions;

    Application(int K) {
        admissions.resize(K);
    }
};

int N, M, K;
vector<int> School;
vector<Application> Apps;

bool cmp(Application a, Application b) {
    if (a.Ge + a.Gi > b.Ge + b.Gi) return true;
    if (a.Ge + a.Gi < b.Ge + b.Gi) return false;

    if (a.Ge > b.Ge) return true;
    if (a.Ge < b.Ge) return false;

    if (a.id < b.id) return true;
    return false;
}

bool idCmp(Application a, Application b) {
    return a.id < b.id;
}

bool equal(Application a, Application b) {
    return (a.Ge + a.Gi == b.Ge + b.Gi) && (a.Ge == b.Ge);
}

void solver() {
    vector<vector<Application>> Accepted(M);
    sort(Apps.begin(), Apps.end(), cmp);

    for (int i = 0; i < Apps.size(); ++i) {
        Application app = Apps[i];

        for (int j = 0; j < K; ++j) {
            int sid = app.admissions[j];
            if (School[sid] > 0 || equal(Accepted[sid].back(), app)) {
                School[sid]--;
                Accepted[sid].push_back(app);
                break;
            }
        }
    }

    for (int i = 0; i < Accepted.size(); ++i) {
        sort(Accepted[i].begin(), Accepted[i].end(), idCmp);
        for (int aid = 0; aid < Accepted[i].size(); ++aid) {
            printf("%d", Accepted[i][aid].id);
            if (aid != Accepted[i].size() - 1) printf(" ");
        }
        if (i != Accepted.size())printf("\n");
    }
}

int main() {
    scanf("%d %d %d", &N, &M, &K);

    School.resize(M);
    Apps.resize(N, Application(K));
    for (int i = 0; i < M; ++i)
        scanf("%d", &School[i]);

    for (int i = 0; i < N; ++i) {
        int ge, gi;
        scanf("%d %d", &ge, &gi);
        Apps[i].id = i;
        Apps[i].Ge = ge;
        Apps[i].Gi = gi;
        for (int j = 0; j < K; ++j)
            scanf("%d", &Apps[i].admissions[j]);
    }

    solver();
    return 0;
}