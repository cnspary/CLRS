//
// Created by cnspary on 2020/3/19.
//

#include "bits/stdc++.h"

using namespace std;

map<int, int> Ids;
map<int, pair<int, int>> Property;
vector<vector<int>> UFData;
int cnt = 0;

bool cmp(vector<int> a, vector<int> b) {
    if (a[3] * b[1] > a[1] * b[3]) return true;
    else if (a[3] * b[1] < a[1] * b[3]) return false;
    else return a[0] < b[0];
}

class UnionFind {
private:
    vector<int> uf;
    vector<int> id;
    int component;
public:

    UnionFind(map<int, int> Data) {
        component = Data.size();
        uf = vector<int>(Data.size());
        id = vector<int>(Data.size());
        for (int i = 0; i < uf.size(); ++i)
            uf[i] = i;
        for (auto x: Data) {
            id[x.second] = x.first;
        }
    }

    int find(int x) {
        int root = x;
        while (uf[root] != root)
            root = uf[root];

        while (uf[x] != root) {
            int t = uf[x];
            uf[x] = root;
            x = t;
        }
        return root;
    }

    void uunion(int a, int b) {
        int r1 = find(a);
        int r2 = find(b);
        if (r1 == r2) return;

        component--;

        if (id[r1] < id[r2])
            uf[r2] = r1;
        else
            uf[r1] = r2;
    }

    int getComponent() {
        return component;
    }

    void output() {
        vector<vector<int>> ans;
        map<int, vector<int>> t;
        for (int i = 0; i < uf.size(); ++i) {
            int root = find(i);
            if (t.find(id[root]) == t.end())
                t.insert({id[root], {id[i]}});
            else t[id[root]].push_back(id[i]);
        }

        for (auto x : t) {
            int root = x.first;
            int size = x.second.size();
            int sites = 0, areas = 0;
            for (int id : x.second) {
                sites += Property[id].first;
                areas += Property[id].second;
            }
            ans.push_back({root, size, sites, areas});
        }

        sort(ans.begin(), ans.end(), cmp);
        for (int i = 0; i < ans.size(); ++i) {
            printf("%04d %d %.3lf %.3lf\n", ans[i][0], ans[i][1],
                   (ans[i][2] * 1.0) / ans[i][1],
                   (ans[i][3] * 1.0) / ans[i][1]);
        }
        return;
    }
};

void solver() {
    UnionFind uf(Ids);
    for (int i = 0; i < UFData.size(); ++i) {
        for (int j = 1; j < UFData[i].size(); ++j) {
            int a = Ids[UFData[i][j - 1]];
            int b = Ids[UFData[i][j]];
            uf.uunion(a, b);
        }
    }
    cout << uf.getComponent() << "\n";
    uf.output();
    return;
}

int main() {
    int N;
    scanf("%d", &N);

    int id, fid, mid;
    int k, cid;
    int m, area;

    for (int i = 0; i < N; ++i) {
        vector<int> f;
        scanf("%d %d %d", &id, &fid, &mid);
        f.push_back(id);
        if (fid != -1) f.push_back(fid);
        if (mid != -1) f.push_back(mid);
        scanf("%d", &k);
        for (int p = 0; p < k; ++p) {
            scanf("%d", &cid);
            f.push_back(cid);
        }
        scanf("%d %d", &m, &area);
        Property.insert({id, {m, area}});

        for (int id : f) {
            if (Ids.find(id) == Ids.end())
                Ids.insert({id, cnt++});
        }

        UFData.push_back(f);
    }
    solver();
    return 0;
}