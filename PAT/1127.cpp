//
// Created by cnspary on 2020/3/20.
//

#include "bits/stdc++.h"

using namespace std;

struct TreeNode {
    int val;
    TreeNode *lc, *rc;

    TreeNode(int v) :
            val(v), lc(nullptr), rc(nullptr) {}
};

int N;
vector<int> IN, POST;

TreeNode *buildTree(int INL, int INR, int POL, int POR) {
    if (INL > INR || POL > POR) return nullptr;
    TreeNode *root = new TreeNode(POST[POR]);

    int INM;
    for (INM = INL; INM < INR; ++INM)
        if (IN[INM] == POST[POR]) break;
    int LSize = INM - INL, RSize = INR - INM;
    root->lc = buildTree(INL, INM - 1, POL, POL + LSize - 1);
    root->rc = buildTree(INM + 1, INR, POR - RSize, POR - 1);
    return root;
}

void ZigZagTravel(TreeNode *root) {
    vector<int> ans;
    stack<TreeNode *> S1;
    stack<TreeNode *> S2;

    S1.push(root);

    while (!S1.empty() || !S2.empty()) {

        while (!S1.empty()) {
            auto p = S1.top();
            S1.pop();
            ans.push_back(p->val);
            if (p->rc) S2.push(p->rc);
            if (p->lc) S2.push(p->lc);
        }

        while (!S2.empty()) {
            auto p = S2.top();
            S2.pop();
            ans.push_back(p->val);
            if (p->lc) S1.push(p->lc);
            if (p->rc) S1.push(p->rc);
        }
    }
    for (int i = 0; i < ans.size(); ++i) {
        printf("%d", ans[i]);
        if (i != ans.size() - 1)printf(" ");
    }
    return;
}

void solver() {
    TreeNode *root = buildTree(0, N - 1, 0, N - 1);
    ZigZagTravel(root);
    return;
}

int main() {
    scanf("%d", &N);
    IN.resize(N);
    POST.resize(N);
    for (int i = 0; i < N; ++i)
        scanf("%d", &IN[i]);
    for (int i = 0; i < N; ++i)
        scanf("%d", &POST[i]);
    solver();
    return 0;
}