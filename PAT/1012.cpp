//
// Created by cnspary on 2020/2/25.
//

#include "bits/stdc++.h"

using namespace std;

struct Student {
    int id;
    vector<int> grades;

    Student(int ID, int C, int M, int E) {
        id = ID;
        grades = {C, M, E, (C + M + E) / 3};
    }
};

struct Rank {
    vector<int> rank;

    void getBestRank() {
        int bestSub = 3;
        int bestRank = rank[3];
        for (int sub = 0; sub < 3; sub++) {
            if (bestRank > rank[sub]) {
                bestRank = rank[sub];
                bestSub = sub;
            }
        }

        printf("%d ", bestRank);
        switch (bestSub) {
            case 0:
                printf("C");
                break;
            case 1:
                printf("M");
                break;
            case 2:
                printf("E");
                break;
            case 3:
                printf("A");
                break;
        }
        return;
    }

    Rank() {
        rank.resize(4);
    }
};

map<int, Rank> HT;

// 0 -> c
// 1 -> m
// 2 -> e
// 3 -> a
int cmpKey;

bool cmp(Student s1, Student s2) {
    return s1.grades[cmpKey] > s2.grades[cmpKey];
}

void setRank(vector<Student> students, int key) {
    int count = 1;
    HT[students[0].id].rank[key] = count;
    for (int i = 1; i < students.size(); ++i) {
        if (students[i].grades[key] < students[i - 1].grades[key])
            HT[students[i].id].rank[key] = i + 1;
        else if (students[i].grades[key] == students[i - 1].grades[key])
            HT[students[i].id].rank[key] = HT[students[i - 1].id].rank[key];
    }
    return;
}

void solver(vector<Student> students, vector<int> query) {
    for (cmpKey = 0; cmpKey < 4; ++cmpKey) {
        sort(students.begin(), students.end(), cmp);
        setRank(students, cmpKey);
    }

    for (int i = 0; i < query.size() - 1; ++i) {
        if (HT.find(query[i]) == HT.end()) {
            printf("N/A\n");
        } else {
            HT[query[i]].getBestRank();
            printf("\n");
        }
    }

    if (HT.find(query.back()) == HT.end()) {
        printf("N/A");
    } else {
        HT[query.back()].getBestRank();
    }
    return;

}

int main() {
    int N, M;
    scanf("%d %d", &N, &M);

    vector<Student> students;
    vector<int> query;

    for (int i = 0; i < N; ++i) {
        int id, c, m, e;
        scanf("%d %d %d %d", &id, &c, &m, &e);
        students.push_back(Student(id, c, m, e));
    }

    for (int i = 0; i < M; ++i) {
        int id;
        scanf("%d", &id);
        query.push_back(id);
    }

    solver(students, query);
    return 0;
}