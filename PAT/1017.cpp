//
// Created by cnspary on 2020/2/26.
//

#include "bits/stdc++.h"

using namespace std;

void solver(int K, vector<pair<int, int>> time) {
    priority_queue<int, vector<int>, greater<int>> PQ;
    sort(time.begin(), time.end());

    double wait = 0;
    for (int i = 0; i < time.size(); ++i) {
        if (time[i].first < 0) {
            wait += -time[i].first;
            time[i].first = 0;
        } else
            break;
    }

    int i;
    for (i = 0; i < time.size(); ++i) {

        int arriveTime = time[i].first;
        int serviceTime = time[i].second;

        if (arriveTime > 9 * 3600)
            break;

        while (!PQ.empty() && PQ.top() <= arriveTime)
            PQ.pop();

        if (PQ.size() >= K) {
            wait += PQ.top() - arriveTime;
            arriveTime = PQ.top();
            PQ.pop();
        }
        PQ.push(arriveTime + serviceTime);
    }

    printf("%.1lf", wait / i / 60);
    return;
}

int main() {
    int N, K;
    scanf("%d %d", &N, &K);
    vector<pair<int, int>> time;
    for (int i = 0; i < N; ++i) {
        int h, m, s, t;
        scanf("%d:%d:%d %d", &h, &m, &s, &t);
        time.push_back({h * 3600 + m * 60 + s - 8 * 3600, t * 60});
    }

    solver(K, time);
    return 0;
}

