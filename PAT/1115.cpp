//
// Created by cnspary on 2020/3/19.
//

#include "bits/stdc++.h"

using namespace std;

struct TreeNode {
    int val;
    TreeNode *lc, *rc;

    TreeNode(int v) : val(v), lc(nullptr), rc(nullptr) {}
};

class BST {
private:
    TreeNode *root;
    TreeNode *_hot;
public:
    BST() : root(nullptr), _hot(nullptr) {}

    BST(vector<int> nums) {
        root = nullptr;
        _hot = nullptr;
        for (int v : nums)
            insert(v);
    }

    void search(int v) {
        _hot = nullptr;
        TreeNode *s = root;

        while (s != nullptr) {
            _hot = s;
            if (v <= s->val) s = s->lc;
            else s = s->rc;
        }
        return;
    }

    void insert(int val) {
        if (root == nullptr) {
            root = new TreeNode(val);
            return;
        }
        search(val);
        if (val <= _hot->val) _hot->lc = new TreeNode(val);
        else _hot->rc = new TreeNode(val);
        return;
    }

    void BFS() {
        queue<pair<TreeNode *, int>> Q;
        int last1 = 0, last2 = 0;
        int level = -1;
        Q.push({root, 0});

        while (!Q.empty()) {
            TreeNode *p = Q.front().first;
            int l = Q.front().second;
            Q.pop();

            if (l != level) {
                last2 = last1;
                last1 = 1;
                level = l;
            } else last1++;

            if (p->lc) Q.push({p->lc, l + 1});
            if (p->rc) Q.push({p->rc, l + 1});
        }

        printf("%d + %d = %d", last1, last2, last1 + last2);
        return;
    }
};

void solver(vector<int> nums) {
    BST bst(nums);
    bst.BFS();
    return;
}

int main() {
    int N;
    scanf("%d", &N);
    vector<int> nums(N);
    for (int i = 0; i < N; ++i)
        scanf("%d", &nums[i]);
    solver(nums);
    return 0;
}