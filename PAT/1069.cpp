//
// Created by cnspary on 2020/3/9.
//

#include "bits/stdc++.h"

using namespace std;

void solver(string s) {
    while (true) {
        s.insert(0, 4 - s.length(), '0');
        string s1 = s;
        sort(s1.begin(), s1.end(), greater<char>());
        string s2 = s;
        sort(s2.begin(), s2.end(), less<char>());

        int s3 = stoi(s1) - stoi(s2);
        printf("%04d - %04d = %04d\n", stoi(s1), stoi(s2), s3);
        if (s3 == 0 || s3 == 6174) break;
        else s = to_string(s3);
    }
    return;
}

int main() {
    string s;
    cin >> s;
    solver(s);
    return 0;
}