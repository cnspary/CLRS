//
// Created by cnspary on 2020/3/18.
//

#include "bits/stdc++.h"

using namespace std;

int N;
double P, r;
vector<vector<int>> T;

void solver() {
    queue<pair<int, int>> Q;
    Q.push({0, 0});

    int HL = -1, cnt = 0;
    while (!Q.empty()) {
        int p = Q.front().first;
        int l = Q.front().second;
        Q.pop();

        if (T[p].size() == 0) {
            if (HL == -1 || HL == l) {
                HL = l;
                cnt++;
            } else break;
        }

        for (int c : T[p])
            Q.push({c, l + 1});
    }

    double MinPrice = P * pow((1 + r / 100), HL);
    printf("%.4lf %d", MinPrice, cnt);
    return;
}

int main() {
    scanf("%d %lf %lf", &N, &P, &r);
    T.resize(N);
    for (int i = 0; i < N; ++i) {
        int c;
        scanf("%d", &c);
        T[i].resize(c);
        for (int j = 0; j < c; ++j)
            scanf("%d", &T[i][j]);
    }
    solver();
    return 0;
}

