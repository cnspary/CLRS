//
// Created by cnspary on 2020/3/9.
//

#include "bits/stdc++.h"

using namespace std;

void solver(vector<int> coins, int N, int M) {
    sort(coins.begin() + 1, coins.end(), greater<int>());

    vector<int> dp(M + 3, 0);
    vector<vector<int>> check(N + 3, vector<int>(M + 3, 0));

    for (int i = 1; i <= N; ++i) {
        for (int j = M; coins[i] <= j; --j) {
            if (dp[j] <= dp[j - coins[i]] + coins[i]) {
                check[i][j] = 1;
                dp[j] = dp[j - coins[i]] + coins[i];
            }
        }
    }

    if (dp[M] != M) printf("No Solution");
    else {
        vector<int> ans;
        while (N > 0) {
            if (check[N][M]) {
                ans.push_back(coins[N]);
                M -= coins[N];
            }
            N--;
        }
        for (int i = 0; i < ans.size(); ++i) {
            printf("%d", ans[i]);
            if (i != ans.size() - 1) printf(" ");
        }
    }

    return;
}

int main() {
    int N, M;
    scanf("%d %d", &N, &M);
    vector<int> coins(N + 1);
    for (int i = 1; i <= N; ++i)
        scanf("%d", &coins[i]);
    solver(coins, N, M);
    return 0;
}