//
// Created by cnspary on 2020/3/15.
//

#include "bits/stdc++.h"

using namespace std;

struct Rational {
    long int numerator;
    long int denominator;

    Rational() {}

    Rational(long int n, long int d) {
        numerator = n;
        denominator = d;
    }
};

vector<Rational> rs;

long long GCD(long long a, long long b) {
    if (a < b) swap(a, b);
    if (b == 0) return a;

    while (a % b != 0) {
        int c = a % b;
        a = b;
        b = c;
    }
    return b;
}

void solver(vector<Rational> rs) {
    long long N = 0, D = 1, gcd;

    for (int i = 0; i < rs.size(); ++i) {
        N = N * rs[i].denominator + rs[i].numerator * D;
        D = D * rs[i].denominator;
        gcd = GCD(N, D);
        N /= gcd;
        D /= gcd;

    }

    long long I = N / D;
    N %= D;

    if (I != 0 && N != 0) printf("%lld %lld/%lld", I, N, D);
    else if (I != 0 && N == 0)printf("%lld", I);
    else if (I == 0 && N != 0)printf("%lld/%lld", N, D);
    else printf("0");
    return;
}

int main() {
    int N;

    scanf("%d", &N);
    rs.resize(N);

    int n, d;
    for (int i = 0; i < N; ++i)
        scanf("%lld/%lld", &rs[i].numerator, &rs[i].denominator);
    solver(rs);
    return 0;
}