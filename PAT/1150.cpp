//
// Created by cnspary on 2020/3/17.
//

#include "bits/stdc++.h"

using namespace std;

int N, M, K;
vector<map<int, int>> Graph;
vector<vector<int>> Paths;

int check(vector<int> path) {
    vector<int> visited(N + 1, 0);
    int dis = 0;

    bool isSimple = true;
    for (int i = 0; i < path.size() - 1; ++i) {
        int f = path[i];
        int t = path[i + 1];

        if (Graph[f].find(t) == Graph[f].end()) {
            printf("NA (Not a TS cycle)\n");
            return -1;
        }

        dis += Graph[f][t];
        if (visited[f] == 1) isSimple = false;
        else visited[f] = 1;
    }

    int vistedNum = accumulate(visited.begin() + 1, visited.end(), 0);

    if (vistedNum == N && isSimple && path[0] == path.back()) {
        printf("%d (TS simple cycle)\n", dis);
        return dis;
    } else if (vistedNum == N && !isSimple && path[0] == path.back()) {
        printf("%d (TS cycle)\n", dis);
        return dis;
    } else {
        printf("%d (Not a TS cycle)\n", dis);
        return -1;
    }

}

void solver() {
    int ShortestId = -1;
    int ShortestDis = INT_MAX;

    for (int i = 0; i < Paths.size(); ++i) {
        printf("Path %d: ", i + 1);
        int d = check(Paths[i]);
        if (d != -1 && d < ShortestDis) {
            ShortestId = i + 1;
            ShortestDis = d;
        }
    }

    printf("Shortest Dist(%d) = %d", ShortestId, ShortestDis);
    return;
}

int main() {
    scanf("%d %d", &N, &M);
    Graph.resize(N + 1);
    for (int i = 0; i < M; ++i) {
        int f, t, w;
        scanf("%d %d %d", &f, &t, &w);
        Graph[f].insert({t, w});
        Graph[t].insert({f, w});
    }
    scanf("%d", &K);
    Paths.resize(K);
    for (int i = 0; i < K; ++i) {
        int n;
        scanf("%d", &n);
        Paths[i].resize(n);
        for (int j = 0; j < n; ++j)
            scanf("%d", &Paths[i][j]);
    }
    solver();
    return 0;
}