//
// Created by cnspary on 2020/2/24.
//

#include "bits/stdc++.h"

using namespace std;

struct Poly {
    int ex;
    double coeff;

    Poly(int e, double c) :
            ex(e), coeff(c) {};
};

void solver(vector<Poly> p1, vector<Poly> p2) {
    map<int, double, greater<int>> HT;

    for (Poly a : p1)
        for (Poly b : p2) {
            int newExp = a.ex + b.ex;
            double newCoeff = a.coeff * b.coeff;

            if (HT.find(newExp) != HT.end())
                HT[newExp] += newCoeff;
            else
                HT.insert({newExp, newCoeff});
        }

    vector<pair<int, double>> ans;
    for (auto p : HT)
        if (p.second != 0)
            ans.push_back({p.first, p.second});
    printf("%d", ans.size());
    for (auto p : ans)
        printf(" %d %.1lf", p.first, p.second);
    return;
}

int main() {
    vector<Poly> p1, p2;
    int n, e;
    double c;
    scanf("%d", &n);
    for (int i = 0; i < n; ++i) {
        scanf("%d %lf", &e, &c);
        p1.push_back(Poly(e, c));
    }

    scanf("%d", &n);
    for (int i = 0; i < n; ++i) {
        scanf("%d %lf", &e, &c);
        p2.push_back(Poly(e, c));
    }

    solver(p1, p2);

    return 0;
}