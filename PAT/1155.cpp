//
// Created by cnspary on 2020/3/17.
//

#include "bits/stdc++.h"

using namespace std;
int N;
vector<int> Heap;
vector<int> S;
bool isMinHeap = false, isMaxHeap = false;

void PreOrder(int p) {
    S.push_back(Heap[p]);
    bool isLeaf = true;
    if (p * 2 + 2 < Heap.size()) {
        PreOrder(p * 2 + 2);
        isLeaf = false;
    }
    if (p * 2 + 1 < Heap.size()) {
        PreOrder(p * 2 + 1);
        isLeaf = false;
    }

    if (isLeaf) {
        cout << S[0];
        for (int i = 1; i < S.size(); ++i) {
            if (isMaxHeap && S[i] > S[i - 1]) isMaxHeap = false;
            else if (isMinHeap && S[i] < S[i - 1]) isMinHeap = false;
            cout << " " << S[i];
        }
        cout << endl;
    }
    S.pop_back();
    return;

}

void solver() {
    if (Heap[0] > Heap[1]) isMaxHeap = true;
    else isMinHeap = true;
    PreOrder(0);
    if (isMaxHeap) cout << "Max Heap";
    else if (isMinHeap) cout << "Min Heap";
    else cout << "Not Heap";
    return;
}

int main() {
    ios::sync_with_stdio(false);

    scanf("%d", &N);
    Heap.resize(N);
    for (int i = 0; i < N; ++i)
        scanf("%d", &Heap[i]);
    solver();
    return 0;
}