//
// Created by cnspary on 2020/3/9.
//

#include "bits/stdc++.h"

using namespace std;

struct MoonCake {
    double inventory;
    double prices;

    MoonCake() {};
};

bool cmp(MoonCake a, MoonCake b) {
    return a.prices * b.inventory > a.inventory * b.prices;
}

void solver(vector<MoonCake> cakes, double M) {
    sort(cakes.begin(), cakes.end(), cmp);
    double profit = 0;
    for (int i = 0; i < cakes.size(); ++i) {
        if (M >= cakes[i].inventory) {
            profit += cakes[i].prices;
            M -= cakes[i].inventory;
        } else {
            profit += cakes[i].prices * M / cakes[i].inventory;
            break;
        }
    }
    printf("%.2lf", profit);
    return;
}

int main() {
    int N;
    double M;
    scanf("%d %lf", &N, &M);

    vector<MoonCake> cakes(N);
    for (int i = 0; i < N; ++i)
        scanf("%lf", &cakes[i].inventory);
    for (int i = 0; i < N; ++i)
        scanf("%lf", &cakes[i].prices);

    solver(cakes, M);
    return 0;
}