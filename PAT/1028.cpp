//
// Created by cnspary on 2020/2/27.
//

#include "bits/stdc++.h"

using namespace std;

struct Record {
    string ID;
    string name;
    int grade;

    Record(string id, string n, int g) {
        ID = id;
        name = n;
        grade = g;
    }
};

int cmpKey;

bool operator<(Record a, Record b) {
    switch (cmpKey) {
        case 1:
            return a.ID < b.ID;
        case 2:
            return a.name < b.name || a.name == b.name && a.ID < b.ID;
        case 3:
            return a.grade < b.grade || a.grade == b.grade && a.ID < b.ID;
    }
}

bool cmp(Record a, Record b) {
    switch (cmpKey) {
        case 1:
            return a.ID < b.ID;
        case 2:
            return a.name < b.name || a.name == b.name && a.ID < b.ID;
        case 3:
            return a.grade < b.grade || a.grade == b.grade && a.ID < b.ID;
    }
}

void solver(vector<Record> records) {
    sort(records.begin(), records.end(), cmp);
    cout << records[0].ID << " "
         << records[0].name << " "
         << records[0].grade;

    for (int i = 1; i < records.size(); ++i)
        printf("\n%s %s %d", records[i].ID.c_str(), records[i].name.c_str(), records[i].grade);
}

int main() {
    int N;
    scanf("%d %d", &N, &cmpKey);
    vector<Record> records;
    for (int i = 0; i < N; ++i) {
        string a, b;
        int c;
        cin >> a >> b >> c;
        records.push_back(Record(a, b, c));
    }
    solver(records);
    return 0;
}