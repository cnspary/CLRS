//
// Created by cnspary on 2020/3/12.
//

#include "bits/stdc++.h"

using namespace std;

int reSetMSize(int MSize) {
    if (MSize <= 1) return 2;
    for (int i = 2; i <= sqrt(MSize); ++i)
        if (MSize % i == 0) return reSetMSize(MSize + 1);
    return MSize;
}

void solver(int MSize, vector<int> nums) {
    MSize = reSetMSize(MSize);
    vector<int> bitmap(MSize, 0);
    vector<int> ans(nums.size(), -1);

    for (int i = 0; i < nums.size(); ++i) {
        int prob = 0, next;
        do {
            next = (nums[i] + prob * prob) % MSize;
            prob++;
        } while (prob < MSize && bitmap[next] == 1);

        if (prob < MSize) {
            bitmap[next] = 1;
            ans[i] = next;
        }

    }

    for (int i = 0; i < ans.size(); ++i) {
        if (ans[i] == -1) printf("-");
        else printf("%d", ans[i]);

        if (i != ans.size() - 1) printf(" ");
    }
    return;
}

int main() {
    int MSize, N;
    scanf("%d %d", &MSize, &N);

    vector<int> nums(N);
    for (int i = 0; i < N; ++i)
        scanf("%d", &nums[i]);

    solver(MSize, nums);
    return 0;
}