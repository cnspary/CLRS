//
// Created by cnspary on 2020/3/5.
//

#include "bits/stdc++.h"

using namespace std;

void solver(vector<int> coins, int M) {
    sort(coins.begin(), coins.end());
    int l = 0, r = coins.size() - 1;
    while (l < r) {
        if (coins[l] + coins[r] < M) ++l;
        else if (coins[l] + coins[r] > M) --r;
        else {
            printf("%d %d", coins[l], coins[r]);
            return;
        }
    }
    printf("No Solution");
    return;
}

int main() {
    int N, M;
    scanf("%d %d", &N, &M);

    vector<int> coins(N);
    for (int i = 0; i < N; ++i)
        scanf("%d", &coins[i]);

    solver(coins, M);
    return 0;
}