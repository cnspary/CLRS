//
// Created by cnspary on 2020/3/2.
//

#include "bits/stdc++.h"

using namespace std;

vector<int> pre, post;

bool buildPost(int L, int R, bool asc) {
    if (R <= L) return true;

    int root = L;
    int l = L + 1, r = L + 1;
    if (asc) for (; r < R && pre[r] < pre[root]; r++);
    else for (; r < R && pre[r] >= pre[root]; r++);

    for (int i = r; i < R; ++i) {
        if (asc && pre[i] < pre[root]) return false;
        else if (!asc && pre[i] >= pre[root]) return false;
    }

    if (buildPost(l, r, asc) && buildPost(r, R, asc)) {
        post.push_back(pre[root]);
        return true;
    } else return false;
}

void solver() {
    if (!buildPost(0, pre.size(), true)) {
        post.clear();
        if (!buildPost(0, pre.size(), false)) {
            printf("NO");
            return;
        }
    }

    printf("YES\n%d", post[0]);
    for (int i = 1; i < post.size(); ++i)
        printf(" %d", post[i]);

    return;
}

int main() {
    int N;
    scanf("%d", &N);

    int v;
    for (int i = 0; i < N; ++i) {
        scanf("%d", &v);
        pre.push_back(v);
    }

    solver();
    return 0;
}