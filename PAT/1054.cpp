//
// Created by cnspary on 2020/3/6.
//

#include "bits/stdc++.h"

using namespace std;

int main() {
    int M, N;
    scanf("%d %d", &M, &N);
    map<int, int> HT;
    map<int, int>::iterator Iter;

    int a;
    for (int i = 0; i < M * N; ++i) {
        scanf("%d", &a);
        HT[a]++;
    }
    for (auto x : HT) {
        if (x.second > M * N / 2) {
            printf("%d", x.first);
            return 0;
        }
    }
    return 0;
}