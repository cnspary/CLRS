//
// Created by cnspary on 2020/2/24.
//

#include "bits/stdc++.h"

using namespace std;

vector<string> num = {"zero",
                      "one",
                      "two",
                      "three",
                      "four",
                      "five",
                      "six",
                      "seven",
                      "eight",
                      "nine"};

void solver(string S) {
    int sum = 0;
    for (int i = 0; i < S.size(); ++i)
        sum += S[i] - '0';

    stack<int> ST;
    do {
        ST.push(sum % 10);
        sum /= 10;
    } while (sum != 0);

    while (ST.size() > 1) {
        printf("%s ", num[ST.top()].c_str());
        ST.pop();
    }

    printf("%s", num[ST.top()].c_str());
    return;
}

int main() {
    string S;
    cin >> S;
    solver(S);
    return 0;
}