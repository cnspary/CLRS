//
// Created by cnspary on 2020/2/27.
//

#include "bits/stdc++.h"

using namespace std;


void solver(string s) {
    int n2 = (s.size() + 2 + 3) / 3;

    if ((s.size() - n2) % 2 != 0) {
        if ((n2 - 1) >= (s.size() - (n2 - 1)) / 2 + 1)  n2--;
        else n2++;
    }

    int i = 0, j = s.size() - 1;
    while (true) {
        if (j - i + 1 > n2) {
            printf("%c", s[i++]);
            for (int k = 1; k <= n2 - 2; ++k)
                printf(" ");
            printf("%c\n", s[j--]);
        } else {
            while (i <= j) printf("%c", s[i++]);
            break;
        }
    }
    return;
}

int main() {
    string s;
    cin >> s;
    solver(s);
    return 0;
}