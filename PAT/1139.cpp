//
// Created by cnspary on 2020/3/20.
//

#include "bits/stdc++.h"

using namespace std;

map<string, set<string>> Graph;
vector<pair<string, string>> Query;

bool isSameGender(string a, string b) {
    return a.size() == b.size();
}

string removeGender(string s) {
    if (s.size() == 5) return s.substr(1, 4);
    return s;
}

void solver() {
    vector<pair<string, string>> ans;
    for (auto q : Query) {
        ans.clear();
        string a = q.first;
        string b = q.second;

        for (string x : Graph[a]) {
            if (x == b) continue;
            for (string y : Graph[b]) {
                if (y == a) continue;

                if (isSameGender(x, a) &&
                    isSameGender(y, b) &&
                    Graph[x].find(y) != Graph[x].end())
                    ans.push_back({removeGender(x), removeGender(y)});
            }
        }

        sort(ans.begin(), ans.end());
        printf("%d\n", ans.size());
        for (auto a : ans)
            printf("%s %s\n", a.first.c_str(), a.second.c_str());
    }
    return;
}

int main() {
    int N, M;
    scanf("%d %d", &N, &M);
    for (int i = 0; i < M; ++i) {
        string a, b;
        cin >> a >> b;

        Graph[a].insert(b);
        Graph[b].insert(a);
    }
    scanf("%d", &M);
    for (int i = 0; i < M; ++i) {
        string a, b;
        cin >> a >> b;
        Query.push_back({a, b});
    }
    solver();
    return 0;
}