//
// Created by cnspary on 2020/3/17.
//

#include "bits/stdc++.h"

using namespace std;

void solver(vector<int> nums) {
    vector<int> preMax(nums.size()), suffMin(nums.size());

    int M = -1, m = INT_MAX;
    for (int i = 0; i < nums.size(); ++i) {
        preMax[i] = M;
        M = max(M, nums[i]);
    }
    for (int i = nums.size() - 1; -1 < i; --i) {
        suffMin[i] = m;
        m = min(m, nums[i]);
    }

    vector<int> ans;
    for (int i = 0; i < nums.size(); ++i)
        if (preMax[i] < nums[i] && nums[i] < suffMin[i])
            ans.push_back(nums[i]);

    printf("%d\n", ans.size());
    for (int i = 0; i < ans.size(); ++i) {
        printf("%d", ans[i]);
        if (i != ans.size() - 1) printf(" ");
    }
    printf("\n");
    return;
}

int main() {
    int N;
    scanf("%d", &N);
    vector<int> nums(N);
    for (int i = 0; i < N; ++i)
        scanf("%d", &nums[i]);
    solver(nums);
    return 0;
}