//
// Created by cnspary on 2020/3/16.
//

#include "bits/stdc++.h"

using namespace std;

struct Edge {
    int from;
    int to;
    int weight;

    Edge(int f, int t, int w) {
        from = f;
        to = t;
        weight = w;
    }
};

string startCity;
map<string, int> Index;
vector<string> CityName;
vector<int> H; // happiness
vector<int> V; // visited
vector<int> S; // stack
vector<vector<Edge>> Graph;
int N, K;

int minCost = INT_MAX;
vector<vector<int>> minCostRoute;

void DFS(int x, int cost, int end) {
    if (x == end) {
        if (cost < minCost) {
            minCost = cost;
            minCostRoute = {S};
        } else if (cost == minCost) {
            minCostRoute.push_back(S);
        }
    }

    for (int i = 0; i < Graph[x].size() && cost < minCost; ++i) {
        int next = Graph[x][i].to;
        if (V[next] == 0) {
            V[next] = 1;
            S.push_back(next);
            DFS(next, cost + Graph[x][i].weight, end);
            S.pop_back();
            V[next] = 0;
        }
    }
}

void findRecommand() {
    int REC = 0;
    int recH = 0;
    int recAH = 0;
    for (int i = 0; i < minCostRoute[0].size(); ++i)
        recH += H[minCostRoute[0][i]];
    recAH = recH / (minCostRoute[0].size() - 1);

    for (int i = 1; i < minCostRoute.size(); ++i) {
        int trecH = 0, trecAH = 0;
        for (int j = 0; j < minCostRoute[i].size(); ++j)
            trecH += H[minCostRoute[i][j]];
        trecAH = trecH / (minCostRoute[i].size() - 1);

        if (trecH > recH || (trecH == recH && trecAH > recAH)) {
            REC = i;
            recH = trecH;
            recAH = trecAH;
        }
    }

    cout << recH << " ";
    cout << recAH << "\n";

    for (int i = 0; i < minCostRoute[REC].size(); ++i) {
        int cid = minCostRoute[REC][i];
        cout << CityName[cid];
        if (i != minCostRoute[REC].size() - 1) cout << "->";
    }

    return;
}

void solver(int start, int end) {
    V[start] = 1;
    S.push_back(start);
    DFS(start, 0, end);

    cout << minCostRoute.size() << " ";
    cout << minCost << " ";
    findRecommand();
    return;
}

int main() {
    cin >> N >> K >> startCity;
    Index.insert({startCity, 0});
    CityName.push_back(startCity);
    H.resize(N, 0);
    V.resize(N, 0);
    for (int i = 1; i < N; ++i) {
        string city;
        int h;
        cin >> city >> h;
        Index.insert({city, i});
        CityName.push_back(city);
        H[i] = h;
    }

    Graph.resize(N);
    for (int i = 0; i < K; ++i) {
        string city1, city2;
        int weight;
        cin >> city1 >> city2 >> weight;

        int cid1, cid2;
        cid1 = Index[city1];
        cid2 = Index[city2];

        Graph[cid1].push_back({cid1, cid2, weight});
        Graph[cid2].push_back({cid2, cid1, weight});
    }

    solver(Index[startCity], Index["ROM"]);
    return 0;
}