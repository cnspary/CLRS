//
// Created by cnspary on 2020/3/5.
//

#include "bits/stdc++.h"

using namespace std;

void solver(string s1, string s2) {
    set<char> alpset;
    string ans = "";
    for (int i = 0; i < s2.size(); ++i)
        alpset.insert(s2[i]);

    for (int i = 0; i < s1.size(); ++i) {
        if (alpset.find(s1[i]) == alpset.end())
            ans += s1[i];
    }
    cout << ans;
    return;
}

int main() {
    string s1, s2;
    getline(cin, s1);
    getline(cin, s2);
    solver(s1, s2);
    return 0;
}