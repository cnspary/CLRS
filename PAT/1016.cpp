//
// Created by cnspary on 2020/2/26.
//

#include "bits/stdc++.h"

using namespace std;

enum RecordType {
    On, Off
};

int allDayPrice = 0;
vector<int> price;

struct Record {
    string name;
    int month, day, hour, minute;
    RecordType type;

    Record(string n, int M, int d, int h, int m, RecordType rt) :
            name(n), month(M), day(d), hour(h), minute(m), type(rt) {}
};

bool cmp(Record a, Record b) {
    if (a.day < b.day) return true;
    if (a.day > b.day) return false;

    if (a.hour < b.hour) return true;
    if (a.hour > b.hour) return false;

    if (a.minute < b.minute) return true;
    if (a.minute > b.minute) return false;

    return true;
}


int lasting(Record On, Record Off) {
    int x = (Off.day - On.day) * 1440;

    x -= On.hour * 60;
    x += Off.hour * 60;

    x -= On.minute;
    x += Off.minute;
    return x;
}

double money(Record On, Record Off) {
    double x = (Off.day - On.day) * allDayPrice;

    for (int i = 0; i < On.hour; ++i)
        x -= price[i] * 60;
    for (int i = 0; i < Off.hour; ++i)
        x += price[i] * 60;

    x -= price[On.hour] * On.minute;
    x += price[Off.hour] * Off.minute;
    return x / 100;
}

bool bill(string name, vector<Record> rs) {
    double total = 0;

    int i = 1;
    while (i < rs.size()) {
        if (rs[i].type == Off && rs[i - 1].type == On) {

            if (total == 0)
                printf("%s %02d\n", name.c_str(), rs[0].month);

            Record on = rs[i - 1];
            Record off = rs[i];

            int l = lasting(on, off);
            double m = money(on, off);

            printf("%02d:%02d:%02d %02d:%02d:%02d ", on.day, on.hour, on.minute, off.day, off.hour, off.minute);
            printf("%d ", l);
            printf("$%.2lf\n", m);

            total += m;
        }
        i++;
    }
    if (total > 0) {
        printf("Total amount: $%.2lf", total);
        return true;
    } else return false;
}

void solver(vector<Record> records) {
    sort(records.begin(), records.end(), cmp);
    map<string, vector<Record>> HT;
    for (auto x: records) {
        if (HT.find(x.name) != HT.end())
            HT[x.name].push_back(x);
        else
            HT.insert({x.name, {x}});
    }

    map<string, vector<Record>>::iterator Iter;
    for (Iter = HT.begin(); Iter != --HT.end(); ++Iter) {

        if (bill(Iter->first, Iter->second))
            printf("\n");
    }
    Iter = --HT.end();
    bill(Iter->first, Iter->second);

    return;
}

int main() {
    int N;
    price.resize(24);
    vector<Record> records;
    for (int i = 0; i < 24; ++i) {
        scanf("%d", &price[i]);
        allDayPrice += price[i] * 60;
    }

    scanf("%d", &N);

    string name;
    int M, d, h, m;
    RecordType rt;
    string OnOff;
    for (int i = 0; i < N; ++i) {
        cin >> name;

        scanf("%d:%d:%d:%d", &M, &d, &h, &m);

        cin >> OnOff;
        if (OnOff == "on-line") rt = On;
        else rt = Off;

        records.push_back(Record(name, M, d, h, m, rt));
    }

    solver(records);

    return 0;
}
