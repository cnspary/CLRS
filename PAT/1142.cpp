//
// Created by cnspary on 2020/3/19.
//

#include "bits/stdc++.h"

using namespace std;

int N, M, K;
map<int, set<int>> Graph;
vector<vector<int>> Cliques;

bool isCompGraph(vector<int> clique) {
    for (int i = 0; i < clique.size(); ++i)
        for (int j = i + 1; j < clique.size(); ++j)
            if (Graph[clique[i]].find(clique[j]) == Graph[clique[i]].end())
                return false;
    return true;
}

bool isMaxCompGraph(vector<int> clique) {
    set<int> InSide;
    for (int v : clique)
        InSide.insert(v);

    for (int v = 1; v <= N; ++v) {
        if (InSide.find(v) != InSide.end()) continue;

        bool canAdd = true;
        for (int inv: InSide) {
            if (Graph[v].find(inv) == Graph[v].end()) {
                canAdd = false;
                break;
            }
        }
        if (canAdd) return false;
    }
    return true;
}

void solver() {
    for (auto clique : Cliques) {
        if (!isCompGraph(clique)) printf("Not a Clique\n");
        else if (!isMaxCompGraph(clique)) printf("Not Maximal\n");
        else printf("Yes\n");
    }
    return;
}

int main() {
    scanf("%d %d", &N, &M);
    for (int i = 0; i < M; ++i) {
        int a, b;
        scanf("%d %d", &a, &b);
        Graph[a].insert(b);
        Graph[b].insert(a);
    }

    scanf("%d", &K);
    Cliques.resize(K);
    for (int i = 0; i < K; ++i) {
        int c;
        scanf("%d", &c);
        Cliques[i].resize(c);
        for (int j = 0; j < c; ++j)
            scanf("%d", &Cliques[i][j]);
    }
    solver();
    return 0;
}