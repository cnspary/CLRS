//
// Created by cnspary on 2020/3/19.
//

#include "bits/stdc++.h"

using namespace std;


struct LinkNode {
    int Address;
    int Data;
    int next;

    LinkNode(int a, int d, int n) :
            Address(a), Data(d), next(n) {}
};

int Start, N, K;
map<int, int> Index;
vector<LinkNode> List;

void solver() {
    vector<LinkNode> OrderList;
    int i = Index[Start];
    while (true) {
        OrderList.push_back(List[i]);
        if (List[i].next == -1) break;
        i = Index[List[i].next];
    }

    vector<LinkNode> tmp1, tmp2, tmp3;

    for (LinkNode x : OrderList)
        if (x.Data < 0) tmp1.push_back(x);
        else if (0 <= x.Data && x.Data <= K) tmp2.push_back(x);
        else tmp3.push_back(x);

    OrderList.clear();
    for (LinkNode x : tmp1) OrderList.push_back(x);
    for (LinkNode x : tmp2) OrderList.push_back(x);
    for (LinkNode x : tmp3) OrderList.push_back(x);

    for (int i = 0; i < OrderList.size() - 1; ++i)
        OrderList[i].next = OrderList[i + 1].Address;
    OrderList.back().next = -1;

    for (LinkNode x : OrderList)
        if (x.next != -1)printf("%05d %d %05d\n", x.Address, x.Data, x.next);
        else printf("%05d %d %d", x.Address, x.Data, x.next);

    return;
}

int main() {
    scanf("%d %d %d", &Start, &N, &K);
    for (int i = 0; i < N; ++i) {
        int a, d, n;
        scanf("%d %d %d", &a, &d, &n);
        List.push_back({a, d, n});
        Index.insert({a, List.size() - 1});
    }
    solver();
    return 0;
}