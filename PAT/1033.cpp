//
// Created by cnspary on 2020/2/28.
//

#include "bits/stdc++.h"

using namespace std;

struct Station {
    double price;
    double dis;

    Station(double p, double d) : price(p), dis(d) {}
};

bool cmp(Station a, Station b) {
    return a.dis < b.dis;
}

double C, D, Davg, N;

#define DOUBLE_MAX INT64_MAX;

void solver(vector<Station> stations) {
    stations.push_back({0, D});
    sort(stations.begin(), stations.end(), cmp);

    if (stations[0].dis > 0) {
        printf("The maximum travel distance = 0.00");
        return;
    }

    double price = 0;
    double distance = 0;

    double tank = 0;
    double maxDistanceOnce = C * Davg;

    for (int i = 0, j = 0; i < stations.size() - 1;) {
        int Case;
        int minPriceStation = -1;
        double minPrice = DOUBLE_MAX;

        j = i + 1;
        if (stations[j].dis - stations[i].dis > maxDistanceOnce)
            Case = 1;
        else {
            Case = 2;
            while (j < stations.size() && stations[j].dis - stations[i].dis <= maxDistanceOnce) {
                if (stations[j].price < minPrice) {
                    minPriceStation = j;
                    minPrice = min(minPrice, stations[j].price);
                }
                if (minPrice <= stations[i].price) {
                    Case = 3;
                    break;
                }
                j++;
            }
        }

        switch (Case) {
            case 1:
                price += (C - tank) * stations[i].price;
                distance += maxDistanceOnce;
                printf("The maximum travel distance = %.2lf", distance);
                return;
            case 2:
                price += (C - tank) * stations[i].price;
                distance += stations[minPriceStation].dis - stations[i].dis;
                tank = C - (stations[minPriceStation].dis - stations[i].dis) / Davg;
                i = minPriceStation;
                break;
            case 3:
                if (tank * Davg >= (stations[minPriceStation].dis - stations[i].dis)) {
                    tank -= (stations[minPriceStation].dis - stations[i].dis) / Davg;
                    distance += stations[minPriceStation].dis - stations[i].dis;
                } else {
                    double need = (stations[minPriceStation].dis - stations[i].dis) / Davg - tank;
                    price += need * stations[i].price;
                    distance += stations[minPriceStation].dis - stations[i].dis;
                    tank = 0;
                }
                i = minPriceStation;
                break;
        }
    }

    printf("%.02lf", price);

    return;
}

int main() {
    scanf("%lf %lf %lf %lf", &C, &D, &Davg, &N);

    double price, dis;
    vector<Station> stations;
    for (int i = 0; i < N; ++i) {
        scanf("%lf %lf", &price, &dis);
        stations.push_back(Station(price, dis));
    }

    solver(stations);
    return 0;
}