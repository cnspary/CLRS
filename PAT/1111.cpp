//
// Created by cnspary on 2020/3/19.
//

#include "bits/stdc++.h"

using namespace std;

int N, M;
int S, E;
int CMPKEY;

struct Edge {
    int from;
    int to;
    int distance;
    int time;

    Edge(int f, int t, int d, int ti) :
            from(f), to(t), distance(d), time(ti) {}

    bool operator<(const Edge &that) const {
        if (CMPKEY == 1) return distance < that.distance;
        else return time < that.time;
    }
};

vector<vector<Edge>> Graph;
int MinDistance, MinTime, MinDistanceTimeCost = INT_MAX;
vector<vector<int>> PreTree;
vector<int> tmp;
vector<int> MinDistancePath, MinTimePath;

int Dij(int ckey) {
    PreTree.resize(N);
    vector<int> visited(N, 0);
    vector<int> MIN(N, 999999);
    MIN[S] = 0;

    CMPKEY = ckey;
    priority_queue<Edge, vector<Edge>> PQ;

    visited[S] = 1;
    for (Edge e : Graph[S])
        PQ.push(e);

    while (!PQ.empty()) {
        auto e = PQ.top();
        PQ.pop();

        int from = e.from, to = e.to;
        if (CMPKEY == 0) {
            if (MIN[from] + e.distance < MIN[to]) {
                MIN[to] = MIN[from] + e.distance;
                PreTree[to].clear();
                PreTree[to].push_back(from);
            } else if (MIN[from] + e.distance == MIN[to])
                PreTree[to].push_back(from);
        } else if (CMPKEY == 1) {
            if (MIN[from] + e.time < MIN[to]) {
                MIN[to] = MIN[from] + e.time;
                PreTree[to].clear();
                PreTree[to].push_back(from);
            } else if (MIN[from] + e.time == MIN[to])
                PreTree[to].push_back(from);
        }

        if (visited[to] == 0) {
            visited[to] = 1;
            for (Edge x : Graph[to])
                PQ.push(x);
        }
    }
    return MIN[E];
}

void DFS(int x, int cost, int ckey) {
    if (x == S) {
        if (ckey == 0) { // distance
            if (cost < MinDistanceTimeCost)
                MinDistancePath = tmp;
        } else if (ckey == 1) { // time
            if (MinTimePath.empty() || tmp.size() < MinTimePath.size())
                MinTimePath = tmp;
        }
        return;
    }

    for (int parent :PreTree[x]) {
        tmp.push_back(parent);
        if (ckey == 0) DFS(parent, cost + Graph[parent][x].time, ckey);
        else DFS(parent, 0, ckey);
        tmp.pop_back();
    }
    return;
}

void solver() {
    MinDistance = Dij(0);
    tmp.clear();
    tmp.push_back(E);
    DFS(E, 0, 0);

    MinTime = Dij(1);
    tmp.clear();
    tmp.push_back(E);
    DFS(E, 0, 1);
    if (MinDistancePath == MinTimePath) {
        printf("Distance = %d; Time = %d: ", MinDistance, MinTime);
        for (int i = MinDistancePath.size() - 1; -1 < i; --i) {
            printf("%d", MinDistancePath[i]);
            if (i != 0) printf(" -> ");
        }
    } else {
        printf("Distance = %d: ", MinDistance);
        for (int i = MinDistancePath.size() - 1; -1 < i; --i) {
            printf("%d", MinDistancePath[i]);
            if (i != 0) printf(" -> ");
        }
        printf("\nTime = %d: ", MinTime);
        for (int i = MinTimePath.size() - 1; -1 < i; --i) {
            printf("%d", MinTimePath[i]);
            if (i != 0) printf(" -> ");
        }
    }
    return;
}

int main() {
    scanf("%d %d", &N, &M);
    Graph.resize(N);

    for (int i = 0; i < M; ++i) {
        int v1, v2, type, dis, time;
        scanf("%d %d %d %d %d", &v1, &v2, &type, &dis, &time);
        Graph[v1].push_back({v1, v2, dis, time});
        if (type == 0)
            Graph[v2].push_back({v2, v1, dis, time});
    }

    scanf("%d %d", &S, &E);
    solver();
    return 0;
}