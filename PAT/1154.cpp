//
// Created by cnspary on 2020/3/17.
//

#include "bits/stdc++.h"

using namespace std;
int N, M, K;
vector<vector<int>> Graph;
vector<vector<int>> Colors;

vector<int> Visited;
set<int> UsedColor;

bool BFS(int r, int k) {
    queue<int> Q;
    Q.push(r);
    Visited[r] = 0;
    UsedColor.insert(Colors[k][r]);

    while (!Q.empty()) {
        int p = Q.front();
        Q.pop();

        for (int i = 0; i < Graph[p].size(); ++i) {
            int c = Graph[p][i];
            if (Colors[k][p] == Colors[k][c]) return false;
            UsedColor.insert(Colors[k][c]);
            if (Visited[c] == 0) {
                Visited[c] = 1;
                Q.push(c);
            }
        }
    }
    return true;
}

void solver() {
    for (int k = 0; k < K; ++k) {
        // check;
        bool isPropoe = true;
        Visited.clear();
        Visited.resize(N, 0);

        UsedColor.clear();
        for (int p = 0; p < N; ++p) {
            if (Visited[p] != 1 && !BFS(p, k)) {
                isPropoe = false;
                break;
            }
        }
        if (isPropoe) printf("%d-coloring\n", UsedColor.size());
        else printf("No\n");
    }
}

int main() {
    scanf("%d %d", &N, &M);
    Graph.resize(N);
    for (int i = 0; i < M; ++i) {
        int a, b;
        scanf("%d %d", &a, &b);
        Graph[a].push_back(b);
        Graph[b].push_back(a);
    }
    scanf("%d", &K);
    Colors.resize(K);
    for (int i = 0; i < K; ++i) {
        for (int j = 0; j < N; ++j) {
            int c;
            scanf("%d", &c);
            Colors[i].push_back(c);
        }
    }
    solver();
    return 0;
}