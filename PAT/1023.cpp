//
// Created by cnspary on 2020/2/27.
//

#include "bits/stdc++.h"

using namespace std;

vector<int> cnt(10, 0);

string doubling(string bigNum) {
    int cBit = 0;
    string d = "";
    for (int i = bigNum.size() - 1; -1 < i; --i) {
        int x = bigNum[i] - '0';
        int x2 = (x * 2 + cBit) % 10;
        cBit = (x * 2 + cBit) / 10;
        d += to_string(x2);
    }

    if (cBit != 0)
        d += to_string(cBit);
    return d;
}

void solver(string bigNum) {
    for (int i = 0; i < bigNum.size(); ++i)
        cnt[bigNum[i] - '0']++;
    string bigNum2 = doubling(bigNum);
    for (int i = 0; i < bigNum2.size(); ++i)
        cnt[bigNum2[i] - '0']--;

    bool isPermu = true;
    for (int c : cnt)
        if (c != 0)
            isPermu = false;

    if (isPermu) printf("Yes\n");
    else printf("No\n");

    for (int i = bigNum2.size() - 1; -1 < i; --i)
        printf("%c", bigNum2[i]);

    return;
}


int main() {
    string bigNum;
    cin >> bigNum;
    solver(bigNum);
    return 0;
}