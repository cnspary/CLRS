//
// Created by cnspary on 2020/3/7.
//

#include "bits/stdc++.h"

using namespace std;

class Stack {
public:
    vector<int> S;
    multiset<int, greater<int>> maxHeap;
    multiset<int, less<int>> minHeap;

    void push(int x) {
        S.push_back(x);

        if (maxHeap.size() > minHeap.size()) minHeap.insert(x);
        else maxHeap.insert(x);

        return;
    }

    void pop() {
        if (S.size() == 0) {
            cout << "Invalid";
            return;
        }
        int x = S.back();
        S.pop_back();

        multiset<int>::iterator it;
        if ((it = maxHeap.find(x)) != maxHeap.end())
            maxHeap.erase(it);
        else if ((it = minHeap.find(x)) != minHeap.end())
            minHeap.erase(it);
        cout << x;
        return;
    }

    void PeekMedian() {
        if (S.size() == 0) {
            cout << "Invalid";
            return;
        }

        while (maxHeap.size() < minHeap.size()) {
            maxHeap.insert(*minHeap.begin());
            minHeap.erase(minHeap.begin());
        }

        while (minHeap.size() < maxHeap.size()) {
            minHeap.insert(*maxHeap.begin());
            maxHeap.erase(maxHeap.begin());
        }

        while (!maxHeap.empty() && !minHeap.empty()
               && *maxHeap.begin() > *minHeap.begin()) {
            int a = *maxHeap.begin();
            int b = *minHeap.begin();
            maxHeap.erase(maxHeap.begin());
            minHeap.erase(minHeap.begin());
            maxHeap.insert(b);
            minHeap.insert(a);
        }

        cout << ((maxHeap.size() >= minHeap.size()) ? *maxHeap.begin() : *minHeap.begin());
        return;
    }
};

void solver(vector<pair<int, int >> query) {
    Stack S;
    for (int i = 0; i < query.size(); ++i) {
        int op = query[i].first;
        int x = query[i].second;

        switch (op) {
            case 0:
                S.pop();
                if (i != query.size() - 1) cout << "\n";
                break;
            case 1:
                S.push(x);
                break;
            case 2:
                S.PeekMedian();
                if (i != query.size() - 1) cout << "\n";
                break;
        }
    }
    return;
}

int main() {
    ios::sync_with_stdio(false);
    int N;
    cin >> N;
    vector<pair<int, int>> query;

    for (int i = 0; i < N; ++i) {
        string op;
        cin >> op;
        if (op == "Pop")
            query.push_back({0, 0});
        else if (op == "Push") {
            int x;
            cin >> x;
            query.push_back({1, x});
        } else if (op == "PeekMedian")
            query.push_back({2, 0});
    }

    solver(query);
    return 0;
}