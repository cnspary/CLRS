//
// Created by cnspary on 2020/3/16.
//

#include "bits/stdc++.h"

using namespace std;

long long GCD(long long a, long long b) {
    if (a < b) swap(a, b);
    if (b == 0) return a;

    while (a % b != 0) {
        int c = a % b;
        a = b;
        b = c;
    }
    return b;
}

struct Rational {
    long int numerator;
    long int denominator;

    Rational() {}

    Rational(long int n, long int d) {
        numerator = n;
        denominator = d;
    }

    string Rstring() {
        if (numerator == 0) return "0";

        long int n = abs(numerator);
        long int d = denominator;
        bool neg = (numerator < 0);

        long int gcd = GCD(n, d);
        n /= gcd;
        d /= gcd;

        long int I = n / d;
        n %= d;

        string r;

        if (I != 0 && n != 0) r = to_string(I) + " " + to_string(n) + "/" + to_string(d);
        else if (I != 0 && n == 0) r = to_string(I);
        else if (I == 0 && n != 0) r = to_string(n) + "/" + to_string(d);

        if (neg) r = "(-" + r + ")";
        return r;
    }
};

void solver(Rational a, Rational b) {
    // a + b
    Rational sum;
    sum.numerator = a.numerator * b.denominator + a.denominator * b.numerator;
    sum.denominator = a.denominator * b.denominator;
    printf("%s + %s = %s\n", a.Rstring().c_str(), b.Rstring().c_str(), sum.Rstring().c_str());

    // a - b
    Rational difference;
    difference.numerator = a.numerator * b.denominator - a.denominator * b.numerator;
    difference.denominator = a.denominator * b.denominator;
    printf("%s - %s = %s\n", a.Rstring().c_str(), b.Rstring().c_str(), difference.Rstring().c_str());

    // a * b
    Rational product;
    product.numerator = a.numerator * b.numerator;
    product.denominator = a.denominator * b.denominator;
    printf("%s * %s = %s\n", a.Rstring().c_str(), b.Rstring().c_str(), product.Rstring().c_str());

    // a / b
    if (b.Rstring() == "0") printf("%s / %s = Inf", a.Rstring().c_str(), b.Rstring().c_str());
    else {
        Rational quotient;
        quotient.numerator = a.numerator * (b.numerator / abs(b.numerator)) * b.denominator;
        quotient.denominator = a.denominator * abs(b.numerator);
        printf("%s / %s = %s", a.Rstring().c_str(), b.Rstring().c_str(), quotient.Rstring().c_str());
    }
    return;
}

int main() {
    Rational a, b;
    scanf("%ld/%ld", &a.numerator, &a.denominator);
    scanf("%ld/%ld", &b.numerator, &b.denominator);
    solver(a, b);
    return 0;
}