//
// Created by cnspary on 2020/3/6.
//

#include "bits/stdc++.h"

using namespace std;

bool check(vector<int> aim, int M, int N) {
    stack<int> S;
    int i = 1, j = 0;
    while (i <= N) {
        if (S.size() < M) {
            S.push(i++);
            while (!S.empty() && S.top() == aim[j]) {
                S.pop();
                j++;
            }
        } else break;
    }
    if (S.empty()) return true;
    else return false;
}

void solver(vector<vector<int>> seqs, int M, int N) {
    for (int i = 0; i < seqs.size(); ++i) {
        if (check(seqs[i], M, N)) printf("YES\n");
        else printf("NO\n");
    }
    return;
}

int main() {
    int M, N, K;
    scanf("%d %d %d", &M, &N, &K);
    vector<vector<int>> seqs(K, vector<int>(N));

    for (int i = 0; i < K; ++i) {
        for (int j = 0; j < N; ++j) {
            scanf("%d", &seqs[i][j]);
        }
    }

    solver(seqs, M, N);

    return 0;
}