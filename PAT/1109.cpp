//
// Created by cnspary on 2020/3/18.
//

#include "bits/stdc++.h"

using namespace std;

struct Person {
    string name;
    int height;

    Person() {}

    Person(string n, int h) {
        name = n;
        height = h;
    }
};

bool cmp(Person a, Person b) {
    if (a.height > b.height) return true;
    else if (a.height == b.height && a.name < b.name) return true;
    else return false;
}

int N, M;
vector<Person> People;

void solver() {
    vector<vector<Person>> ans;
    sort(People.begin(), People.end(), cmp);
    M = People.size() / M;

    int i = 0, j = People.size() % M + M;
    for (; j <= People.size(); i = j, j += M) {
        vector<Person> t(j - i);
        int mid = (j - i) / 2;
        t[mid] = People[i];
        int right = mid - 1, left = mid + 1, index = i + 1;
        while (right >= 0 || left < (j - i)) {
            if (right >= 0) t[right--] = People[index++];
            if (left < (j - i)) t[left++] = People[index++];
        }
        ans.push_back(t);
    }

    for (int i = 0; i < ans.size(); ++i) {
        for (int j = 0; j < ans[i].size(); ++j) {
            cout << ans[i][j].name;
            if (j != ans[i].size() - 1) cout << " ";
        }
        cout << "\n";
    }
    return;
}

int main() {
    cin >> N >> M;
    string n;
    int h;
    for (int i = 0; i < N; ++i) {
        cin >> n >> h;
        People.push_back({n, h});
    }
    solver();
    return 0;
}