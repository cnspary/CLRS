//
// Created by cnspary on 2020/3/15.
//

#include "bits/stdc++.h"

using namespace std;

struct TreeNode {
    int id;
    int val;
    int lc, rc;
};
vector<TreeNode> T;
vector<int> InOrderId;
vector<int> LevelOrderId;

void InOrderTrave(TreeNode r) {
    if (r.lc != -1) InOrderTrave(T[r.lc]);
    InOrderId.push_back(r.id);
    if (r.rc != -1) InOrderTrave(T[r.rc]);
    return;
}

void LevelOrderTrave(TreeNode root) {
    queue<TreeNode> Q;
    Q.push(root);

    while (!Q.empty()) {
        auto c = Q.front();
        Q.pop();
        LevelOrderId.push_back(c.id);

        if (c.lc != -1) Q.push(T[c.lc]);
        if (c.rc != -1) Q.push(T[c.rc]);
    }
    return;
}

void solver(TreeNode root, vector<int> values) {
    InOrderTrave(root);
    LevelOrderTrave(root);
    sort(values.begin(), values.end());

    vector<int> valueOfNode(InOrderId.size(), 0);

    for (int i = 0; i < values.size(); ++i) {
        int nid = InOrderId[i];
        int val = values[i];
        valueOfNode[nid] = val;
    }

    for (int i = 0; i < LevelOrderId.size(); ++i) {
        printf("%d", valueOfNode[LevelOrderId[i]]);
        if (i != LevelOrderId.size() - 1) printf(" ");
    }
    return;
}

int main() {
    int N;
    scanf("%d", &N);
    T.resize(N);
    vector<int> values;
    for (int i = 0; i < N; ++i) {
        int l, r;
        scanf("%d %d", &l, &r);
        T[i].id = i;
        T[i].lc = l;
        T[i].rc = r;
    }
    for (int i = 0; i < N; ++i) {
        int v;
        scanf("%d", &v);
        values.push_back(v);
    }

    solver(T[0], values);
    return 0;
}