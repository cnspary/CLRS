//
// Created by cnspary on 2020/3/20.
//

#include "bits/stdc++.h"

using namespace std;

vector<string> nums;

void solver() {
    for (string num : nums) {
        int a = stoi(num);
        int b = stoi(num.substr(0, num.size() / 2));
        int c = stoi(num.substr(num.size() / 2));

        if (c != 0 && a % (b * c) == 0) printf("Yes\n");
        else printf("No\n");
    }
}

int main() {
    int N;
    scanf("%d", &N);

    for (int i = 0; i < N; ++i) {
        string s;
        cin >> s;
        nums.push_back(s);
    }
    solver();
    return 0;
}