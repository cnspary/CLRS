//
// Created by cnspary on 2020/3/17.
//

#include "bits/stdc++.h"

using namespace std;

vector<int> NUM(100003, 0);

int main() {
    int N, x;
    scanf("%d", &N);
    for (int i = 0; i < N; ++i) {
        scanf("%d", &x);
        if (x > 0 && x <= 100002) NUM[x] = 1;
    }

    for (int i = 1; i <= 100002; ++i)
        if (NUM[i] == 0) {
            printf("%d", i);
            break;
        }
    return 0;
}