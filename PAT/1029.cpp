//
// Created by cnspary on 2020/2/27.
//

#include "bits/stdc++.h"

using namespace std;

void solver(vector<long int> s1, vector<long int> s2) {
    long int x;
    int mid = (s1.size() + s2.size() + 1) / 2;
    for (int i = 0, j = 0;;) {
        if (!(j < s2.size()) || s1[i] < s2[j]) {
            x = s1[i];
            i++;
        } else if (!(i < s1.size()) || s1[i] >= s2[j]) {
            x = s2[j];
            j++;
        }
        mid--;
        if (mid == 0) {
            printf("%ld", x);
            break;
        }
    }
    return;
}

int main() {
    int N;
    vector<long int> s1, s2;

    scanf("%d", &N);
    s1.resize(N);
    for (int i = 0; i < N; ++i)
        scanf("%ld", &s1[i]);

    scanf("%d", &N);
    s2.resize(N);
    for (int i = 0; i < N; ++i)
        scanf("%ld", &s2[i]);

    solver(s1, s2);
    return 0;
}