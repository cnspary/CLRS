//
// Created by cnspary on 2020/3/18.
//

#include "bits/stdc++.h"

using namespace std;

int N;

int Root;
vector<pair<int, int>> Tree;
vector<int> output;

void LevelTravel() {
    queue<int> Q;
    Q.push(Root);

    vector<int> output;

    while (!Q.empty()) {
        int p = Q.front();
        Q.pop();

        output.push_back(p);

        if (Tree[p].first != -1) Q.push(Tree[p].first);
        if (Tree[p].second != -1) Q.push(Tree[p].second);
    }

    for (int i = 0; i < output.size(); ++i) {
        printf("%d", output[i]);
        if (i != output.size() - 1) printf(" ");
    }
    return;
}


void InTravel() {
    stack<int> S;
    int p = Root;
    auto goalongLeftBranch = [](int p, stack<int> &S) -> void {
        while (p != -1) {
            S.push(p);
            p = Tree[p].first;
        }
    };

    vector<int> output;

    while (true) {
        goalongLeftBranch(p, S);
        if (S.empty()) break;
        p = S.top();
        S.pop();
        output.push_back(p);
        p = Tree[p].second;
    }


    for (int i = 0; i < output.size(); ++i) {
        printf("%d", output[i]);
        if (i != output.size() - 1) printf(" ");
    }
    return;
}

int main() {
    scanf("%d", &N);
    vector<int> Child(N, 0);
    Tree.resize(N, {-1, -1});
    for (int i = 0; i < N; ++i) {
        char l, r;
        getchar();
        scanf("%c %c", &l, &r);
        if (r != '-') {
            Tree[i].first = r - '0';
            Child[r - '0'] = 1;
        }
        if (l != '-') {
            Tree[i].second = l - '0';
            Child[l - '0'] = 1;
        }
    }

    for (int i = 0; i < Child.size(); ++i)
        if (Child[i] == 0) Root = i;

    LevelTravel();
    printf("\n");
    InTravel();
    return 0;
}