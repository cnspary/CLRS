//
// Created by cnspary on 2020/3/8.
//

#include "bits/stdc++.h"

using namespace std;

void solver(vector<set<int>> sets, vector<pair<int, int>> query) {
    for (int i = 0; i < query.size(); ++i) {
        int count = 0;
        int a = query[i].first;
        int b = query[i].second;

        for (int x : sets[b])
            if (sets[a].find(x) != sets[a].end())
                count++;

        printf("%.1lf%%\n", (double(count)) / (sets[a].size() + sets[b].size() - count) * 100);
    }
    return;
}

int main() {
    int N;
    scanf("%d", &N);
    vector<set<int>> sets(N);
    int m, x;
    for (int i = 0; i < N; ++i) {
        scanf("%d", &m);
        for (int j = 0; j < m; ++j) {
            scanf("%d", &x);
            sets[i].insert(x);
        }
    }

    scanf("%d", &N);
    vector<pair<int, int>> query;
    for (int i = 0; i < N; ++i) {
        scanf("%d %d", &m, &x);
        query.push_back({m - 1, x - 1});
    }

    solver(sets, query);
    return 0;
}