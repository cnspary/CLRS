//
// Created by cnspary on 2020/2/28.
//

#include "bits/stdc++.h"

using namespace std;

int length(string head, vector<pair<string, string>> index, map<string, int> HT) {
    int p = HT[head], l = 1;
    while (index[p].second != "-1") {
        l++;
        p = HT[index[p].second];
    }
    return l;
}

void solver(string head1, string head2, vector<pair<string, string>> index) {
    map<string, int> HT;
    for (int i = 0; i < index.size(); ++i)
        HT[index[i].first] = i;

    if (HT.find(head1) == HT.end() || HT.find(head2) == HT.end()) {
        cout << -1;
        return;
    }

    int l1 = length(head1, index, HT);
    int l2 = length(head2, index, HT);

    if (l1 < l2) {
        swap(head1, head2);
        swap(l1, l2);
    }
    int step = l1 - l2;
    int h1 = HT[head1], h2 = HT[head2];
    while (step > 0) {
        h1 = HT[index[h1].second];
        step--;
    }

    while (index[h1].second != "-1" && index[h2].second != "-1") {
        if (index[h1].first == index[h2].first)
            break;
        h1 = HT[index[h1].second];
        h2 = HT[index[h2].second];
    }

    if (index[h1].first == index[h2].first)
        cout << index[h1].first;
    else cout << -1;
    return;
}


int main() {
    ios::sync_with_stdio(false);
    string head1, head2;
    int N;
    cin >> head1 >> head2 >> N;

    string cur, letter, next;
    vector<pair<string, string>> index;
    for (int i = 0; i < N; ++i) {
        cin >> cur;
        cin >> letter;
        cin >> next;
        index.push_back({cur, next});
    }

    solver(head1, head2, index);
    return 0;
}