//
// Created by cnspary on 2020/3/15.
//

#include "bits/stdc++.h"

using namespace std;

vector<int> init;
vector<int> process;

void solver() {
    int Max = -1;
    for (int i = 0; i < process.size(); ++i) {
        Max = max(Max, process[i]);
    }

    if (process.back() != Max) { // Insertion Sort
        printf("Insertion Sort\n");
        int i, tmp;
        for (i = 1; i < process.size(); ++i)
            if (process[i] < process[i - 1]) break;
        tmp = process[i];
        while (0 < i && tmp < process[i - 1]) {
            process[i] = process[i - 1];
            i--;
        }
        process[i] = tmp;
    } else { // Heap Sort
        printf("Heap Sort\n");
        int i;
        for (i = process.size() - 1; -1 < i; --i)
            if (process[i] < process[0]) break;
        swap(process[0], process[i]);

        int root = 0;
        while (true) {
            if (root * 2 + 2 < i && process[root] <
                                    max(process[root * 2 + 1], process[root * 2 + 2])) {
                if (process[root * 2 + 1] > process[root * 2 + 2]) {
                    swap(process[root], process[root * 2 + 1]);
                    root = root * 2 + 1;
                } else {
                    swap(process[root], process[root * 2 + 2]);
                    root = root * 2 + 2;
                }

            } else if (root * 2 + 1 < i && process[root] < process[root * 2 + 1]) {
                swap(process[root], process[root * 2 + 1]);
                root = root * 2 + 1;
            } else break;
        }
    }


    for (int i = 0; i < process.size(); ++i) {
        printf("%d", process[i]);
        if (i != process.size() - 1) printf(" ");
    }
    return;
}

int main() {
    int N;
    scanf("%d", &N);

    init.resize(N);
    process.resize(N);

    for (int i = 0; i < N; ++i)
        scanf("%d", &init[i]);

    for (int i = 0; i < N; ++i)
        scanf("%d", &process[i]);

    solver();

    return 0;
}