//
// Created by cnspary on 2020/3/20.
//

#include "bits/stdc++.h"

using namespace std;

string getNext(string s) {
    int cnt = 1;
    char pivot = s[0];
    string next = "";
    for (int i = 1; i < s.size(); ++i) {
        if (s[i] == pivot) cnt++;
        else {
            next += pivot + to_string(cnt);
            pivot = s[i];
            cnt = 1;
        }
    }
    next += pivot + to_string(cnt);
    return next;
}

void solver(int n, string s) {
    for (int i = 1; i < n; ++i) {
        s = getNext(s);
    }
    cout << s;
    return;
}

int main() {
    int n;
    string s;
    cin >> s >> n;
    solver(n, s);
    return 0;
}