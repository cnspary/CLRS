//
// Created by cnspary on 2020/3/17.
//

#include "bits/stdc++.h"

using namespace std;

int N;
vector<int> Statement;

bool judge(int i, int j, int wlier) {
    for (int hlier = 1; hlier <= N; ++hlier) {
        if (hlier == i || hlier == j) continue;

        bool isOk = true;
        Statement[wlier] *= -1;
        Statement[hlier] *= -1;

        for (int x = 1; x < Statement.size(); ++x) {
            if (Statement[x] > 0 && (Statement[x] == i || Statement[x] == j)) {
                isOk = false;
                break;
            }

            if (Statement[x] < 0 && (Statement[x] != -i && Statement[x] != -j)) {
                isOk = false;
                break;
            }
        }

        Statement[wlier] *= -1;
        Statement[hlier] *= -1;

        if (isOk) return true;
    }
    return false;
}

void solver() {
    for (int i = 1; i <= N - 1; ++i)
        for (int j = i + 1; j <= N; ++j) {
            if (judge(i, j, i) || judge(i, j, j)) {
                printf("%d %d", i, j);
                return;
            }
        }
    printf("No Solution\n");
    return;
}

int main() {
    scanf("%d", &N);
    Statement.resize((N + 1));
    for (int i = 1; i <= N; ++i)
        scanf("%d", &Statement[i]);
    solver();
    return 0;
}