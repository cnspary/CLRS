//
// Created by cnspary on 2020/3/19.
//

#include "bits/stdc++.h"

using namespace std;

int N, M, K;
map<int, set<int>> Graph;
vector<vector<int>> Paths;

void solver() {
    for (auto path:Paths) {
        if (path.size() != N + 1) {
            printf("NO\n");
            continue;
        } else if (path[0] != path.back()) {
            printf("NO\n");
            continue;
        } else {
            vector<int> visited(N + 1, 0);
            bool isOK = true;
            for (int i = 1; i < path.size(); ++i) {
                int f = path[i - 1];
                int t = path[i];
                visited[t]++;
                if (Graph[f].find(t) == Graph[f].end()) {
                    isOK = false;
                    break;
                }
            }

            if (!isOK) {
                printf("NO\n");
                continue;
            }
            for (int i = 1; i <= N; ++i)
                if (visited[i] != 1) {
                    isOK = false;
                    break;
                }
            if (!isOK) printf("NO\n");
            else printf("YES\n");
        }
    }
    return;
}

int main() {
    scanf("%d %d", &N, &M);

    for (int i = 0; i < M; ++i) {
        int a, b;
        scanf("%d %d", &a, &b);
        Graph[a].insert(b);
        Graph[b].insert(a);
    }
    scanf("%d", &K);
    Paths.resize(K);
    for (int i = 0; i < K; ++i) {
        int c;
        scanf("%d", &c);
        Paths[i].resize(c);
        for (int j = 0; j < c; ++j)
            scanf("%d", &Paths[i][j]);
    }
    solver();
    return 0;
}