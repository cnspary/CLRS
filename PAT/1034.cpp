//
// Created by cnspary on 2020/2/28.
//

#include "bits/stdc++.h"

using namespace std;

struct Call {
    string name1;
    string name2;
    int lasting;

    Call(string n1, string n2, int l) {
        name1 = n1;
        name2 = n2;
        lasting = l;
    }
};

map<string, int> IndexTable;
vector<int> Visited;
vector<int> Time;
vector<string> Name;
vector<vector<int>> Graph;

void dfs(int c, int &headId, int &sumOfLasting, int &numOfMember) {
    for (int i = 0; i < Graph[c].size(); ++i) {
        int next = Graph[c][i];
        if (Visited[next] == 0) {
            Visited[next] = 1;
            if (Time[next] > Time[headId])
                headId = next;
            sumOfLasting += Time[next];
            numOfMember++;
            dfs(next, headId, sumOfLasting, numOfMember);
        }
    }
}

void solver(vector<Call> calls, int K) {
    int index = 0;
    for (int i = 0; i < calls.size(); ++i) {
        string n1 = calls[i].name1, n2 = calls[i].name2;
        if (IndexTable.find(n1) == IndexTable.end()) {
            IndexTable.insert({n1, index});
            index++;
        }
        if (IndexTable.find(n2) == IndexTable.end()) {
            IndexTable.insert({n2, index});
            index++;
        }
    }

    Visited.resize(IndexTable.size());
    Time.resize(IndexTable.size());
    Name.resize(IndexTable.size());
    Graph.resize(IndexTable.size());

    for (int i = 0; i < calls.size(); ++i) {
        int n1 = IndexTable[calls[i].name1], n2 = IndexTable[calls[i].name2];
        Time[n1] += calls[i].lasting;
        Name[n1] = calls[i].name1;
        Time[n2] += calls[i].lasting;
        Name[n2] = calls[i].name2;

        Graph[n1].push_back(n2);
        Graph[n2].push_back(n1);
    }

    int headId;
    int sumOfLasting;
    int numOfMember;
    map<string, int> ans;
    for (int i = 0; i < index; ++i) {
        if (Visited[i] == 0) {
            Visited[i] = 1;
            headId = i;
            sumOfLasting = Time[i];
            numOfMember = 1;
            dfs(i, headId, sumOfLasting, numOfMember);
            if (sumOfLasting > 2 * K && numOfMember > 2)
                ans.insert({Name[headId], numOfMember});
        }
    }

    cout << ans.size();
    map<string, int>::iterator Iter;
    for (Iter = ans.begin(); Iter != ans.end(); ++Iter)
        cout << endl << (*Iter).first << " " << (*Iter).second;

    return;
}

int main() {
    ios::sync_with_stdio(false);
    int N, K;
    cin >> N >> K;

    vector<Call> calls;
    string n1, n2;
    int l;
    for (int i = 0; i < N; ++i) {
        cin >> n1 >> n2 >> l;
        calls.push_back(Call(n1, n2, l));
    }
    solver(calls, K);
    return 0;
}

