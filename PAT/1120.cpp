//
// Created by cnspary on 2020/3/19.
//

#include "bits/stdc++.h"

using namespace std;

vector<int> nums;

int getID(int x) {
    int id = 0;
    while (x != 0) {
        id += x % 10;
        x /= 10;
    }
    return id;
}

void solver() {
    set<int> IDs;
    int id;
    for (int n : nums) {
        id = getID(n);
        if (IDs.find(id) == IDs.end())
            IDs.insert(id);
    }

    printf("%d\n", IDs.size());
    for (set<int>::iterator Iter = IDs.begin(); Iter != IDs.end();) {
        printf("%d", *Iter);
        if (++Iter != IDs.end()) printf(" ");
    }
    return;
}

int main() {
    int N;
    scanf("%d", &N);
    nums.resize(N);
    for (int i = 0; i < N; ++i)
        scanf("%d", &nums[i]);
    solver();
    return 0;
}