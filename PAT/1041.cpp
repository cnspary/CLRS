//
// Created by cnspary on 2020/3/2.
//

#include "bits/stdc++.h"

using namespace std;

bool cmp(pair<int, int> a, pair<int, int> b) {
    return a.first < b.first;
}

void solver(vector<pair<int, int>> bets) {
    pair<int, int> ans = {-1, 1000000};
    sort(bets.begin(), bets.end(), cmp);
    for (int i = 0; i < bets.size();) {
        int j = i + 1;
        while (j < bets.size() && bets[j].first == bets[i].first) ++j;

        if (j - i == 1 && bets[i].second < ans.second)
            ans = bets[i];

        i = j;
    }

    if (ans.first < 0) printf("None");
    else printf("%d", ans.first);
    return;
}

int main() {
    int N;
    scanf("%d", &N);

    int a;
    vector<pair<int, int>> bets;
    for (int i = 0; i < N; ++i) {
        scanf("%d", &a);
        bets.push_back({a, i});
    }

    solver(bets);
    return 0;
}