//
// Created by cnspary on 2020/3/19.
//

#include "bits/stdc++.h"

using namespace std;

bool isPrime(int N) {
    for (int i = 2; i <= sqrt(N); ++i)
        if (N % i == 0) return false;
    return true;
}

void solver(vector<int> rank, vector<int> query) {
    set<int> checked;
    map<int, int> Awards;
    for (int i = 1; i < rank.size(); ++i) {
        if (i == 1) Awards.insert({rank[i], 0});
        else if (isPrime(i)) Awards.insert({rank[i], 1});
        else Awards.insert({rank[i], 2});
    }

    for (int i = 0; i < query.size(); ++i) {
        int id = query[i];

        if (Awards.find(id) == Awards.end()) printf("%04d: Are you kidding?\n", id);
        else if (checked.find(id) != checked.end()) printf("%04d: Checked\n", id);
        else {
            checked.insert(id);
            switch (Awards[id]) {
                case 0:
                    printf("%04d: Mystery Award\n", id);
                    break;
                case 1:
                    printf("%04d: Minion\n", id);
                    break;
                case 2:
                    printf("%04d: Chocolate\n", id);
                    break;
            }
        }
    }
    return;
}

int main() {
    int N;
    scanf("%d", &N);
    vector<int> rank(N + 1);
    for (int i = 1; i <= N; ++i)
        scanf("%d", &rank[i]);

    scanf("%d", &N);
    vector<int> query(N);
    for (int i = 0; i < N; ++i)
        scanf("%d", &query[i]);
    solver(rank, query);
    return 0;
}