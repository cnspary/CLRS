//
// Created by cnspary on 2020/3/7.
//

#include "bits/stdc++.h"

using namespace std;

struct Prog {
    int id;
    int value;
    int rank;

    Prog() {}

    Prog(int i, int v) : id(i), value(v), rank(-1) {}
};


void solver(vector<int> Mice, vector<int> initOrder, int N, int G) {
    vector<vector<Prog>> ans;
    vector<Prog> orderedProgs(N);
    vector<Prog> progs;
    for (int i : initOrder)
        progs.push_back({i, Mice[i]});
    for (Prog p: progs)
        orderedProgs[p.id] = p;

    ans.push_back(progs);

    while (progs.size() > 1) {
        vector<Prog> winner;
        int count = 0;
        int MaxIndex = 0;
        for (int i = 0; i < progs.size(); ++i) {
            if (progs[MaxIndex].value < progs[i].value)
                MaxIndex = i;
            count++;
            if (count == G) {
                count = 0;
                winner.push_back(progs[MaxIndex]);
                MaxIndex = i + 1;
            }
        }
        if (count != 0) winner.push_back(progs[MaxIndex]);
        progs = winner;
        ans.push_back(progs);
    }

    int level = 1;
    for (int i = ans.size() - 1; -1 < i; --i) {
        int count = 0;
        for (Prog p : ans[i]) {
            if (orderedProgs[p.id].rank == -1) {
                count++;
                orderedProgs[p.id].rank = level;
            }
        }
        level += count;
    }

    printf("%d", orderedProgs[0].rank);
    for (int i = 1; i < orderedProgs.size(); ++i)
        printf(" %d", orderedProgs[i].rank);

    return;
}

int main() {
    int N, G;
    scanf("%d %d", &N, &G);

    vector<int> Mice(N);
    for (int i = 0; i < N; ++i)
        scanf("%d", &Mice[i]);
    vector<int> initOrder(N);
    for (int i = 0; i < N; ++i)
        scanf("%d", &initOrder[i]);

    solver(Mice, initOrder, N, G);
    return 0;
}