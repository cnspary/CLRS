//
// Created by cnspary on 2020/3/16.
//

#include "bits/stdc++.h"

using namespace std;

int N, M;
vector<vector<int>> T;

void solver() {
    int curLevel = 0;
    int maxLevel = 0;
    int maxPopulation = 0;

    queue<pair<int, int>> Q;
    Q.push({1, 1});
    while (!Q.empty()) {
        int p = Q.front().first;
        int l = Q.front().second;
        Q.pop();

        if (l != curLevel && maxPopulation < Q.size() + 1) {
            maxPopulation = Q.size() + 1;
            maxLevel = l;
        }
        curLevel = l;

        for (int c : T[p])
            Q.push({c, l + 1});
    }
    printf("%d %d", maxPopulation, maxLevel);
    return;
}

int main() {
    scanf("%d %d", &N, &M);
    T.resize(N + 1);
    for (int i = 0; i < M; ++i) {
        int id, k;
        scanf("%d %d", &id, &k);
        T[id].resize(k);
        for (int j = 0; j < k; ++j)
            scanf("%d", &T[id][j]);
    }
    solver();
    return 0;
}