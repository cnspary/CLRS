//
// Created by cnspary on 2020/2/24.
//
#include "bits/stdc++.h"

using namespace std;

bool earlier(string t1, string t2) {
    int h1, h2;
    int m1, m2;
    int s1, s2;

    h1 = (t1[0] - '0') * 10 + (t1[1] - '0');
    h2 = (t2[0] - '0') * 10 + (t2[1] - '0');

    if (h1 < h2) return true;
    if (h1 > h2) return false;

    m1 = (t1[3] - '0') * 10 + (t1[4] - '0');
    m2 = (t2[3] - '0') * 10 + (t2[4] - '0');

    if (m1 < m2) return true;
    if (m1 > m2) return false;

    s1 = (t1[6] - '0') * 10 + (t1[7] - '0');
    s2 = (t2[6] - '0') * 10 + (t2[7] - '0');

    if (s1 < s2) return true;
    if (s1 > s2) return false;
}

void solver(vector<vector<string>> records) {
    int unlocked = 0;
    int locked = 0;

    for (int i = 1; i < records.size(); ++i) {
        if (earlier(records[i][1], records[unlocked][1]))
            unlocked = i;
        if (earlier(records[locked][2], records[i][2]))
            locked = i;
    }

    cout << records[unlocked][0] << " " << records[locked][0];
}

int main() {
    int N;
    scanf("%d", &N);
    vector<vector<string>> records;
    for (int i = 0; i < N; ++i) {
        string id;
        string signin;
        string signout;
        cin >> id >> signin >> signout;
        records.push_back({id, signin, signout});
    }

    solver(records);
    return 0;
}
