//
// Created by cnspary on 2020/3/20.
//

#include "bits/stdc++.h"

using namespace std;

int N, M;

vector<pair<int, int>> Graph;
vector<set<int>> query;

void solver() {
    for (int i = 0; i < query.size(); ++i) {
        bool isOK = true;
        for (auto e : Graph) {
            if (query[i].find(e.first) == query[i].end() && query[i].find(e.second) == query[i].end()) {
                isOK = false;
                break;
            }
        }

        if (isOK) printf("Yes\n");
        else printf("No\n");

    }
}

int main() {
    scanf("%d %d", &N, &M);
    for (int i = 0; i < M; ++i) {
        int a, b;
        scanf("%d %d", &a, &b);
        Graph.push_back({a, b});
    }
    scanf("%d", &M);
    query.resize(M);
    for (int i = 0; i < M; ++i) {
        int c;
        scanf("%d", &c);
        for (int j = 0; j < c; ++j) {
            int x;
            scanf("%d", &x);
            query[i].insert(x);
        }
    }
    solver();
    return 0;
}