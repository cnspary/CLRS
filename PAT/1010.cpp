//
// Created by cnspary on 2020/2/24.
//

#include "bits/stdc++.h"

using namespace std;

long long getNumber(char c) {
    if ('0' <= c && c <= '9')
        return c - '0';
    else
        return c - 'a' + 10;
}

long long toLongInt(string s, long long radix) {
    long long target = 0;
    for (int i = 0; i < s.size(); ++i) {
        long long a = getNumber(s[i]);
        target = target * radix + a;
    }
    return target;
}

long long getMinRadix(string s) {
    long long min_radix = getNumber(s[0]);
    for (int i = 1; i < s.size(); ++i)
        min_radix = max(min_radix, getNumber(s[i]));
    return min_radix + 1;
}

void solver(string n1, string n2, int tag, long long radix) {
    if (tag == 2) {
        string tmp = n1;
        n1 = n2;
        n2 = tmp;
    }

    long long n1LongInt = toLongInt(n1, radix);

    long long lo = getMinRadix(n2), hi = n1LongInt + 1;
    while (lo < hi) {
        long long mid = lo + (hi - lo) / 2;
        long long n2LongInt = toLongInt(n2, mid);

        // 0 < n2LongInt : no-overflow
        if (0 < n2LongInt && n2LongInt < n1LongInt)
            lo = mid + 1;
        // n2LongInt < 0 : overflow
        else if (n2LongInt >= n1LongInt || n2LongInt < 0)
            hi = mid;
    }
    if (toLongInt(n2, lo) == n1LongInt) printf("%lld", lo);
    else printf("Impossible");

    return;
}

int main() {
    string n1, n2;
    int tag, radix;
    cin >> n1 >> n2 >> tag >> radix;

    solver(n1, n2, tag, radix);
    return 0;
}