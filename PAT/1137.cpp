//
// Created by cnspary on 2020/3/20.
//

#include "bits/stdc++.h"

using namespace std;

int N, M, K;

struct Student {
    string name;
    int assigment;
    int mid;
    int final;
    int grade;

    Student(string n) :
            name(n), assigment(-1), mid(-1), final(-1), grade(-1) {}
};

map<string, int> Index;
vector<Student> Info;

bool cmp(Student a, Student b) {
    if (a.grade > b.grade) return true;
    if (a.grade < b.grade) return false;
    return a.name < b.name;
}

void solver() {
    for (int i = 0; i < Info.size(); ++i) {
        if (200 <= Info[i].assigment) {
            int G;
            if (Info[i].final >= Info[i].mid) G = Info[i].final;
            else G = int((double(Info[i].mid) * 0.4 + double(Info[i].final) * 0.6) + 0.5);
            if (G >= 60) Info[i].grade = G;
        }
    }

    sort(Info.begin(), Info.end(), cmp);

    for (Student s : Info) {
        if (s.grade < 0) break;

        cout << s.name << " ";
        cout << s.assigment << " ";
        cout << s.mid << " ";
        cout << s.final << " ";
        cout << s.grade << "\n";
    }
    return;
}

int main() {
    cin >> N >> M >> K;

    for (int i = 0; i < N; ++i) {
        string name;
        int ass;
        cin >> name >> ass;

        if (ass > 900) ass = 900;

        if (Index.find(name) == Index.end()) {
            Student s(name);
            s.assigment = ass;
            Info.push_back(s);
            Index.insert({name, Info.size() - 1});
        } else {
            Info[Index[name]].assigment = ass;
        }

    }
    for (int i = 0; i < M; ++i) {
        string name;
        int mid;
        cin >> name >> mid;

        if (mid > 100) mid = 100;

        if (Index.find(name) == Index.end()) {
            Student s(name);
            s.mid = mid;
            Info.push_back(s);
            Index.insert({name, Info.size() - 1});
        } else {
            Info[Index[name]].mid = mid;
        }
    }
    for (int i = 0; i < K; ++i) {
        string name;
        int final;
        cin >> name >> final;

        if (final > 100) final = 100;

        if (Index.find(name) == Index.end()) {
            Student s(name);
            s.final = final;
            Info.push_back(s);
            Index.insert({name, Info.size() - 1});
        } else {
            Info[Index[name]].final = final;
        }
    }

    solver();
    return 0;
}