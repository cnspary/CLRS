//
// Created by cnspary on 2020/3/16.
//

#include "bits/stdc++.h"

using namespace std;

#define MOD 1000000007

void solver(string s) {
    vector<int> prefixP(s.size());
    vector<int> suffixT(s.size());

    int cntP = 0, cntT = 0;
    for (int i = 0; i < s.size(); ++i) {
        if (s[i] == 'P') cntP++;
        else if (s[i] == 'A') prefixP[i] = cntP;
    }

    for (int i = s.size() - 1; -1 < i; --i) {
        if (s[i] == 'T') cntT++;
        else if (s[i] == 'A') suffixT[i] = cntT;
    }

    int ans = 0;
    for (int i = 0; i < s.size(); ++i) {
        if (s[i] == 'A') {
            ans += (prefixP[i] * suffixT[i]) % MOD;
            ans %= MOD;
        }
    }

    cout << ans;
    return;
}

int main() {
    string s;
    cin >> s;
    solver(s);
    return 0;
}