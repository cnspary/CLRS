//
// Created by cnspary on 2020/3/19.
//

#include "bits/stdc++.h"

using namespace std;

void solver(int N, vector<int> nums) {
    sort(nums.begin(), nums.end(), greater<int>());
    int i;
    for (i = 0; i < N; ++i) {
        if (nums[i] - 1 <= i + 1) break;
    }
    if (nums[i] - 1 <= N) printf("%d", nums[i] - 1);
    else printf("0");
    return;
}

int main() {
    int N;
    scanf("%d", &N);
    vector<int> ride(N);
    for (int i = 0; i < N; ++i)
        scanf("%d", &ride[i]);
    solver(N, ride);
    return 0;
}