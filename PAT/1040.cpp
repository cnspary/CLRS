//
// Created by cnspary on 2020/2/29.
//

#include "bits/stdc++.h"

using namespace std;

string expend(string s) {
    string news = "#";
    for (int i = 0; i < s.size(); ++i) {
        news += s[i];
        news += "#";
    }
    return news;
}

void solver(string s) {
    int maxLength = -1;
    s = expend(s);
    int l, r;
    for (int i = 0; i < s.size(); ++i) {
        l = i - 1;
        r = i + 1;
        while (-1 < l && r < s.size() && s[l] == s[r]) {
            l--;
            r++;
        }
        maxLength = max(maxLength, (r - l - 1) / 2);
    }
    cout << maxLength;
    return;
}

int main() {
    string s;
    getline(cin, s);
    solver(s);
    return 0;
}