//
// Created by cnspary on 2020/3/17.
//

#include "bits/stdc++.h"

using namespace std;

struct Registration {
    string level;
    string siteNumber;
    string Date;
    string testeeNumber;
    int grade;

    Registration(string s, int g) {
        level = s.substr(0, 1);
        siteNumber = s.substr(1, 3);
        Date = s.substr(4, 6);
        testeeNumber = s.substr(10, 3);
        grade = g;
    }

    string Rstring() {
        return level + siteNumber + Date + testeeNumber;
    }
};

bool cmp(Registration a, Registration b) {
    if (a.grade > b.grade) return true;
    else if (a.grade == b.grade && a.Rstring() < b.Rstring()) return true;
    else return false;
}

bool cmp2(pair<string, int> a, pair<string, int> b) {
    if (a.second > b.second) return true;
    else if (a.second == b.second && a.first < b.first) return true;
    else return false;
}

int N, M;
vector<Registration> regs;
vector<pair<int, string>> query;

void solver() {
    vector<vector<Registration>> TAB(3);
    map<string, pair<int, int>> SiteMap;
    map<string, map<string, int>> DataMap;
    map<string, vector<pair<string, int>>> SortDataMap;

    for (auto r : regs) {
        if (r.level == "T") TAB[0].push_back(r);
        else if (r.level == "A") TAB[1].push_back(r);
        else if (r.level == "B") TAB[2].push_back(r);

        SiteMap[r.siteNumber].first++;
        SiteMap[r.siteNumber].second += r.grade;

        if (DataMap.find(r.Date) == DataMap.end())
            DataMap.insert({r.Date, {{r.siteNumber, 1}}});
        else DataMap[r.Date][r.siteNumber]++;
    }

    sort(TAB[0].begin(), TAB[0].end(), cmp);
    sort(TAB[1].begin(), TAB[1].end(), cmp);
    sort(TAB[2].begin(), TAB[2].end(), cmp);

    for (auto x : DataMap) {
        vector<pair<string, int>> z;
        for (auto y : x.second)
            z.push_back({y.first, y.second});
        sort(z.begin(), z.end(), cmp2);
        SortDataMap.insert({x.first, z});
    }

    for (int q = 0; q < query.size(); ++q) {
        int op = query[q].first;
        string s = query[q].second;

        printf("Case %d: %d %s\n", q + 1, op, s.c_str());
        vector<Registration> ans;
        switch (op) {
            case 1:
                int index;
                if (s == "T") index = 0;
                else if (s == "A") index = 1;
                else index = 2;
                if (TAB[index].size() == 0) printf("NA\n");
                else {
                    for (int i = 0; i < TAB[index].size(); ++i)
                        printf("%s %d\n", TAB[index][i].Rstring().c_str(), TAB[index][i].grade);
                }
                break;
            case 2:
                if (SiteMap.find(s) == SiteMap.end()) printf("NA\n");
                else printf("%d %d\n", SiteMap[s].first, SiteMap[s].second);
                break;
            case 3:
                if (SortDataMap.find(s) == SortDataMap.end()) printf("NA\n");
                else {
                    for (auto x : SortDataMap[s])
                        printf("%s %d\n", x.first.c_str(), x.second);
                }
                break;
        }
    }
    return;
}

int main() {
    cin >> N >> M;

    for (int i = 0; i < N; ++i) {
        string S;
        int grade;
        cin >> S >> grade;
        regs.push_back({S, grade});
    }

    for (int i = 0; i < M; ++i) {
        int op;
        string S;
        cin >> op >> S;
        query.push_back({op, S});
    }

    solver();
    return 0;
}