//
// Created by cnspary on 2020/3/8.
//

#include "bits/stdc++.h"

using namespace std;

void solver(string s1, string s2, string s3, string s4) {
    int date = -1, hour = -1, min = -1;

    int i, j;
    for (i = 0, j = 0; i < s1.size() && j < s2.size(); ++i, ++j) {
        if (s1[i] == s2[j] && 'A' <= s1[i] && s1[i] <= 'G') {
            date = s2[i] - 'A';
            break;
        }
    }

    for (i = i + 1, j = j + 1; i < s1.size() && j < s2.size(); ++i, ++j) {
        if (s1[i] == s2[j] && (('0' <= s1[i] && s1[i] <= '9') || ('A' <= s1[i] && s1[i] <= 'N'))) {
            if ('0' <= s1[i] && s1[i] <= '9') hour = s1[i] - '0';
            else if ('A' <= s1[i] && s1[i] <= 'N') hour = s1[i] - 'A' + 10;
            else continue;
            break;
        }
    }

    for (int i = 0, j = 0; i < s3.size() && j < s4.size(); ++i, ++j) {
        if (s3[i] == s4[j] && (('a' <= s3[i] && s3[i] <= 'z') || ('A' <= s3[i] && s3[i] <= 'Z'))) {
            min = i;
            break;
        }
    }
    vector<string> Date = {"MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"};

    printf("%s %02d:%02d", Date[date].c_str(), hour, min);
    return;
}

int main() {
    string s1, s2, s3, s4;
    cin >> s1 >> s2 >> s3 >> s4;
    solver(s1, s2, s3, s4);
    return 0;
}
