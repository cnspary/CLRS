//
// Created by cnspary on 2020/3/20.
//

#include "bits/stdc++.h"

using namespace std;

struct TreeNode {
    string val;
    int lc, rc;

    TreeNode(string v, int l, int r)
            : val(v), lc(l), rc(r) {}
};

int N;
vector<TreeNode> Tree;

string Expression(int p) {
    if (p == -1) return "";
    if (Tree[p].lc == -1 && Tree[p].rc == -1) return Tree[p].val;
    else return "(" + Expression(Tree[p].lc) + Tree[p].val + Expression(Tree[p].rc) + ")";
}

void solver(int root) {
    string exp = Expression(Tree[root].lc) + Tree[root].val + Expression(Tree[root].rc);
    cout << exp;
    return;
}

int main() {
    scanf("%d", &N);

    int root = N * (N + 1) / 2;
    Tree.push_back({"-1", -1, -1});

    string val;
    int l, r;
    for (int i = 1; i <= N; ++i) {
        cin >> val >> l >> r;
        Tree.push_back({val, l, r});
        if (l != -1) root -= l;
        if (r != -1) root -= r;
    }
    solver(root);
    return 0;
}