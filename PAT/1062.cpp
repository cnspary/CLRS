//
// Created by cnspary on 2020/3/8.
//

#include "bits/stdc++.h"

using namespace std;

struct Grade {
    int id;
    int v;
    int t;
    int total;

    Grade(int id, int v, int t) :
            id(id), v(v), t(t), total(v + t) {}
};

bool cmp(Grade a, Grade b) {
    if (a.total > b.total) return true;
    else if (a.total == b.total && a.v > b.v) return true;
    else if (a.total == b.total && a.v == b.v && a.id < b.id) return true;
    else return false;
}

void solver(vector<Grade> sage, vector<Grade> noble, vector<Grade> fool, vector<Grade> rest) {
    sort(sage.begin(), sage.end(), cmp);
    sort(noble.begin(), noble.end(), cmp);
    sort(fool.begin(), fool.end(), cmp);
    sort(rest.begin(), rest.end(), cmp);

    printf("%d\n", sage.size() + noble.size() + fool.size() + rest.size());
    for (auto x : sage)
        printf("%d %d %d\n", x.id, x.v, x.t);
    for (auto x : noble)
        printf("%d %d %d\n", x.id, x.v, x.t);
    for (auto x : fool)
        printf("%d %d %d\n", x.id, x.v, x.t);
    for (auto x : rest)
        printf("%d %d %d\n", x.id, x.v, x.t);
    return;
}

int main() {
    int N, L, H;
    scanf("%d %d %d", &N, &L, &H);

    vector<Grade> sage;
    vector<Grade> noble;
    vector<Grade> fool;
    vector<Grade> rest;
    int id, v, t;
    for (int i = 0; i < N; ++i) {
        scanf("%d %d %d", &id, &v, &t);
        if (v >= H && t >= H) sage.push_back({id, v, t});
        else if (v >= H && L <= t && t < H) noble.push_back({id, v, t});
        else if (L <= v && v < H && L <= t && t < H && v >= t) fool.push_back({id, v, t});
        else if (L <= v && L <= t) rest.push_back({id, v, t});
        else continue;
    }

    solver(sage, noble, fool, rest);
    return 0;
}