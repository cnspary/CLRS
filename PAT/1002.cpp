//
// Created by cnspary on 2020/2/24.
//

#include <bits/stdc++.h>

using namespace std;

struct Poly {
    int ex;
    double coeff;

    Poly(int e, double c) :
            ex(e), coeff(c) {};
};

void solve(vector<Poly> p1, vector<Poly> p2) {
    vector<Poly> ans;
    for (int i = 0, j = 0; i < p1.size() || j < p2.size();) {
        if (p2.size() <= j || p1[i].ex > p2[j].ex)
            ans.push_back(p1[i++]);
        else if (p1.size() <= i || p2[j].ex > p1[i].ex)
            ans.push_back(p2[j++]);
        else {
            if (p1[i].coeff + p2[j].coeff != 0) {
                auto newP = Poly(p1[i].ex, p1[i].coeff + p2[j].coeff);
                ans.push_back(newP);
            }
            i++;
            j++;
        }
    }

    printf("%d", ans.size());
    for (int i = 0; i < ans.size(); ++i)
        printf(" %d %.1lf", ans[i].ex, ans[i].coeff);
    return;
}

int main() {
    vector<Poly> p1;
    vector<Poly> p2;

    int n, e;
    double c;
    scanf("%d", &n);
    for (int i = 0; i < n; ++i) {
        scanf("%d %lf", &e, &c);
        p1.push_back(Poly(e, c));
    }

    scanf("%d", &n);
    for (int i = 0; i < n; ++i) {
        scanf("%d %lf", &e, &c);
        p2.push_back(Poly(e, c));
    }

    solve(p1, p2);
    return 0;
}