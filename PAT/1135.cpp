//
// Created by cnspary on 2020/3/19.
//

#include "bits/stdc++.h"

using namespace std;

typedef enum {
    RED, BLACK
} RBColor;

struct TreeNode {
    int val;
    RBColor color;
    TreeNode *lc, *rc;

    TreeNode(int v, RBColor c) : val(v), color(c), lc(nullptr), rc(nullptr) {}
};

class BST {
private:
    TreeNode *root;
    TreeNode *_hot;
public:
    BST() : root(nullptr), _hot(nullptr) {}

    BST(vector<int> nums) {
        root = nullptr;
        _hot = nullptr;
        for (int v : nums)
            insert(v);
    }

    void search(int v) {
        _hot = nullptr;
        TreeNode *s = root;

        while (s != nullptr) {
            _hot = s;
            if (v <= s->val) s = s->lc;
            else s = s->rc;
        }
        return;
    }

    void insert(int val) {
        if (root == nullptr) {
            root = new TreeNode(abs(val), (val > 0) ? BLACK : RED);
            return;
        }
        int absVal = abs(val);
        search(absVal);
        if (absVal <= _hot->val) _hot->lc = new TreeNode(absVal, (val > 0) ? BLACK : RED);
        else _hot->rc = new TreeNode(absVal, (val > 0) ? BLACK : RED);
        return;
    }

    bool isRBTree() {
        if (root->color == RED) return false;

        int BlackHeight = -1;
        queue<pair<TreeNode *, int>> Q;
        Q.push({root, 1});

        while (!Q.empty()) {
            TreeNode *p = Q.front().first;
            int bh = Q.front().second;
            Q.pop();

            if (p->color == RED) {
                if (p->lc && p->lc->color == RED) return false;
                if (p->rc && p->rc->color == RED) return false;
            }

            if (p->lc) {
                if (p->lc->color == BLACK) Q.push({p->lc, bh + 1});
                else Q.push({p->lc, bh});

            };

            if (p->rc) {
                if (p->rc->color == BLACK) Q.push({p->rc, bh + 1});
                else Q.push({p->rc, bh});
            }

            if (p->lc == nullptr || p->rc == nullptr) {
                if (BlackHeight == -1) BlackHeight = bh;
                else if (BlackHeight != bh) return false;
            }
        }
        return true;
    }
};

int main() {
    int N;
    scanf("%d", &N);

    for (int i = 0; i < N; ++i) {
        int c;
        scanf("%d", &c);
        vector<int> Tree(c);
        for (int j = 0; j < c; ++j)
            scanf("%d", &Tree[j]);

        if (Tree[0] < 0) printf("No\n");
        else {
            BST bst(Tree);
            if (bst.isRBTree()) printf("Yes\n");
            else printf("No\n");
        }
    }
    return 0;
}