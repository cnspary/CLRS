//
// Created by cnspary on 2020/3/7.
//

#include "bits/stdc++.h"

using namespace std;

string format(int K, string num) {
    string snum;
    while (num.size() > 0 && num[0] == '0')
        num.erase(num.begin());

    int i, exp;
    if (num[0] == '.') {
        i = 1, exp = 0;
        for (; i < num.size(); ++i)
            if (num[i] == '0') exp--;
            else break;
        for (; i < num.size(); ++i)
            snum += num[i];
        if (snum.size() == 0) exp = 0;
    } else {
        i = 0, exp = 0;
        for (; i < num.size(); ++i) {
            if (num[i] == '.') {
                i++;
                break;
            }
            exp++;
            snum += num[i];
        }
        for (; i < num.size(); ++i)
            snum += num[i];
    }

    while (snum.size() < K) snum += "0";
    if (snum.size() > K) snum = snum.substr(0, K);

    string ans = "0." + snum + "*10^" + to_string(exp);
    return ans;
}

void solver(int K, string num1, string num2) {
    string snum1 = format(K, num1);
    string snum2 = format(K, num2);
    if (snum1 == snum2) cout << "YES " << snum1;
    else cout << "NO " << snum1 << " " << snum2;
    return;
}

int main() {
    int K;
    string num1, num2;
    cin >> K >> num1 >> num2;
    solver(K, num1, num2);
    return 0;
}


