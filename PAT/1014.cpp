//
// Created by cnspary on 2020/2/25.
//

#include "bits/stdc++.h"

using namespace std;

struct Customer {
    int id;
    int startTime;
    int endTime;
    int winNo;

    Customer(int ID, int st, int et, int no) :
            id(ID), startTime(st), endTime(et), winNo(no) {}
};

bool operator>(Customer a, Customer b) {
    return (a.endTime > b.endTime) || (a.endTime == b.endTime && a.winNo > b.winNo);
}

void solver(int N, int M, vector<int> serTime, vector<int> query) {
    priority_queue<Customer, vector<Customer>, greater<Customer>> PQ;
    vector<int> windowBaseTime(N, 0);
    vector<int> ans(serTime.size(), -1);

    int id;
    for (id = 0; id < serTime.size() && id < N * M; ++id) {
        int winNo = id % N;
        int startTime = windowBaseTime[winNo];
        int endTime = serTime[id] + windowBaseTime[winNo];
        windowBaseTime[winNo] = endTime;
        PQ.push(Customer(id, startTime, endTime, winNo));
    }

    while (!PQ.empty()) {
        auto c = PQ.top();
        PQ.pop();

        if (c.startTime < 540) {
            ans[c.id] = c.endTime;
            if (id < serTime.size()) {
                int winNo = c.winNo;
                int startTime = windowBaseTime[winNo];
                int endTime = serTime[id] + windowBaseTime[winNo];
                windowBaseTime[winNo] = endTime;
                PQ.push(Customer(id, startTime, endTime, winNo));
                id++;
            }
        }
    }

    if (ans[query[0]] == -1)
        printf("Sorry");
    else
        printf("%02d:%02d", (8 + ans[query[0]] / 60), (ans[query[0]] % 60));

    for (int i = 1; i < query.size(); ++i) {
        if (ans[query[i]] == -1) printf("\nSorry");
        else printf("\n%02d:%02d", (8 + ans[query[i]] / 60), (ans[query[i]] % 60));
    }
    return;
}

int main() {
    int N, M, K, Q;
    scanf("%d %d %d %d", &N, &M, &K, &Q);

    vector<int> serTime;
    vector<int> query;
    for (int i = 0; i < K; ++i) {
        int time;
        scanf("%d", &time);
        serTime.push_back(time);
    }

    for (int i = 0; i < Q; i++) {
        int id;
        scanf("%d", &id);
        query.push_back(id - 1);
    }

    solver(N, M, serTime, query);
    return 0;
}