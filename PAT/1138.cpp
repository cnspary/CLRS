//
// Created by cnspary on 2020/3/20.
//

#include "bits/stdc++.h"

using namespace std;

int N;

vector<int> PRE, IN;

void find(int PREL, int PRER, int INL, int INR) {
    if (PREL == PRER) {
        printf("%d", PRE[PREL]);
        return;
    }
    int root = PRE[PREL];
    int INM;
    for (INM = INL; INM <= INR; ++INM)
        if (IN[INM] == root) break;

    int LSize = INM - INL;
    if (LSize != 0) find(PREL + 1, PREL + LSize, INL, INM - 1);
    else find(PREL + 1, PRER, INM + 1, INR);
    return;
}

void solver() {
    find(0, N - 1, 0, N - 1);
    return;
}

int main() {
    scanf("%d", &N);
    PRE.resize(N);
    IN.resize(N);
    for (int i = 0; i < N; ++i)
        scanf("%d", &PRE[i]);
    for (int i = 0; i < N; ++i)
        scanf("%d", &IN[i]);
    solver();
    return 0;
}