//
// Created by cnspary on 2020/3/17.
//

#include "bits/stdc++.h"

using namespace std;


int reSetMSize(int MSize) {
    if (MSize <= 1) return 2;
    for (int i = 2; i <= sqrt(MSize); ++i)
        if (MSize % i == 0) return reSetMSize(MSize + 1);
    return MSize;
}

void solver(int MSize, vector<int> insert, vector<int> find) {
    MSize = reSetMSize(MSize);
    vector<int> bitmap(MSize, 0);

    for (int i = 0; i < insert.size(); ++i) {
        int prob = 0, next;
        do {
            next = (insert[i] + prob * prob) % MSize;
            prob++;
        } while (prob < MSize && bitmap[next] != 0);

        if (prob < MSize) {
            bitmap[next] = insert[i];
        } else {
            printf("%d cannot be inserted.\n", insert[i]);
        }

    }

    int cnt = 0;
    for (int i = 0; i < find.size(); ++i) {
        int prob = 0, next;
        do {
            next = (find[i] + prob * prob) % MSize;
            cnt++;
            prob++;
        } while (prob <= MSize && bitmap[next] != 0 && bitmap[next] != find[i]);
    }

    printf("%0.1lf", cnt * 1.0 / find.size());
    return;
}

int main() {
    int MSize, N, M;
    scanf("%d %d %d", &MSize, &N, &M);

    vector<int> insert(N);
    vector<int> find(M);
    for (int i = 0; i < N; ++i)
        scanf("%d", &insert[i]);
    for (int i = 0; i < M; ++i)
        scanf("%d", &find[i]);

    solver(MSize, insert, find);
    return 0;
}