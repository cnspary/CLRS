//
// Created by cnspary on 2020/3/19.
//

#include "bits/stdc++.h"

using namespace std;

map<int, int> Couples;
set<int> Guests;

void solver() {
    set<int> Singles;
    for (int id : Guests) {
        if (Couples.find(id) == Couples.end())
            Singles.insert(id);
        else if (Guests.find(Couples[id]) == Guests.end())
            Singles.insert(id);
    }
    printf("%d\n", Singles.size());
    for (set<int>::iterator Iter = Singles.begin(); Iter != Singles.end();) {
        printf("%05d", *Iter);
        if (++Iter != Singles.end()) printf(" ");
    }
    return;
}

int main() {
    int N;
    scanf("%d", &N);
    for (int i = 0; i < N; ++i) {
        int a, b;
        scanf("%d %d", &a, &b);
        Couples.insert({a, b});
        Couples.insert({b, a});
    }
    scanf("%d", &N);
    for (int i = 0; i < N; ++i) {
        int a;
        scanf("%d", &a);
        Guests.insert(a);
    }
    solver();
    return 0;
}