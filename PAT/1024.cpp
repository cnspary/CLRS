//
// Created by cnspary on 2020/2/27.
//

#include "bits/stdc++.h"

using namespace std;

string reverse(string bigNum) {
    string r = "";
    for (int i = bigNum.size() - 1; -1 < i; --i)
        r += bigNum[i];
    return r;
}

bool isPalindromic(string bigNum) {
    for (int i = 0, j = bigNum.size() - 1; i < j; ++i, --j)
        if (bigNum[i] != bigNum[j])
            return false;
    return true;
}

string add(string bigNum1, string bigNum2) {
    string sum = "";
    int cBit = 0;
    for (int i = bigNum1.size() - 1; -1 < i; --i) {
        sum += '0' + (((bigNum1[i] - '0') + (bigNum2[i] - '0') + cBit) % 10);
        cBit = ((bigNum1[i] - '0') + (bigNum2[i] - '0') + cBit) / 10;
    }
    if (cBit != 0) sum += to_string(cBit);
    return reverse(sum);
}

void solver(string bigNum, int K) {
    int step = 0;
    while (step < K && !isPalindromic(bigNum)) {
        step++;
        string a = bigNum;
        string b = reverse(bigNum);
        bigNum = add(a, b);
    }
    printf("%s\n", bigNum.c_str());
    printf("%d", step);
}

int main() {
    string bigNum;
    int K;
    cin >> bigNum >> K;
    solver(bigNum, K);
    return 0;
}