//
// Created by cnspary on 2020/3/15.
//

#include "bits/stdc++.h"

using namespace std;

vector<int> nums;

int binSearch(int target) {
    int l = 0, h = nums.size();

    while (l < h) {
        int m = l + (h - l) / 2;
        if (target <= nums[m]) h = m;
        else if (target > nums[m]) l = m + 1;
    }
    return l;
}

void solver(int p) {
    sort(nums.begin(), nums.end());
    int MaxIndex = nums.size() - 1, MinIndex;
    int Max = nums.back(), Min;
    int ans = 0;

    while (MaxIndex + 1 > ans) {
        Min = (Max + p - 1) / p;
        MinIndex = binSearch(Min);
        ans = max(ans, MaxIndex - MinIndex + 1);
        while (nums[MaxIndex] == Max) MaxIndex--;
        Max = nums[MaxIndex];
    }

    printf("%d", ans);
    return;
}

int main() {
    int N, p;
    scanf("%d %d", &N, &p);
    nums.resize(N);
    for (int i = 0; i < N; ++i)
        scanf("%d", &nums[i]);

    solver(p);
    return 0;
}