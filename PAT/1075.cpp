//
// Created by cnspary on 2020/3/11.
//

#include "bits/stdc++.h"

using namespace std;

struct UserInfo {
    int id;
    int total;
    vector<int> score;
    int perfectly_solved;
    int needRank;
};

int N, K, M;
vector<int> FullMark;
vector<vector<int>> Data;
vector<UserInfo> Users;

bool cmp(UserInfo a, UserInfo b) { // a > b
    if (a.needRank > b.needRank) return true;
    if (a.needRank < b.needRank) return false;

    if (a.total > b.total) return true;
    if (a.total < b.total) return false;

    if (a.perfectly_solved > b.perfectly_solved) return true;
    if (a.perfectly_solved < b.perfectly_solved) return false;

    if (a.id < b.id) return true;
    return false;

}

void solver(vector<vector<int>> Data) {
    Users.resize(N);
    for (int i = 0; i < N; ++i) {
        Users[i].id = i;
        Users[i].needRank = 0;
        Users[i].score.resize(K, -2);
    }

    for (auto x : Data) {
        int uid = x[0] - 1;
        int pid = x[1] - 1;
        int grade = x[2];
        Users[uid].score[pid] = max(Users[uid].score[pid], grade);
    }

    for (int i = 0; i < Users.size(); ++i) {
        Users[i].total = 0;
        for (int pid = 0; pid < K; ++pid)
            if (Users[i].score[pid] >= 0) {
                Users[i].needRank = 1;
                Users[i].total += Users[i].score[pid];
                if (Users[i].score[pid] == FullMark[pid])
                    Users[i].perfectly_solved += 1;
            }
    }

    sort(Users.begin(), Users.end(), cmp);

    int rank;
    for (int i = 0; i < Users.size(); ++i) {
        if (Users[i].needRank == 0) break;
        if (i == 0) rank = 1;
        else if (Users[i - 1].total != Users[i].total)
            rank = i + 1;
        printf("%d %05d %d", rank, Users[i].id + 1, Users[i].total);
        for (int j = 0; j < K; ++j) {
            if (Users[i].score[j] == -2) printf(" -");
            else if (Users[i].score[j] == -1) printf(" 0");
            else printf(" %d", Users[i].score[j]);
        }
        printf("\n");
    }
    return;
}

int main() {
    scanf("%d %d %d", &N, &K, &M);
    FullMark.resize(K);
    Data.resize(M, vector<int>(3));

    for (int i = 0; i < K; ++i)
        scanf("%d", &FullMark[i]);
    for (int i = 0; i < M; ++i)
        scanf("%d %d %d", &Data[i][0], &Data[i][1], &Data[i][2]);

    solver(Data);
    return 0;
}