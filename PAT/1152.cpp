//
// Created by cnspary on 2020/3/17.
//

#include "bits/stdc++.h"

using namespace std;

bool isPrime(int N) {
    for (int p = 2; p <= sqrt(N); ++p)
        if (N % p == 0) return false;
    return true;
}

void solver(string bigNum, int m) {
    for (int i = 0; i <= bigNum.size() - m; ++i) {
        int N = stoi(bigNum.substr(i, m));
        if (isPrime(N)) {
            printf("%s", bigNum.substr(i, m).c_str());
            return;
        }
    }
    printf("404");
}

int main() {
    int n, m;
    string bigNum;
    cin >> n >> m;
    cin >> bigNum;

    if (n < m) {
        printf("404");
        return 0;
    }

    solver(bigNum, m);
    return 0;
}