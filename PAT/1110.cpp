//
// Created by cnspary on 2020/3/18.
//

#include "bits/stdc++.h"

using namespace std;

int N;
int root;
vector<pair<int, int>> T;

void solver() {
    queue<pair<int, int>> Q;
    Q.push({root, 0});
    int lastP = -1, lastPid = -1;
    while (!Q.empty()) {
        int p = Q.front().first;
        int pid = Q.front().second;
        lastP = p;
        lastPid = pid;
        Q.pop();

        if (T[p].first != -1) Q.push({T[p].first, pid * 2 + 1});
        if (T[p].second != -1) Q.push({T[p].second, pid * 2 + 2});
    }

    if (lastPid == (N - 1)) printf("YES %d", lastP);
    else printf("NO %d", root);
}

int main() {
    cin >> N;
    T.resize(N, {-1, -1});
    root = (N - 1) * N / 2;
    for (int i = 0; i < N; ++i) {
        string a, b;
        cin >> a >> b;
        if (a != "-") {
            T[i].first = stoi(a);
            root -= stoi(a);
        }
        if (b != "-") {
            T[i].second = stoi(b);
            root -= stoi(b);
        }

    }
    solver();

    return 0;
}