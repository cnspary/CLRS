//
// Created by cnspary on 2020/3/18.
//

#include "bits/stdc++.h"

using namespace std;

int N;
vector<string> nums;

bool judge(string a, double &num) {
    int index = 0;
    if (a[0] == '-') index = 1;

    double Interger = 0, Fractional = 0;
    for (; a[index] != '.' && index < a.size(); index++) {
        if (!isdigit(a[index])) return false;
        else Interger = Interger * 10 + a[index] - '0';
    }

    index++;
    int decimal;
    if (index < a.size() && a.size() - index > 2) return false;
    else decimal = a.size() - index;

    for (; index < a.size(); ++index) {
        if (!isdigit(a[index])) return false;
        else Fractional = Fractional * 10 + a[index] - '0';
    }

    if (Fractional >= 100) return false;

    num = Interger + Fractional / pow(10, decimal);
    if (a[0] == '-') num *= -1;
    if (num < -1000 || num > 1000) return false;
    return true;
}

void solver() {
    int cnt = 0;
    double sum = 0.0, x;
    for (string s : nums)
        if (judge(s, x)) {
            sum += x;
            cnt++;
        } else printf("ERROR: %s is not a legal number\n", s.c_str());

    if (cnt > 1) printf("The average of %d numbers is %.2f", cnt, sum / cnt);
    else if (cnt == 1) printf("The average of %d number is %.2f", cnt, sum);
    else printf("The average of %d numbers is Undefined", cnt);
    return;
}

int main() {
    cin >> N;
    string s;
    for (int i = 0; i < N; ++i) {
        cin >> s;
        nums.push_back(s);
    }
    solver();
    return 0;
}