//
// Created by cnspary on 2020/3/20.
//

#include "bits/stdc++.h"

using namespace std;

int N, K;
vector<int> Click;

void solver() {
    auto cmp = [](pair<int, int> a, pair<int, int> b) -> bool {
        if (a.second > b.second) return true;
        else if (a.second == b.second && a.first < b.first) return true;
        return false;
    };
    map<int, int> Count;
    set<pair<int, int>, decltype(cmp)> Rec(cmp);

    Count.insert({Click[0], 1});
    Rec.insert({Click[0], 1});

    for (int i = 1; i < Click.size(); ++i) {
        printf("%d:", Click[i]);
        int cnt = 0;
        for (auto x : Rec) {
            printf(" %d", x.first);
            cnt++;
            if (cnt == K) break;
        }
        printf("\n");

        if (Count.find(Click[i]) != Count.end()) {
            int oldCount = Count[Click[i]];
            Count[Click[i]]++;

            Rec.erase({Click[i], oldCount});
            Rec.insert({Click[i], oldCount + 1});
        } else {
            Count.insert({Click[i], 1});
            Rec.insert({Click[i], 1});
        }
    }
    return;
}

int main() {
    scanf("%d %d", &N, &K);
    Click.resize(N);
    for (int i = 0; i < N; ++i)
        scanf("%d", &Click[i]);
    solver();
    return 0;
}