//
// Created by cnspary on 2020/2/24.
//

#include "bits/stdc++.h"

using namespace std;

struct Edge {
    int from;
    int to;
    int weight;

    Edge(int f, int t, int w) : from(f), to(t), weight(w) {};
};

vector<int> rescues;
vector<vector<Edge>> Graph;
vector<int> visited;

int numOfminiPath = 0;
int miniPath = INT_MAX;
int maxRescues = -1;

void dfs(int from, int res, int path, int to) {
    if (from == to) {
        if (path < miniPath) {
            numOfminiPath = 1;
            miniPath = path;
            maxRescues = res;
        } else if (path == miniPath) {
            numOfminiPath++;
            maxRescues = max(maxRescues, res);
        }
        return;
    }

    visited[from] = 1;
    for (int i = 0; i < Graph[from].size(); ++i) {
        int next = Graph[from][i].to;
        int p = Graph[from][i].weight;
        if (visited[next] == 0 && p + path <= miniPath) {
            dfs(next, rescues[next] + res, p + path, to);
        }
    }
    visited[from] = 0;
    return;
}

int main() {
    int N, M, C1, C2;

    scanf("%d %d %d %d", &N, &M, &C1, &C2);

    rescues.resize(N);
    Graph.resize(N);
    visited.resize(N);

    for (int i = 0; i < N; ++i) {
        scanf("%d", &rescues[i]);
        visited[i] = 0;
    }

    for (int i = 0; i < M; ++i) {
        int f, t, w;
        scanf("%d %d %d", &f, &t, &w);
        Graph[f].push_back(Edge(f, t, w));
        Graph[t].push_back(Edge(t, f, w));
    }
    dfs(C1, rescues[C1], 0, C2);
    printf("%d %d", numOfminiPath, maxRescues);
    return 0;
}