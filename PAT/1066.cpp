//
// Created by cnspary on 2020/3/9.
//

#include "bits/stdc++.h"

using namespace std;

struct TreeNode {
    int val;
    int height;
    TreeNode *parent;
    TreeNode *lc, *rc;

    TreeNode() {}

    TreeNode(int v) :
            val(v), height(1), parent(nullptr), lc(nullptr), rc(nullptr) {}
};

class AVLTree {
private:
    TreeNode *root;
    TreeNode *_hot;

    void getInsertionPos(int e) {
        TreeNode *insertPos = root;
        _hot = nullptr;
        while (insertPos != nullptr) {
            _hot = insertPos;
            if (e < insertPos->val) insertPos = insertPos->lc;
            else if (e > insertPos->val) insertPos = insertPos->rc;
            else return;
        }
        return;
    }

    int getHeight(TreeNode *x) {
        if (x == nullptr) return 0;
        else return x->height;
    }

    void updateHeight(TreeNode *x) {
        x->height = max(getHeight(x->lc), getHeight(x->rc)) + 1;
    }

    bool isBalance(TreeNode *x) {
        int lh = getHeight(x->lc);
        int rh = getHeight(x->rc);
        if (-2 < lh - rh && lh - rh < 2) return true;
        else return false;
    }

    TreeNode *tallerChild(TreeNode *x) {
        if (getHeight(x->lc) > getHeight(x->rc)) return x->lc;
        else return x->rc;
    }

    TreeNode *connect34(TreeNode *L, TreeNode *M, TreeNode *R,
                        TreeNode *t1, TreeNode *t2, TreeNode *t3, TreeNode *t4) {
        M->lc = L;
        L->parent = M;
        M->rc = R;
        R->parent = M;

        L->lc = t1;
        if (t1) t1->parent = L;
        L->rc = t2;
        if (t2) t2->parent = L;
        R->lc = t3;
        if (t3) t3->parent = R;
        R->rc = t4;
        if (t4) t4->parent = R;

        updateHeight(L);
        updateHeight(R);
        updateHeight(M);
        return M;
    }

    void rotate(TreeNode *r, TreeNode *p, TreeNode *g) {
        TreeNode *gp = g->parent;
        TreeNode *x;
        if (r == p->lc) {
            if (p == g->lc) {
                x = connect34(r, p, g, r->lc, r->rc, p->rc, g->rc);
            } else {
                x = connect34(g, r, p, g->lc, r->lc, r->rc, p->rc);
            }
        } else {
            if (p == g->lc) {
                x = connect34(p, r, g, p->lc, r->lc, r->rc, g->rc);
            } else {
                x = connect34(g, p, r, g->lc, p->lc, r->lc, r->rc);
            }
        }

        if (gp == nullptr) {
            root = x;
            root->parent = nullptr;
        } else {
            if (x->val < gp->val) gp->lc = x;
            else gp->rc = x;
            x->parent = gp;
        }
    }

public:

    AVLTree() {
        root = nullptr;
        _hot = nullptr;
    }

    AVLTree(vector<int> num) {
        root = nullptr;
        _hot = nullptr;
        for (int x:num)
            insert(x);
    }

    void insert(int e) {
        if (root == nullptr) {
            root = new TreeNode(e);
            return;
        }
        getInsertionPos(e);
        TreeNode *x = new TreeNode(e);
        if (e < _hot->val) _hot->lc = x;
        else if (_hot->val < e) _hot->rc = x;
        x->parent = _hot;

        for (TreeNode *g = _hot; g != nullptr; g = g->parent) {
            if (!isBalance(g)) {
                rotate(tallerChild(tallerChild(g)), tallerChild(g), g);
                break;
            } else {
                updateHeight(g);
            }
        }
        return;
    }

    int getRootVal() {
        return root->val;
    }
};

int main() {
    int N;
    scanf("%d", &N);
    vector<int> num(N);
    for (int i = 0; i < N; ++i)
        scanf("%d", &num[i]);
    AVLTree avl(num);
    cout << avl.getRootVal();
    return 0;
}