//
// Created by cnspary on 2020/2/25.
//

#include "bits/stdc++.h"

using namespace std;

bool isPrime(int x) {
    if (x < 2) return false;
    if (x == 2) return true;
    for (int i = 2; i <= sqrt(x); i++)
        if (x % i == 0)
            return false;
    return true;
}

bool ReversPrime(int num, int radix) {
    vector<int> num_radix;
    int num_rev = 0;
    while (num != 0) {
        num_radix.push_back(num % radix);
        num /= radix;
    }

    for (int i = 0; i < num_radix.size(); ++i)
        num_rev = num_rev * radix + num_radix[i];
    return isPrime(num_rev);
}

void solver(vector<pair<int, int>> query) {
    if (isPrime(query[0].first) && ReversPrime(query[0].first, query[0].second)) printf("Yes");
    else printf("No");

    for (int i = 1; i < query.size(); ++i) {
        if (isPrime(query[i].first) && ReversPrime(query[i].first, query[i].second)) printf("\nYes");
        else printf("\nNo");
    }
}

int main() {
    int num, radix;
    vector<pair<int, int>> query;
    do {
        scanf("%d", &num);
        if (num < 0)
            break;
        scanf("%d", &radix);
        query.push_back({num, radix});
    } while (true);
    solver(query);
    return 0;
}