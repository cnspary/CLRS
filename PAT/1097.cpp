//
// Created by cnspary on 2020/3/15.
//

#include "bits/stdc++.h"

using namespace std;

int head, N;
vector<vector<int>> Data;

map<int, int> Index;

struct LinkNode {
    int add;
    int val;
    int next;

    LinkNode *pre;
    LinkNode *succ;

    LinkNode() {};

    LinkNode(int a, int v, int n) :
            add(a), val(v), next(n) {}
};

void solver() {
    LinkNode *SeqListHead = new LinkNode(), *RemoveListHead = new LinkNode();
    LinkNode *SeqListTail = new LinkNode(), *RemoveListTail = new LinkNode();

    SeqListHead->succ = SeqListTail;
    SeqListTail->pre = SeqListHead;

    RemoveListHead->succ = RemoveListTail;
    RemoveListTail->pre = RemoveListHead;

    while (head != -1) {
        int add = Data[Index[head]][0];
        int val = Data[Index[head]][1];
        int next = Data[Index[head]][2];

        LinkNode *x = new LinkNode(add, val, next);

        x->pre = SeqListTail->pre;
        x->succ = SeqListTail;
        x->pre->succ = x;
        x->succ->pre = x;

        head = next;
    }

    LinkNode *pivot = SeqListHead->succ;
    set<int> dup;
    while (pivot != SeqListTail) {
        int absVal = abs(pivot->val);
        if (dup.find(absVal) != dup.end()) {
            LinkNode *tmp = pivot;
            pivot = pivot->succ;

            tmp->pre->succ = tmp->succ;
            tmp->succ->pre = tmp->pre;

            tmp->succ = RemoveListTail;
            tmp->pre = RemoveListTail->pre;
            tmp->pre->succ = tmp;
            tmp->succ->pre = tmp;
        } else {
            dup.insert(absVal);
            pivot = pivot->succ;
        }

    }

    pivot = SeqListHead->succ;
    while (pivot != SeqListTail) {
        if (pivot->succ != SeqListTail) printf("%05d %d %05d\n", pivot->add, pivot->val, pivot->succ->add);
        else printf("%05d %d -1\n", pivot->add, pivot->val);
        pivot = pivot->succ;
    }

    pivot = RemoveListHead->succ;
    while (pivot != RemoveListTail) {
        if (pivot->succ != RemoveListTail) printf("%05d %d %05d\n", pivot->add, pivot->val, pivot->succ->add);
        else printf("%05d %d -1\n", pivot->add, pivot->val);
        pivot = pivot->succ;
    }

    return;
}

int main() {
    scanf("%d %d", &head, &N);

    int add, val, next;
    for (int i = 0; i < N; ++i) {
        scanf("%d %d %d", &add, &val, &next);
        Index.insert({add, i});
        Data.push_back({add, val, next});
    }

    solver();
    return 0;
}