//
// Created by cnspary on 2020/3/8.
//

#include "bits/stdc++.h"

using namespace std;

void solver(vector<vector<long long >> query) {
    for (int i = 0; i < query.size(); ++i) {
        long long a = query[i][0];
        long long b = query[i][1];
        long long c = query[i][2];
        long long x = a + b;

        if (a < 0 && b < 0 && x >= 0) printf("Case #%d: false\n", i + 1);
        else if (a > 0 && b > 0 && x < 0) printf("Case #%d: true\n", i + 1);
        else if (x > c) printf("Case #%d: true\n", i + 1);
        else printf("Case #%d: false\n", i + 1);
    }
    return;
}

int main() {
    int T;
    scanf("%d", &T);
    vector<vector<long long >> query;
    long long a, b, c;
    for (int i = 0; i < T; ++i) {
        cin >> a >> b >> c;
        query.push_back({a, b, c});
    }
    solver(query);
    return 0;
}