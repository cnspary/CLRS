//
// Created by cnspary on 2020/3/11.
//

#include "bits/stdc++.h"

using namespace std;

void solver(string notation) {
    bool numPos, expPos;
    string Num;
    int expNum;
    numPos = (notation[0] == '+') ? true : false;
    int i;
    for (i = 1; i < notation.size(); ++i) {
        if (notation[i] == 'E') break;
        else if (notation[i] != '.') Num += notation[i];
    }

    expPos = (notation[i + 1] == '+') ? true : false;
    expNum = stoi(notation.substr(i + 2));

    if (expNum == 0) {
        notation = notation.substr(0, i);
        printf("%s", notation.c_str());
        return;
    }

    if (!numPos) printf("-");
    if (expPos == true) {
        if (Num.size() - 1 <= expNum) {
            Num = Num.insert(Num.length(), expNum - (Num.size() - 1), '0');
            printf("%s", Num.c_str());
        } else {
            string integer, fractional;
            integer = Num.substr(0, 1 + expNum);
            fractional = Num.substr(1 + expNum);
            for(int j = Num.size()-1; 1 + expNum <= j;--j){
                if(Num[j] != 0) break;
            }
            printf("%s.%s", integer.c_str(), fractional.c_str());
        }
    } else {
        Num.insert(0, expNum - 1, '0');
        printf("0.%s", Num.c_str());
    }
    return;
}

int main() {
    string notation;
    cin >> notation;
    solver(notation);
    return 0;
}