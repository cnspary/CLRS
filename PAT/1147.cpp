//
// Created by cnspary on 2020/3/17.
//

#include "bits/stdc++.h"

using namespace std;

int N, M;

vector<vector<int>> Heaps;

bool check(int k, bool isMinHeap, bool isMaxHeap) {
    queue<int> Q;
    Q.push(0);
    while (!Q.empty()) {
        int p = Q.front();
        Q.pop();

        if (p * 2 + 1 < M) {
            if (isMinHeap && Heaps[k][p] > Heaps[k][p * 2 + 1])
                return false;
            else if (isMaxHeap && Heaps[k][p] < Heaps[k][p * 2 + 1])
                return false;
            else Q.push(p * 2 + 1);
        }

        if (p * 2 + 2 < M) {
            if (isMinHeap && Heaps[k][p] > Heaps[k][p * 2 + 2])
                return false;
            else if (isMaxHeap && Heaps[k][p] < Heaps[k][p * 2 + 2])
                return false;
            else Q.push(p * 2 + 2);
        }
    }
    return true;
}

void Post(int n, int p) {
    if (p * 2 + 1 < M) Post(n, p * 2 + 1);
    if (p * 2 + 2 < M) Post(n, p * 2 + 2);
    printf("%d", Heaps[n][p]);
    if (p != 0) printf(" ");
    else printf("\n");
}

void solver() {
    for (int n = 0; n < N; ++n) {
        bool isMinHeap = false, isMaxHeap = false;
        if (Heaps[n][0] > Heaps[n][1]) isMaxHeap = true;
        else isMinHeap = true;

        if (check(n, isMinHeap, isMaxHeap)) {
            if (isMinHeap) printf("Min Heap\n");
            else printf("Max Heap\n");
        } else printf("Not Heap\n");

        Post(n, 0);
    }
    return;
}

int main() {
    scanf("%d %d", &N, &M);
    Heaps.resize(N, vector<int>(M));
    for (int i = 0; i < N; ++i)
        for (int j = 0; j < M; ++j)
            scanf("%d", &Heaps[i][j]);
    solver();
    return 0;
}