//
// Created by cnspary on 2020/2/25.
//

#include "bits/stdc++.h"

using namespace std;

class UnionFind {
private:
    int cnt;
    vector<int> uf;
    vector<int> rank;

public:
    UnionFind(int n) {
        cnt = n;
        uf = vector<int>(n);
        rank = vector<int>(n, 1);
        for (int i = 0; i < n; ++i)
            uf[i] = i;
    }

    int find(int a) {
        int p = a;
        while (uf[a] != a) a = uf[a];
        int root = uf[a];
        while (uf[p] != root) {
            int x = uf[p];
            uf[p] = root;
            p = x;
        }
        return root;
    }

    bool uunion(int a, int b) {
        int r1 = find(a);
        int r2 = find(b);
        if (r1 == r2) return false;

        cnt--;
        if (rank[r1] < rank[r2]) {
            uf[r1] = r2;
            rank[r2] += rank[r1];
        } else {
            uf[r2] = r1;
            rank[r1] += rank[r2];
        }
        return true;
    }

    int count() {
        return cnt;
    }

    bool connected(int x, int y) {
        return find(x) == find(y);
    }
};

void solver(int N, vector<pair<int, int>> graph, vector<int> query) {
    vector<int> ans;
    for (int occupied : query) {
        UnionFind uf(N);
        for (int i = 0; i < graph.size(); ++i) {
            int a = graph[i].first;
            int b = graph[i].second;
            if (a != occupied && b != occupied) {
                uf.uunion(a, b);
            }
        }
        ans.push_back(uf.count() - 2);
    }

    printf("%d", ans[0]);
    for (int i = 1; i < ans.size(); ++i)
        printf("\n%d", ans[i]);
    return;
}

int main() {
    int N, M, K;
    scanf("%d %d %d", &N, &M, &K);
    vector<pair<int, int>> graph;
    vector<int> query;

    for (int i = 0; i < M; ++i) {
        int a, b;
        scanf("%d %d", &a, &b);
        graph.push_back({a - 1, b - 1});
    }

    for (int i = 0; i < K; ++i) {
        int a;
        scanf("%d", &a);
        query.push_back(a - 1);
    }

    solver(N, graph, query);
    return 0;
}
