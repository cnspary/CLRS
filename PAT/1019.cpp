//
// Created by cnspary on 2020/2/26.
//

#include "bits/stdc++.h"

using namespace std;

void solver(int N, int radix) {
    vector<int> s;
    do {
        s.push_back(N % radix);
        N /= radix;
    } while (N != 0);

    bool isPal = true;
    for (int i = 0, j = s.size() - 1; i < j; ++i, --j) {
        if (s[i] != s[j]) {
            isPal = false;
            break;
        }
    }

    if (isPal) printf("Yes\n");
    else printf("No\n");
    for (int i = s.size() - 1; 0 < i; --i)
        printf("%d ", s[i]);
    printf("%d", s[0]);
    return;
}

int main() {
    int N, radix;
    scanf("%d %d", &N, &radix);
    solver(N, radix);
    return 0;
}