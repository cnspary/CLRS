//
// Created by cnspary on 2020/3/5.
//

#include "bits/stdc++.h"

using namespace std;

void solver(int n) {
    int factor = 1, lowNum = 0, cur = 0, highNum = 0, count = 0;
    while (n / factor != 0) {
        lowNum = n % factor;
        cur = (n / factor) % 10;
        highNum = n / factor / 10;

        if (cur == 0)
            count += highNum * factor;
        else if (cur == 1)
            count += highNum * factor + lowNum + 1;
        else
            count += (highNum + 1) * factor;

        factor *= 10;
    }
    printf("%d", count);
    return;
}

int main() {
    int n;
    scanf("%d", &n);
    solver(n);
    return 0;
}