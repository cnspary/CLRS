//
// Created by cnspary on 2020/3/11.
//
#include "bits/stdc++.h"

using namespace std;

struct Edge {
    int tail;
    int head;
    int weight;

    Edge(int t, int h, int w) : tail(t), head(h), weight(w) {}
};

int N, M, K, R;
vector<int> Dis;
vector<vector<Edge>> Graph;

void dijkstra(int root) {
    Dis[root] = 0;
    auto cmp = [](Edge A, Edge B) -> bool { return B.weight < A.weight; };
    priority_queue<Edge, vector<Edge>, decltype(cmp)> PQ(cmp);

    for (int i = 0; i < Graph[root].size(); ++i)
        PQ.push(Graph[root][i]);

    while (!PQ.empty()) {
        auto x = PQ.top();
        PQ.pop();

        int tail = x.tail;
        int head = x.head;

        if (Dis[tail] + x.weight < Dis[head]) {
            Dis[head] = Dis[tail] + x.weight;
            for (int i = 0; i < Graph[head].size(); ++i)
                PQ.push(Graph[head][i]);
        }
    }
    return;
}

void solver() {
    vector<vector<double>> ans;

    for (int root = N; root < N + M; ++root) {
        Dis.clear();
        Dis.resize(N + M, INT_MAX);
        dijkstra(root);

        bool vaildation = true;
        double minimum = R, average = 0;
        for (int i = 0; i < N; ++i) {
            if (Dis[i] > R) {
                vaildation = false;
                break;
            }
            if (Dis[i] < minimum)
                minimum = Dis[i];
            average += Dis[i];

        }

        if (vaildation) ans.push_back({double(root), minimum, average});
    }

    if (ans.size() == 0) {
        printf("No Solution");
        return;
    }

    int G = 0;
    for (int i = 1; i < ans.size(); ++i) {
        if (ans[i][1] > ans[G][1]) G = i;
        else if (ans[i][1] == ans[G][1] && ans[i][2] < ans[G][2]) G = i;
    }

    printf("G%.0lf\n%.1lf %.1lf", ans[G][0] + 1 - N, ans[G][1] * 1.0, ans[G][2] * 1.0 / N);
    return;
}

int main() {
    ios::sync_with_stdio(false);

    cin >> N >> M >> K >> R;
    Graph.resize(N + M, vector<Edge>());

    string f, t, w;
    for (int i = 0; i < K; ++i) {
        cin >> f >> t >> w;
        int tail, head;
        if (*f.begin() != 'G') tail = stoi(f) - 1;
        else tail = N + stoi(f.substr(1)) - 1;

        if (*t.begin() != 'G') head = stoi(t) - 1;
        else head = N + stoi(t.substr(1)) - 1;

        Graph[tail].push_back({tail, head, stoi(w)});
        Graph[head].push_back({head, tail, stoi(w)});
    }

    solver();

    return 0;
}
