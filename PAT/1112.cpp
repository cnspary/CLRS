//
// Created by cnspary on 2020/3/19.
//

#include "bits/stdc++.h"

using namespace std;

void solver(int N, string s) {
    map<char, bool> Stucked;
    bool isStuck;
    for (int i = 0; i <= s.size() - N;) {
        isStuck = true;
        for (int j = i + 1; j < i + N; ++j) {
            if (s[i] != s[j]) {
                isStuck = false;
                break;
            }
        }
        if (Stucked.find(s[i]) != Stucked.end()) {
            if (Stucked[s[i]] && !isStuck)
                Stucked[s[i]] = false;
        } else Stucked.insert({s[i], isStuck});

        if (!isStuck) i++;
        else i += N;
    }

    set<char> detected;
    for (int i = 0; i < s.size(); ++i) {
        if (Stucked[s[i]] && detected.find(s[i]) == detected.end()) {
            cout << s[i];
            detected.insert(s[i]);
        }
    }
    cout << "\n";
    for (int i = 0; i < s.size();) {
        cout << s[i];
        if (!Stucked[s[i]]) ++i;
        else i += N;
    }
    return;
}

int main() {
    int N;
    string s;
    cin >> N >> s;
    solver(N, s);
    return 0;
}