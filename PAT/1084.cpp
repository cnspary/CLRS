//
// Created by cnspary on 2020/3/15.
//

#include "bits/stdc++.h"

using namespace std;

char toUpper(char c) {
    if ('a' <= c && c <= 'z')
        return 'A' + c - 'a';
    return c;
}

void solver(string a, string b) {
    set<char> queryTable;
    set<char> searchTable;
    for (char c : b)
        searchTable.insert(toUpper(c));

    for (char c : a)
        if (queryTable.find(toUpper(c)) == queryTable.end() &&
            searchTable.find(toUpper(c)) == searchTable.end()) {
            queryTable.insert(toUpper(c));
            printf("%c", toUpper(c));
        }
    return;
}

int main() {
    string a, b;
    getline(cin, a);
    getline(cin, b);
    solver(a, b);
    return 0;
}