//
// Created by cnspary on 2020/3/19.
//

#include "bits/stdc++.h"

using namespace std;

vector<vector<int>> Puzzles;

void solver() {
    for (auto puzzle : Puzzles) {
        int size = puzzle.size() - 1;
        vector<int> ROW(size + 1, 0);
        vector<int> COL(size + 1, 0);
        vector<int> MainDiagonal(2 * size, 0);
        vector<int> SecondDiagonal(2 * size, 0);
        bool isOK = true;
        for (int col = 1; col <= size; ++col) {
            int row = puzzle[col];

            if (ROW[row] == 1) {
                isOK = false;
                break;
            } else ROW[row] = 1;

            if (COL[col] == 1) {
                isOK = false;
                break;
            } else COL[col] = 1;

            if (MainDiagonal[row + col] == 1) {
                isOK = false;
                break;
            } else MainDiagonal[row + col] = 1;

            if (SecondDiagonal[size + row - col] == 1) {
                isOK = false;
                break;
            } else SecondDiagonal[size + row - col] = 1;
        }

        if (isOK) printf("YES\n");
        else printf("NO\n");;
    }
    return;
}

int main() {
    int N;
    scanf("%d", &N);
    Puzzles.resize(N);
    for (int i = 0; i < N; ++i) {
        int c;
        scanf("%d", &c);
        Puzzles[i].resize(c + 1);
        for (int j = 1; j <= c; ++j)
            scanf("%d", &Puzzles[i][j]);
    }
    solver();
    return 0;
}