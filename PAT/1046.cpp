//
// Created by cnspary on 2020/3/5.
//

#include "bits/stdc++.h"

using namespace std;

void solver(vector<int> dis, vector<pair<int, int>> query) {
    vector<int> prefixSum(dis.size());

    for (int i = 1; i < dis.size(); ++i)
        prefixSum[i] += prefixSum[i - 1] + dis[i - 1];
    prefixSum[0] = prefixSum.back() + dis.back();

    vector<int> ans;
    for (int i = 0; i < query.size(); ++i) {
        int s = query[i].first, e = query[i].second;
        if (s > e) swap(s, e);
        if (s == e) {
            ans.push_back(0);
            continue;
        }

        int dis1 = prefixSum[e] - ((s == 0) ? 0 : prefixSum[s]);
        int dis2 = prefixSum[0] - dis1;

        ans.push_back(min(dis1, dis2));
    }

    for (int x:ans)
        cout << x << endl;
}

int main() {
    int N, K;
    scanf("%d", &N);
    vector<int> dis(N);
    for (int i = 0; i < N; ++i)
        scanf("%d", &dis[i]);

    scanf("%d", &K);
    vector<pair<int, int>> query;
    for (int i = 0; i < K; ++i) {
        int s, e;
        scanf("%d %d", &s, &e);
        query.push_back({s - 1, e - 1});
    }

    solver(dis, query);
    return 0;
}