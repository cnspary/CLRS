//
// Created by cnspary on 2020/3/12.
//

#include "bits/stdc++.h"

using namespace std;

vector<string> sentences;
int shortest = INT_MAX;

void solver() {
    string suffix = "";
    bool finish = false;
    for (int i = 1; i <= shortest; ++i) {
        for (int j = 1; j < sentences.size(); ++j) {
            if (sentences[j - 1][sentences[j - 1].size() - i] != sentences[j][sentences[j].size() - i]) {
                finish = true;
                break;
            }
        }

        if (!finish) suffix = sentences[0][sentences[0].size() - i] + suffix;
        else break;
    }

    if (suffix == "") cout << "nai";
    else cout << suffix;
    return;
}

int main() {
    int N;
    string s;
    cin >> N;
    getchar();
    for (int i = 0; i < N; ++i) {
        getline(cin, s);
        sentences.push_back(s);
        if (shortest > s.size())
            shortest = s.size();
    }
    solver();
    return 0;
}