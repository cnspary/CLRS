//
// Created by cnspary on 2020/3/11.
//

#include "bits/stdc++.h"

using namespace std;

struct LinkNode {
    int address;
    int val;
    int next;
};

void solver(vector<LinkNode> nodes, int start, int K) {
    map<int, LinkNode> ht;
    for (auto x : nodes)
        ht.insert({x.address, x});

    vector<LinkNode> Link;
    do {
        Link.push_back(ht[start]);
        start = ht[start].next;
    } while (start != -1);

    int l;
    for (l = 0; l + K <= Link.size(); l += K) {
        for (int i = l, j = l + K - 1; i < j;)
            swap(Link[i++], Link[j--]);
    }

    for (l = 0; l < Link.size() - 1; ++l)
        Link[l].next = Link[l + 1].address;
    Link.back().next = -1;

    for (auto x : Link)
        if (x.next == -1) printf("%05d %d %d\n", x.address, x.val, x.next);
        else printf("%05d %d %05d\n", x.address, x.val, x.next);
    return;
}

int main() {
    int start, N, K;
    scanf("%d %d %d", &start, &N, &K);
    vector<LinkNode> nodes(N);
    for (int i = 0; i < N; ++i)
        scanf("%d %d %d", &nodes[i].address, &nodes[i].val, &nodes[i].next);
    solver(nodes, start, K);
    return 0;
}