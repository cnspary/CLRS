//
// Created by cnspary on 2020/3/7.
//

#include "bits/stdc++.h"

using namespace std;

long int nextPri(long int n) {
    bool isP;
    long int x = n + 1;
    while (true) {
        isP = true;
        for (long int i = 2; i <= sqrt(x); ++i)
            if (x % i == 0) {
                isP = false;
                break;
            }
        if (isP) break;
        else x++;
    }
    return x;
}

void solver(long int x) {
    printf("%ld=", x);
    if (x == 1) {
        printf("1");
        return;
    }
    vector<pair<long int, long int>> ans;
    long int p = 2;
    while (x != 1) {
        long int exp = 0;
        while (x % p == 0) {
            exp++;
            x /= p;
        }
        if (exp != 0) ans.push_back({p, exp});
        p = nextPri(p);
    }
    for (int i = 0; i < ans.size(); ++i) {
        if (ans[i].second != 1) printf("%ld^%ld", ans[i].first, ans[i].second);
        else printf("%ld", ans[i].first);
        if (i != ans.size() - 1) printf("*");
    }
    return;
}

int main() {
    long int x;
    scanf("%ld", &x);
    solver(x);
    return 0;
}