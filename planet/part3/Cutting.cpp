//
// Created by cnspary on 2019/10/13.
//

#include "bits/stdc++.h"

using namespace std;

vector<int> prices = {0,
                      1, 5, 8, 10, 13, 17, 18, 22, 25, 30,
                      1, 5, 8, 10, 13, 17, 18, 22, 25, 30,
                      1, 5, 8, 10, 13, 17, 18, 22, 25, 30};

vector<int> memo(prices.size());

vector<int> dp(prices.size());

int cutting_v1(int len) { // recursion
    if (len == 0) return 0;

    int revenue = -1;
    for (int i = 1; i <= len; ++i)
        revenue = max(revenue, cutting_v1(len - i) + prices[i]);
    return revenue;
}

int memoized_cut_rod_aux(int len) {
    if (memo[len] >= 0) return memo[len];

    if (len == 0) return 0;

    int revenue = -1;
    for (int i = 1; i <= len; ++i)
        revenue = max(revenue, memoized_cut_rod_aux(len - i) + prices[i]);

    memo[len] = revenue;

    return revenue;
}

int cutting_v2(int len) { // top-down with memoization}
    memo[0] = 0;
    memo[1] = prices[1];
    for (int i = 2; i <= len; i++)
        memo[i] = -1;

    return memoized_cut_rod_aux(len);
}

vector<int> cutting_point(prices.size() + 1);

int cutting_v3(int len) { // bottom up method dp
    dp[0] = 0;

    for (int i = 1; i <= len; i++) {
        cutting_point[i] = i;
        dp[i] = -1;
        for (int p = 1; p <= i; ++p) {
            if (dp[i] < prices[p] + dp[i - p]) {
                cutting_point[i] = p;
                dp[i] = prices[p] + dp[i - p];
            }
        }
    }

    return dp[len];
}

int main() {
    clock_t startTime, endTime;
    startTime = clock();
    cout << cutting_v1(30) << endl;
    endTime = clock();
    cout << "The run time is: " << (double) (endTime - startTime) / CLOCKS_PER_SEC << "s" << endl;

    startTime = clock();
    cout << cutting_v2(30) << endl;
    endTime = clock();
    cout << "The run time is: " << (double) (endTime - startTime) / CLOCKS_PER_SEC << "s" << endl;

    startTime = clock();
    cout << cutting_v3(30) << endl;
    endTime = clock();
    cout << "The run time is: " << (double) (endTime - startTime) / CLOCKS_PER_SEC << "s" << endl;

    for (int x : cutting_point)
        cout << x << " ";
    cout << endl;

    return 0;
}