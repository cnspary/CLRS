//
// Created by cnspary on 2019/10/18.
//

#include "bits/stdc++.h"

using namespace std;

void printBST(vector<vector<int>> dp_c, int l, int r) {
    if (r < l) {
        cout << "D" << r << " ";
        return;
    }
    cout << "K" << dp_c[l][r] << " ";
    printBST(dp_c, l, dp_c[l][r] - 1);
    printBST(dp_c, dp_c[l][r] + 1, r);
}

double OPT_BST(vector<double> nodePro, vector<double> leafPro) {
    int row = nodePro.size() + 1;
    int col = leafPro.size();
    vector<vector<double>> dp(row, vector<double>(col));
    vector<vector<int>> dp_c(row, vector<int>(col));

    vector<double> preSumNode;
    vector<double> preSumLeaf;

    double tmpS1 = 0.0, tmpS2 = 0.0;
    for (int i = 0; i < nodePro.size(); ++i) {
        tmpS1 += nodePro[i];
        tmpS2 += leafPro[i];
        preSumNode.push_back(tmpS1);
        preSumLeaf.push_back(tmpS2);
    }

    dp[0][0] = 0;
    for (int i = 1; i < col; ++i) {
        dp[i][i - 1] = leafPro[i - 1];
        dp[i][i] = nodePro[i] + leafPro[i - 1] * 2 + leafPro[i] * 2;
        dp_c[i][i] = i;
    }
    dp.back().back() = leafPro.back();

    for (int indent = 1; indent < col - 1; ++indent) {
        for (int i = 1, j = i + indent; i < row && j < col; ++i, j = i + indent) {
            dp[i][j] = MAXFLOAT;
            dp_c[i][j] = -1;
            for (int k = i; k <= j; ++k) {
                double change = preSumNode[j] - preSumNode[i - 1];
                if (i - 2 >= 0) change += preSumLeaf[j] - preSumLeaf[i - 2];
                else change += preSumLeaf[j];

                if (change + dp[i][k - 1] + dp[k + 1][j] < dp[i][j]) {
                    dp[i][j] = change + dp[i][k - 1] + dp[k + 1][j];
                    dp_c[i][j] = k;
                }
            }
        }
    }

    printBST(dp_c, 1, col - 1);
    cout << endl;

    return dp[1][col - 1];
}

int main() {
    //                               K1    K2    K3    K4    K5
    vector<double> nodePro1 = {0.00, 0.15, 0.10, 0.05, 0.10, 0.20};

    //                         D0    D1    D2    D3    D4    D5
    vector<double> leafPro1 = {0.05, 0.10, 0.05, 0.05, 0.05, 0.10};
    cout << OPT_BST(nodePro1, leafPro1) << endl;


    //                               K1    K2    K3    K4    K5    K6    K7
    vector<double> nodePro2 = {0.00, 0.04, 0.06, 0.08, 0.02, 0.10, 0.12, 0.14};

    //                         D0    D1    D2    D3    D4    D5    D6    D7
    vector<double> leafPro2 = {0.06, 0.06, 0.06, 0.06, 0.05, 0.05, 0.05, 0.05};
    cout << OPT_BST(nodePro2, leafPro2) << endl;
    return 0;
}