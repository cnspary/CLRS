//
// Created by cnspary on 2019/10/16.
//

#include "bits/stdc++.h"

using namespace std;

struct infoNode {
    int times;
    int cutting;

    infoNode() {};

    infoNode(int T, int C) : times(T), cutting(C) {};
};

int transIndex(int row, int col, int N) {
    int index = 0;
    for (int i = 0; i < row; ++i)
        index += N - i;
    index += col - row;
    return index;
}

void PrintChain(vector<infoNode> &dp, int l, int r, int N) {
    if (l == r)
        cout << "A" << l;

    else {
        cout << "(";
        int index = transIndex(l, r, N);
        PrintChain(dp, l, dp[index].cutting, N);
        PrintChain(dp, dp[index].cutting + 1, r, N);
        cout << ")";
    }
}

int Chain(vector<pair<int, int>> matrixes) {
    int N = matrixes.size();
    vector<infoNode> dp(N * (N + 1) / 2);
    for (int i = 0; i < N; ++i) {
        int index = transIndex(i, i, N);
        dp[index] = infoNode(0, index);
    }

    for (int col = 1; col < N; ++col) {
        for (int i = 0, j = col; j < N; ++i, ++j) {
            int index = transIndex(i, j, N);
            dp[index] = infoNode(INT_MAX, -1);;

            for (int k = i; k < j; ++k) {
                int a1 = transIndex(i, k, N);
                int a2 = transIndex(k + 1, j, N);

                if (dp[index].times >
                    dp[a1].times + dp[a2].times + matrixes[i].first * matrixes[k].second * matrixes[j].second) {
                    dp[index].times =
                            dp[a1].times + dp[a2].times + matrixes[i].first * matrixes[k].second * matrixes[j].second;
                    dp[index].cutting = k;
                }
            }
        }
    }

    PrintChain(dp, 0, N - 1, N);
    cout << endl;
    return dp[N - 1].times;
}

int main() {
    vector<pair<int, int>> matrixes = {{30, 35},
                                       {35, 15},
                                       {15, 5},
                                       {5,  10},
                                       {10, 20},
                                       {20, 25}};
    cout << Chain(matrixes) << endl;
    return 0;
}