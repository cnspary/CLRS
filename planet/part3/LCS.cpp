//
// Created by cnspary on 2019/10/17.
//

#include "bits/stdc++.h"

using namespace std;

void printStr(vector<vector<int>> dp, string str1, string str2) {
    stack<char> S;
    int i = dp.size() - 1, j = dp[0].size() - 1;
    while (0 < i && 0 < j) {
        if (str1[i - 1] == str2[j - 1]) {
            S.push(str1[i - 1]);
            --i;
            --j;
        } else {
            if (dp[i - 1][j] > dp[i][j - 1]) --i;
            else --j;
        }
    }

    while (!S.empty()) {
        cout << S.top();
        S.pop();
    }
    cout << endl;
    return;
}

int LCS(string str1, string str2) {
    vector<vector<int>> dp(str1.length() + 1, vector<int>(str2.length() + 1));
    for (int i = 0; i < dp.size(); ++i)
        dp[i][0] = 0;
    for (int i = 0; i < dp[0].size(); ++i)
        dp[0][i] = 0;

    for (int i = 1; i < dp.size(); ++i)
        for (int j = 1; j < dp[0].size(); ++j)
            dp[i][j] = max(max(dp[i - 1][j], dp[i][j - 1]),
                           dp[i - 1][j - 1] + (int) (str1[i - 1] == str2[j - 1]));

    printStr(dp, str1, str2);
    return dp.back().back();
}

int main() {
    string str1 = "ABCBDBAB", str2 = "BDCABA";
    cout << LCS(str1, str2) << endl;
    return 0;
}