//
// Created by cnspary on 2019/10/1.
//

#include "bits/stdc++.h"
#include "TreeNode.h"

using namespace std;

template<typename NodePos>
inline void attachAsLChild(NodePos p, NodePos lc) {
    p->lc = lc;
    if (lc) lc->parent = p;
}

template<typename NodePos>
inline void attachAsRChild(NodePos p, NodePos rc) {
    p->rc = rc;
    if (rc) rc->parent = p;
}

template<typename T>
class Splay {
private:
    TreeNode<T> *root, *_hot;

    TreeNode<T> *splay(TreeNode<T> *v) {
        if (!v) return nullptr;
        TreeNode<T> *p, *g;
        while ((p = v->parent) && (g = p->parent)) {
            TreeNode<T> *attach = g->parent;
            if (IsLChild(v)) {
                if (IsLChild(p)) {
                    attachAsLChild(p, v->rc);
                    attachAsLChild(g, p->rc);
                    attachAsRChild(p, g);
                    attachAsRChild(v, p);
                } else {
                    attachAsRChild(g, v->lc);
                    attachAsLChild(p, v->rc);
                    attachAsLChild(v, g);
                    attachAsRChild(v, p);
                }
            } else {
                if (IsLChild(p)) {
                    attachAsRChild(p, v->lc);
                    attachAsLChild(g, v->rc);
                    attachAsLChild(v, p);
                    attachAsRChild(v, g);
                } else {
                    attachAsRChild(g, p->lc);
                    attachAsRChild(p, v->lc);
                    attachAsLChild(p, g);
                    attachAsLChild(v, p);
                }
            }
            if (!attach) v->parent = nullptr;
            else {
                (g == attach->lc) ? attachAsLChild(attach, v) : attachAsRChild(attach, v);
            }
        }

        if (p = v->parent) {
            if (IsLChild(v)) {
                attachAsLChild(p, v->rc);
                attachAsRChild(v, p);
            } else {
                attachAsRChild(p, v->lc);
                attachAsLChild(v, p);
            }
        }

        v->parent = nullptr;
        return v;
    }

public:
    Splay(vector<T>
          A) {
        root = nullptr;
        _hot = nullptr;
        for (T a : A)
            insert(a);
    }

    TreeNode<T> *search(T e) {
        if (!root) return nullptr;

        TreeNode<T> *s = root;
        _hot = nullptr;

        while (s) {
            _hot = s;
            if (e < s->val) s = s->lc;
            else if (s->val < e) s = s->rc;
            else break;
        }

        root = splay(s ? s : _hot);
        return root;
    }

    void insert(T e) {
        if (!root) {
            root = new TreeNode<T>(e, nullptr);
            return;
        }

        if (e == search(e)->val) return;

        TreeNode<T> *t = root;
        if (root->val < e) {
            root = t->parent = new TreeNode<T>(e, nullptr);
            root->lc = t;
            root->rc = t->rc;
            t->rc = nullptr;
            if (root->rc) root->rc->parent = root;
        } else {
            root = t->parent = new TreeNode<T>(e, nullptr);
            root->lc = t->lc;
            root->rc = t;
            t->lc = nullptr;
            if (root->lc) root->lc->parent = root;
        }

        return;
    }

    void remove(T e) {
        if (!root || e != search(e)->val) return;
        TreeNode<T> *w = root;
        if (!root->lc) {
            root = root->rc;
            if (root) root->parent = nullptr;
        } else if (!root->rc) {
            root = root->lc;
            if (root) root->parent = nullptr;
        } else {
            TreeNode<T> *lTree = root->lc;
            lTree->parent = nullptr;
            root->lc = nullptr;

            root = root->rc;
            root->parent = nullptr;
            search(w->val);

            root->lc = lTree;
            lTree->parent = root;
        }
    }

    void outputTree() { // level
        LEVELPRINT(root);
    }
};

int main() {
    Splay<int> sp({24, 205, 393, 275, 517,
                   716, 247, 193, 602, 769,
                   595, 425, 340, 943, 483,
                   652, 531, 707, 217, 175,
                   746, 974, 671, 249, 728,
                   343, 537, 614, 414, 787,
                   740, 906, 633, 218, 107,
                   454, 697, 918, 813, 143});
    sp.outputTree();

    for (int i = 0; i < 10; ++i) {
        sp.search(random() % 1000 + 1);
        sp.outputTree();
    }

    sp.remove(1000);
    sp.remove(697);
    sp.remove(974);
    sp.remove(707);
    sp.remove(787);
    sp.outputTree();
    return 0;
}