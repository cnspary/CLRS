//
// Created by cnspary on 2019/9/18.
//

#include "bits/stdc++.h"

using namespace std;

template<typename T>
int partition(vector<T> &array, int lo, int hi) {
    int pivot = lo + random() % (hi - lo + 1);
    int temp = array[pivot];
    swap(array[lo], array[pivot]);

    while (lo < hi) {
        while (lo < hi && temp <= array[hi]) hi--;
        if (lo < hi)
            array[lo] = array[hi];

        while (lo < hi && array[lo] <= temp) lo++;
        if (lo < hi)
            array[hi] = array[lo];
    }

    array[lo] = temp;

    return lo;
}

template<typename T>
T selection(vector<T> &array, int lo, int hi, int k) {
    int mi = partition(array, lo, hi);

    if (mi == k) return array[k];
    else if (mi < k) return selection(array, mi + 1, hi, k);
    else return selection(array, lo, mi - 1, k);

}

int main() {
    vector<int> array = {9, 4, 2, 3, 5, 1, 23, 124, 14, 25, 1, 23, 123, 12, 3, 34, 346, 6, 34, 123, 1};
    cout << selection(array, 0, array.size() - 1, 7) << endl;
    return 0;
}