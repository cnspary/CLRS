//
// Created by cnspary on 2019/9/11.
//

#include "bits/stdc++.h"

using namespace std;

template<typename T, typename Comparator>
class Heap {

private:
    vector<T> heap;
    int size;
    Comparator cmp;
    
protected:
    void percolateDown(int x) {
        while (x * 2 <= size) {
            int swapIndex;
            if (x * 2 + 1 <= size) swapIndex = cmp(heap[x * 2], heap[x * 2 + 1]) ? (x * 2) : (x * 2 + 1);
            else swapIndex = x * 2;

            if (!cmp(heap[x], heap[swapIndex])) {
                heap[0] = heap[x];
                heap[x] = heap[swapIndex];
                heap[swapIndex] = heap[0];
                percolateDown(swapIndex);
            }

            x = swapIndex;
        }
    }

    void percolateUp(int x) {
        while (1 < x) {
            int parent = x / 2;
            if (!cmp(heap[parent], heap[x])) {
                heap[0] = heap[x];
                heap[x] = heap[parent];
                heap[parent] = heap[0];
                percolateDown(parent);
            } else break;
            x = parent;
        }
    }

    void heapify() { // FloydAlg O(n)
        int x = size / 2;
        while (0 < x) {
            int swapIndex;
            if (x * 2 + 1 <= size) swapIndex = cmp(heap[x * 2], heap[x * 2 + 1]) ? (x * 2) : (x * 2 + 1);
            else swapIndex = x * 2;

            if (!cmp(heap[x], heap[swapIndex])) {
                heap[0] = heap[x];
                heap[x] = heap[swapIndex];
                heap[swapIndex] = heap[0];
                percolateDown(swapIndex);
            }

            x--;
        }
    }

public:

    Heap(vector<T> A, Comparator C) {
        heap.push_back(0);
        for (int i = 0; i < A.size(); i++)
            heap.push_back(A[i]);
        cmp = C;
        size = A.size();
        heapify();
    }

    void insert(T const &e) {
        heap.push_back(e);
        size++;
        percolateUp(size);
    }

    T getTop() { return heap[1]; };

    T delTop() {
        heap[0] = heap[1];
        heap[1] = heap[size];
        size--;
        percolateDown(1);
        return heap[0];
    };
};


template<typename T>
struct LessCmp {
    virtual bool operator()(T &a, T &b) { return (a < b); }
};

template<typename T>
struct GreatCmp {
    virtual bool operator()(T &a, T &b) { return (a > b); }
};

int main() {
    vector<int> A = {199, 513, 132, 17, 443, 845, 203, 1103, 910, 831, 110, 1095, 148, 537, 75, 1296, 677, 239, 200,
                     459, 1148, 1380, 1172, 792, 847, 35, 601, 1061, 1093, 57, 735, 537, 1442, 158, 158, 1011, 742,
                     1168, 660, 1045, 335, 601, 1220, 645, 1003, 48, 135, 555, 1200, 1150, 33, 442, 915, 732, 16, 779,
                     796, 969, 871, 835, 365, 1296, 477, 1388, 1125, 774, 25, 683, 437, 1111, 110, 876, 831, 1091, 1066,
                     1115, 1092, 1154, 869, 424, 911, 586, 1256, 1284, 55, 823, 36, 395, 875, 1387, 1082, 1094, 880,
                     1420, 958, 1051, 485, 1103, 1420, 1275, 648, 1048, 1200, 127, 1043, 966, 1333, 491, 588, 410, 967,
                     395, 732, 692, 786, 70, 7, 1044, 396, 241, 1389, 598, 1452, 1476, 1215, 987, 173, 447, 1462, 966,
                     266, 345, 1081, 1462, 1142, 1314, 546, 806, 424, 603, 423, 1127, 181, 346, 109, 187, 1458, 1111,
                     86, 661, 294, 182, 433, 209, 199, 722, 936, 68, 517, 675, 644, 1237, 88, 670, 1379, 1089, 1122,
                     356, 726, 1413, 349, 129, 105, 404, 602, 1201, 579, 32, 882, 627, 1352, 1182, 1497, 640, 292, 1176,
                     247, 194, 116, 892, 851, 621, 492, 556, 1454, 574, 1254, 231, 338, 405};


    LessCmp<int> LessCMP;
    Heap<int, LessCmp<int> > minHeap(A, LessCMP);

    for (int i = 0; i < A.size() / 10; i++) {
        cout << minHeap.getTop() << " ";
        minHeap.delTop();
    }
    cout << endl;

    GreatCmp<int> GreatCMP;
    Heap<int, GreatCmp<int> > maxHeap(A, GreatCMP);

    for (int i = 0; i < A.size() / 10; i++) {
        cout << maxHeap.getTop() << " ";
        maxHeap.delTop();
    }
    cout << endl;

    return 0;
}