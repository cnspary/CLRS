//
// Created by cnspary on 2019/9/14.
//

#include "bits/stdc++.h"

using namespace std;

template<typename T>
int partition(vector<T> &array, int lo, int hi) {
    int pivot = lo + random() % (hi - lo + 1);
    int temp = array[pivot];
    swap(array[lo], array[pivot]);

    while (lo < hi) {
        while (lo < hi && temp <= array[hi]) hi--;
        if (lo < hi)
            array[lo] = array[hi];

        while (lo < hi && array[lo] <= temp) lo++;
        if (lo < hi)
            array[hi] = array[lo];
    }

    array[lo] = temp;

    return lo;
}

template<typename T>
void quicksort(vector<T> &array, int lo, int hi) {
    // Exception O(n)
    if (hi <= lo) return;

    int mi = partition(array, lo, hi);
    quicksort(array, lo, mi - 1);
    quicksort(array, mi + 1, hi);
}

int main() {
    vector<int> array = {9, 4, 2, 3, 5, 1, 23, 124, 14, 25, 1, 23, 123, 12, 3, 34, 346, 6, 34, 123, 1};
    quicksort(array, 0, array.size() - 1);
    for (size_t i = 0; i < array.size(); i++)
        cout << array[i] << " ";
    cout << endl;
    return 0;
}