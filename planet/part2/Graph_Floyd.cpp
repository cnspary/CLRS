//
// Created by cnspary on 2019/9/9.
//

#include "bits/stdc++.h"

using namespace std;

struct Edge {
    int tail;
    int head;
    int weight;

    Edge(int t, int h, int w) : tail(t), head(h), weight(w) {}
};

void Floyd(int n, vector<vector<int>> &pre) {
    for (int round = 0; round < n; ++round) {
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                pre[i][j] = min(pre[i][j], pre[i][round] + pre[round][j]);
            }
        }
    }
}

int main() {
    vector<vector<int>> Graph = {{0,       967, 637,     198, 1000000},
                                 {967,     0,   900,     82,  647},
                                 {637,     900, 0,       771, 1000000},
                                 {198,     82,  771,     0,   196},
                                 {1000000, 647, 1000000, 196, 0}};

    Floyd(5, Graph);

    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 5; j++)
            printf("%d ", Graph[i][j]);
        printf("\n");
    }

    return 0;
}