//
// Created by cnspary on 2019/9/27.
//

#include <bits/stdc++.h>
#include "TreeNode.h"

using namespace std;

template<typename T>
class BST {
private:
    TreeNode<T> *_hot, *root;
    size_t _size;
public:
    BST(vector<T> A) {
        root = nullptr;
        _hot = nullptr;
        _size = 0;

        for (auto a : A)
            insert(a);
    }

    TreeNode<T> *search(T e) {
        if (!root) return nullptr;

        TreeNode<T> *s = root;
        _hot = nullptr;

        while (s) {
            _hot = s;
            if (e < s->val) s = s->lc;
            else if (s->val < e) s = s->rc;
            else break;
        }

        return s;
    }

    void insert(T e) {
        TreeNode<T> *c = search(e);
        if (c) return;

        TreeNode<T> *x = new TreeNode<T>(e, _hot);

        if (!_hot) root = x;
        else (e < _hot->val) ? _hot->lc = x : _hot->rc = x;

        _size++;
    }

    void remove(T e) {
        TreeNode<T> *c = search(e);
        if (!c) return;

        TreeNode<T> *s;
        if (s = c->succ()) {
            swap(c->val, s->val);
            c = s;
        }

        FromParent(c) = c->lc;

        _size--;
    }

    void outputTree() { // level
        LEVELPRINT(root);
    }
};

int main() {
    BST<int> bst({10, 4, 15, 3, 5, 12, 18, 2, 7, 11, 13, 17, 20, 1, 6, 8, 14, 19});

    bst.outputTree();

    bst.remove(8);
    bst.remove(3);
    bst.remove(18);
    bst.remove(10);

    bst.remove(100); // delete not exsit node

    bst.outputTree();
    return 0;
}