//
// Created by cnspary on 2019/9/23.
//

#include "bits/stdc++.h"

using namespace std;

template<typename T>
struct ListNode {
    T val;
    int pos;
    ListNode *pre;
    ListNode *succ;
    ListNode *up;
    ListNode *down;

    ListNode(T v) : val(v), pos(-1),
                    pre(nullptr), succ(nullptr),
                    up(nullptr), down(nullptr) {}
};

template<typename T>
class SkipList {
private:
    vector<ListNode<T> *> sl;

    void insertAfter(ListNode<T> *c, ListNode<T> *x, ListNode<T> *d) {
        // 水平方向
        x->pre = c;
        x->succ = c->succ;
        x->succ->pre = x;
        c->succ = x;

        // 垂直方向
        if (d) {
            x->down = d;
            d->up = x;
        }
    }

public:
    SkipList(int Layer) {
        ListNode<T> *qH = nullptr, *qT = nullptr;

        for (size_t i = 0; i < Layer; ++i) {
            ListNode<T> *head, *tail;

            head = new ListNode<T>(NULL);
            tail = new ListNode<T>(NULL);

            head->pre = nullptr;
            head->succ = tail;
            tail->pre = head;
            tail->succ = nullptr;

            if (qH && qT) {
                head->down = qH;
                qH->up = head;

                tail->down = qT;
                qT->up = tail;
            }

            sl.push_back(head);

            qH = head;
            qT = tail;
        }

    }

    ListNode<T> *search(T v) {
        ListNode<T> *p = sl.back();

        do {
            while (p->succ->succ && p->succ->val <= v)
                p = p->succ;

            if (p->down) p = p->down;
            else break;

        } while (true);
        return p;
    }

    void insert(T v) {
        ListNode<T> *c;
        c = search(v);
        if (c->pre && c->val == v) return;

        ListNode<T> *d = NULL;
        do {
            ListNode<T> *x = new ListNode<T>(v);
            insertAfter(c, x, d);

            while (c->pre && !c->up)
                c = c->pre;
            c = c->up;
            d = x;
        } while (c && random() % 2 == 0);
    }

    void remove(T v) {
        ListNode<T> *c;
        c = search(v);
        if (!c->pre || c->val != v) return;

        ListNode<T> *u = c;
        while (u) {
            u = c->up;
            c->pre->succ = c->succ;
            c->succ->pre = c->pre;
            delete (c);
            c = u;
        }
    }

    void printSkipList() {
        int level = 0, offset = 0;
        ListNode<T> *cur = sl[0]->succ, *u;
        while (cur->succ) {
            cur->pos = offset;
            u = cur;
            while (u->up) {
                u = u->up;
                u->pos = offset;
            }

            offset++;
            cur = cur->succ;
        }

        level = sl.size() - 1;

        while (level >= 0) {
            cur = sl[level]->succ;
            while (cur->succ) {
                for (int j = 1; j < (cur->pos - cur->pre->pos); ++j)
                    cout << setw(4) << " ";
                cout << setw(4) << cur->val;
                cur = cur->succ;
            }
            cout << endl;
            level--;
        }
    }
};

int main() {
    SkipList<int> SL(5);

    vector<int> array = {181, 129, 403, 232, 26,
                         162, 496, 56, 394, 362,
                         145, 41, 264, 221, 98,
                         80, 371, 44, 303, 152,
                         76, 387, 436, 153, 335,
                         183, 405, 359, 457, 107,
                         352, 39, 344, 147, 448,
                         356, 314, 369, 489, 71};
    for (int i = 0; i < array.size(); ++i)
        SL.insert(array[i]);

    SL.printSkipList();

    assert(SL.search(151)->val == 147);
    assert(SL.search(147)->val == 147);

    SL.remove(145);
    SL.remove(98);
    SL.printSkipList();

    return 0;
}
