//
// Created by cnspary on 2019/9/9.
//

#include "bits/stdc++.h"

using namespace std;

struct Edge {
    int tail;
    int head;
    int weight;

    Edge(int t, int h, int w) : tail(t), head(h), weight(w) {}
};

int MST(vector<vector<int>> Graph) {
    vector<int> Vertex(Graph.size());
    vector<Edge> EdgesOfMST;

    auto cmp = [](Edge A, Edge B) -> bool { return B.weight < A.weight; };
    priority_queue<Edge, vector<Edge>, decltype(cmp)> PQ(cmp);

    auto findroot = [&Vertex](int x) -> int {
        int root = x;
        while (root != Vertex[root]) {
            root = Vertex[root];
        }

        while (x != root) {
            int pre = Vertex[x];
            Vertex[x] = root;
            x = pre;
        }

        return root;
    };

    for (int i = 0; i < Vertex.size(); ++i) Vertex[i] = i;

    for (int i = 0; i < Graph.size(); ++i)
        for (int j = 0; j < Graph.size(); ++j)
            if (i != j) PQ.push(Edge(i, j, Graph[i][j]));

    while (!PQ.empty()) {
        auto x = PQ.top();
        PQ.pop();
        int tail = x.tail;
        int head = x.head;

        if (findroot(tail) != findroot(head)) {
            EdgesOfMST.push_back(x);
            Vertex[findroot(head)] = tail;
        }
    }

    int sum = 0;
    for (auto x :EdgesOfMST)
        sum += x.weight;
    return sum;
}

int main() {
    vector<vector<int>> Graph = {{0,    1005, 6963, 392,  1182},
                                 {1005, 0,    1599, 4213, 1451},
                                 {6963, 1599, 0,    9780, 2789},
                                 {392,  4213, 9780, 0,    5236},
                                 {1182, 1451, 2789, 5236, 0}};


    cout << MST(Graph) << endl;

    return 0;
}
