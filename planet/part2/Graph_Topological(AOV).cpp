//
// Created by cnspary on 2019/9/9.
//

#include "bits/stdc++.h"

using namespace std;

struct Edge {
    int tail;
    int head;

    Edge(int t, int h) : tail(t), head(h) {}
};

bool Topological(int v, vector<vector<int>> Edges) {
    vector<int> inDegree(v);
    vector<vector<int>> Graph(v);

    for (int i = 0; i < inDegree.size(); ++i) inDegree[i] = 0;
    for (int i = 0; i < Edges.size(); ++i) {
        int tail = Edges[i][0] - 1;
        int head = Edges[i][1] - 1;

        inDegree[head]++;
        Graph[tail].push_back(head);
    }

    int count = 0;

    queue<int> Q;
    for (int i = 0; i < inDegree.size(); ++i) {
        if (inDegree[i] == 0) {
            Q.push(i);
            count++;
        }
    }

    while (!Q.empty()) {
        auto node = Q.front();
        Q.pop();
        for (int i = 0; i < Graph[node].size(); ++i) {
            inDegree[Graph[node][i]]--;
            if (inDegree[Graph[node][i]] == 0) {
                Q.push(Graph[node][i]);
                count++;
            }
        }
    }

    if (count == v) return true;
    else return false;
}

int main() {
    int v = 4;
    vector<vector<int>> Edges = {{1, 2},
                                 {1, 3},
                                 {2, 4},
                                 {3, 2},
                                 {3, 4}};

    cout << Topological(v, Edges) << endl;
    return 0;
}