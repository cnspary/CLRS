//
// Created by cnspary on 2019/9/11.
//

#include "bits/stdc++.h"

using namespace std;

vector<int> buildNext(string P) {
    vector<int> next(P.size());

    int j = -1, i = 0, pl = P.size();
    next[0] = -1;

    while (i < pl - 1) {
        if (j < 0 || P[i] == P[j]) {
            i++;
            j++;
            next[i] = j;
//            next[i] = (P[i] != P[j] ? j : next[j]);
        } else
            j = next[j];
    }

    cout << "P   :";
    for (char c : P)
        cout << setw(5) << c;
    cout << endl;

    cout << "Next:";
    for (int x : next)
        cout << setw(5) << x;
    cout << endl << endl;

    return next;
}


int KMP(string T, string P) {
    vector<int> next = buildNext(P);
    int i = 0, tl = T.size();
    int j = 0, pl = P.size();

    while (i < tl && j < pl) {
        if (j < 0 || T[i] == P[j]) {
            i++;
            j++;
        } else
            j = next[j];
    }

    return i - j;
}

int main() {
    string T = "#3#4#1#null#null#2#null#null#5#null#null";
    string P = "#4#1#null#null#2#null#null";
    cout << KMP(T, P) << endl;
    return 0;
}