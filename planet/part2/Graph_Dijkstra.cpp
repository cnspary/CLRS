//
// Created by cnspary on 2019/9/9.
//

#include "bits/stdc++.h"

using namespace std;

struct Edge {
    int tail;
    int head;
    int weight;

    Edge(int t, int h, int w) : tail(t), head(h), weight(w) {}
};

int Dijkstra(int N, vector<vector<Edge>> Edges, int start, int end) {
    vector<int> Dis(N + 1);
    for (int i = 0; i < Dis.size(); ++i) Dis[i] = INT_MAX;
    Dis[start] = 0;


    auto cmp = [](Edge A, Edge B) -> bool { return B.weight < A.weight; };
    priority_queue<Edge, vector<Edge>, decltype(cmp)> PQ(cmp);

    for (int i = 0; i < Edges[start].size(); ++i)
        PQ.push(Edges[start][i]);

    while (!PQ.empty()) {
        auto x = PQ.top();
        PQ.pop();

        int tail = x.tail;
        int head = x.head;

        if (Dis[tail] + x.weight < Dis[head]) {
            Dis[head] = Dis[tail] + x.weight;
            for (int i = 0; i < Edges[head].size(); ++i)
                PQ.push(Edges[head][i]);
        }
    }
    return Dis[end];
}

int main() {
    vector<vector<int>> Graph = {{1, 2, 708},
                                 {2, 3, 112},
                                 {3, 4, 721},
                                 {4, 5, 339},
                                 {5, 4, 960},
                                 {1, 5, 849},
                                 {2, 5, 98},
                                 {1, 4, 99},
                                 {2, 4, 25},
                                 {2, 1, 200},
                                 {3, 1, 146},
                                 {3, 2, 106},
                                 {1, 4, 860},
                                 {4, 1, 795},
                                 {5, 4, 479},
                                 {5, 4, 280},
                                 {3, 4, 341},
                                 {1, 4, 622},
                                 {4, 2, 362},
                                 {2, 3, 415},
                                 {4, 1, 904},
                                 {2, 1, 716},
                                 {2, 5, 575}};

    vector<vector<Edge>> edges(5 + 1);

    for (int i = 0; i < Graph.size(); ++i) {
        int tail = Graph[i][0];
        int head = Graph[i][1];
        int weight = Graph[i][2];

        edges[tail].push_back(Edge(tail, head, weight));
        edges[head].push_back(Edge(head, tail, weight));
    }

    cout << Dijkstra(5, edges, 5, 4);
    return 0;
}