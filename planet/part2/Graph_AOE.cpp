//
// Created by cnspary on 2019/9/10.
//

#include "bits/stdc++.h"

using namespace std;

struct Edge {
    int tail;
    int head;
    int duration;

    Edge(int t, int h, int d) : tail(t), head(h), duration(d) {}
};


void forwardVertex(int vertex,
                   vector<int> &inDegree,
                   vector<int> &BegTime,
                   vector<vector<Edge>> &GraphAdj) {
    queue<int> Q;
    Q.push(vertex);

    while (!Q.empty()) {
        int node = Q.front();
        Q.pop();

        for (int i = 0; i < GraphAdj[node].size(); ++i) {
            int tail = GraphAdj[node][i].tail;
            int head = GraphAdj[node][i].head;
            int duration = GraphAdj[node][i].duration;
            BegTime[head] = max(BegTime[head], BegTime[tail] + duration);

            inDegree[head]--;
            if (inDegree[head] == 0) Q.push(head);
        }
    }

    cout << "Forward Vertex Time: " << endl;
    for (int i = 1; i < BegTime.size(); ++i) {
        cout << i << " begtime : " << BegTime[i] << endl;
    }
}

void backwardVertex(int vertex,
                    vector<int> &inDegree,
                    vector<int> &BegTime,
                    vector<vector<Edge>> &GraphAdj) {

    queue<int> Q;
    Q.push(vertex);

    while (!Q.empty()) {
        int node = Q.front();
        Q.pop();

        for (int i = 0; i < GraphAdj[node].size(); ++i) {
            int tail = GraphAdj[node][i].tail;
            int head = GraphAdj[node][i].head;
            int duration = GraphAdj[node][i].duration;
            BegTime[head] = min(BegTime[head], BegTime[tail] + duration);

            inDegree[head]--;
            if (inDegree[head] == 0) Q.push(head);
        }
    }

    cout << "Backward Vertex Time: " << endl;
    for (int i = 1; i < BegTime.size(); ++i) {
        cout << i << " begtime : " << BegTime[i] << endl;
    }
}

void AOE(int N,
         int M,
         int start_vertex,
         int end_vertex,
         vector<vector<int>> Graph) {

    vector<int> inDegree(N + 1);
    vector<int> inDegreeRev(N + 1);
    vector<int> forwardBegTime(N + 1);
    vector<int> backwardBegTime(N + 1);
    vector<int> actForwardBegTime(M + 1);
    vector<int> actBackwardBegTime(M + 1);
    for (int i = 0; i < inDegree.size(); ++i) {
        inDegree[i] = 0;
        inDegreeRev[i] = 0;
        forwardBegTime[i] = 0;
        backwardBegTime[i] = INT_MAX;
    }

    for (int i = 0; i < actForwardBegTime.size(); ++i) {
        actForwardBegTime[i] = 0;
        actBackwardBegTime[i] = 0;
    }

    vector<vector<Edge>> GraphAdj(N + 1);
    vector<vector<Edge>> GraphAdjRev(N + 1);
    for (int i = 0; i < Graph.size(); ++i) {
        int tail = Graph[i][0], head = Graph[i][1], duration = Graph[i][2];
        GraphAdj[tail].push_back(Edge(tail, head, duration));
        inDegree[head]++;

        GraphAdjRev[head].push_back(Edge(head, tail, -duration));
        inDegreeRev[tail]++;
    }

    forwardVertex(start_vertex, inDegree, forwardBegTime, GraphAdj);

    backwardBegTime[end_vertex] = forwardBegTime[end_vertex];
    backwardVertex(end_vertex, inDegreeRev, backwardBegTime, GraphAdjRev);

    for (int i = 0; i < Graph.size(); ++i) {
        int tail = Graph[i][0], head = Graph[i][1], duration = Graph[i][2];
        actForwardBegTime[i] = forwardBegTime[tail];
        actBackwardBegTime[i] = backwardBegTime[head] - duration;
    }

    cout << "The Critical Path : " << endl;
    for (int i = 0; i < Graph.size(); ++i) {
        if (actBackwardBegTime[i] - actForwardBegTime[i] == 0)
            cout << Graph[i][0] << "->"
                 << Graph[i][1] << " : "
                 << Graph[i][2] << endl;
    }

    return;
}

int main() {
    int N = 9; // number of vertexs
    int M = 13; // number of edges
    vector<vector<int>> Graph = {{1, 2, 2},
                                 {1, 3, 5},
                                 {1, 5, 5},
                                 {2, 3, 2},
                                 {3, 5, 1},
                                 {2, 4, 3},
                                 {3, 6, 3},
                                 {5, 7, 6},
                                 {4, 6, 2},
                                 {6, 7, 3},
                                 {6, 8, 4},
                                 {7, 9, 4},
                                 {8, 9, 2}};
    int start_vertex = 1;
    int end_vertex = 9;

    AOE(N, M, start_vertex, end_vertex, Graph);
    return 0;
}