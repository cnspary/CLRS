//
// Created by cnspary on 2019/9/27.
//

#ifndef CLRS_TREENODE_H
#define CLRS_TREENODE_H

#include <bits/stdc++.h>

using namespace std;

#define HasChild(p) ((p->lc) || (p->rc))
#define FromParent(p) ((p->parent) ?  \
                      ((p->parent->lc == p) ? p->parent->lc : p->parent->rc) : \
                      (root))
#define IsLChild(p) ((p->parent) && (p->parent->lc == p))
#define IsRChild(p) ((p->parent) && (p->parent->rc == p))
#define Height(p) ((p) ? p->_height : -1)

template<typename T>
struct TreeNode {
    T val;
    int _height;
    int priority;
    TreeNode<T> *parent;
    TreeNode<T> *lc, *rc;

    TreeNode(T e, int pri, TreeNode<T> *p) :
            val(e),
            _height(0),
            priority(pri),
            parent(p), lc(nullptr), rc(nullptr) {}

    TreeNode(T e, TreeNode<T> *p) :
            val(e),
            _height(0),
            parent(p), lc(nullptr), rc(nullptr) {}

    TreeNode<T> *succ() {
        TreeNode<T> *s = rc;
        while (s && s->lc)
            s = s->lc;
        return s;
    }

};

template<typename T>
void LEVELPRINT(T root) { // level
    queue<T> Q1;
    queue<T> Q2;
    Q1.push(root);
    while (!Q1.empty() || !Q2.empty()) {
        while (!Q1.empty()) {
            auto e = Q1.front();
            Q1.pop();
            if (e == nullptr)
                cout << "null ";
            else {
                cout << e->val << " ";

                Q2.push(e->lc);
                Q2.push(e->rc);
            }
        }
        cout << endl;
        while (!Q2.empty()) {
            auto e = Q2.front();
            Q2.pop();
            if (e == nullptr)
                cout << "null ";
            else {
                cout << e->val << " ";
                Q1.push(e->lc);
                Q1.push(e->rc);
            }
        }
        cout << endl;
    }
    cout << endl;
}

#endif //CLRS_TREENODE_H
