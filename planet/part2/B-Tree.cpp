//
// Created by cnspary on 2019/9/29.
//

#include "bits/stdc++.h"

using namespace std;

template<typename T>
struct BNode {
    vector<T> data;
    BNode<T> *parent;
    vector<BNode<T> *> child;

    BNode() {
        parent = nullptr;
        child.push_back(nullptr);
    }

    BNode(T e, BNode<T> *lc, BNode<T> *rc) {
        parent = nullptr;
        data.insert(0, e);
        child.insert(0, lc);
        child.insert(1, rc);

        if (lc) lc->parent = this;
        if (rc) rc->parent = this;

    }
};

template<typename T>
class B_Tree {
private:
    BNode<T> *root, *_hot;
    int _order;

    template<typename DType>
    int searchInNode(vector<DType> a, DType e) {
        int rank = a.size() - 1;
        while (0 <= rank && e < a[rank]) {
            rank--;
        }
        return rank;
    }

    template<typename DType>
    void insertInNode(vector<DType> &a, int rank, DType e) {
        a.push_back(0);
        for (int i = a.size() - 1; rank + 1 < i; --i)
            a[i] = a[i - 1];
        a[rank + 1] = e;
    }

    template<typename DType>
    void removeInNode(vector<DType> &a, int rank) {
        for (; rank < a.size() - 1; ++rank) {
            a[rank] = a[rank + 1];
        }
        a.pop_back();
    }

    void solveOverflow(BNode<T> *x) {
        if (x->child.size() <= _order) return;

        int rank = _order / 2;
        T bak = x->data[rank];

        BNode<T> *u = new BNode<T>();
        u->data.clear();
        u->child.clear();

        for (int i = rank + 1; i < _order; ++i) {
            u->data.push_back(x->data[i]);
            u->child.push_back(x->child[i]);
        }
        u->child.push_back(x->child.back());

        while (rank < x->child.size() - 1) {
            x->child.pop_back();
            x->data.pop_back();
        }

        if (u->child[0])
            for (int j = 0; j < u->child.size(); ++j)
                u->child[j]->parent = u;

        BNode<T> *p = x->parent;

        if (!p) {
            root = p = new BNode<T>();
            p->child[0] = x;
        }

        x->parent = p;
        u->parent = p;

        rank = searchInNode(p->data, bak);
        insertInNode(p->data, rank, bak);
        insertInNode(p->child, rank + 1, u);

        solveOverflow(p);
    }

    void solveUnderflow(BNode<T> *x) {
        if ((_order + 1) / 2 <= x->child.size()) return;

        BNode<T> *p = x->parent;

        // case: x is root yet
        if (!p) {
            if (x->data.size() == 0 && x->child[0]) {
                root = x->child[0];
                root->parent = nullptr;
            }
            return;
        }

        int rank = 0;
        while (p->child[rank] != x) rank++;

        // case A: 借关键码
        // 此时 x 和 他的兄弟 s(左右兄弟, 必存在一个)  满足可借出条件
        // s->child.size() >=  (_order + 1) / 2 + 1
        // s 借出一个关键码之后 s->child.size() - 1 >= (_order + 1) / 2

        // case A.1: 向左兄弟借关键码(data)
        if (0 < rank) {
            BNode<T> *ls = p->child[rank - 1];
            if ((_order + 1) / 2 < ls->child.size()) { // 严格大于 _order/2 向上取整，以保证至少有一个节点可以借出
                insertInNode(x->data, -1, p->data[rank - 1]);
                p->data[rank - 1] = ls->data[ls->data.size() - 1];
                insertInNode(x->child, -1, ls->child[ls->child.size() - 1]);
                ls->data.pop_back();
                ls->child.pop_back();

                if (x->child[0])
                    x->child[0]->parent = x;
                return;
            }
        }

        // case A.2: 向右兄弟借关键码(data)
        if (rank < p->child.size() - 1) {
            BNode<T> *rs = p->child[rank + 1];
            if ((_order + 1) / 2 < rs->child.size()) {// 严格大于 _order/2 向上取整，以保证至少有一个节点可以借出
                x->data.push_back(p->data[rank]);
                p->data[rank] = rs->data[0];
                x->child.push_back(rs->child[0]);
                removeInNode(rs->data, 0);
                removeInNode(rs->child, 0);

                if (x->child[x->child.size() - 1])
                    x->child[x->child.size() - 1]->parent = x;
                return;
            }
        }

        // case B: 合并
        // 此时 x 和 他的兄弟 s(左右兄弟, 必存在一个)  过瘦
        // s->child.size() == (_order + 1) / 2
        // x->child.size() == (_order + 1) / 2 - 1
        // 将 x 与 s 合并 (x + s)->child.size() == _order

        // case B.1 与左兄弟合并
        if (0 < rank) {
            BNode<T> *ls = p->child[rank - 1];

            ls->data.push_back(p->data[rank - 1]);
            removeInNode(p->data, rank - 1);
            removeInNode(p->child, rank);

            for (int i = 0; i < x->data.size(); ++i) {
                ls->data.push_back(x->data[i]);
                ls->child.push_back(x->child[i]);
                if (x->child[i])
                    x->child[i]->parent = ls;
            }
            ls->child.push_back(x->child[x->child.size() - 1]);
            if (ls->child[ls->child.size() - 1])
                ls->child[ls->child.size() - 1]->parent = ls;

            // return 不可以return
            // 由于上层减少了一个节点 有可能引发上层的Underflow

        } else { // case B.2 与右兄弟合并
            BNode<T> *rs = p->child[rank + 1];

            p->child[rank + 1] = x;
            x->data.push_back(p->data[rank]);
            removeInNode(p->data, rank);
            removeInNode(p->child, rank);

            for (int i = 0; i < rs->data.size(); ++i) {
                x->data.push_back(rs->data[i]);
                x->child.push_back(rs->child[i]);
                if (rs->child[i])
                    rs->child[i]->parent = x;
            }

            x->child.push_back(rs->child[rs->child.size() - 1]);
            if (x->child[x->child.size() - 1])
                x->child[x->child.size() - 1]->parent = x;

            // return 不可以return
            // 由于上层减少了一个节点 有可能引发上层的Underflow
        }

        solveUnderflow(p);
    }

public:
    B_Tree(int ord, vector<T> A) {
        _order = ord;
        _hot = root = nullptr;
        for (auto a : A)
            insert(a);
    }

    BNode<T> *search(T e) {
        BNode<T> *s = root;
        _hot = nullptr;

        while (s) {
            int rank = searchInNode(s->data, e);
            if ((0 <= rank) && (e == s->data[rank])) return s;
            _hot = s;
            s = s->child[rank + 1];
        }

        return nullptr;
    }

    void insert(T e) {
        BNode<T> *s = search(e);
        if (s) return;

        if (!_hot) {
            root = _hot = new BNode<T>();
        }

        int rank = searchInNode(_hot->data, e);
        insertInNode(_hot->data, rank, e);
        _hot->child.push_back(nullptr);

        solveOverflow(_hot);
    }

    bool remove(T e) {
        BNode<T> *v = search(e);
        if (!v) return false;

        int rank = searchInNode(v->data, e);
        if (v->child[0]) {
            BNode<T> *u = v->child[rank + 1];
            while (u->child[0]) u = u->child[0];
            swap(v->data[rank], u->data[0]);
            v = u;
            rank = 0;
        }

        removeInNode(v->data, rank);
        removeInNode(v->child, rank + 1);
        solveUnderflow(v);
        return true;
    }

    void outputTree() { // level
        queue<BNode<T> *> Q1;
        queue<BNode<T> *> Q2;
        int count = 0;
        Q1.push(root);
        while (!Q1.empty() || !Q2.empty()) {
            while (!Q1.empty()) {
                BNode<T> *x = Q1.front();
                Q1.pop();
                for (int i = 0; i < x->data.size(); ++i)
                    cout << x->data[i] << " ";
                for (int i = 0; i < x->child.size(); ++i)
                    if (x->child[i]) Q2.push(x->child[i]);

                cout << "|";
            }
            cout << endl;

            while (!Q2.empty()) {
                BNode<T> *x = Q2.front();
                Q2.pop();
                for (int i = 0; i < x->data.size(); ++i)
                    cout << x->data[i] << " ";
                for (int i = 0; i < x->child.size(); ++i)
                    if (x->child[i]) Q1.push(x->child[i]);

                cout << "|";
            }
            cout << endl;
        }
        cout << endl;
    }
};

int main() {
    B_Tree<int> b_tree(3, {2, 5, 3, 7, 4, 8, 1, 9, 10});
    b_tree.outputTree();

    // test case A1
    b_tree.remove(4);
    b_tree.outputTree();

    // test case A2
    b_tree.remove(7);
    b_tree.outputTree();

    // test case B1
    b_tree.remove(10);
    b_tree.outputTree();

    // test case B2
    b_tree.remove(2);
    b_tree.outputTree();
    b_tree.remove(1);
    b_tree.outputTree();

//    vector<int> node;
//    for (int i = 1; i <= 500; ++i)
//        node.push_back(random() % 500);
//    B_Tree<int> b_tree(4, node);
//    b_tree.outputTree();

    return 0;
}