//
// Created by cnspary on 2019/9/9.
//

#include "bits/stdc++.h"

using namespace std;

struct Edge {
    int tail;
    int head;
    int weight;

    Edge(int t, int h, int w) : tail(t), head(h), weight(w) {}
};

int MST(vector<vector<int>> Graph) {
    int root = 0;
    auto cmp = [](Edge A, Edge B) -> bool { return B.weight < A.weight; };

    vector<int> Vertex(Graph.size(), -1);
    priority_queue<Edge, vector<Edge>, decltype(cmp)> PQ(cmp);
    vector<Edge> EdgesOfMST;

    Vertex[root] = 1;
    for (int i = 0; i < Graph[root].size(); ++i)
        if (Graph[root][i] != 0)
            PQ.push(Edge(root, i, Graph[root][i]));

    while (!PQ.empty()) {
        auto x = PQ.top();
        PQ.pop();
        root = x.head;

        if (Vertex[root] == -1) {
            EdgesOfMST.push_back(x);
            for (int i = 0; i < Graph[root].size(); ++i)
                if (Graph[root][i] != 0 && Vertex[i] == -1)
                    PQ.push(Edge(root, i, Graph[root][i]));
            Vertex[root] = 1;
        }
    }

    int sum = 0;
    for (auto x :EdgesOfMST)
        sum += x.weight;
    return sum;
}

int main() {
    vector<vector<int>> Graph = {{0,    1005, 6963, 392,  1182},
                                 {1005, 0,    1599, 4213, 1451},
                                 {6963, 1599, 0,    9780, 2789},
                                 {392,  4213, 9780, 0,    5236},
                                 {1182, 1451, 2789, 5236, 0}};


    cout << MST(Graph) << endl;
    return 0;
}
