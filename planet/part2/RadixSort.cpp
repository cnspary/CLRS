//
// Created by cnspary on 2019/9/18.
//

#include "bits/stdc++.h"

using namespace std;

size_t getRadix(size_t a, int radix) {
    while (radix-- > 0) {
        a = a / 10;
    }
    return a % 10;
}

void radixsort(vector<size_t> &telephones) {
    vector<vector<size_t>> queue(10);

    for (int radix = 0; radix < 11; ++radix) {
        for (int i = 0; i < telephones.size(); ++i) {
            queue[getRadix(telephones[i], radix)].push_back(telephones[i]);
        }

        int k = 0;
        for (int i = 0; i < queue.size(); ++i) {
            for (int j = 0; j < queue[i].size(); ++j)
                telephones[k++] = queue[i][j];
            queue[i].clear();
        }
    }

}

int main() {
    vector<size_t> telephones = {33569988525,
                                 95552552697,
                                 15757705830,
                                 35654239708,
                                 14802248279,
                                 32453956330,
                                 30957432608,
                                 51468114989,
                                 40886629848,
                                 65032579418,
                                 33617745567,
                                 23593649020,
                                 34494804218,
                                 75523494142,
                                 60667637942,
                                 39817192239,
                                 64897804500,
                                 50866884448,
                                 45858079671,
                                 68368618562,
                                 68202940250,
                                 71087997965,
                                 51523669664,
                                 22408464944,
                                 52707787044,
                                 28517456870,
                                 49055081585,
                                 79223242319,
                                 42967098206,
                                 98049956290,
                                 37008614569,
                                 43951580466,
                                 94353038030,
                                 43738898221,
                                 98733106027,
                                 19063643531,
                                 50045689733,
                                 78792521502,
                                 70902998414,
                                 62597893178};

    radixsort(telephones);
    for (auto x : telephones)
        cout << x << endl;

    return 0;
}