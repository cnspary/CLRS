//
// Created by cnspary on 2019/9/30.
//

#include "bits/stdc++.h"
#include "TreeNode.h"

#define Priority(p) ((p)? p->priority : INT_MAX)
#define LowerPriorityChild(p) ((Priority(p->lc) > Priority(p->rc)) ? p->rc : p->lc)

using namespace std;

template<typename T>
class Theap {
private:
    TreeNode<T> *root, *_hot;

    TreeNode<T> *rotate(TreeNode<T> *x, TreeNode<T> *g) {
        if (g->lc == x) {
            // zig
            x->parent = g->parent;
            g->parent = x;
            g->lc = x->rc;
            if (x->rc) x->rc->parent = g;
            x->rc = g;
            return x;
        } else {
            // zag
            x->parent = g->parent;
            g->parent = x;
            g->rc = x->lc;
            if (x->lc) x->lc->parent = g;
            x->lc = g;
            return x;
        }
    }

    void percolationUp(TreeNode<T> *x, TreeNode<T> *_hot) {
        if (!_hot) return;
        for (TreeNode<T> *g = _hot; g; g = g->parent) {
            if (x->priority >= g->priority) return;

            TreeNode<T> *attach = g->parent;
            TreeNode<T> *xx = g = rotate(x, g);

            if (attach == nullptr) {
                root = xx;
            } else {
                if (xx->val < attach->val) attach->lc = xx;
                else attach->rc = xx;
            }
        }
        return;
    }

    void percolationDown(TreeNode<T> *x) {
        while (HasChild(x)) {
            TreeNode<T> *attach = x->parent;
            TreeNode<T> *xx = rotate(LowerPriorityChild(x), x);

            if (attach == nullptr) {
                root = xx;
            } else {
                if (xx->val < attach->val) attach->lc = xx;
                else attach->rc = xx;
            }
        }
    }

public:
    Theap(vector<T> A) {
        root = nullptr;
        _hot = nullptr;

        for (auto a : A)
            insert(a);
    }

    TreeNode<T> *search(T e) {
        if (!root) return nullptr;

        TreeNode<T> *s = root;
        _hot = nullptr;

        while (s) {
            _hot = s;
            if (e < s->val) s = s->lc;
            else if (s->val < e) s = s->rc;
            else break;
        }

        return s;
    };

    void insert(T e) {
        TreeNode<T> *c = search(e);
        if (c) return;

        TreeNode<T> *x = new TreeNode<T>(e, int(random() % 999), _hot);

        if (!_hot) root = x;
        else (e < _hot->val) ? _hot->lc = x : _hot->rc = x;

        percolationUp(x, _hot);
    };

    void remove(T e) {
        TreeNode<T> *c = search(e);
        if (!c) return;

        c->priority = INT_MAX;

        percolationDown(c);

        if (c->parent->lc == c) c->parent->lc = nullptr;
        else c->parent->rc = nullptr;
    };

    void outputTree() { // level
        queue<TreeNode<T> *> Q1;
        queue<TreeNode<T> *> Q2;
        Q1.push(root);
        while (!Q1.empty() || !Q2.empty()) {
            while (!Q1.empty()) {
                auto e = Q1.front();
                Q1.pop();
                if (e == nullptr)
                    cout << "[ null | -1 ] ";
                else {
                    cout << "[" << e->val << " | " << e->priority << "] ";
                    Q2.push(e->lc);
                    Q2.push(e->rc);
                }
            }

            cout << endl;

            while (!Q2.empty()) {
                auto e = Q2.front();
                Q2.pop();
                if (e == nullptr)
                    cout << "[ null | -1 ] ";
                else {
                    cout << "[" << e->val << " | " << e->priority << "] ";
                    Q1.push(e->lc);
                    Q1.push(e->rc);
                }
            }

            cout << endl;
        }
        cout << endl;
    }
};

int main() {
    Theap<int> theap({10, 4, 15, 3, 5, 12, 18, 2, 7, 11, 13, 17, 20, 1, 6, 8, 14, 19});
    theap.outputTree();

    theap.remove(14);
    theap.outputTree();
    return 0;
}