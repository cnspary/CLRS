//
// Created by cnspary on 2019/9/27.
//

#include <bits/stdc++.h>
#include "TreeNode.h"

using namespace std;

#define balancFac(p) (abs((Height(p->lc) - Height(p->rc))))
#define tallerChild(p) ((Height(p->lc) > Height(p->rc)) ? p->lc : p->rc)

template<typename T>
class BBST {
private:
    TreeNode<T> *_hot, *root;
    size_t _size;

    TreeNode<T> *rotate(TreeNode<T> *x, TreeNode<T> *p, TreeNode<T> *g) {
        if (g->lc == p) {
            if (p->lc == x) {
                // zig-zig
                p->parent = g->parent;
                return connect34(x, p, g, x->lc, x->rc, p->rc, g->rc);
            } else {
                // zag-zig
                x->parent = g->parent;
                return connect34(p, x, g, p->lc, x->lc, x->rc, g->rc);
            }
        } else {
            if (p->lc == x) {
                // zig-zag
                x->parent = g->parent;
                return connect34(g, x, p, g->lc, x->lc, x->rc, p->rc);
            } else {
                // zag-zag
                p->parent = g->parent;
                return connect34(g, p, x, g->lc, p->lc, x->lc, x->rc);
            }
        }
    }

    TreeNode<T> *connect34(TreeNode<T> *T1, TreeNode<T> *T2, TreeNode<T> *T3,
                           TreeNode<T> *a, TreeNode<T> *b, TreeNode<T> *c, TreeNode<T> *d) {
        T1->lc = a;
        if (a) a->parent = T1;

        T1->rc = b;
        if (b) b->parent = T1;

        updateHeight(T1);

        T3->lc = c;
        if (c) c->parent = T3;

        T3->rc = d;
        if (d) d->parent = T3;

        updateHeight(T3);

        T2->lc = T1;
        T1->parent = T2;

        T2->rc = T3;
        T3->parent = T2;

        updateHeight(T2);

        return T2;
    }

public:
    BBST(vector<T> A) {
        root = nullptr;
        _hot = nullptr;
        _size = 0;

        for (auto a : A)
            insert(a);
    }

    TreeNode<T> *search(T e) {
        if (!root) return nullptr;

        TreeNode<T> *s = root;
        _hot = nullptr;

        while (s) {
            _hot = s;
            if (e < s->val) s = s->lc;
            else if (s->val < e) s = s->rc;
            else break;
        }

        return s;
    }

    void insert(T e) {
        TreeNode<T> *c = search(e);
        if (c) return;

        TreeNode<T> *x = new TreeNode<T>(e, _hot);

        if (!_hot) root = x;
        else (e < _hot->val) ? _hot->lc = x : _hot->rc = x;

        _size++;

        if (!_hot) return;

        updateHeight(_hot);
        for (TreeNode<T> *g = _hot->parent; g; g = g->parent, x = x->parent) {
            if (1 < balancFac(g)) {
                TreeNode<T> *attach = g->parent;
                TreeNode<T> *xx = rotate(x, x->parent, g);

                if (attach == nullptr) {
                    root = xx;
                } else {
                    if (xx->val < attach->val) attach->lc = xx;
                    else attach->rc = xx;
                }
                break;
            } else updateHeight(g);
        }
    }

    void remove(T e) {
        TreeNode<T> *c = search(e);
        if (!c) return;

        TreeNode<T> *s;
        if (s = c->succ()) {
            swap(c->val, s->val);
            c = s;
        }
        _hot = c->parent;
        FromParent(c) = c->lc;
        _size--;

        for (TreeNode<T> *g = _hot; g; g = g->parent) {
            if (1 < balancFac(g)) {
                TreeNode<T> *attach = g->parent;
                TreeNode<T> *xx = g = rotate(tallerChild(tallerChild(g)), tallerChild(g), g);

                if (attach == nullptr) {
                    root = xx;
                } else {
                    if (xx->val < attach->val) attach->lc = xx;
                    else attach->rc = xx;
                }
            }
            updateHeight(g);
        }
    }

    int updateHeight(TreeNode<T> *x) {
        x->_height = 1 + max(Height(x->lc), Height(x->rc));
        return x->_height;
    }

    void outputTree() { // level
        LEVELPRINT(root);
    }
};

int main() {
    BBST<int> bbst({1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
    bbst.outputTree();

    bbst.remove(1);
    bbst.remove(3);

    bbst.outputTree();
    return 0;
}