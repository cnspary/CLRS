//
// Created by cnspary on 2019/8/27.
//

#include "bits/stdc++.h"

using namespace std;

class Fib {
private:
    pair<uint64_t, uint64_t> MatrixCompute(size_t n) {
        if (n < 1)
            return {1, 0};

        auto pre = MatrixCompute(n / 2);
        pair<uint64_t, uint64_t> cur;

        if (n % 2 != 0) {
            cur.first = (pre.first + 2 * pre.second) * pre.first;
            cur.second = pre.first * pre.first + pre.second * pre.second;
        } else {
            cur.first = pre.first * pre.first + pre.second * pre.second;
            cur.second = (2 * pre.first - pre.second) * pre.second;
        }

        return cur;
    }

    uint64_t IterCompute(size_t n) {
        if (n < 2) return n;
        uint64_t pre = 0, cur = 1;
        for (int i = 1; i < n; i++) {
            uint64_t t = pre + cur;
            pre = cur;
            cur = t;
        }
        return cur;
    };

    uint64_t RecCompute(size_t n) {
        if (n < 2) return n;
        return RecCompute(n - 1) + RecCompute(n - 2);
    };

public:
    Fib(size_t n) {
        clock_t startTime, endTime;

        startTime = clock();
        cout << "MatrixCompute : " << MatrixCompute(n).second;
        endTime = clock();
        cout << " ( Totle Time : " << (double) (endTime - startTime) / CLOCKS_PER_SEC << "s )" << endl;

        startTime = clock();
        cout << "IterCompute   : " << IterCompute(n);
        endTime = clock();
        cout << " ( Totle Time : " << (double) (endTime - startTime) / CLOCKS_PER_SEC << "s )" << endl;

        startTime = clock();
        cout << "RecCompute    : " << RecCompute(n);
        endTime = clock();
        cout << " ( Totle Time : " << (double) (endTime - startTime) / CLOCKS_PER_SEC << "s )" << endl;

    }
};

int main() {
    Fib(40);
    return 0;
}