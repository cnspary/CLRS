//
// Created by cnspary on 2019/8/26.
//

#include "bits/stdc++.h"

using namespace std;

template<typename T>
vector<T> selectSort(vector<T> &a) {
    for (size_t j = 0; j < a.size() - 1; j++) {
        size_t minIndex = j;
        for (size_t i = j + 1; i < a.size(); i++)
            if (a[i] < a[minIndex]) minIndex = i;

        T tem = a[minIndex];
        a[minIndex] = a[j];
        a[j] = tem;
    }
    return a;
}

int main() {
    vector<int> array = {9, 4, 2, 3, 5, 1, 23, 124, 14, 25, 1, 23, 123, 12, 3, 34, 346, 6, 34, 123, 1};
    selectSort(array);
    for (size_t i = 0; i < array.size(); i++)
        cout << array[i] << " ";
    cout << endl;
    return 0;
}
