//
// Created by cnspary on 2019/8/21.
//

#include "bits/stdc++.h"

struct Point {
    int x;
    int y;
};


class LEGO {
private:

    std::vector<std::vector<int>> baseplate;    // 存储底板编号数据的矩阵
    Point init_spot;    // 初始已铺平瓦的位置坐标
    int length;         // 底板长度
    int label;          // 编号 为新铺砖标记编号且不断增加

    void SolveLEGO(int N, Point start_point, Point exist_point) {
        if (N > 1) {
            Point spots[2][2];

            int half = N >> 1;

            spots[0][0].x = start_point.x + half - 1;
            spots[0][0].y = start_point.y + half - 1;

            spots[0][1].x = start_point.x + half - 1;
            spots[0][1].y = start_point.y + half;

            spots[1][0].x = start_point.x + half;
            spots[1][0].y = start_point.y + half - 1;

            spots[1][1].x = start_point.x + half;
            spots[1][1].y = start_point.y + half;

            int ex = (exist_point.x - start_point.x) / half;
            int ey = (exist_point.y - start_point.y) / half;
            spots[ex][ey].x = exist_point.x;
            spots[ex][ey].y = exist_point.y;

            int tempLabel = baseplate[exist_point.x][exist_point.y];
            baseplate[spots[0][0].x][spots[0][0].y] = label;
            baseplate[spots[0][1].x][spots[0][1].y] = label;
            baseplate[spots[1][0].x][spots[1][0].y] = label;
            baseplate[spots[1][1].x][spots[1][1].y] = label;

            baseplate[exist_point.x][exist_point.y] = tempLabel;
            label++;

            Point p1, p2, p3, p4;

            p1.x = start_point.x;
            p1.y = start_point.y;

            p2.x = start_point.x;
            p2.y = start_point.y + half;

            p3.x = start_point.x + half;
            p3.y = start_point.y;

            p4.x = start_point.x + half;
            p4.y = start_point.y + half;

            SolveLEGO(half, p1, spots[0][0]);
            SolveLEGO(half, p2, spots[0][1]);
            SolveLEGO(half, p3, spots[1][0]);
            SolveLEGO(half, p4, spots[1][1]);

        }
    }

public:
    LEGO(int N, Point init) {
        length = N;
        init_spot.x = init.x;
        init_spot.y = init.y;

        for (int i = 0; i < N; ++i)
            baseplate.push_back(std::vector<int>(N, -1));

        baseplate[init_spot.x][init_spot.y] = 0;
    }

    void Solve() {
        label = 1;

        Point start_point;
        start_point.x = 0;
        start_point.y = 0;

        SolveLEGO(length, start_point, init_spot);
    }

    void Output() {
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++)
                std::cout << std::setw(5) << baseplate[i][j];
            std::cout << std::endl;
        }
    }
};

int main() {
    Point init;
    init.x = 0;
    init.y = 1;
    auto lego = LEGO(4, init);
    lego.Solve();
    lego.Output();
}