//
// Created by cnspary on 2019/8/20.
//
#include "bits/stdc++.h"

using namespace std;

template<typename T>
int binSearch(std::vector<T> sorted_array, T target) {
    // why do not use size_t ? (hint: when X = 0, (size_t(X) - 1) ?= (int(X) - 1))
    int L = 0, R = sorted_array.size();
    while (L < R) {
        int M = L + ((R - L) >> 1);
        if (sorted_array[M] < target)
            L = M + 1;
        else if (sorted_array[M] > target)
            R = M - 1;
        else
            return M;
    }
    return L; // if not searched , return the insertion index;
}

int main() {
    vector<int> array = {1, 2, 3, 5, 6, 7, 8, 9, 10, 11, 13};

    cout << binSearch(array, 4) << endl;
    cout << binSearch(array, 1) << endl;
    cout << binSearch(array, 13) << endl;
    cout << binSearch(array, 0) << endl;

    return 0;
}