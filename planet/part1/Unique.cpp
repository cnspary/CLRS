//
// Created by cnspary on 2019/8/18.
//
#include "bits/stdc++.h"

using namespace std;

template <typename T>
void unique_V1(std::vector<T> &array) {
    std::sort(array.begin(), array.end());

    size_t L = 0, R;
    while (L < array.size()) {
        for (R = L + 1; R < array.size(); ++R)
            if (array[R] != array[L]) break;
        std::cout << array[L] << ":" << R - L << std::endl;
        L = R;
    }
}

template <typename T>
void unique_V2(std::vector<T> &array) {
    std::sort(array.begin(), array.end());

    size_t L, R = 0;
    for (L = R; R < array.size(); ++R) {
        if (array[R] != array[L]) {
            std::cout << array[L] << ":" << R - L << std::endl;
            L = R;
        }
    }

    if (array.size() > 0)
        std::cout << array[L] << ":" << R - L << std::endl;
}

int main() {
    vector<int> array = {9, 4, 2, 3, 5, 1, 23, 124, 14, 25, 1, 23, 123, 12, 3, 34, 346, 6, 34, 123, 1};
    unique_V1(array);

    cout << endl;

    unique_V2(array);
}