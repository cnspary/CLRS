//
// Created by cnspary on 2019/8/3.
//

#include "bits/stdc++.h"

using namespace std;

template<typename T>
vector<T> insertSort(vector<T> &array) {
    T key;
    for (size_t i = 1; i < array.size(); ++i) {
        key = array[i];

        int j = i - 1;
        while (0 <= j && key < array[j]) {
            array[j + 1] = array[j];
            --j;
        }

        array[j + 1] = key;
    }
    return array;
}

int main() {
    vector<int> array = {9, 4, 2, 3, 5, 1, 23, 124, 14, 25, 1, 23, 123, 12, 3, 34, 346, 6, 34, 123, 1};
    insertSort(array);
    for (size_t i = 0; i < array.size(); i++)
        cout << array[i] << " ";
    cout << endl;
    return 0;
}