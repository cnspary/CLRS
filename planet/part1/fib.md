### Fibnoacci数列
#### 1.定义:
$$
fib(n)=\begin{cases} 0 & \text{(n=0)} \\[2ex]
1 & \text{(n=1)} \\[2ex] fib(n-1) + fib(n-2) & \text{(n >1)} \end{cases}
$$ ㅤㅤㅤㅤ

#### 2.推导:
$$
\begin{align}
    \begin{bmatrix}
        fib_{n+1} \\ fib_{n}
    \end{bmatrix} & =
    \begin{bmatrix}
        1 &  1 \\ 1 &  0
    \end{bmatrix}
    \begin{bmatrix}
        fib_{n} \\ fib_{n-1}
    \end{bmatrix} \\[2ex] & =
    \begin{bmatrix}
        1 &  1 \\ 1 &  0
    \end{bmatrix}^n
    \begin{bmatrix}
        fib_{1} \\ fib_{0}
    \end{bmatrix} \\[2ex]
    \begin{bmatrix}
        1 &  1 \\ 1 &  0
    \end{bmatrix}^n & =
    \begin{bmatrix}
        fib_{n+1} &  fib_{(n)} \\ fib_{(n)} &  fib_{(n-1)}
    \end{bmatrix} \\[2ex]
    \color{red}{
    \begin{bmatrix}
        fib_{2m+1} &  fib_{2m} \\ fib_{2m} &  fib_{2m-1}
    \end{bmatrix}^{2m}} & =
    \begin{bmatrix}
        1 &  1 \\ 1 &  0
    \end{bmatrix}^{2m} \\[2ex] & = \begin{bmatrix}
        1 &  1 \\ 1 &  0
    \end{bmatrix}^{m} \begin{bmatrix}
        1 &  1 \\ 1 &  0
    \end{bmatrix}^{m} \\[2ex] & =
    \begin{bmatrix}
        fib_{m+1} &  fib_{(m)} \\ fib_{(m)} &  fib_{(m-1)}
    \end{bmatrix}
    \begin{bmatrix}
        fib_{m+1} &  fib_{(m)} \\ fib_{(m)} &  fib_{(m-1)}
    \end{bmatrix} \\[2ex] & =
    \color{red}{
    \begin{bmatrix}
        fib^2_{m+1} + fib^2_{m} &  fib_{m+1}fib_{m} + fib_{m}fib_{m-1} \\ fib_{m+1}fib_{m} + fib_{m}fib_{m-1} & fib^2_{m} + fib^2_{m-1}
    \end{bmatrix}} \\[5ex]
    根据矩阵相同定义得: & \begin{cases}\color{red}{fib_{2m+1} = fib^2_{m+1} + fib^2_{m} \\[2ex]
    fib_{2m} = fib_{m+1}fib_{m} + fib_{m}fib_{m-1}} \end{cases} \\[2ex]
\end{align}
$$

#### 3.计算:
#####1. n = 2m + 1
$$
\begin{align}
    \begin{bmatrix}
        fib_{n+1} \\ fib_{n}
    \end{bmatrix} & =
    \begin{bmatrix}
        fib_{2m+2} \\ fib_{2m+1}
    \end{bmatrix} \\[2ex] & =
    \begin{bmatrix}
        fib_{m+2}fib_{m+1} + fib_{m+1}fib_{m} \\ fib^2_{m+1} + fib^2_{m}
    \end{bmatrix} \\[2ex] & =
    \begin{bmatrix}
        (fib_{m+1} + 2fib_{m}) fib_{m+1} \\ fib^2_{m+1} + fib^2_{m}
    \end{bmatrix} \\[5ex]
    & 其中只需要计算 fib_{m+1} 与 fib_{m},这两项可以通过递归调用 fib(n/2) = fib(m)得到
\end{align}
$$

##### 2. n = 2m
$$
\begin{align}
    \begin{bmatrix}
        fib_{n+1} \\ fib_{n}
    \end{bmatrix} & =
    \begin{bmatrix}
        fib_{2m+1} \\ fib_{2m}
    \end{bmatrix}  \\[2ex] & =
    \begin{bmatrix}
        fib^2_{m+1} + fib^2_{m} \\ fib_{m+1}fib_{m} + fib_{m}fib_{m-1}
    \end{bmatrix} \\[2ex] & =
    \begin{bmatrix}
        fib^2_{m+1} + fib^2_{m} \\ (2fib_{m+1} - fib_{m})fib_{m}
    \end{bmatrix} \\[5ex]
    & 其中只需要计算 fib_{m+1} 与 fib_{m},这两项可以通过递归调用 fib(n/2) = fib(m)得到
\end{align}
$$
