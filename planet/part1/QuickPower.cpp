//
// Created by cnspary on 2020/3/24.
//

#include "bits/stdc++.h"
#include <time.h>

using namespace std;

#define MOD 1000

int Power(long long int base, long long int power) {
    int ans = 1;
    while (power > 0) {
        ans *= base;
        power--;
        ans %= MOD;
    }
    return ans;
}

int quickPower(int base, int power) {
    int ans = 1;
    while (power > 0) {
        if (power & 1 == 1) ans = (ans * base) % MOD;
        power = power >> 1;
        base = base * base % MOD;
    }
    return ans;
}

int main() {
    clock_t start, finish;
    long long int base = 2, power = 1000000000;

    start = clock();
    cout << Power(base, power) << endl;
    finish = clock();
    cout << "the time cost is" << double(finish - start) / CLOCKS_PER_SEC << endl;


    start = clock();
    cout << quickPower(base, power) << endl;
    finish = clock();
    cout << "the time cost is" << double(finish - start) / CLOCKS_PER_SEC << endl;
    return 0;
}