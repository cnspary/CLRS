//
// Created by cnspary on 2019/8/20.
//
#include "bits/stdc++.h"

using namespace std;

template<typename T>
class TwoWayMergeSort {
private:
    std::vector<T> array;

    void merge_2way(size_t lo, size_t mi, size_t hi) {
        size_t lb = mi - lo;
        std::vector<T> bak(lb);

        for (size_t i = 0; i < lb; ++i) bak[i] = array[lo + i];

        for (size_t k = lo, i = 0, j = mi; i < lb;) {
            if (j < hi && array[j] < bak[i])
                array[k++] = array[j++];
            if (!(j < hi) || (i < lb && bak[i] <= array[j]))
                array[k++] = bak[i++];
        }
    }

public:
    TwoWayMergeSort(std::vector<T> A) {
        for (size_t i = 0; i < A.size(); ++i)
            array.push_back(A[i]);
    }

    ~TwoWayMergeSort() {
        array.clear();
    }

    void mergeSort_2way(size_t lo, size_t hi) {
        if (hi <= lo + 1) return;

        size_t mi = lo + ((hi - lo) >> 1);
        mergeSort_2way(lo, mi);
        mergeSort_2way(mi, hi);
        merge_2way(lo, mi, hi);
    }

    void output() {
        for (size_t i = 0; i < array.size(); ++i)
            std::cout << array[i] << " ";
        std::cout << std::endl;
    }
};

int main() {
    vector<int> array = {9, 4, 2, 3, 5, 1, 23, 124, 14, 25, 1, 23, 123, 12, 3, 34, 346, 6, 34, 123, 1};
    auto m = TwoWayMergeSort<int>(array);
    m.mergeSort_2way(0, array.size());
    m.output();
    return 0;
}