//
// Created by cnspary on 2019/10/29.
//

#include "bits/stdc++.h"

using namespace std;

int actSelect_dp_vector(vector<vector<int>> acts) {
    // not suit for the large duration time
    // time: O( acts' number * the last ending time ) = O(nm)
    // space: O(nm)
    vector<int> dp(acts.back()[1] + 1, 0);

    for (int i = 0; i < acts.size(); ++i) {
        int start = acts[i][0];
        int end = acts[i][1];
        dp[end] = max(dp[start] + 1, dp[end]);
        for (int j = end + 1; j < dp.size(); ++j)
            dp[j] = dp[j - 1];
    }

    return dp.back();
}

int actSelect_dp_map(vector<vector<int>> acts) {
    // using Map to optimal time&space costs
    map<int, int> Map;
    int maxSize = 0;
    Map.insert({0, 0});
    for (int i = 0; i < acts.size(); ++i) {
        int start = acts[i][0];
        int end = acts[i][1];

        auto iter1 = Map.upper_bound(start);
        --iter1;
        auto iter2 = Map.upper_bound(end);
        --iter2;

        Map[end] = max(iter1->second + 1, iter2->second);
        maxSize = Map[end];
    }
    return maxSize;
}

int actSelect_greedy(vector<vector<int>> acts) {
    // time: O(n)
    // space: O(1)
    int maxSize = 1;
    int index = 0;
    int m = index + 1;

    for (int i = 0, j = 1; j < acts.size(); ++j) {
        if (acts[j][0] >= acts[i][1]) {
            i = j;
            ++maxSize;
        }
    }

    return maxSize;
}

int main() {
    vector<vector<int>> acts = {{1,  4},
                                {3,  5},
                                {0,  6},
                                {5,  7},
                                {3,  9},
                                {5,  9},
                                {6,  10},
                                {8,  11},
                                {8,  12},
                                {2,  14},
                                {12, 5000000}};

    clock_t startTime, endTime;
    startTime = clock();
    cout << actSelect_dp_vector(acts) << endl;
    endTime = clock();
    cout << "The run time is: " << (double) (endTime - startTime) / CLOCKS_PER_SEC << "s" << endl;

    startTime = clock();
    cout << actSelect_dp_map(acts) << endl;
    endTime = clock();
    cout << "The run time is: " << (double) (endTime - startTime) / CLOCKS_PER_SEC << "s" << endl;

    startTime = clock();
    cout << actSelect_greedy(acts) << endl;
    endTime = clock();
    cout << "The run time is: " << (double) (endTime - startTime) / CLOCKS_PER_SEC << "s" << endl;
    return 0;
}