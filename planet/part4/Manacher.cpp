//
// Created by cnspary on 2020/2/29.
//

#include "bits/stdc++.h"

using namespace std;

string expend(string s) {
    string news = "(#";
    for (int i = 0; i < s.size(); ++i) {
        news += s[i];
        news += "#";
    }
    news += ")";
    return news;
}

void longestPalindrome(string origS) {
    vector<int> p;
    string s = expend(origS);
    p.resize(s.size(), 1);

    int maxL = -1, maxIndex = -1;
    int center = 0, maxR = 0;
    for (int c = 1; c < s.size() - 1; ++c) {
        if (c < maxR)
            p[c] = min(p[2 * center - c], maxR - c);

        while (s[c - p[c]] == s[c + p[c]])
            p[c]++;

        if (c + p[c] > maxR) {
            center = c;
            maxR = c + p[c];
        }

        if (p[c] - 1 > maxL) {
            maxL = p[c] - 1;
            maxIndex = c;
        }
    }

    int start = (maxIndex - maxL) / 2;
    string ans = origS.substr(start, maxL);
    cout << ans << endl;

    for (int i = 0; i < s.size(); ++i)
        cout << setw(3) << s[i];
    cout << endl;
    for (int i = 0; i < p.size(); ++i)
        cout << setw(3) << p[i];

    return;
}

int main() {
    string s;
    getline(cin, s);
    longestPalindrome(s);
    return 0;
}