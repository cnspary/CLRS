//
// Created by cnspary on 2020/3/4.
//

#include "bits/stdc++.h"

using namespace std;

typedef enum {
    RED, BLACK
} RBColor;

template<typename T>
struct TreeNode {
public:
    T val;
    RBColor color;
    TreeNode<T> *parent;
    TreeNode<T> *lc;
    TreeNode<T> *rc;

    TreeNode(T v, RBColor c = RED, TreeNode<T> *p = nullptr,
             TreeNode<T> *l = nullptr,
             TreeNode<T> *r = nullptr) {
        val = v;
        color = c;
        parent = p;
        lc = l;
        rc = r;
    }
};

template<typename T>
class RBTree {
public:
    TreeNode<T> *leave;
    TreeNode<T> *root;
    TreeNode<T> *_hot;

    RBTree() {
        leave = new TreeNode<T>(-1, BLACK);
        root = leave;
    };

    RBTree(vector<T> vs) {
        leave = new TreeNode<T>(-1, BLACK);
        root = leave;
        for (T v : vs)
            insert(v);
    };

    TreeNode<T> *search(T v) {
        _hot = leave;
        TreeNode<T> *searchNode = root;
        while (searchNode != leave) {
            _hot = searchNode;
            if (v < searchNode->val)
                searchNode = searchNode->lc;
            else if (v > searchNode->val)
                searchNode = searchNode->rc;
            else
                break;
        }
        return searchNode;
    }

    bool isExist(T v) {
        TreeNode<T> *s = search(v);
        if (s != leave && s->val == v) return true;
        else return false;
    }

    void insert(T v) {
        if (isExist(v)) return;
        TreeNode<T> *insertNode = new TreeNode<T>(v, RED, _hot, leave, leave);
        if (_hot == leave) root = insertNode;
        if (v < _hot->val) _hot->lc = insertNode;
        else _hot->rc = insertNode;
        solverDoubleRed(insertNode);
        root = leave->lc ? leave->lc : leave->rc;
        return;
    }

    void remove(T v) {
        RBColor delColor;
        TreeNode<T> *s = search(v);
        if (s == leave) return;
        TreeNode<T> *r = removeAt(s, delColor);

        if (_hot == leave) { // remove root
            root->color = BLACK;
            return;
        }

        if (delColor == RED) return; // delete red node;

        if (r != leave && delColor == BLACK && r->color == RED) {
            r->color = BLACK;
            return;
        }

        solverDoubleBlack(r);
    }

    TreeNode<T> *removeAt(TreeNode<T> *x, RBColor &delColor) {
        TreeNode<T> *w = x;
        TreeNode<T> *succ = nullptr;

        if (x->lc == leave)
            succ = x->rc;
        else if (x->rc == leave)
            succ = x->lc;
        else {
            w = x->rc;
            while (w->lc != leave)
                w = w->lc;
            swap(x->val, w->val);
            succ = w->rc;
        }

        _hot = w->parent;
        if (_hot->lc == w) _hot->lc = succ;
        else _hot->rc = succ;
        if (succ != leave) succ->parent = _hot;

        root = leave->lc ? leave->lc : leave->rc;

        delColor = w->color;
        free(w);
        return succ;
    }

    TreeNode<T> *connect34(TreeNode<T> *L, TreeNode<T> *M, TreeNode<T> *R,
                           TreeNode<T> *l1, TreeNode<T> *l2, TreeNode<T> *r1, TreeNode<T> *r2) {
        M->lc = L;
        L->parent = M;

        M->rc = R;
        R->parent = M;

        L->lc = l1;
        if (l1 != leave) l1->parent = L;
        L->rc = l2;
        if (l2 != leave) l2->parent = L;
        R->lc = r1;
        if (r1 != leave) r1->parent = R;
        R->rc = r2;
        if (r2 != leave) r2->parent = R;

        return M;
    }

    TreeNode<T> *rotate(TreeNode<T> *x, TreeNode<T> *p, TreeNode<T> *g) {
        TreeNode<T> *gg = g->parent;
        T v = g->val;

        TreeNode<T> *temp;

        if (x == p->lc) {
            if (p == g->lc)
                temp = connect34(x, p, g, x->lc, x->rc, p->rc, g->rc);
            else
                temp = connect34(g, x, p, g->lc, x->lc, x->rc, p->rc);

        } else {
            if (p == g->lc)
                temp = connect34(p, x, g, p->lc, x->lc, x->rc, g->rc);
            else
                temp = connect34(g, p, x, g->lc, p->lc, x->lc, x->rc);

        }
        temp->parent = gg;
        if (v < gg->val) gg->lc = temp;
        else gg->rc = temp;
        return temp;
    }

    void solverDoubleRed(TreeNode<T> *x) {
        if (x->parent == leave) {
            x->color = BLACK;
            return;
        }

        TreeNode<T> *p = x->parent;
        if (p->color == BLACK) return;

        TreeNode<T> *g = p->parent;
        TreeNode<T> *u = (p == g->lc) ? g->rc : g->lc;

        if (u->color == BLACK) {
            TreeNode<T> *np = rotate(x, p, g);
            np->color = BLACK;
            np->lc->color = RED;
            np->rc->color = RED;
        } else {
            g->color = RED;
            p->color = u->color = BLACK;
            solverDoubleRed(g);
        }
        return;
    }

    void solverDoubleBlack(TreeNode<T> *x) {
        TreeNode<T> *p = (x == leave) ? _hot : x->parent;
        if (p == leave) return;

        TreeNode<T> *s = (x == p->lc) ? p->rc : p->lc;

        if (s->color == BLACK) {
            TreeNode<T> *t = nullptr;
            if (s->rc->color == RED) t = s->rc;
            if (s->lc->color == RED) t = s->lc;
            if (t) {
                RBColor oldColor = p->color;
                TreeNode<T> *np = rotate(t, s, p);
                np->color = oldColor;
                if (np->lc != leave) np->lc->color = BLACK;
                if (np->rc != leave) np->rc->color = BLACK;
            } else {
                s->color = RED;
                if (p->color == RED)
                    p->color = BLACK;
                else
                    solverDoubleBlack(p);
            }
        } else {
            s->color = BLACK;
            p->color = RED;
            TreeNode<T> *t = (s == p->lc) ? s->lc : s->rc;
            rotate(t, s, p);
            solverDoubleBlack(x);
        }
    }
};

template<typename T>
void outputTree(RBTree<T> rbt, string op) { // level
    TreeNode<T> *root = rbt.root, *leave = rbt.leave;
    if (root == leave) return;

    queue<TreeNode<T> *> Q;
    Q.push(root);

    ofstream outfile("/Users/cnspary/dev/CLRS/planet/part4/RBTree/" + op, ios::trunc);


    outfile << "digraph G {\n";
    outfile << "\tnode[shape=circle, style=filled, fontcolor=white, fixedsize=true]\n";
    outfile << "\tgraph [nodesep=0.1]\n";
    outfile << "\tedge [arrowhead=vee]\n\n";
    outfile << "\t" << root->val << "[group=" << root->val << ", color=black]\n";

    while (!Q.empty()) {
        TreeNode<T> *e = Q.front();
        Q.pop();
        if (e->lc != leave) {
            Q.push(e->lc);
            outfile << "\t" << e->lc->val << "[group=" << e->lc->val << ", color=";
            if (e->lc->color == RED) outfile << "red]\n";
            else outfile << "black]\n";
            outfile << "\t" << e->val << " -> " << e->lc->val << "\n";
        }

        if (e->lc != leave || e->rc != leave) {
            outfile << "\t_" << e->val << "[group=" << e->val << ", style=invis, label=\"\", width=0]\n";
            outfile << "\t" << e->val << " -> _" << e->val << "[style=invis]\n";
        }

        if (e->rc != leave) {
            Q.push(e->rc);
            outfile << "\t" << e->rc->val << "[group=" << e->rc->val << ", color=";
            if (e->rc->color == RED) outfile << "red]\n";
            else outfile << "black]\n";
            outfile << "\t" << e->val << " -> " << e->rc->val << "\n";
        }
    }
    outfile << "\n";

    Q.push(root);
    while (!Q.empty()) {
        TreeNode<T> *e = Q.front();
        Q.pop();

        if (e->lc != leave) Q.push(e->lc);
        if (e->rc != leave) Q.push(e->rc);

        if (e->lc != leave && e->rc != leave) {
            TreeNode<T> *pre = e->lc, *succ = e->rc;
            int preDepth = 1, succDepth = 1;
            while (pre->rc != leave && ++preDepth) pre = pre->rc;
            while (succ->lc != leave && ++succDepth) succ = succ->lc;

            if (preDepth < succDepth)
                outfile << "\t{rank=same; _" << e->val << ", " << pre->val << "}\n";
            else
                outfile << "\t{rank=same; _" << e->val << ", " << succ->val << "}\n";
        }
    }


    outfile << "}";
    outfile.close();
    return;
}

string indexOp(int i) {
    string s = to_string(i);
    s.insert(0, 4 - s.length(), '0');
    return s;
}

int main() {
    vector<pair<int, int>> op = {{1, 520},
                                 {1, 839},
                                 {1, 916},
                                 {1, 832},
                                 {0, 832},
                                 {1, 909},
                                 {1, 446},
                                 {1, 279},
                                 {1, 485},
                                 {0, 909},
                                 {1, 522},
                                 {1, 204},
                                 {1, 56},
                                 {1, 497},
                                 {0, 279},
                                 {1, 345},
                                 {1, 792},
                                 {1, 884},
                                 {1, 784},
                                 {1, 967},
                                 {1, 170},
                                 {1, 127},
                                 {1, 48},
                                 {1, 23},
                                 {1, 531},
                                 {0, 884},
                                 {1, 551},
                                 {1, 235},
                                 {1, 70},
                                 {1, 166},
                                 {1, 610},
                                 {1, 208},
                                 {1, 212},
                                 {1, 842},
                                 {1, 699},
                                 {1, 984},
                                 {1, 159},
                                 {1, 98},
                                 {1, 210},
                                 {1, 685},
                                 {1, 891},
                                 {1, 505},
                                 {1, 350},
                                 {0, 70},
                                 {1, 442},
                                 {1, 632},
                                 {0, 446},
                                 {1, 384},
                                 {1, 434},
                                 {1, 506},
                                 {1, 482},
                                 {1, 41},
                                 {0, 632},
                                 {1, 292},
                                 {1, 821},
                                 {1, 573},
                                 {1, 26},
                                 {0, 784},
                                 {0, 520},
                                 {0, 235},
                                 {0, 916}};
    RBTree<int> rbtree;

    ofstream outfile("/Users/cnspary/dev/CLRS/planet/part4/RBTree/buildPng.sh", ios::trunc);
    outfile << "#!/bin/zsh\n";
    outfile << "## http://www.graphviz.org/download/\n";
    for (int i = 0; i < op.size(); ++i) {
        string name;
        if (op[i].first == 1) { // insert
            name = indexOp(i) + "_insert_" + to_string(op[i].second);
            rbtree.insert(op[i].second);
            outputTree(rbtree, name + ".dot");
        } else { // remove
            name = indexOp(i) + "_remove_" + to_string(op[i].second);
            rbtree.remove(op[i].second);
            outputTree(rbtree, name + ".dot");
        }
        outfile << "dot -Tpng -o " + name + ".png " + name + ".dot\n";
    }
    outfile << "rm *.dot\n";
    outfile.close();
    return 0;
}